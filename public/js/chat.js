(function () {
    var options = {
        whatsapp: "6281517374089", // WhatsApp number
        email: "support@xone-coin.com", // Email
        call_to_action: "Kontak Kami", // Call to action
        button_color: "#129BF4", // Color of button
        position: "right", // Position may be 'right' or 'left'
        company_logo_url: "image/logo_xone.png", // URL of company logo (png, jpg, gif)
        greeting_message: "Kontak kami jika Anda memerlukan asistensi atau bantuan.", // Text of greeting message
        order: "whatsapp,email" // Order of buttons
    };
    var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
    s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
})();