<?php

use Illuminate\Database\Seeder;
use App\Model\Bonussetting;

class DatabaseSeeder extends Seeder {

   public function run(){

       DB::table('users')->delete();
       DB::table('coin_setting')->delete();
       DB::table('master_coin')->delete();
       DB::table('bank')->delete();

       //Users
       $users = array(
            array(
                'name' => 'Super Admin',
                'email' => 'superadmin@xone-coin.com',
                'password' => bcrypt('superadmin_xonecoin_2020'),
                'user_code' => 'superadmin@xone-coin.com',
                'is_active' => 1,
                'user_type' => 1,
            ),
            array(
                'name' => 'Master Admin',
                'email' => 'masteradmin@xone-coin.com',
                'password' => bcrypt('masteradmin_xonecoin_2020'),
                'user_code' => 'masteradmin@xone-coin.com',
                'is_active' => 1,
                'user_type' => 2,
            ),
            array(
                'name' => 'Admin',
                'email' => 'admin@xone-coin.com',
                'password' => bcrypt('admin_xonecoin_2020'),
                'user_code' => 'admin@xone-coin.com',
                'is_active' => 1,
                'user_type' => 3,
            ),
            array(
                'name' => 'Exchanger 001',
                'email' => 'exchanger@xone-coin.com',
                'password' => bcrypt('xonecoin_2020'),
                'user_code' => 'ex001',
                'hp' => '08111111111',
                'is_active' => 1,
                'user_type' => 10,
                'member_type' => 1,
                'active_at' => date('Y-m-d H:i:s'),
            ),
        );
        foreach($users as $row){
            DB::table('users')->insert($row);
        }

        $bankPerusahaan = array(
            'user_id' => 2,
            'bank_name' => 'BCA',
            'account_no' => '4140751842',
            'account_name' => 'CV. Exo Indonesia Sejahtera',
            'bank_type' => 1,
            'active_at' => date('Y-m-d H:i:s')
        );
        DB::table('bank')->insert($bankPerusahaan);

        $setting1 = array(
            'type' => 1,
            'is_active' => 1,
            'buy_price' => 20000,
            'sell_price' => 19000,
            'buy_fee' => 10,
            'sell_fee' => 5,
            'buy_min' => 50,
            'sell_min' => 10
        );
        DB::table('coin_setting')->insert($setting1);
        $setting2 = array(
            'type' => 2,
            'is_active' => 1,
            'buy_price' => 20000,
            'sell_price' => 19000,
            'buy_fee' => 10,
            'sell_fee' => 5,
            'buy_min' => 50,
            'sell_min' => 10
        );
        DB::table('coin_setting')->insert($setting2);

        $coinPerusahaan = array(
            'total_coin' => 1000000,
            'type' => 1,
        );
        DB::table('master_coin')->insert($coinPerusahaan);
        
        $coinPosting = array(
            'persentase' => 21,
            'min_posting' => 50,
            'min_convert' => 10
        );
        DB::table('setting_posting')->insert($coinPosting);
        
        $coinBonus = array(
            'bonus_sponsor' => 10,
            'bonus_profit' => 10,
            'max_user_profit' => 5
        );
        DB::table('setting_app')->insert($coinBonus);

        // dd('done All seed');
    }
}
