<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPin extends Migration  {

    public function up() {
        Schema::create('master_coin', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('total_coin', 15, 4)->default(0);
            $table->smallInteger('type')->default(1)->comment('1 = coin bertambah ke perusahaan, 2 = coin berkurang dari perusahaan');
            $table->integer('transaction_id')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index('type');
            $table->index('transaction_id');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('master_coin');
    }
}
