<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusMember extends Migration {

    public function up() {
        Schema::create('bonus_member', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('from_user_id')->nullable();
            $table->smallInteger('type')->comment('1 => Bonus Sponsor Rp, 2 => Bonus profit posting ');
            $table->double('bonus', 15, 4);
            $table->date('bonus_date');
            $table->integer('setting_id')->nullable();
            $table->timestamp('created_at')->useCurrent();

            $table->index('user_id');
            $table->index('from_user_id');
            $table->index('type');
            $table->index('bonus_date');
            $table->index('setting_id');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('bonus_member');
    }
}
