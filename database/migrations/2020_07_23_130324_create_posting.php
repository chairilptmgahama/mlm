<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosting extends Migration {

    public function up() {
        Schema::create('posting', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->smallInteger('type')->default(1)->comment('jenis posting. 1 => posting aktif, 2 => posting stop');
            $table->double('total_coin', 15, 4)->default(0);
            $table->smallInteger('status')->default(0)->comment('0 = berjalan, 1 = tuntas');
            $table->integer('posting_setting_id');
            $table->integer('transaction_id');
            $table->integer('phase')->default(0);
            $table->integer('total_post')->default(0);
            $table->text('detail')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('tuntas_at')->nullable();


            $table->index('user_id');
            $table->index('type');
            $table->index('status');
            $table->index('posting_setting_id');
            $table->index('transaction_id');
            $table->index('phase');
            $table->index('total_post');
            $table->index('created_at');
            $table->index('tuntas_at');
        });
    }

    public function down() {
        Schema::dropIfExists('posting');
    }
}
