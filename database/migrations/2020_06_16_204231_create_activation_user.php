<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivationUser extends Migration {

    public function up() {
        Schema::create('activation_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('sponsor_id')->nullable();
            $table->timestamp('created_at')->useCurrent();
            
            $table->index('user_id');
            $table->index('sponsor_id');
            $table->index('created_at');
        });
    }


    public function down() {
        Schema::dropIfExists('activation_user');
    }
}
