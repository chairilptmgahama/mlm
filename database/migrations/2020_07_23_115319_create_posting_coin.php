<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostingCoin extends Migration {

    public function up() {
        Schema::create('posting_coin', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('posting_id')->nullable();
            $table->smallInteger('type')->comment('1 = coin bertambah, 2 = coin berkurang');
            $table->double('qty', 8, 4);
            $table->double('persentase', 8, 4);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('active_at')->nullable();

            $table->index('user_id');
            $table->index('posting_id');
            $table->index('type');
            $table->index('created_at');
            $table->index('active_at');
        });
    }

    public function down() {
        Schema::dropIfExists('posting_coin');
    }
}
