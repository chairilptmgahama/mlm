<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPin extends Migration {

    public function up() {
        Schema::create('member_coin', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('transaction_id');
            $table->smallInteger('type')->comment('1 = coin bertambah, 2 = coin berkurang');
            $table->double('qty', 12, 4);
            $table->double('price', 15, 4);
            $table->smallInteger('bonus_type')->nullable()->comment('1 = bonus sponsor, 2 => bonus profit posting');
            $table->timestamp('created_at')->useCurrent();

            $table->index('user_id');
            $table->index('transaction_id');
            $table->index('type');
            $table->index('bonus_type');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('member_coin');
    }
}
