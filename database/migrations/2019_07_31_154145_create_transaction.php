<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaction extends Migration {

    public function up() {
        Schema::create('transaction', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('seller_id');
            $table->string('transaction_code', 25)->nullable();
            $table->smallInteger('type')->default(1)->comment('jenis transaksi. 1 => beli coin, 2 => transfer coin, 3 => jual coin, 4 => posting coin user_id == seller_id, 5 => posting coin tuntas user_id == seller_id, 6 => get bonus posting coin berjalan user_id == seller_id , 7 => get bonus Sponsor user_id == seller_id');
            $table->double('total_coin', 15, 4)->default(0);
            $table->double('price', 15, 2);
            $table->integer('unique_digit');
            $table->double('admin_fee', 15, 4)->default(0);
            $table->smallInteger('status')->default(0)->comment('0 = belum, 1 = pembeli transfer, 2 = tuntas konfirm, 3 = batal');
            $table->integer('coin_setting_id');

            $table->smallInteger('buy_metode')->default(1)->comment('1 = Transfer Bank');
            $table->string('bank_name', 100)->nullable();
            $table->string('account_no', 50)->nullable();
            $table->string('account_name', 100)->nullable();
            $table->string('reason', 150)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('tuntas_at')->nullable();


            $table->index('user_id');
            $table->index('seller_id');
            $table->index('transaction_code');
            $table->index('type');
            $table->index('status');
            $table->index('coin_setting_id');

            $table->index('buy_metode');
            $table->index('created_at');
            $table->index('deleted_at');
            $table->index('tuntas_at');
        });
    }

    public function down() {
        Schema::dropIfExists('transaction');
    }
}
