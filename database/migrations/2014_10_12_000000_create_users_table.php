<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 60);
            $table->string('email', 100);
            $table->string('hp', 25)->nullable();
            $table->string('password', 100);
            $table->string('user_code', 30)->nullable();
            $table->tinyInteger('is_login')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->tinyInteger('is_active')->default(0)->comment('0 = tidak aktif, 1 = aktif');
            $table->smallInteger('user_type')->default(10)->comment('1 = super admin, 2 = master admin, 3 = admin, 10 = member');
            $table->smallInteger('member_type')->default(0)->comment('1 => Exchanger, 3 => Member');
            $table->string('code', 25)->nullable();
            $table->integer('sponsor_id')->nullable();
            $table->smallInteger('total_sponsor')->default(0);
            $table->integer('upline_id')->nullable();
            $table->text('upline_detail')->nullable();
            $table->tinyInteger('is_referal_link')->default(0)->comment('0 = bukan, 1 = iya');
            $table->tinyInteger('is_bank')->default(0)->comment('0 = bukan, 1 = iya');
            
            $table->tinyInteger('is_profile')->default(0)->comment('0 = belum, 1 = sudah');
            $table->string('full_name', 100)->nullable()->comment('buat di account_name bank');
            $table->smallInteger('gender')->nullable()->comment('1 = laki-laki, 2 = perempuan');
            $table->string('alamat', 255)->nullable();
            $table->string('provinsi', 70)->nullable();
            $table->string('kota', 100)->nullable();
            $table->string('kecamatan', 120)->nullable();
            $table->string('kode_pos', 12)->nullable();
            $table->string('pin_code', 10)->nullable();
            
            $table->tinyInteger('is_wallet')->default(0)->comment('0 = belum, 1 = sudah');
            $table->string('wallet', 200)->nullable();
            $table->timestamp('wallet_at')->nullable();
            
            $table->timestamp('active_at')->nullable();
            $table->timestamp('package_id_at')->nullable();
            $table->timestamp('upgrade_at')->nullable();
            $table->timestamp('profile_created_at')->nullable();
            $table->timestamp('profile_updated_at')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            
            $table->index('name');
            $table->index('email');
            $table->index('password');
            $table->index('hp');
            $table->index('user_code');
            $table->index('is_login');
            $table->index('is_active');
            $table->index('user_type');
            $table->index('member_type');
            $table->index('sponsor_id');
            $table->index('code');
            $table->index('total_sponsor');
            $table->index('upline_id');
            $table->index('is_referal_link');
            $table->index('is_bank');
            
            $table->index('is_profile');
            $table->index('gender');
            $table->index('full_name');
            $table->index('provinsi');
            $table->index('kota');
            $table->index('kecamatan');
            $table->index('pin_code');
            $table->index('is_wallet');
            $table->index('wallet');
            $table->index('wallet_at');
            
            $table->index('active_at');
            $table->index('package_id_at');
            $table->index('upgrade_at');
            $table->index('profile_created_at');
            $table->index('profile_updated_at');
            $table->index('created_at');
            $table->index('deleted_at');
            
        });
    }

    public function down() {
        Schema::dropIfExists('users');
    }
}
