<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxUserProfit extends Migration {

    public function up() {
        Schema::table('setting_app', function(Blueprint $table){
            $table->integer('max_user_profit')->default(0);
        });
    }

    public function down() {
        Schema::table('setting_app', function(Blueprint $table){
            $table->dropColumn('max_user_profit');
        });
    }
}
