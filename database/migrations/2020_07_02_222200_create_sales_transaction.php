<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTransaction extends Migration {

    public function up() {
//        Schema::create('s_transaction', function (Blueprint $table) {
//            $table->engine = 'InnoDB';
//            $table->increments('id');
//            $table->integer('user_id')->unsigned();
//            $table->string('invoice', 40);
//            $table->double('total_price', 15, 2);
//            $table->date('sale_date');
//            $table->smallInteger('status')->default(0)->comment('0 = belum, 1 = konfirmasi member, 2 = konfirmasi admin, 3 = batal');
//            $table->string('reason', 175)->nullable();
//            $table->smallInteger('buy_metode')->default(0)->comment('1 = COD, 2 = Transfer Bank');
//            $table->string('bank_name', 100)->nullable();
//            $table->string('account_no', 50)->nullable();
//            $table->string('account_name', 100)->nullable();
//            $table->timestamp('created_at')->useCurrent();
//            $table->timestamp('updated_at')->nullable();
//            $table->timestamp('tuntas_at')->nullable();
//            $table->timestamp('deleted_at')->nullable();
//
//            $table->index('user_id');
//            $table->index('invoice');
//            $table->index('total_price');
//            $table->index('sale_date');
//            $table->index('status');
//            $table->index('buy_metode');
//            $table->index('created_at');
//            $table->index('updated_at');
//            $table->index('tuntas_at');
//            $table->index('deleted_at');
//        });
    }

    public function down() {
//        Schema::dropIfExists('s_transaction');
    }
}
