<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinSetting extends Migration {

    public function up() {
        Schema::create('coin_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('type')->comment('1 = exchanger, 2 = member');
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->double('buy_price', 15, 2);
            $table->double('sell_price', 15, 2);
            $table->integer('buy_fee');
            $table->integer('sell_fee');
            $table->integer('buy_min');
            $table->integer('sell_min');
            $table->timestamp('created_at')->useCurrent();

            $table->index('type');
            $table->index('is_active');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('coin_setting');
    }
}
