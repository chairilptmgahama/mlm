<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingPosting extends Migration {

    public function up() {
        Schema::create('setting_posting', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('is_active')->default(1)->comment('0 = tidak aktif, 1 = aktif');
            $table->integer('persentase');
            $table->integer('min_posting');
            $table->integer('min_convert');
            $table->timestamp('created_at')->useCurrent();

            $table->index('is_active');
            $table->index('created_at');
        });
    }

    public function down() {
        Schema::dropIfExists('setting_posting');
    }
}
