<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsFiveOnUsers extends Migration {

    public function up() {
        Schema::table('users', function(Blueprint $table){
            $table->tinyInteger('is_five')->default(1)->comment('0 = bukan, 1 = iya');
            $table->index('is_five');
        });
    }

    public function down() {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('is_five');
        });
    }
}
