<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimProductActive extends Migration {

    public function up() {
//        Schema::create('claim_product_active', function (Blueprint $table) {
//            $table->engine = 'InnoDB';
//            $table->increments('id');
//            $table->integer('user_id');
//            $table->string('full_name', 100);
//            $table->string('hp', 20);
//            $table->string('alamat', 120)->nullable();
//            $table->string('kelurahan', 120)->nullable();
//            $table->string('kecamatan', 90)->nullable();
//            $table->string('kota', 90)->nullable();
//            $table->string('provinsi', 70)->nullable();
//           
//            $table->smallInteger('status')->default(0)->comment('0 = belum, 1 = sudah dikirim, 2 = batal');
//            $table->string('reason', 150)->nullable();
//            $table->string('kurir_name', 70)->nullable();
//            $table->string('no_resi', 30)->nullable();
//            $table->timestamp('created_at')->useCurrent();
//            $table->timestamp('kirim_at')->nullable();
//            $table->timestamp('deleted_at')->nullable();
//            
//            $table->index('user_id');
//            $table->index('status');
//            $table->index('created_at');
//            $table->index('kirim_at');
//            $table->index('kurir_name');
//            $table->index('no_resi');
//        });
    }

    public function down() {
        Schema::dropIfExists('claim_product_active');
    }
}
