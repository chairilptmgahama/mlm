<?php

Route::get('/', 'Admin\HomeController@getFront')->name('frontLogin');
//Route::get('/admin/area/login', 'Admin\HomeController@getAdminLogin')->name('adminLogin');
Route::get('/login', 'Admin\HomeController@getMemberLogin')->name('memberLogin');
Route::post('/login_admin', 'Admin\HomeController@postAdminLogin');
//referal link
Route::get('/ref/{code_referal}', 'FrontEnd\ReferalController@getAddReferalLink')->name('referalLink');
Route::post('/refsp', 'FrontEnd\ReferalController@postAddReferalLink');
Route::get('/m/forgot/passwd', 'FrontEnd\FrontEndController@getForgotPassword')->name('forgotPasswd');
Route::post('/m/forgot/passwd', 'FrontEnd\FrontEndController@postForgotPassword');
Route::get('/m/auth/passwd/{code}/{email}', 'FrontEnd\FrontEndController@getAuthPassword')->name('passwdauth');
Route::post('/m/auth/passwd', 'FrontEnd\FrontEndController@postAuthPassword');

//Auth::routes();
Route::prefix('/')->group(function () {

    Route::get('/adm/dashboard', 'Admin\DashboardController@getDashboard')->name('admDashboard')->middleware('auth');
    Route::get('/admin_logout', 'Admin\HomeController@getAdminLogout')->middleware('auth');

    //Wilayah Admin
        //admin
        Route::get('/adm/add-admin', 'Admin\MasterAdminController@getAddAdmin')->name('addCrew')>name('addCrew')->middleware('auth');
        Route::post('/adm/new-admin', 'Admin\MasterAdminController@postAddAdmin')->middleware('auth');
        Route::post('/adm/admin', 'Admin\MasterAdminController@postEditRemoveAdmin')->middleware('auth');

        //setting
//        Route::get('/adm/add/pin-setting', 'Admin\MasterAdminController@getAddPinSetting')->name('addSettingPin')->middleware('auth');
//        Route::post('/adm/add/pin-setting', 'Admin\MasterAdminController@postPinSetting')->middleware('auth');
        Route::get('/adm/packages', 'Admin\MasterAdminController@getAllPackage')->name('allPackage')->middleware('auth');
        Route::post('/adm/package', 'Admin\MasterAdminController@postUpdatePackage')->middleware('auth');
        Route::get('/adm/bank', 'Admin\MasterAdminController@getBankPerusahaan')->name('adm_bankPerusahaan')->middleware('auth');
        Route::post('/adm/bank', 'Admin\MasterAdminController@postBankPerusahaan')->middleware('auth');
        Route::get('/adm/add/bank', 'Admin\MasterAdminController@getAddBankPerusahaan')->name('adm_addBankPerusahaan')->middleware('auth');
        Route::post('/adm/add/bank', 'Admin\MasterAdminController@postAddBankPerusahaan')->middleware('auth');
        Route::get('/adm/add/master-pin', 'Admin\MasterAdminController@getAddMasterPin')->name('addMasterPin')->middleware('auth');
        Route::post('/adm/add/master-pin', 'Admin\MasterAdminController@postAddMasterPin')->middleware('auth');
        Route::get('/adm/bonus-start', 'Admin\MasterAdminController@getBonusStart')->name('adm_bonusStart')->middleware('auth');
        Route::post('/adm/bonus-start', 'Admin\MasterAdminController@postBonusStart')->middleware('auth');
        Route::get('/adm/add/bonus-reward', 'Admin\MasterAdminController@getAddBonusReward')->name('adm_addBonusReward')->middleware('auth');
        Route::post('/adm/add/bonus-reward', 'Admin\MasterAdminController@postAddBonusReward')->middleware('auth');
        Route::get('/adm/bonus-reward', 'Admin\MasterAdminController@getBonusReward')->name('adm_bonusReward')->middleware('auth');
        Route::post('/adm/bonus-reward', 'Admin\MasterAdminController@postBonusReward')->middleware('auth');
        Route::get('/adm/bonus-level', 'Admin\MasterAdminController@getBonusLevel')->name('adm_bonusLevel')->middleware('auth');
        Route::post('/adm/bonus-level', 'Admin\MasterAdminController@postBonusLevel')->middleware('auth');
//        Route::get('/adm/bonus-pasangan', 'Admin\MasterAdminController@getBonusPasangan')->name('adm_bonusPasangan')->middleware('auth');
//        Route::post('/adm/bonus-pasangan', 'Admin\MasterAdminController@postBonusPasangan')->middleware('auth');
        Route::get('/adm/add/bonus-pool', 'Admin\MasterAdminController@getAddBonusPoolSharing')->name('adm_addBonusPoolSharing')->middleware('auth');
        Route::post('/adm/add/bonus-pool', 'Admin\MasterAdminController@postAddBonusPoolSharing')->middleware('auth');
        Route::get('/adm/bonus-pool', 'Admin\MasterAdminController@getBonusPoolSharing')->name('adm_bonusPoolSharing')->middleware('auth');
        Route::post('/adm/bonus-pool', 'Admin\MasterAdminController@postBonusPoolSharing')->middleware('auth');


        //Pin & Transaction
        Route::get('/adm/list/transactions', 'Admin\MasterAdminController@getListTransactions')->name('adm_listTransaction')->middleware('auth');
        Route::post('/adm/confirm/transaction', 'Admin\MasterAdminController@postConfirmTransaction')->middleware('auth');
//        Route::get('/adm/list/kirim-paket', 'Admin\MasterAdminController@getListKirimPaket')->name('adm_listKirimPaket')->middleware('auth');
//        Route::get('/adm/kirim-paket/{id}/{user_id}', 'Admin\MasterAdminController@getKirimPaketByID')->name('adm_KirimPaketID')->middleware('auth');
//        Route::post('/adm/kirim-paket', 'Admin\MasterAdminController@postConfirmKirimPaket')->middleware('auth');

        //News
        Route::get('/adm/list/news', 'Admin\MasterAdminController@getListNews')->name('adm_listNews')->middleware('auth');
        Route::get('/adm/add/news', 'Admin\MasterAdminController@getAddNews')->name('adm_addNews')->middleware('auth');
        Route::post('/adm/add/news', 'Admin\MasterAdminController@postAddNews')->middleware('auth');
        Route::get('/adm/news/{id}', 'Admin\MasterAdminController@getNewsByID')->name('adm_idNews')->middleware('auth');
        Route::post('/adm/edit/news', 'Admin\MasterAdminController@postEditNews')->middleware('auth');
        Route::post('/adm/rm/news', 'Admin\MasterAdminController@postRemoveNews')->middleware('auth');

        //Member
        Route::get('/adm/list/member', 'Admin\MasterAdminController@getListMember')->name('adm_listMember')->middleware('auth');
        Route::post('/adm/change/passwd/member', 'Admin\MasterAdminController@postAdminChangePasswordMember')->middleware('auth');
        Route::get('/adm/member/req/kirim', 'Admin\MasterAdminController@getMemberRequestKirim')->name('adm_memberReqKirim')->middleware('auth');
        Route::post('/adm/member/req/kirim', 'Admin\MasterAdminController@postMemberRequestKirim')->middleware('auth');
        Route::post('/adm/change/data/member', 'Admin\MasterAdminController@postAdminChangeDataMember')->middleware('auth');
        Route::post('/adm/change/block/member', 'Admin\MasterAdminController@postAdminChangeBlockMember')->middleware('auth');
        Route::get('/adm/pin/member', 'Admin\MasterAdminController@getlistPinMember')->name('adm_listPinMember')->middleware('auth');
        Route::get('/adm/bonus/member', 'Admin\MasterAdminController@getlistBonusMember')->name('adm_listBonusMember')->middleware('auth');
        Route::get('/adm/saldo/member', 'Admin\MasterAdminController@getlistSaldoMember')->name('adm_listSaldoMember')->middleware('auth');
        Route::post('/adm/search-list/member', 'Admin\MasterAdminController@postAdminsearchListMember')->middleware('auth');
        Route::get('/adm/top/member-omzet', 'Admin\MasterAdminController@getListTopTenMemberOmzetMonthly')->name('adm_listTopTenMemberOmzet')->middleware('auth');
        Route::get('/adm/member/bonus-sp', 'Admin\MasterAdminController@getListMemberBonusSponsor')->name('adm_listMemberBonusSponsor')->middleware('auth');
        Route::get('/adm/member/bonus-binary', 'Admin\MasterAdminController@getListMemberBonusBinary')->name('adm_listMemberBonusBinary')->middleware('auth');
        Route::get('/adm/member/bonus-level', 'Admin\MasterAdminController@getListMemberBonusLevel')->name('adm_listMemberBonusLevel')->middleware('auth');
        Route::get('/adm/list/claim/activation/product', 'Admin\MasterAdminController@getListClaimPrudukAktifasi')->name('adm_listClaimPrudukAktifasi')->middleware('auth');
        Route::get('/adm/claim/activation/product/{id}', 'Admin\MasterAdminController@getClaimPrudukAktifasiID')->middleware('auth');
        Route::post('/adm/claim/activation/product', 'Admin\MasterAdminController@postClaimPrudukAktifasi')->middleware('auth');

        //Laporan Bonus
        Route::get('/adm/list/wd-sponsor', 'Admin\MasterAdminController@getAllWDSponsor')->name('adm_listWDSponsor')->middleware('auth');
        Route::get('/adm/list/claim-phu', 'Admin\MasterAdminController@getAllClaimPHU')->name('adm_listClaimPHU')->middleware('auth');
        Route::post('/adm/check/wd', 'Admin\MasterAdminController@postCheckWD')->middleware('auth');
        Route::post('/adm/reject/wd', 'Admin\MasterAdminController@postRejectWD')->middleware('auth');

        Route::get('/adm/list/history/wd-sponsor', 'Admin\MasterAdminController@getAllHistoryWDSponsor')->name('adm_listHistoryWDSponsor')->middleware('auth');
        Route::get('/adm/list/history/claim-phu', 'Admin\MasterAdminController@getAllHistoryClaimPHU')->name('adm_listHistoryClaimPHU')->middleware('auth');
//        Route::get('/adm/list/history/wd-binary', 'Admin\MasterAdminController@getAllHistoryWDBinary')->name('adm_listHistoryWDBinary')->middleware('auth');
//        Route::get('/adm/list/history/wd-reward', 'Admin\MasterAdminController@getAllHistoryWDReward')->name('adm_listHistoryWDReward')->middleware('auth');
//        Route::get('/adm/list/history/wd-ro', 'Admin\MasterAdminController@getAllHistoryWDRO')->name('adm_listHistoryWDRO')->middleware('auth');
//        Route::get('/adm/list/history/wd-level', 'Admin\MasterAdminController@getAllHistoryWDLevel')->name('adm_listHistoryWDLevel')->middleware('auth');
//        Route::get('/adm/list/history/wd-safra-poin', 'Admin\MasterAdminController@getAllHistoryWDSafraPoin')->name('adm_listHistoryWDSafraPoin')->middleware('auth');

        Route::get('/adm/member/bonus-phu', 'Admin\MasterAdminController@getMemberBonusPHU')->name('adm_memberBonusPHU')->middleware('auth');

//        Route::get('/adm/global/bonus', 'Admin\MasterAdminController@getMemberBonusPHU')->name('adm_memberBonusPHU')->middleware('auth');
//        Route::get('/adm/global/sales', 'Admin\MasterAdminController@getGlobalReportSales')->name('adm_globalReportSales')->middleware('auth');
//        Route::get('/adm/global/member-transaction', 'Admin\MasterAdminController@getGlobalReportMemberTransaction')->name('adm_globalReportMemberTrans')->middleware('auth');

        //Sales
        Route::get('/adm/list/sales/purchase', 'Admin\MasterAdminController@getListSalesPurchase')->name('adm_listSalesPurchases')->middleware('auth');
        Route::get('/adm/add/sales/purchase', 'Admin\MasterAdminController@getAddSalesPurchase')->name('adm_addSalesPurchase')->middleware('auth');
        Route::post('/adm/add/sales/purchase', 'Admin\MasterAdminController@postAddSalesPurchase')->middleware('auth');
        Route::get('/adm/sales/purchase/{id}', 'Admin\MasterAdminController@getSalesPurchaseByID')->name('adm_idSalesPurchase')->middleware('auth');
        Route::post('/adm/edit/sales/purchase', 'Admin\MasterAdminController@postEditSalesPurchase')->middleware('auth');
        Route::post('/adm/rm/sales/purchase', 'Admin\MasterAdminController@postRemoveSalesPurchase')->middleware('auth');
        Route::get('/adm/list/member/sales', 'Admin\MasterAdminController@getListMemberSales')->name('adm_listMemberSales')->middleware('auth');
        Route::get('/adm/member/sales/{id}', 'Admin\MasterAdminController@getMembeSalesByID')->name('adm_salesByID')->middleware('auth');
        Route::post('/adm/member/confirm/sales', 'Admin\MasterAdminController@getConfirmMemberSalesByID')->middleware('auth');
        Route::post('/adm/member/reject/sales', 'Admin\MasterAdminController@getRejectMemberSalesByID')->middleware('auth');

        //Ajax
        Route::get('/ajax/adm/admin/{type}/{id}', 'Admin\AjaxController@getAdminById')->middleware('auth');
        Route::get('/ajax/adm/package/{id}', 'Admin\AjaxController@getPackageById')->middleware('auth');
        Route::get('/ajax/adm/cek/transaction/{id}/{user_id}', 'Admin\AjaxController@getCekTransactionById')->middleware('auth');
        Route::get('/ajax/adm/bank/{id}', 'Admin\AjaxController@getBankPerusahaan')->middleware('auth');
        Route::get('/ajax/adm/kirim-paket/{id}/{user_id}', 'Admin\AjaxController@getKirimPaket')->middleware('auth');
        Route::get('/ajax/adm/cek/kirim-paket', 'Admin\AjaxController@getCekKirimPaket')->middleware('auth');
        Route::get('/ajax/adm/change-passwd/member/{id}', 'Admin\AjaxController@getAdminChangePasswordMember')->middleware('auth');
        Route::get('/ajax/adm/cek/reject-wd/{id}/{type}', 'Admin\AjaxController@getCekRejectWD')->middleware('auth');
        Route::get('/ajax/adm/cek/detail-wd/{id}', 'Admin\AjaxController@getCekDetailWD')->middleware('auth');
        Route::get('/ajax/adm/cek/bonus-reward/{id}', 'Admin\AjaxController@getCekDetailBonusReward')->middleware('auth');
        Route::get('/ajax/adm/cek/bonus-level/{id}', 'Admin\AjaxController@getCekDetailBonusLevel')->middleware('auth');
        Route::get('/ajax/adm/cek/username', 'Admin\AjaxController@getSearchUsername')->middleware('auth');
        Route::get('/ajax/adm/change-data/member/{id}', 'Admin\AjaxController@getAdminChangeDataMember')->middleware('auth');
        Route::get('/ajax/adm/change-block/member/{id}', 'Admin\AjaxController@getAdminChangeBlockMember')->middleware('auth');
        Route::get('/ajax/adm/cek/safra/transaction', 'Admin\AjaxController@getCekSafraTransactionById')->middleware('auth');
        Route::get('/ajax/adm/cek/bonus-pool/{id}', 'Admin\AjaxController@getCekDetailBonusPoolSharing')->middleware('auth');
        Route::get('/ajax/adm/cek/transaction/{id}/{user_id}', 'Admin\AjaxController@getCekTransactionById')->middleware('auth');
        Route::get('/ajax/adm/cek/confirm/belanja/{id}', 'Admin\AjaxController@getCekConfirmSalesMember')->middleware('auth');
        Route::get('/ajax/adm/cek/reject/belanja/{id}', 'Admin\AjaxController@getCekRejectSalesMember')->middleware('auth');

        Route::get('/adm/test-email', 'Admin\MasterAdminController@getTestEmail')->middleware('auth');

        ////////////////////////////////////////////////////////////////////////
        //##########################
        ////////////////////////////////////////////////////////////////////////
        //#########################


    //Wilayah Member
        Route::get('/m/dashboard', 'Admin\DashboardController@getMemberDashboard')->name('mainDashboard')->middleware('auth');
        //profile
        Route::get('/m/profile', 'Admin\MemberController@getMyProfile')->name('m_myProfile')->middleware('auth'); //Done
        Route::get('/m/add/profile', 'Admin\MemberController@getAddMyProfile')->name('m_newProfile')->middleware('auth'); //Done
        Route::post('/m/add/profile', 'Admin\MemberController@postAddMyProfile')->middleware('auth'); //Done
        Route::get('/m/edit/password', 'Admin\MemberController@getEditPassword')->name('m_editPassword')->middleware('auth'); //Done
        Route::post('/m/edit/password', 'Admin\MemberController@postEditPassword')->middleware('auth'); //Done

        //Bank
        Route::get('/m/bank', 'Admin\MemberController@getMyBank')->name('m_myBank')->middleware('auth'); //Done
        Route::post('/m/add/bank', 'Admin\MemberController@postAddBank')->middleware('auth'); //Done
        Route::get('/m/activate/bank/{id}', 'Admin\MemberController@getActivateBank')->middleware('auth'); //Done
        Route::post('/m/activate/bank', 'Admin\MemberController@postActivateBank')->middleware('auth'); //Done

        //Kode Pin
        Route::get('/m/add/code/pin', 'Admin\MemberController@getAddCodePin')->name('m_addCodePin')->middleware('auth');
        Route::post('/m/add/code/pin', 'Admin\MemberController@postAddCodePin')->middleware('auth');

        //Pin & Transaction
        Route::get('/m/add/pin', 'Admin\MemberController@getAddPin')->name('m_newPin')->middleware('auth');
        Route::post('/m/add/pin', 'Admin\MemberController@postAddPin')->middleware('auth');
        Route::get('/m/list/buyer/transactions', 'Admin\MemberController@getListBuyerTransactions')->name('m_listBuyerTransactions')->middleware('auth');
        Route::get('/m/list/seller/transactions', 'Admin\MemberController@getListSellerTransactions')->name('m_listSellerTransactions')->middleware('auth');
        Route::get('/m/buyer/transaction/{id}', 'Admin\MemberController@getBuyerTransactionID')->name('m_buyerTransactionID')->middleware('auth');
        Route::get('/m/seller/transaction/{id}', 'Admin\MemberController@getSellerTransactionID')->name('m_sellerTransactionID')->middleware('auth');
        Route::post('/m/add/buyer-transaction', 'Admin\MemberController@postAddBuyerTransaction')->middleware('auth');
        Route::post('/m/reject/buyer-transaction', 'Admin\MemberController@postRejectBuyerTransaction')->middleware('auth');
        Route::post('/m/add/seller-transaction', 'Admin\MemberController@postAddSellerTransaction')->middleware('auth');
        Route::post('/m/reject/seller-transaction', 'Admin\MemberController@postRejectSellerTransaction')->middleware('auth');

        Route::get('/m/pin/stock', 'Admin\MemberController@getMyPinStock')->name('m_myPinStock')->middleware('auth');
        Route::get('/m/pin/history', 'Admin\MemberController@getMyPinHistory')->name('m_myPinHistory')->middleware('auth');
//        Route::post('/m/add/kirim-paket', 'Admin\MemberController@postAddKirimPaket')->middleware('auth');
//        Route::get('/m/kirim-paket', 'Admin\MemberController@getMyKirimPaket')->name('m_myKirimPaket')->middleware('auth');
        Route::get('/m/add/transfer-pin', 'Admin\MemberController@getTransferPin')->name('m_addTransferPin')->middleware('auth');
        Route::post('/m/add/transfer-pin', 'Admin\MemberController@postAddTransferPin')->middleware('auth');
        Route::get('/m/list/transfer/pin', 'Admin\MemberController@getListTransferPin')->name('m_listTransferPin')->middleware('auth');
        Route::get('/m/detail/transfer/pin/{id}/{type}', 'Admin\MemberController@getDetailTransferPin')->name('m_detailTransferPin')->middleware('auth');
        Route::post('/m/add-confirm/transfer/pin', 'Admin\MemberController@postAddConfirmTransferPin')->middleware('auth');
        Route::post('/m/reject-confirm/transfer/pin', 'Admin\MemberController@postRejectConfirmTransferPin')->middleware('auth');


        //Sponsor
        Route::get('/m/add/sponsor-package', 'Admin\MemberController@getAddSponsorPackage')->name('m_packageSponsor')->middleware('auth');
        Route::get('/m/add/sponsor', 'Admin\MemberController@getAddSponsor')->name('m_newSponsor')->middleware('auth');
        Route::post('/m/add/sponsor', 'Admin\MemberController@postAddSponsor')->middleware('auth');
        Route::get('/m/status/sponsor', 'Admin\MemberController@getStatusSponsor')->name('m_statusSponsor')->middleware('auth');
        Route::get('/m/my/sponsor', 'Admin\MemberController@getMySponsor')->name('m_mySponsor')->middleware('auth');
        Route::get('/m/my/binary', 'Admin\MemberController@getMyBinary')->name('m_myBinary')->middleware('auth');
        Route::get('/m/status/member', 'Admin\MemberController@getStatusMember')->name('m_statusMember')->middleware('auth');
        Route::get('/m/detail/member-binary', 'Admin\MemberController@getDetailMemberBinary')->name('m_detailMemberBinary')->middleware('auth');
        Route::get('/m/status/level', 'Admin\MemberController@getStatusLevel')->name('m_statusLevel')->middleware('auth');
        Route::get('/m/all/ds', 'Admin\MemberController@getAllDS')->name('m_allDS')->middleware('auth');
        Route::get('/m/my/sponsor-tree', 'Admin\MemberController@getMySponsorTree')->name('m_mySponsorTree')->middleware('auth');

        Route::get('/m/add/member-stockist', 'Admin\MemberController@getAddMemberStockist')->name('m_addMemberStockist')->middleware('auth');
        Route::post('/m/add/member-stockist', 'Admin\MemberController@postAddMemberStockist')->middleware('auth');
        Route::get('/m/add/annual-annual/package', 'Admin\MemberController@getAddAnnualMemberPackage')->name('m_packageAnnualMember')->middleware('auth');
        Route::get('/m/add/annual-member', 'Admin\MemberController@getAddMemberOpening')->name('m_addMemberOpening')->middleware('auth');
        Route::post('/m/add/annual-member', 'Admin\MemberController@postAddMemberOpening')->middleware('auth');
        Route::get('/m/list-stockist', 'Admin\MemberController@getListStockistByMaster')->name('m_listStockistbyMaster')->middleware('auth');

        //Package
        Route::get('/m/add/package', 'Admin\MemberController@getAddPackage')->name('m_newPackage')->middleware('auth');
        Route::post('/m/add/package', 'Admin\MemberController@postAddPackage')->middleware('auth');
        Route::get('/m/list/order-package', 'Admin\MemberController@getListOrderPackage')->name('m_listOrderPackage')->middleware('auth');
        Route::get('/m/detail/order-package/{paket_id}', 'Admin\MemberController@getDetailOrderPackage')->name('m_detailOrderPackage')->middleware('auth');
        Route::post('/m/confirm/package', 'Admin\MemberController@postActivatePackage')->middleware('auth');
        Route::get('/m/add/upgrade', 'Admin\MemberController@getAddUpgrade')->name('m_newUpgrade')->middleware('auth');
        Route::post('/m/add/upgrade', 'Admin\MemberController@postAddUpgrade')->middleware('auth');
        Route::get('/m/add/repeat-order', 'Admin\MemberController@getAddRO')->name('m_newRO')->middleware('auth');
        Route::post('/m/add/repeat-order', 'Admin\MemberController@postAddRO')->middleware('auth');

        Route::get('/m/claim/activate/product', 'Admin\MemberController@getClaimActivatedProduct')->name('m_claimAktifasiProduk')->middleware('auth');
        Route::post('/m/claim/activate/product', 'Admin\MemberController@postClaimActivatedProduct')->middleware('auth');
        Route::get('/m/activate/product/{id}', 'Admin\MemberController@getActivatedProductID')->name('m_claimAktifasiProduk')->middleware('auth');

        //Menu Bonus
//        Route::get('/m/summary/bonus', 'Admin\BonusmemberController@getMySummaryBonus')->name('m_myBonusSummary')->middleware('auth');
        Route::get('/m/sponsor/bonus', 'Admin\BonusmemberController@getMySponsorBonus')->name('m_myBonusSponsor')->middleware('auth');
        Route::get('/m/sponsor/poin', 'Admin\BonusmemberController@getMyPoinBonus')->name('m_myBonusPoin')->middleware('auth');
        Route::get('/m/sponsor/fly', 'Admin\BonusmemberController@getMyFlyBonus')->name('m_myBonusFly')->middleware('auth');
        Route::post('/m/claim/bonus-phu', 'Admin\BonusmemberController@postClaimBonusPHU')->middleware('auth');

        Route::post('/m/claim/poin', 'Admin\BonusmemberController@postClaimBonusPoin')->middleware('auth');


        Route::get('/m/report/bonus-sponsor', 'Admin\BonusmemberController@getMyReportSponsorBonus')->name('m_myReportBonusSponsor')->middleware('auth');
//        Route::get('/m/req/wd/bonus-sponsor', 'Admin\BonusmemberController@getRequestWDSponsorBonus')->name('m_myReqWDBonusSponsor')->middleware('auth');
//        Route::post('/m/req/wd/bonus-sponsor', 'Admin\BonusmemberController@postRequestWDSponsorBonus')->middleware('auth');
        Route::get('/m/req/pulsa', 'Admin\BonusmemberController@getRequestWDPulsa')->name('m_myReqWDPulsa')->middleware('auth');
        Route::post('/m/req/pulsa', 'Admin\BonusmemberController@postRequestWDPulsa')->middleware('auth');
        Route::get('/m/report/req-pulsa', 'Admin\BonusmemberController@getMyReportRewuestPulsa')->name('m_myReportRequestPulsa')->middleware('auth');


        //Belanja
        Route::get('/m/add/belanja', 'Admin\MemberController@getTransferBelanja')->name('m_addBelanja')->middleware('auth');
        Route::post('/m/add/belanja', 'Admin\MemberController@postAddBelanja')->middleware('auth');
        Route::get('/m/list/belanja', 'Admin\MemberController@getListBelanja')->name('m_listBelanja')->middleware('auth');
        Route::get('/m/detail/belanja/{id}', 'Admin\MemberController@getDetailBelanja')->name('m_detailBelanja')->middleware('auth');
        Route::post('/m/add-confirm/belanja', 'Admin\MemberController@postAddConfirmBelanja')->middleware('auth');
        Route::post('/m/reject-confirm/belanja', 'Admin\MemberController@postRejectConfirmBelanja')->middleware('auth');

        //News
        Route::get('/m/list/news', 'Admin\MemberController@getMemberListNews')->name('m_listNews')->middleware('auth');
        Route::get('/m/news/{id}', 'Admin\MemberController@getMemberNewsByID')->name('m_listNewsID')->middleware('auth');

        //Testing
        Route::get('/m/testing', 'Admin\MemberController@getMemberTestingCheck')->middleware('auth');




        //Ajax
        Route::get('/m/add/buyer-transaction', 'Admin\AjaxmemberController@postAddBuyerTransaction')->middleware('auth');
        Route::get('/m/reject/buyer-transaction', 'Admin\AjaxmemberController@postRejectBuyerTransaction')->middleware('auth');
        Route::get('/m/add/seller-transaction', 'Admin\AjaxmemberController@postAddSellerTransaction')->middleware('auth');
        Route::get('/m/reject/seller-transaction', 'Admin\AjaxmemberController@postRejectSellerTransaction')->middleware('auth');
        Route::get('/m/add/transferpin', 'Admin\AjaxmemberController@postAddTransferPin')->middleware('auth');
        Route::get('/m/reject/transferpin', 'Admin\AjaxmemberController@postRejectTransferPin')->middleware('auth');


        Route::get('/m/cek/add-sponsor', 'Admin\AjaxmemberController@postCekAddSponsor')->middleware('auth');
        Route::get('/m/cek/add-package/{id_paket}', 'Admin\AjaxmemberController@getCekAddPackage')->middleware('auth');
        Route::get('/m/cek/add-pin', 'Admin\AjaxmemberController@postCekAddPin')->middleware('auth');
        Route::get('/m/cek/add-profile', 'Admin\AjaxmemberController@postCekAddProfile')->middleware('auth');
//        Route::get('/m/cek/add-transaction', 'Admin\AjaxmemberController@postCekAddTransaction')->middleware('auth');
//        Route::get('/m/cek/reject-transaction', 'Admin\AjaxmemberController@postCekRejectTransaction')->middleware('auth');
        Route::get('/m/cek/confirm-order', 'Admin\AjaxmemberController@getCekConfirmOrderPackage')->middleware('auth');
        Route::get('/m/cek/add-bank', 'Admin\AjaxmemberController@getCekAddBank')->middleware('auth');
        Route::get('/m/activate/bank/{id}', 'Admin\AjaxmemberController@getActivateBank')->middleware('auth');
        Route::get('/m/cek/kirim-paket', 'Admin\AjaxmemberController@getCekConfirmKirimPaket')->middleware('auth');
        Route::get('/m/cek/transfer-pin', 'Admin\AjaxmemberController@getCekTransferPin')->middleware('auth');
        Route::get('/m/cek/upgrade-package/{id_paket}', 'Admin\AjaxmemberController@getCekUpgrade')->middleware('auth');
        Route::get('/m/cek/placement/{id}/{type}', 'Admin\AjaxmemberController@getCekPlacementKiriKanan')->middleware('auth');
        Route::get('/m/cek/usercode', 'Admin\AjaxmemberController@getSearchUserCode')->middleware('auth');
        Route::get('/m/cek/usercode-all', 'Admin\AjaxmemberController@getSearchUserCodeAll')->middleware('auth');
        Route::get('/m/cek/repeat-order', 'Admin\AjaxmemberController@getCekRO')->middleware('auth');
        Route::get('/m/cek/detail/member-binary', 'Admin\AjaxmemberController@getAjaxDetailMemberBinary')->middleware('auth');
        Route::get('/m/cek/news/{id}', 'Admin\AjaxmemberController@getCekDetailNews')->middleware('auth');
        Route::get('/m/cek/bonus-sp/confirm-wd', 'Admin\AjaxmemberController@getCekReqWDBonusSponsor')->middleware('auth');
        Route::get('/m/cek/bonus-binary/confirm-wd', 'Admin\AjaxmemberController@getCekReqWDBonusBinary')->middleware('auth');
        Route::get('/m/cek/claim-reward/{id}', 'Admin\AjaxmemberController@getCekReqClaimReward')->middleware('auth');
        Route::get('/m/cek/bonus-level/confirm-wd', 'Admin\AjaxmemberController@getCekReqWDBonusLevel')->middleware('auth');
        Route::get('/m/cek/bonus-ro/confirm-wd', 'Admin\AjaxmemberController@getCekReqWDBonusRO')->middleware('auth');
        Route::get('/m/cek/safra-poin/confirm-wd', 'Admin\AjaxmemberController@getCekReqWDSafraPoin')->middleware('auth');
        Route::get('/m/cek/shoping', 'Admin\AjaxmemberController@getCekBuyShoping')->middleware('auth');
        Route::get('/m/cek/safra/transaction', 'Admin\AjaxmemberController@getCekSafraTransaction')->middleware('auth');
        Route::get('/m/cek/safra-reject/transaction', 'Admin\AjaxmemberController@getCekSafraRejectTransaction')->middleware('auth');
        Route::get('/m/cek/edit-password', 'Admin\AjaxmemberController@getCekEditPassword')->middleware('auth');
        Route::get('/m/cek/pulsa', 'Admin\AjaxmemberController@getCekReqWDPulsa')->middleware('auth');

        Route::get('/m/cek/add-stockist', 'Admin\AjaxmemberController@postCekAddStockist')->middleware('auth');
        Route::get('/m/cek/add-openingmember', 'Admin\AjaxmemberController@postAddOpeningMember')->middleware('auth');
        Route::get('/m/cek/pin_code', 'Admin\AjaxmemberController@getCekPinCode')->middleware('auth');
        Route::get('/m/cek/explore-{type}', 'Admin\AjaxmemberController@getCekExploreStockist')->middleware('auth');
        Route::get('/m/cek/claim/bonus-phu', 'Admin\AjaxmemberController@getCekClaimBonusPHU')->middleware('auth');
        Route::get('/m/cek/claim/activate/product', 'Admin\AjaxmemberController@getCekClaimActivatedProduct')->middleware('auth');

        Route::get('/m/cek/claim/poin/{id}', 'Admin\AjaxmemberController@getCekClaimBonusPoinID')->middleware('auth');
        Route::get('/m/cek/belanja', 'Admin\AjaxmemberController@getAjaxBelanja')->middleware('auth');
        Route::get('/m/cek/confirm/belanja', 'Admin\AjaxmemberController@getAjaxConfirmBelanja')->middleware('auth');
        Route::get('/m/cek/reject/belanja', 'Admin\AjaxmemberController@getAjaxRejectBelanja')->middleware('auth');

});
