<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Memberpackage;
use App\Model\Bonus;
use App\Model\Transferwd;
use App\Model\Package;
use App\Model\Pin;
use App\Model\Startwd;

class CronWDBonusSponsor extends Command {

    protected $signature = 'wd_bonus_sponsor';
    protected $description = 'Cron Harian WD bonus sponsor';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelWD = new Transferwd;
        $modelMember = new Member;
        $modelBonus = New Bonus;
        $modelPackage = New Package;
        $getData = $modelBonus->getCronBonusSponsor();
        $admin_fee = $modelBonus->getAdminFee(); //5%
        $max_wd = $modelBonus->getMaxWD();
        $dataWD = array();
        if($getData != null){
            foreach($getData as $row){
                if($row->total_bonus >= $max_wd){
//                    $WDSblm = $modelWD->getTotalWD($row->id_user);
                    $getWD = $row->total_bonus;
                    if($getWD >= $max_wd){
                        $fee_admin = ($getWD * $admin_fee)/100;
                        $wd_yg_didapat = $getWD - $fee_admin;
                        $codeWD = $modelWD->getCodeWD();
                        $dataWD = array(
                            'user_id' => $row->id_user,
                            'user_bank' => $row->id_bank,
                            'wd_code' => 'WDSp'.date('Ymd').$codeWD,
                            'type' => 1,
                            'wd_total' => $wd_yg_didapat,
                            'wd_date' => date('Y-m-d'),
                            'admin_fee' => $fee_admin,
                        );
                        $modelWD->getInsertWD($dataWD);
                        //update is_wd_sp table bonus_member
                        $dataUpdateIsWD = array(
                            'is_wd_sp' => 1,
                            'wd_sp_at' => date('Y-m-d H:i:s')
                        );
                        $modelBonus->getUpdateBonusSponsor($row->id_user, $dataUpdateIsWD);
                    }
                }
            }
        }
        dd('done wd bonus sponsor');
        dd($dataWD);
    }
    
    
    
    
}
