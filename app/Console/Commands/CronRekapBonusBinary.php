<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Binaryhistory;
use App\Model\Bonussetting;
use App\Model\Bonus;

class CronRekapBonusBinary extends Command {

    protected $signature = 'rekap_binary';
    protected $description = 'Cron Get Rekap Bonus Binary';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelBonus = new Bonus;
//        $getDataBinary = $modelBonus->getBonusBinaryByDate();
        $dateYesterday =  date('Y-m-d', strtotime('-1 days'));
        $getDataBinary = $modelBonus->getBonusBinaryYesterday($dateYesterday);
        //klo live ganti ke sini
        $dataArray = array();
        if($getDataBinary != null){
            foreach($getDataBinary as $row){
                $getCek = $modelBonus->getRekapBonusBinary($row->user_id, $row->day);
                if($getCek == null){
                    $bonus_wd = $row->total_get_bonus;
                    if($row->total_get_bonus > $row->max_day){
                        $bonus_wd = $row->max_day;
                    }
                    $dataArray = array(
                        'user_id' => $row->user_id,
                        'type' => 2,
                        'bonus_get' => $row->total_get_bonus,
                        'bonus_wd' => $bonus_wd,
                        'rekap_date' => $row->day,
                    );
                $modelBonus->getInsertRekapBonus($dataArray);
                }
            }
        }
        dd('done');
    }
    
    
    
    
}
