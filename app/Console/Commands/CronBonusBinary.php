<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Member;
use App\Model\Binaryhistory;
use App\Model\Bonussetting;
use App\Model\Bonus;

class CronBonusBinary extends Command {

    protected $signature = 'bonus_binary';
    protected $description = 'Cron Get Bonus Binary';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelMember = new Member;
        $modelBinaryHistory = new Binaryhistory;
        $modelBonusSetting = new Bonussetting;
        $modelBonus = new Bonus;
        dd('ini masih salah');
        $getId = $modelMember->getMemberHaveTwoSponsor();
        $dataArray = array();
        foreach($getId as $row){
            $kiri = 0;
            if($row->kiri_id != null){
                $downlineKiri = $row->upline_detail.',['.$row->id.']'.',['.$row->kiri_id.']';
                if($row->upline_detail == null){
                    $downlineKiri = '['.$row->id.']'.',['.$row->kiri_id.']';
                }
                $kiri = $modelMember->getCountMyDownline($downlineKiri);
            }
            $kanan = 0;
            if($row->kanan_id != null){
                $downlineKanan = $row->upline_detail.',['.$row->id.']'.',['.$row->kanan_id.']';
                if($row->upline_detail == null){
                    $downlineKanan = '['.$row->id.']'.',['.$row->kanan_id.']';
                }
                $kanan = $modelMember->getCountMyDownline($downlineKanan);
            }
            $getHistoryBinary = $modelBinaryHistory->getBinaryHistory($row->id);
            $kiriCheck = $kiri - $getHistoryBinary->sum_total_kiri;
            $kananCheck = $kanan - $getHistoryBinary->sum_total_kanan;
            if($kiriCheck > 0 && $kananCheck > 0){
                $pasangan = $kiriCheck;
                if($kiriCheck > $kananCheck){
                    $pasangan = $kananCheck;
                }
                if($kiriCheck < $kananCheck){
                    $pasangan = $kiriCheck;
                }
                $getSetting =$modelBonusSetting->getActiveBonusTeam($row->member_type);
                $getSafraPoin = $modelBonusSetting->getSafraPoin();
                $bonus_price = $getSetting->team_price * $pasangan;
                $safra_price = $getSafraPoin/100 * $bonus_price;
                $real_bonus_price = $bonus_price - $safra_price;
                $date = date('Y-m-d');
                $dataArray[] = array(
                    'id' => $row->id,
                    'kiri' => $pasangan,
                    'kanan' => $pasangan,
                    'binary_date' => $date
                );
                $dataInsertBonus = array(
                    'user_id' => $row->id,
                    'type' => 2,
                    'bonus_price' => $real_bonus_price,
                    'bonus_date' => $date,
                    'poin_type' => 1,
                    'total_binary' => $pasangan,
                    'total_pin' => $pasangan
                );
                $modelBonus->getInsertBonusMember($dataInsertBonus);
                $dataInsertSafra = array(
                    'user_id' => $row->id,
                    'type' => 2,
                    'bonus_price' => $safra_price,
                    'bonus_date' => date('Y-m-d'),
                    'poin_type' => 2,
                    'total_binary' => $pasangan,
                    'total_pin' => $pasangan
                );
                $modelBonus->getInsertBonusMember($dataInsertSafra);
                $dataInsert = array(
                    'user_id' => $row->id,
                    'total_left' => $pasangan,
                    'total_right' => $pasangan,
                    'binary_date' => $date
                );
                $modelBinaryHistory->getInsertBinaryHistory($dataInsert);
            }
        }
        dd(count($dataArray).' Member yang mendapatkan bonus binary');
    }
    
    
    
    
}
