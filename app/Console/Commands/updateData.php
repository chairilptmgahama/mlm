<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Model\Pin;
use App\Model\Masterpin;

class updateData extends Command {

    protected $signature = 'update_data';
    protected $description = 'Cron Update Data';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $modelPin = New Pin;
        $modelMasterpin = New Masterpin;
        $dataInsertCoinMember1 = array(
            'user_id' => 4,
            'transaction_id' => 5,
            'type' => 1,
            'qty' => 10000,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataInsertCoinMember1);
        $dataInsertCoinKurang1 = array(
            'total_coin' => 10000,
            'type' => 2,
            'transaction_id' => 5
        );
        $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang1);
        
        $dataInsertCoinMember2 = array(
            'user_id' => 4,
            'transaction_id' => 18,
            'type' => 1,
            'qty' => 10000,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataInsertCoinMember2);
        $dataInsertCoinKurang2 = array(
            'total_coin' => 10000,
            'type' => 2,
            'transaction_id' => 18
        );
        $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang2);
        
        $dataInsertCoinMember3 = array(
            'user_id' => 4,
            'transaction_id' => 19,
            'type' => 1,
            'qty' => 15000,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataInsertCoinMember3);
        $dataInsertCoinKurang3 = array(
            'total_coin' => 15000,
            'type' => 2,
            'transaction_id' => 19
        );
        $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang3);
        
        $dataInsertCoinMember4 = array(
            'user_id' => 4,
            'transaction_id' => 27,
            'type' => 1,
            'qty' => 6000,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataInsertCoinMember4);
        $dataInsertCoinKurang4 = array(
            'total_coin' => 6000,
            'type' => 2,
            'transaction_id' => 27
        );
        $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang4);
        
        dd('Done Update');
    }
    
    
    
    
}
