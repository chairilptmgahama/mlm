<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Model\Member;
use App\Model\Pinsetting;
use App\Model\Package;
use App\Model\Memberpackage;
use App\Model\Transaction;
use App\Model\Pin;
use App\Model\Bonussetting;
use App\Model\Bonus;
use App\Model\Bank;
use App\Model\Pengiriman;
use App\Model\Membership;
use App\Model\Placementhistory;
use App\Model\Binaryhistory;
use App\Model\News;
use App\Model\Sale;
use App\Model\Masterpin;

class MemberController extends Controller {

    public function __construct() {
        
    }
    
    public function getMyProfile() {
//        dd(env('MAIL_HOST'));
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_profile == 0) {
            return redirect()->route('m_newProfile')
                            ->with('message', 'Profil data diri belum ada, silakan isi data profil anda')
                            ->with('messageclass', 'danger');
        }
        return view('member.profile.my-profile')
                        ->with('headerTitle', 'Profil Saya')
                        ->with('dataUser', $dataUser);
    }

    public function getAddMyProfile() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_profile == 1) {
            return redirect()->route('m_myProfile')
                            ->with('message', 'Tidak bisa buat profil')
                            ->with('messageclass', 'danger');
        }
        return view('member.profile.add-profile')
                        ->with('headerTitle', 'Profile')
                        ->with('dataUser', $dataUser);
    }

    public function postAddMyProfile(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $date =  date('Y-m-d H:i:s');
        $dataUpdate = array(
            'full_name' => $request->full_name,
            'gender' => $request->gender,
            'kecamatan' => $request->kecamatan,
            'alamat' => $request->alamat,
            'provinsi' => $request->provinsi,
            'kota' => $request->kota,
            'is_profile' => 1,
            'profile_created_at' => $date,
            'wallet' => $request->wallet,
            'is_wallet' => 1,
            'wallet_at' =>  $date,
        );
        $modelMember = New Member;
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdate);
        return redirect()->route('m_myProfile')
                        ->with('message', 'Profil data diri berhasil dibuat')
                        ->with('messageclass', 'success');
    }

    public function getAddSponsor() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('m_newCoin')
                    ->with('message', 'akun anda belum aktif, silakan beli coin')
                    ->with('messageclass', 'error');
        }
        return view('member.sponsor.add-sponsor')
                        ->with('dataUser', $dataUser);
    }

    public function postAddSponsor(Request $request) {
        $dataUser = Auth::user();
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $sponsor_id = $dataUser->id;
        $upline_detail = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $upline_detail = '[' . $dataUser->id . ']';
        }
        $modelBonusSetting = New Bonussetting;
        $settingBonus = $modelBonusSetting->getActiveBonusSetting();
        //cek total sponsor
        $total_sponsor = $dataUser->total_sponsor + 1;
        $is_five = 1;
        if($total_sponsor > $settingBonus->max_user_profit){ //max user mendapatkan bonus profit posting dibuat dinamis di admin
            $is_five = 0;
        }
        $dataInsertNewMember = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'hp' => $request->hp,
            'user_code' => $request->user_code,
            'member_type' => 3,
            'sponsor_id' => $sponsor_id,
            'upline_id' => $sponsor_id,
            'upline_detail' => $upline_detail,
            'is_five' => $is_five
        );
        $getNew = $modelMember->getInsertUsers($dataInsertNewMember);

        $dataActivation = array(
            'user_id' => $getNew->lastID,
            'sponsor_id' => $sponsor_id,
        );
        $modelMember->getInsertActivationUser($dataActivation);
        
        //Update tambah total sponsor
        $dataUpdateSponsor = array(
            'total_sponsor' => $total_sponsor,
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdateSponsor);

        $dataEmail = array(
            'name' => $request->name,
            'password' => $request->password,
            'hp' => $request->hp,
            'user_code' => $request->user_code,
            'sponsor_name' => $dataUser->name
        );
        $emailSend = $request->email;
        Mail::send('member.email.email', $dataEmail, function($message) use($emailSend) {
            $message->to($emailSend, 'Registrasi Xonecoin')
                    ->subject('Selamat bergabung di Xonecoin');
        });
        return redirect()->route('m_mySponsor')
                        ->with('message', 'Member baru berhasil dibuat. Silakan cek email')
                        ->with('messageclass', 'success');
    }

    public function getMySponsor() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        $modelMember = New Member;
        $getAllSponsor = $modelMember->getAllDownlineSponsor($dataUser);
        return view('member.sponsor.status-sponsor')
                        ->with('getData', $getAllSponsor)
                        ->with('dataUser', $dataUser);
    }

    public function getAddPackage() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id != null) {
            return redirect()->route('mainDashboard');
        }
        $modePackage = New Package;
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        $getAllPackage = $modePackage->getAllPackage();
        return view('member.package.add-package')
                        ->with('headerTitle', 'Package')
                        ->with('allPackage', $getAllPackage)
                        ->with('pinSetting', $getActivePinSetting)
                        ->with('dataUser', $dataUser);
    }

    public function postAddPackage(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id != null) {
            return redirect()->route('mainDashboard');
        }
        $modelMemberPackage = New Memberpackage;
        $modelPackage = New Package;
        $getDetailPackage = $modelPackage->getPackageId($request->id_paket);
        $dataInsert = array(
            'user_id' => $dataUser->sponsor_id,
            'request_user_id' => $dataUser->id,
            'package_id' => $request->id_paket,
            'name' => $getDetailPackage->name,
            'short_desc' => $getDetailPackage->short_desc,
            'total_pin' => $getDetailPackage->pin,
        );
        $modelMemberPackage->getInsertMemberPackage($dataInsert);
        $dataUpdatePackageId = array(
            'package_id' => $getDetailPackage->id,
            'package_id_at' => date('Y-m-d H:i:s')
        );
        $modelMember = New Member;
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdatePackageId);
        return redirect()->route('mainDashboard')
                        ->with('message', 'Order Paket berhasil')
                        ->with('messageclass', 'success');
    }

    public function getListOrderPackage() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelMemberPackage = New Memberpackage;
        $getCheckNewOrder = $modelMemberPackage->getMemberPackageInactive($dataUser);
        if (count($getCheckNewOrder) == 0) {
            return redirect()->route('mainDashboard')
                            ->with('message', 'No one member order package')
                            ->with('messageclass', 'danger');
        }
        return view('member.package.order-list-package')
                        ->with('headerTitle', 'Package')
                        ->with('allPackage', $getCheckNewOrder)
                        ->with('dataUser', $dataUser);
    }

    public function getDetailOrderPackage($paket_id) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelMemberPackage = New Memberpackage;
        $getData = $modelMemberPackage->getDetailMemberPackageInactive($paket_id, $dataUser);
        if ($getData == null) {
            return redirect()->route('mainDashboard');
        }
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        return view('member.package.order-detail-package')
                        ->with('headerTitle', 'Package')
                        ->with('getData', $getData)
                        ->with('pinSetting', $getActivePinSetting)
                        ->with('dataUser', $dataUser);
    }

    public function postActivatePackage(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelMemberPackage = New Memberpackage;
        $modelPin = new Pin;
        $getData = $modelMemberPackage->getDetailMemberPackageInactive($request->id_paket, $dataUser);
        $getTotalPin = $modelPin->getTotalPinMember($dataUser);
        $getMylastPin = $modelPin->getMyLastPin($dataUser);
        $sisaPin = $getTotalPin->sum_pin_masuk - $getTotalPin->sum_pin_keluar;
        if ($sisaPin < $getData->total_pin) {
            return redirect()->route('m_detailOrderPackage', $getData->id)
                            ->with('message', 'Paket tidak cukup untuk mengaktifasi sponsor baru, silakan beli pin')
                            ->with('messageclass', 'danger');
        }
        $code = sprintf("%03s", $getData->total_pin);
        $memberPin = array(
            'user_id' => $dataUser->id,
            'total_pin' => $getData->total_pin,
            'setting_pin' => $getMylastPin->setting_pin,
            'pin_code' => $getMylastPin->pin_code . $code . $getData->request_user_id,
            'is_used' => 1,
            'used_at' => date('Y-m-d H:i:s'),
            'used_user_id' => $getData->request_user_id,
            'pin_status' => 1,
        );
        $modelPin->getInsertMemberPin($memberPin);
        $dataUpdate = array(
            'status' => 1
        );
        $modelMemberPackage->getUpdateMemberPackage('id', $getData->id, $dataUpdate);
        $modelBonusSetting = new Bonussetting;
        $modelBonus = new Bonus;
        $getBonusStart = $modelBonusSetting->getActiveBonusStart();
        $bonus_price = $getBonusStart->start_price * $getData->total_pin;
        $getSafraPoin = $modelBonusSetting->getSafraPoin();
        $safra_price = $getSafraPoin / 100 * $bonus_price;
        $real_bonus_price = $bonus_price - $safra_price;
        $dataInsertBonus = array(
            'user_id' => $dataUser->id,
            'from_user_id' => $getData->request_user_id,
            'type' => 1,
            'bonus_price' => $real_bonus_price,
            'bonus_date' => date('Y-m-d'),
            'poin_type' => 1,
            'total_pin' => $getData->total_pin
        );
        $modelBonus->getInsertBonusMember($dataInsertBonus);
        $dataInsertSafra = array(
            'user_id' => $dataUser->id,
            'from_user_id' => $getData->request_user_id,
            'type' => 1,
            'bonus_price' => $safra_price,
            'bonus_date' => date('Y-m-d'),
            'poin_type' => 2,
            'total_pin' => $getData->total_pin
        );
        $modelBonus->getInsertBonusMember($dataInsertSafra);
        $dataUpdateIsActive = array(
            'is_active' => 1,
            'active_at' => date('Y-m-d H:i:s'),
            'member_type' => $getData->package_id
        );
        $modelMember = New Member;
        $modelMember->getUpdateUsers('id', $getData->request_user_id, $dataUpdateIsActive);
        $total_sponsor = $dataUser->total_sponsor + 1;
        $dataUpdateSponsor = array(
            'total_sponsor' => $total_sponsor,
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdateSponsor);
        return redirect()->route('mainDashboard')
                        ->with('message', 'Berhasil mengaktifasi sponsor baru')
                        ->with('messageclass', 'success');
    }

    // Transaksi Beli Coin/Pin
    public function getAddCoin() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
//        if ($dataUser->is_bank == 0) {
//            return redirect()->route('m_myBank')
//                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
//                            ->with('messageclass', 'error');
//        }
//        if ($dataUser->pin_code == null) {
//            return redirect()->route('m_addCodePin')
//                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
//                            ->with('messageclass', 'error');
//        }
        $modelPinsetting = New Pinsetting;
        $modelMember = New Member;
        $dataSettingCoin = null;
        
        if ($dataUser->member_type == 1) {
            $dataSettingCoin = $modelPinsetting->getActiveCoinSetting(1);
            $getSeller = (object) array(
                'id' => 1
            );
        }

        if ($dataUser->member_type == 3) {
            $dataSettingCoin = $modelPinsetting->getActiveCoinSetting(2);
            $rm = str_replace('[', '', $dataUser->upline_detail);
            $clean1 = str_replace(']', '', $rm);
            $arrayUplineId = explode(',', $clean1);
            $getSeller = $modelMember->getSellerIdfromUpline($arrayUplineId, 1);
        }
        
        if($dataSettingCoin == null){
            return redirect()->route('mainDashboard')
                    ->with('message', 'something went wrong...')
                    ->with('messageclass', 'error');
        }
        return view('member.pin.order-pin')
                        ->with('headerTitle', 'Order Pin')
                        ->with('getData', $dataSettingCoin)
                        ->with('sellerID', $getSeller->id)
                        ->with('dataUser', $dataUser);
    }

    public function postAddCoin(Request $request) {
        $dataUser = Auth::user();
        $modelPinsetting = New Pinsetting;
        $modeTransaction = New Transaction;
        $dataSetting = $modelPinsetting->getActiveCoinSettingId($request->setting_id);
        $harga = $dataSetting->buy_price * $request->total_coin;
        $fee = $harga * $dataSetting->buy_fee / 100;
        $code = $modeTransaction->getCodeTransaction(1);
        $rand = rand(111, 298);
        $dataTransaksi = array(
            'seller_id' => $request->seller_id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'type' => 1,
            'total_coin' => $request->total_coin,
            'price' => $harga,
            'admin_fee' => $fee,
            'unique_digit' => $rand,
            'coin_setting_id' => $dataSetting->id
        );
        //insert ke table transaksi
        $getId = $modeTransaction->getInsertTransaction($dataTransaksi);
        return redirect()->route('m_buyerTransactionID', [$getId->lastID])
                        ->with('message', 'Order Pin berhasil, silakan lakukan proses transfer')
                        ->with('messageclass', 'success');
    }

    public function getListBuyerTransactions() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        $modelSettingTrans = New Transaction;
        $getAllTransaction = $modelSettingTrans->getTransactionsBuyer($dataUser);
        return view('member.pin.list-buyer_transaction')
                        ->with('getData', $getAllTransaction)
                        ->with('dataUser', $dataUser);
    }

    public function getBuyerTransactionID($id) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        $modelTrans = New Transaction;
        $modelBank = New Bank;
        if ($dataUser->member_type == 1) {
            $getTrans = $modelTrans->getDetailTransactionsMember($id, $dataUser);
            $getSellerBank = $modelBank->getBankPerusahaan();
        }
        if ($dataUser->member_type == 3) {
            $getTrans = $modelTrans->getDetailTransactionsBuyer($id, $dataUser);
            $getSellerBank = $modelBank->getNewBankMemberActive($getTrans->seller_id);
        }
        if ($getTrans == null) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'tidak ada data')
                    ->with('messageclass', 'error');
        }
        return view('member.pin.buyer-detail-transaction')
                        ->with('headerTitle', 'Transaction')
                        ->with('bankSeller', $getSellerBank)
                        ->with('getData', $getTrans)
                        ->with('dataUser', $dataUser);
        //history id_type member peringkat
    }

    public function postAddBuyerTransaction(Request $request) {
        $dataUser = Auth::user();
        $modelTrans = New Transaction;
        $id_trans = $request->id_trans;
//        if($request->pin_code == null) {
//            return redirect()->route('m_buyerTransactionID', array($id_trans))
//                            ->with('message', 'Anda tidak mengisi kode pin')
//                            ->with('messageclass', 'error');
//        }
//        if($dataUser->pin_code != $request->pin_code){
//            return redirect()->route('m_buyerTransactionID', array($id_trans))
//                            ->with('message', 'Kode pin tidak sama')
//                            ->with('messageclass', 'error');
//        }
        if ($dataUser->member_type == 1) {
            $getTrans = $modelTrans->getDetailTransactionsMember($id_trans, $dataUser);
        }
        if ($dataUser->member_type == 3) {
            $getTrans = $modelTrans->getDetailTransactionsBuyer($id_trans, $dataUser);
        }
        if ($getTrans == null) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'tidak ada data')
                    ->with('messageclass', 'error');
        }
        $dataUpdate = array(
            'status' => 1,
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'account_name' => $request->account_name,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $modelTrans->getUpdateTransaction('id', $id_trans, $dataUpdate);
        return redirect()->route('m_buyerTransactionID', array($id_trans))
                        ->with('message', 'Konfirmasi transfer berhasil')
                        ->with('messageclass', 'success');
    }

    public function postRejectBuyerTransaction(Request $request) {
        if ($request->reason == null) {
            return redirect()->route('m_buyerTransactionID', [$request->id_trans])
                            ->with('message', 'Alasan harus diisi')
                            ->with('messageclass', 'error');
        }
        $modeTransaction = New Transaction;
        $dataUpdate = array(
            'status' => 3,
            'deleted_at' => date('Y-m-d H:i:s'),
            'reason' => $request->reason
        );
        $modeTransaction->getUpdateTransaction('id', $request->id_trans, $dataUpdate);
        return redirect()->route('m_buyerTransactionID', [$request->id_trans])
                        ->with('message', 'Transaksi dibatalkan')
                        ->with('messageclass', 'success');
    }

    public function getListSellerTransactions() {
        $dataUser = Auth::user();
        $onlyExchanger = array(1, 2);
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if (!in_array($dataUser->member_type, $onlyExchanger)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        $modelSettingTrans = New Transaction;
        $getAllTransaction = $modelSettingTrans->getTransactionsSeller($dataUser);
        return view('member.pin.list-seller_transaction')
                        ->with('getData', $getAllTransaction)
                        ->with('dataUser', $dataUser);
    }

    public function getSellerTransactionID($id) {
        $dataUser = Auth::user();
        $onlyStockist = array(1);
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if (!in_array($dataUser->member_type, $onlyStockist)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        $modelTrans = New Transaction;
        $getTrans = $modelTrans->getDetailTransactionsSeller($id, $dataUser);
        if ($getTrans == null) {
            return redirect()->route('mainDashboard');
        }
        return view('member.pin.seller-detail-transaction')
                        ->with('headerTitle', 'Transaction')
                        ->with('getData', $getTrans)
                        ->with('dataUser', $dataUser);
    }

    public function postAddSellerTransaction(Request $request) {
        $dataUser = Auth::user();
        $modelTrans = New Transaction;
        $modelPin = New Pin;
        $modelMember = New Member;
        $id_trans = $request->id_trans;
//        if($request->pin_code == null) {
//            return redirect()->route('m_sellerTransactionID', array($id_trans))
//                            ->with('message', 'Anda tidak mengisi kode pin')
//                            ->with('messageclass', 'error');
//        }
//        if($dataUser->pin_code != $request->pin_code){
//            return redirect()->route('m_sellerTransactionID', array($id_trans))
//                            ->with('message', 'Kode pin tidak sama')
//                            ->with('messageclass', 'error');
//        }
        $getData = $modelTrans->getDetailTransactionsSeller($id_trans, $dataUser);
        if ($getData == null) {
            return redirect()->route('m_listSellerTransactions')
                            ->with('message', 'Tidak ada data')
                            ->with('messageclass', 'danger');
        }
        
        if($getData->type == 1){
            //coin member bertambah
            $dataCoinMemberTambah = array(
                'user_id' => $getData->user_id,
                'transaction_id' => $getData->id,
                'type' => 1,
                'qty' => $getData->total_coin,
                'price' => $getData->price
            );
            $modelPin->getInsertMemberCoin($dataCoinMemberTambah);

            //coin exchanger berkurang
            $dataCoinExchangerKurang = array(
                'user_id' => $dataUser->id,
                'transaction_id' => $getData->id,
                'type' => 2,
                'qty' => $getData->total_coin,
                'price' => $getData->price
            );
            $modelPin->getInsertMemberCoin($dataCoinExchangerKurang);

            //member active
            $cekPembeli = $modelMember->getUsers('id', $getData->user_id);
            if($cekPembeli->is_active == 0){
                $memberActive = array(
                    'is_active' => 1,
                    'active_at' => date('Y-m-d H:i:s'),
                );
                $modelMember->getUpdateUsers('id', $getData->user_id, $memberActive);
            }
            if($cekPembeli->sponsor_id != null){
                $modelBonus = New Bonus;
                $modelBonusSetting = New Bonussetting;
                $modelMasterCoin = New Masterpin;
                $settingBonus = $modelBonusSetting->getActiveBonusSetting();
                $bonus_coin = $getData->total_coin * $settingBonus->bonus_sponsor / 100;
                $bonusSp = array(
                    'user_id' => $cekPembeli->sponsor_id,
                    'from_user_id' => $cekPembeli->id,
                    'type' => 1,
                    'bonus' => $bonus_coin,
                    'bonus_date' => date('Y-m-d'),
                    'setting_id' => $settingBonus->id
                );
                $modelBonus->getInsertBonusMember($bonusSp);
                
                $code = $modelTrans->getCodeTransaction(7);
                $dataTransaksiBonusSp = array(
                    'seller_id' => $cekPembeli->sponsor_id,
                    'user_id' => $cekPembeli->sponsor_id,
                    'transaction_code' => $code,
                    'type' => 7,
                    'total_coin' => $bonus_coin,
                    'price' => 0,
                    'admin_fee' => 0,
                    'unique_digit' => 0,
                    'coin_setting_id' => $getData->coin_setting_id
                );
                $getIdBonusSp = $modelTrans->getInsertTransaction($dataTransaksiBonusSp);
                
                //coin member bertambah
                $dataCoinSponsorTambah = array(
                    'user_id' => $cekPembeli->sponsor_id,
                    'transaction_id' => $getIdBonusSp->lastID,
                    'type' => 1,
                    'qty' => $bonus_coin,
                    'price' => 0,
                    'bonus_type' => 1
                );
                $modelPin->getInsertMemberCoin($dataCoinSponsorTambah);

                //saldo master terkeruk
                $dataInsertMasterCoin = array(
                    'total_coin' => $bonus_coin,
                    'type' => 4,
                    'transaction_id' => $getIdBonusSp->lastID
                );
                $modelMasterCoin->getInsertMasterCoin($dataInsertMasterCoin);
            }
        }
        
        if($getData->type == 3){
            //coin member bertambah
            $dataCoinMemberTambah = array(
                'user_id' => $dataUser->id,
                'transaction_id' => $getData->id,
                'type' => 1,
                'qty' => $getData->total_coin,
                'price' => ($getData->price - $getData->admin_fee)
            );
            $modelPin->getInsertMemberCoin($dataCoinMemberTambah);

            //coin exchanger berkurang
            $dataCoinExchangerKurang = array(
                'user_id' => $getData->user_id,
                'transaction_id' => $getData->id,
                'type' => 2,
                'qty' => $getData->total_coin,
                'price' => ($getData->price - $getData->admin_fee)
            );
            $modelPin->getInsertMemberCoin($dataCoinExchangerKurang);
        }
        
        $dataUpdate = array(
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $modelTrans->getUpdateTransaction('id', $id_trans, $dataUpdate);
       
        return redirect()->route('m_sellerTransactionID', [$getData->id])
                        ->with('message', 'Konfirmasi transfer berhasil')
                        ->with('messageclass', 'success');
    }

    public function postRejectSellerTransaction(Request $request) {
        $dataUser = Auth::user();
        if ($request->reason == null) {
            return redirect()->route('m_sellerTransactionID', [$request->id_trans])
                            ->with('message', 'Alasan harus diisi')
                            ->with('messageclass', 'danger');
        }
        $modeTransaction = New Transaction;
        $getData = $modeTransaction->getDetailTransactionsSeller($request->id_trans, $dataUser);
        if ($getData == null) {
            return redirect()->route('m_listSellerTransactions')
                            ->with('message', 'Tidak ada data')
                            ->with('messageclass', 'danger');
        }
        $dataUpdate = array(
            'status' => 3,
            'deleted_at' => date('Y-m-d H:i:s'),
            'reason' => $request->reason
        );
        $modeTransaction->getUpdateTransaction('id', $request->id_trans, $dataUpdate);
        return redirect()->route('m_sellerTransactionID', [$request->id_trans])
                        ->with('message', 'Transaksi dibatalkan')
                        ->with('messageclass', 'success');
    }
    
    //Member Jual Coin Ke Exchanger
    public function getSellCoin(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->is_bank == 0) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'error');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'error');
        }
        $modelPinsetting = New Pinsetting;
        $modelMember = New Member;
        $modelCoin = New Pin;
        $modelTransaction = New Transaction;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        if($cekSaldo < 10){
            return redirect()->route('mainDashboard')
                    ->with('message', 'saldo anda tidak cukup untuk melakukan transaksi jual coin')
                    ->with('messageclass', 'error');
        }
        
        //khusus exchanger
        if ($dataUser->member_type == 1) {
//            return redirect()->route('mainDashboard')
//                    ->with('message', 'forbiden area')
//                    ->with('messageclass', 'error');
            $dataSettingCoin = $modelPinsetting->getActiveCoinSetting(1);
            $getExchanger = (object) array(
                'id' => 1
            );
        }
        
        //khusus member
        if ($dataUser->member_type == 3) {
            $dataSettingCoin = $modelPinsetting->getActiveCoinSetting(2);
            $rm = str_replace('[', '', $dataUser->upline_detail);
            $clean1 = str_replace(']', '', $rm);
            $arrayUplineId = explode(',', $clean1);
            $getExchanger = $modelMember->getSellerIdfromUpline($arrayUplineId, 1);
        }
        if($dataSettingCoin == null){
            return redirect()->route('mainDashboard')
                    ->with('message', 'something went wrong...')
                    ->with('messageclass', 'error');
        }
        return view('member.pin.jual-pin')
                        ->with('headerTitle', 'Jual Pin')
                        ->with('getData', $dataSettingCoin)
                        ->with('sellerID', $getExchanger->id)
                        ->with('sisa', $cekSaldo)
                        ->with('dataUser', $dataUser);
    }
    
    public function postSellCoin(Request $request){
        $dataUser = Auth::user();
        if($request->pin_code == null) {
            return redirect()->route('m_sellCoin')
                            ->with('message', 'Anda tidak mengisi kode pin')
                            ->with('messageclass', 'error');
        }
        if($dataUser->pin_code != $request->pin_code){
            return redirect()->route('m_sellCoin')
                            ->with('message', 'Kode pin tidak sama')
                            ->with('messageclass', 'error');
        }
        $modelPinsetting = New Pinsetting;
        $modeTransaction = New Transaction;
        $dataSetting = $modelPinsetting->getActiveCoinSettingId($request->setting_id);
        $harga = $dataSetting->sell_price * $request->total_coin;
        $admin_fee = $harga * $dataSetting->sell_fee / 100;
        $code = $modeTransaction->getCodeTransaction(3);
        $modelBank = New Bank;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        $rand = 0;
        $dataTransaksi = array(
            'seller_id' => $request->exchanger_id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'status' => 1,
            'type' => 3,
            'total_coin' => $request->total_coin,
            'admin_fee' => $admin_fee,
            'price' => $harga,
            'unique_digit' => $rand,
            'coin_setting_id' => $dataSetting->id,
            'bank_name' => $cekBank->bank_name,
            'account_no' => $cekBank->account_no,
            'account_name' => $cekBank->account_name,
        );
        //insert ke table transaksi
        $getId = $modeTransaction->getInsertTransaction($dataTransaksi);
        return redirect()->route('m_buyerTransactionID', [$getId->lastID])
                        ->with('message', 'Proses jual coin berhasil, tunggu konfirmasi exchanger')
                        ->with('messageclass', 'success');
    }

    public function getMyPinStock() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modePackage = New Package;
        $modelTrans = New Transaction;
        $data = $modePackage->getStockistPackage($dataUser->id);
        $getData = array();
        if ($data != null) {
            foreach ($data as $row) {
//                $jml_keluar = $modePackage->getSumStock($dataUser->id, $row->id);
                if($dataUser->member_type == 3){
                    $cekPnAktivasi = $modePackage->getMemberPinKeluarAktivasiPackageId($dataUser->id, $row->id);
                    $total_sisa = $row->total_qty - $cekPnAktivasi->total_keluar;
                }
                if($dataUser->member_type == 2){
                    $cekTransOut = $modelTrans->getStockistTransOutPackage($dataUser->id, $row->id);
                    $jml_keluar = $modePackage->getCekStockistPinKeluarAktivasiPackage($dataUser->id, $row->id);
                    $total_sisa = $row->total_qty - $cekTransOut->total_keluar - $jml_keluar->total_keluar;
                }
                if($dataUser->member_type == 1){
                    $cekPinOut = $modelTrans->getStockistTransOutPackage($dataUser->id, $row->id);
                    $total_sisa = $row->total_qty - $cekPinOut->total_keluar;
                }
                if ($total_sisa < 0) {
                    $total_sisa = 0;
                }
                $getData[] = (object) array(
                            'qty' => $row->total_qty,
                            'name' => $row->name,
                            'code' => $row->code,
                            'image' => $row->image,
                            'id' => $row->id,
                            'total_sisa' => $total_sisa,
                );
            }
        }
        return view('member.pin.pin-stock')
                        ->with('getData', $getData)
                        ->with('dataUser', $dataUser);
    }

    public function getMyPinHistory() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modePackage = New Package;
        $dataMasuk = $modePackage->getHistoryPinMasuk($dataUser->id);
        $dataKeluarAktifasi = null;
        if($dataUser->member_type == 3){
            $dataKeluarAktifasi = $modePackage->getHistoryPinKeluar($dataUser->id);
        }
        if($dataUser->member_type == 2){
            $dataKeluarAktifasi = $modePackage->getHistoryPinKeluarStockist($dataUser->id);
        }
        $dataKeluarPenjualan = $modePackage->getHistoryPinKeluarPenjualan($dataUser->id);
        return view('member.pin.pin-history')
                        ->with('dataMasuk', $dataMasuk)
                        ->with('dataKeluarAktifasi', $dataKeluarAktifasi)
                        ->with('dataKeluarPenjualan', $dataKeluarPenjualan)
                        ->with('dataUser', $dataUser);
    }

    public function getMyKirimPaket() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modelPengiriman = new Pengiriman;
        $modelPin = new Pin;
        $getTotalPin = $modelPin->getTotalPinMember2($dataUser);
        $getAllPengiriman = $modelPengiriman->getMyPengiriman($dataUser);
        $getTotalPinTerkirimDanTuntas = $modelPengiriman->getCekPinPengirimanPaketAll($dataUser);
        return view('member.pin.pengiriman')
                        ->with('getData', $getAllPengiriman)
                        ->with('dataPin', $getTotalPin)
                        ->with('dataPengiriman', $getTotalPinTerkirimDanTuntas)
                        ->with('dataUser', $dataUser);
    }

    public function postAddKirimPaket(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $dataInsert = array(
            'user_id' => $dataUser->id,
            'total_pin' => $request->total_pin,
            'alamat_kirim' => $request->alamat_kirim,
            'day_cream' => $request->day_cream,
            'night_cream' => $request->night_cream,
            'face_toner' => $request->face_toner,
            'facial_wash' => $request->facial_wash,
        );
        $modelPengiriman = new Pengiriman;
        $modelPengiriman->getInsertPengiriman($dataInsert);
        return redirect()->route('m_myKirimPaket')
                        ->with('message', 'Pengiriman paket berhasil ditambahkan')
                        ->with('messageclass', 'success');
    }

    public function getMyBinary(Request $request) {
        $dataUser = Auth::user();
        $sessionUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->upline_id == null) {
            if ($dataUser->id > 4) {
                return redirect()->route('mainDashboard');
            }
        }
        $modelMember = New Member;
        $modelBinaryHistory = new Binaryhistory;
        $back = false;
        $downline = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $downline = '[' . $dataUser->id . ']';
        }
        if ($request->get_id != null) {
            if ($request->get_id != $dataUser->id) {
                $back = true;
                $dataUser = $modelMember->getCekIdDownline($request->get_id, $downline);
            }
        }
        if ($dataUser == null) {
            return redirect()->route('mainDashboard');
        }
        $getBinary = $modelMember->getBinary($dataUser);
        $getKaKi = array();
        foreach ($getBinary as $row) {
            $kiri = null;
            $kanan = null;
            $kiriCheck = null;
            $kananCheck = null;
            if ($row != null) {
                $kanan = 0;
                if ($row->kanan_id != null) {
                    $downlineKanan = $row->upline_detail . ',[' . $row->id . ']' . ',[' . $row->kanan_id . ']';
                    if ($row->upline_detail == null) {
                        $downlineKanan = '[' . $row->id . ']' . ',[' . $row->kanan_id . ']';
                    }
                    $kananLuar = $modelMember->getNewCountMyDownline($downlineKanan, $row->id);
                    $kananPas = $modelMember->getKaKiTerPendek($row->kanan_id, $row->id);
                    $kanan = $kananLuar + $kananPas;
                }
                $kiri = 0;
                if ($row->kiri_id != null) {
                    $downlineKiri = $row->upline_detail . ',[' . $row->id . ']' . ',[' . $row->kiri_id . ']';
                    if ($row->upline_detail == null) {
                        $downlineKiri = '[' . $row->id . ']' . ',[' . $row->kiri_id . ']';
                    }
                    $kiriLuar = $modelMember->getNewCountMyDownline($downlineKiri, $row->id);
                    $kiriPas = $modelMember->getKaKiTerPendek($row->kiri_id, $row->id);
                    $kiri = $kiriLuar + $kiriPas;
                }
                $getHistoryBinary = $modelBinaryHistory->getBinaryHistory($row->id);
                $kiriCheck = $kiri - $getHistoryBinary->sum_total_kiri;
                $kananCheck = $kanan - $getHistoryBinary->sum_total_kanan;
            }
            $getKaKi[] = array(
                'data' => $row,
                'kiri' => $kiri,
                'kanan' => $kanan,
                'sisaKiri' => $kiriCheck,
                'sisaKanan' => $kananCheck,
            );
        }
//        dd($getKaKi);
        return view('member.networking.binary')
//                        ->with('getData', $getBinary)
                        ->with('getData', $getKaKi)
                        ->with('back', $back)
                        ->with('dataUser', $dataUser)
                        ->with('sessionUser', $sessionUser);
    }

    //Bank
    public function getMyBank() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelBank = new Bank;
        $getAllMyBank = $modelBank->getBankMember($dataUser);
        return view('member.profile.bank')
                        ->with('getData', $getAllMyBank)
                        ->with('dataUser', $dataUser);
    }

    public function postAddBank(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $modelMember = New Member;
        $date = date('Y-m-d H:i:s');
        $dateUpdate = array(
            'is_active' => 0,
            'deleted_at' => $date
        );
        $modelBank->getUpdateBank('user_id', $dataUser->id, $dateUpdate);
        $dataInsert = array(
            'user_id' => $dataUser->id,
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'account_name' => $request->account_name,
            'active_at' => $date
        );
        $modelBank->getInsertBank($dataInsert);
        if($dataUser->is_bank == 0){
            $memberBankActive = array(
                'is_bank' => 1,
            );
            $modelMember->getUpdateUsers('id', $dataUser->id, $memberBankActive);
        }
        return redirect()->route('m_myBank')
                        ->with('message', 'Bank berhasil ditambahkan')
                        ->with('messageclass', 'success');
    }

    public function postActivateBank(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $date = date('Y-m-d H:i:s');
        $dateUpdate = array(
            'is_active' => 0,
            'deleted_at' => $date
        );
        $modelBank->getUpdateBank('user_id', $dataUser->id, $dateUpdate);
        $dataActivateBank = array(
            'is_active' => 1,
        );
        $modelBank->getUpdateBank('id', $request->id_bank, $dataActivateBank);
        return redirect()->route('m_myBank')
                        ->with('message', 'salah satu bank berhasil di aktifkan')
                        ->with('messageclass', 'success');
    }

    public function getTransferCoin() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 1) {
            return redirect()->route('mainDashboard')
                        ->with('message', 'forbiden area')
                        ->with('messageclass', 'error');
        }
        if ($dataUser->is_bank == 0) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'error');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'error');
        }
        //cek saldo coin
        $modelCoin = New Pin;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        return view('member.pin.transfer-pin')
                        ->with('headerTitle', 'Coin')
                        ->with('cekCoinTersedia', $cekCoinTersedia)
                        ->with('dataUser', $dataUser);
    }

    public function postAddTransferCoin(Request $request) {
        $dataUser = Auth::user();
        $modeTransaction = New Transaction;
        $code = $modeTransaction->getCodeTransaction(2);
        $modelPinsetting = New Pinsetting;
        $modelMember = New Member;
        $modelPin = New Pin;
        $dataSettingCoin = $modelPinsetting->getActiveCoinSetting(1);
        $dataTransaksi = array(
            'seller_id' => $dataUser->id,
            'user_id' => $request->to_id,
            'transaction_code' => $code,
            'type' => 2,
            'total_coin' => $request->qty,
            'price' => 0,
            'unique_digit' => 0,
            'coin_setting_id' => $dataSettingCoin->id,
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $insertTrans = $modeTransaction->getInsertTransaction($dataTransaksi);
        
        $dataCoinMemberTambah = array(
            'user_id' => $request->to_id,
            'transaction_id' => $insertTrans->lastID,
            'type' => 1,
            'qty' => $request->qty,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataCoinMemberTambah);
        
        //coin exchanger berkurang
        $dataCoinExchangerKurang = array(
            'user_id' => $dataUser->id,
            'transaction_id' => $insertTrans->lastID,
            'type' => 2,
            'qty' => $request->qty,
            'price' => 0
        );
        $modelPin->getInsertMemberCoin($dataCoinExchangerKurang);
        
        //member active
        $cekPembeli = $modelMember->getUsers('id', $request->to_id);
        if($cekPembeli->is_active == 0){
            $memberActive = array(
                'is_active' => 1,
                'active_at' => date('Y-m-d H:i:s'),
            );
            $modelMember->getUpdateUsers('id', $request->to_id, $memberActive);
        }
        if($cekPembeli->sponsor_id != null){
            $modelBonus = New Bonus;
            $modelBonusSetting = New Bonussetting;
            $modelMasterCoin = New Masterpin;
            $settingBonus = $modelBonusSetting->getActiveBonusSetting();
            $bonus_coin = $request->qty * $settingBonus->bonus_sponsor / 100;
            $bonusSp = array(
                'user_id' => $cekPembeli->sponsor_id,
                'from_user_id' => $cekPembeli->id,
                'type' => 1,
                'bonus' => $bonus_coin,
                'bonus_date' => date('Y-m-d'),
                'setting_id' => $settingBonus->id
            );
            $modelBonus->getInsertBonusMember($bonusSp);

            //coin member bertambah
            $dataCoinSponsorTambah = array(
                'user_id' => $cekPembeli->sponsor_id,
                'transaction_id' => $insertTrans->lastID,
                'type' => 1,
                'qty' => $bonus_coin,
                'price' => 0,
                'bonus_type' => 1
            );
            $modelPin->getInsertMemberCoin($dataCoinSponsorTambah);

            //saldo master terkeruk
            $dataInsertMasterCoin = array(
                'total_coin' => $bonus_coin,
                'type' => 4,
                'transaction_id' => $insertTrans->lastID
            );
            $modelMasterCoin->getInsertMasterCoin($dataInsertMasterCoin);
        }
        
        return redirect()->route('m_sellerTransactionID', [$insertTrans->lastID])
                        ->with('message', 'berhasil transfer coin')
                        ->with('messageclass', 'success');
    }
    
    public function getListTransferCoin(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type == 1) {
            return redirect()->route('mainDashboard');
        }
        $modelSettingTrans = New Transaction;
        $getAllTransferMasuk = $modelSettingTrans->getTransferPinMasuk($dataUser->id);
        $getAllTransferKeluar = $modelSettingTrans->getTransferPinKeluar($dataUser->id);
        return view('member.pin.list-transferpin')
                        ->with('getAllTransferMasuk', $getAllTransferMasuk)
                        ->with('getAllTransferKeluar', $getAllTransferKeluar)
                        ->with('dataUser', $dataUser);
    }
    
    public function getDetailTransferCoin($id){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type == 1) {
            return redirect()->route('mainDashboard');
        }
        $modelSettingTrans = New Transaction;
        $getData = null;
        $getPackagePin = null;
        if($type == 1){
            $getData = $modelSettingTrans->getTransferPinMasukDetail($id, $dataUser->id);
            if($getData == null){
                return redirect()->route('mainDashboard');
            }
            $getPackagePin = $modelSettingTrans->getDetailTransactionsBuyerPackagePin($getData->id, $dataUser);
        }
        if($type == 2){
            $getData = $getAllTransferKeluar = $modelSettingTrans->getTransferPinKeluarDetail($id, $dataUser->id);
            if($getData == null){
                return redirect()->route('mainDashboard');
            }
            $getPackagePin = $modelSettingTrans->getDetailTransactionsSellerPackagePin($getData->id);
        }
        
        return view('member.pin.detail-transferpin')
                        ->with('getData', $getData)
                        ->with('getPackagePin', $getPackagePin)
                        ->with('type', $type)
                        ->with('dataUser', $dataUser);
    }
    
    
    //Member Posting Coin
    public function getPostingCoin(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard')
                        ->with('message', 'forbiden area')
                        ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('m_newCoin')
                    ->with('message', 'Akun anda belum aktif, silakan beli coin')
                    ->with('messageclass', 'error');
        }
//        if ($dataUser->is_bank == 0) {
//            return redirect()->route('m_myBank')
//                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
//                            ->with('messageclass', 'error');
//        }
//        if ($dataUser->pin_code == null) {
//            return redirect()->route('m_addCodePin')
//                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
//                            ->with('messageclass', 'error');
//        }
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $modelTransaction = New Transaction;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        if($cekSaldo <= 0){
            return redirect()->route('mainDashboard')
                    ->with('message', 'coin anda tidak cukup untuk melakukan posting')
                    ->with('messageclass', 'error');
        }
        //cek Coin yg diposting
        $getSetting = $modelCoinsetting->getActiveCoinPostingSetting();
        if($getSetting == null){
            return redirect()->route('mainDashboard')
                    ->with('message', 'something went wrong...')
                    ->with('messageclass', 'error');
        }
        return view('member.pin.posting-pin')
                ->with('saldo', $cekSaldo)
                ->with('getData', $getSetting)
                ->with('dataUser', $dataUser);
    }
    
    public function postPostingCoin(Request $request){
        $dataUser = Auth::user();
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $modeTransaction = New Transaction;
        $cekSetting = $modelCoinsetting->getIDCoinPostingSetting($request->setting_id);
        if($cekSetting == null){
            return redirect()->route('m_postingCoin')
                    ->with('message', 'Tidak ada data')
                    ->with('messageclass', 'error');
        }
        //rumus disini
        $phaseHari = $modelCoin->getPhaseHari();
        $data = (object) array(
            'hari' => $phaseHari,
            'phase' => $request->phase,
            'persentase' => $cekSetting->persentase
        );
        $arrayBerjalan = $modelCoin->getArrayPhasePersentase($data);
        $json = json_encode($arrayBerjalan);
        
        //transaksi posting. cek biar ada data keluar masuk coin
        $type = 4;
        $code = $modeTransaction->getCodeTransaction($type);
        $dataSettingCoin = $modelCoinsetting->getActiveCoinSetting(2);
        $dataTransaksi = array(
            'seller_id' => $dataUser->id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'type' => $type,
            'total_coin' => $request->total_coin,
            'price' => 0,
            'unique_digit' => 0,
            'coin_setting_id' => $dataSettingCoin->id,
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $insertTrans = $modeTransaction->getInsertTransaction($dataTransaksi);
        
        $dataTransaksiPosting = array(
            'user_id' => $dataUser->id,
            'total_coin' => $request->total_coin,
            'posting_setting_id' => $request->setting_id,
            'transaction_id' => $insertTrans->lastID,
            'phase' => $request->phase,
            'detail' => $json,
        );
        $getId = $modeTransaction->getInsertPostingTranscation($dataTransaksiPosting);
        
        //coin member berkurang
        $dataCoinMemberKurang = array(
            'user_id' => $dataUser->id,
            'transaction_id' => $insertTrans->lastID,
            'type' => 2,
            'qty' => $request->total_coin,
            'price' => 0
        );
        $modelCoin->getInsertMemberCoin($dataCoinMemberKurang);
        
        return redirect()->route('m_postingByID', [$getId->lastID])
                        ->with('message', 'Posting Coin Berhasil, Proses Mining Coin akan berjalan selama '.($request->phase*30).' hari')
                        ->with('messageclass', 'success');
    }
    
    public function getListPosting(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard')
                        ->with('message', 'forbiden area')
                        ->with('messageclass', 'error');
        }
        $modeTransaction = New Transaction;
        $getData = $modeTransaction->getListPosting($dataUser->id);
        return view('member.pin.list-posting')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getPostingByID($id){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard')
                        ->with('message', 'forbiden area')
                        ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'Keanggotaan adan belum aktif')
                    ->with('messageclass', 'error');
        }
        $modeTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $getData = $modeTransaction->getPostingByID($id, $dataUser->id);
        if($getData == null){
            return redirect()->route('mainDashboard')
                            ->with('message', 'tidak ada adata')
                            ->with('messageclass', 'error');
        }
        $dateRange = $modelCoin->getRange24Hours();
        $fase = $modelCoin->getPhaseHari();
        $totalHari = $fase * $getData->phase;
        $dateStart = $getData->created_at;
        $dateNow = date('Y-m-d H:i:s');
        $diff = abs(strtotime($dateNow) - strtotime($dateStart));  
        $years = floor($diff / (365*60*60*24));  
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
        $days = (int) floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24));
        $is_finnish = false;
        if(($days + 1) >=  $totalHari){
            $is_finnish = true;
        }
//        $arrayData = json_decode($getData->detail, true);
        
//        dd($getData);
         //cek posting coin hari ini
        //cek phase lebih dari 1
        $cekLastCoin24 = $modelCoin->getPostingCoinLast24Hours($id, $dataUser->id, $dateRange);
        $totalGetCoin = $modelCoin->getCoinPostingAvailableByID($id, $dataUser->id, $dateRange);
        $allCoin = $modelCoin->getPostingCoinHistory($id, $dataUser->id, $dateRange);
        return view('member.pin.detail-posting')
                ->with('getData', $getData)
                ->with('phaseHari', $fase)
                ->with('cekLastCoin24', $cekLastCoin24)
                ->with('totalGetCoin', $totalGetCoin)
                ->with('allCoin', $allCoin)
                ->with('is_finnish', $is_finnish)
                ->with('dataUser', $dataUser);
    }
    
    public function postStartPosting(Request $request){
        $dataUser = Auth::user();
        $modeTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $modelMasterCoin = New Masterpin;
        $getData = $modeTransaction->getPostingByID($request->id_posting, $dataUser->id);
//        $cekSetting = $modelCoinsetting->getIDCoinPostingSetting($getData->posting_setting_id);
        $fase = $modelCoin->getPhaseHari();
        $dateStart = $getData->created_at;
        $dateNow = date('Y-m-d H:i:s');
        $diff = abs(strtotime($dateNow) - strtotime($dateStart));  
        $years = floor($diff / (365*60*60*24));  
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
        $days = (int) floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24));
        $arrayPersentase = json_decode($getData->detail, true);
        $getPersen = $arrayPersentase[$days];
        $qty = $getData->total_coin * $getPersen / 100;
        $modelBonusSetting = New Bonussetting;
        $dataSettingCoin = $modelCoinsetting->getActiveCoinSetting(1);
        
        $dataInsertPostingCoin = array(
            'user_id' => $dataUser->id,
            'posting_id' => $getData->id,
            'type' => 1,
            'qty' => $qty,
            'persentase' => $getPersen,
            'active_at' => date('Y-m-d H:i:s')
        );
        $modelCoin->getInsertPostingCoin($dataInsertPostingCoin);
        $dataUpdatePosting = array(
            'total_post' => ($getData->total_post + 1)
        );
        $modeTransaction->getUpdatePostingTranscation('id', $getData->id, $dataUpdatePosting);
        $code = $modeTransaction->getCodeTransaction(6);
        $dataTransaksiBonusPShare = array(
            'seller_id' => $getData->id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'type' => 6,
            'total_coin' => $qty,
            'price' => 0,
            'admin_fee' => 0,
            'unique_digit' => 0,
            'coin_setting_id' => $dataSettingCoin->id
        );
        $getIdBonusSp = $modeTransaction->getInsertTransaction($dataTransaksiBonusPShare);
        $dataInsertMasterCoin = array(
            'total_coin' => $qty,
            'type' => 3,
            'transaction_id' => $getData->transaction_id
        );
        $modelMasterCoin->getInsertMasterCoin($dataInsertMasterCoin);
        
        //bonus
        if($dataUser->is_five == 1){
            if($dataUser->sponsor_id != null){
                $modelBonus = New Bonus;
                
                $modelMasterCoin = New Masterpin;
                $settingBonus = $modelBonusSetting->getActiveBonusSetting();
                $bonus_coin = $qty * $settingBonus->bonus_profit / 100;
                $bonusSp = array(
                    'user_id' => $dataUser->sponsor_id,
                    'from_user_id' => $dataUser->id,
                    'type' => 2,
                    'bonus' => $bonus_coin,
                    'bonus_date' => date('Y-m-d'),
                    'setting_id' => $settingBonus->id
                );
                $modelBonus->getInsertBonusMember($bonusSp);
                
                $code = $modeTransaction->getCodeTransaction(8);
                $dataTransaksiBonusPShare = array(
                    'seller_id' => $dataUser->sponsor_id,
                    'user_id' => $dataUser->sponsor_id,
                    'transaction_code' => $code,
                    'type' => 8,
                    'total_coin' => $bonus_coin,
                    'price' => 0,
                    'admin_fee' => 0,
                    'unique_digit' => 0,
                    'coin_setting_id' => $dataSettingCoin->id
                );
                $getIdBonusSp = $modeTransaction->getInsertTransaction($dataTransaksiBonusPShare);

                //coin member bertambah
                $dataCoinSponsorTambah = array(
                    'user_id' => $dataUser->sponsor_id,
                    'transaction_id' => $getIdBonusSp->lastID,
                    'type' => 1,
                    'qty' => $bonus_coin,
                    'price' => 0,
                    'bonus_type' => 2
                );
                $modelCoin->getInsertMemberCoin($dataCoinSponsorTambah);

                //saldo master terkeruk
                $dataInsertMasterCoin = array(
                    'total_coin' => $bonus_coin,
                    'type' => 5,
                    'transaction_id' => $getIdBonusSp->lastID
                );
                $modelMasterCoin->getInsertMasterCoin($dataInsertMasterCoin);
            }
        }
        
        return redirect()->route('m_listPosting')
//        return redirect()->route('m_postingByID', [$getData->id])
                        ->with('message', 'berhasil posting coin')
                        ->with('messageclass', 'success');
    }
    
    public function postClaimBonusPosting(Request $request){
        $dataUser = Auth::user();
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $modeTransaction = New Transaction;
        $type = 6;
        $dateRange = $modelCoin->getRange24Hours();
        $getData = $modelCoin->getCoinPostingAvailableByID($request->posting_id, $dataUser->id, $dateRange);
        if($getData == null){
            return redirect()->route('m_listPosting')
                            ->with('message', 'tidak ada data')
                            ->with('messageclass', 'error');
        }
        $saldoCoinPosting = $getData->coin_masuk - $getData->coin_keluar;
        if($request->total_coin < 0.01){
            return redirect()->route('m_postingByID', [$request->posting_id])
                            ->with('message', 'Coin Convert tidak boleh lebih kecil dari 0.01')
                            ->with('messageclass', 'error');
        }
        if(($request->total_coin - $saldoCoinPosting) > 0){
            return redirect()->route('m_postingByID', [$request->posting_id])
                            ->with('message', 'Coin Convert lebih besar dari saldo coin bonus posting')
                            ->with('messageclass', 'error');
        }
        $code = $modeTransaction->getCodeTransaction($type);
        $dataSettingCoin = $modelCoinsetting->getActiveCoinSetting(2);
        $dataTransaksi = array(
            'seller_id' => $dataUser->id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'type' => $type,
            'total_coin' => $request->total_coin,
            'price' => 0,
            'unique_digit' => 0,
            'coin_setting_id' => $dataSettingCoin->id,
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $insertTrans = $modeTransaction->getInsertTransaction($dataTransaksi);
        
        //coin posting bonus member berkurang
        $dataInsertPostingCoin = array(
            'user_id' => $dataUser->id,
            'posting_id' => $request->posting_id,
            'type' => 2,
            'qty' => $request->total_coin,
            'persentase' => 0,
            'active_at' => date('Y-m-d H:i:s')
        );
        $modelCoin->getInsertPostingCoin($dataInsertPostingCoin);
        
        //coin member bertambah
        $dataCoinMemberTambah = array(
            'user_id' => $dataUser->id,
            'transaction_id' => $insertTrans->lastID,
            'type' => 1,
            'qty' => $request->total_coin,
            'price' => 0
        );
        $modelCoin->getInsertMemberCoin($dataCoinMemberTambah);
        return redirect()->route('m_postingByID', [$request->posting_id])
                            ->with('message', 'berhasil Convert to Wallet')
                            ->with('messageclass', 'success');
    }
    
    public function postFinnishPosting(Request $request){
        $dataUser = Auth::user();
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $modeTransaction = New Transaction;
        $type = 5;
        $dateRange = $modelCoin->getRange24Hours();
        $getDataPosting = $modeTransaction->getPostingByID($request->posting_id, $dataUser->id);
        if($getDataPosting == null){
            return redirect()->route('m_listPosting')
                        ->with('message', 'tidak ada data')
                        ->with('messageclass', 'error');
        }
        $getData = $modelCoin->getCoinPostingAvailableByID($getDataPosting->id, $dataUser->id, $dateRange);
        $total_posting = $getDataPosting->total_coin;
        $saldoCoinPosting = $getData->coin_masuk - $getData->coin_keluar;
        $sum_all_coin = $total_posting + $saldoCoinPosting;
        
        $code = $modeTransaction->getCodeTransaction($type);
        $dataSettingCoin = $modelCoinsetting->getActiveCoinSetting(2);
        
        //transaksi tuntas posting coin
        $dataTransaksi = array(
            'seller_id' => $dataUser->id,
            'user_id' => $dataUser->id,
            'transaction_code' => $code,
            'type' => $type,
            'total_coin' => $sum_all_coin,
            'price' => 0,
            'unique_digit' => 0,
            'coin_setting_id' => $dataSettingCoin->id,
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $insertTrans = $modeTransaction->getInsertTransaction($dataTransaksi);
        
        //coin posting bonus member berkurang
        $dataInsertPostingCoin = array(
            'user_id' => $dataUser->id,
            'posting_id' => $getDataPosting->id,
            'type' => 2,
            'qty' => $sum_all_coin,
            'persentase' => 0,
            'active_at' => date('Y-m-d H:i:s')
        );
        $modelCoin->getInsertPostingCoin($dataInsertPostingCoin);
        
        //coin member bertambah
        $dataCoinMemberTambah = array(
            'user_id' => $dataUser->id,
            'transaction_id' => $insertTrans->lastID,
            'type' => 1,
            'qty' => $sum_all_coin,
            'price' => 0
        );
        $modelCoin->getInsertMemberCoin($dataCoinMemberTambah);
        return redirect()->route('m_listPosting')
                            ->with('message', 'Posting coin telah tuntas, convert coin ')
                            ->with('messageclass', 'success');
    }
    
    public function getHistoryAllMyTransactionCoin(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
             return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'Keanggotaan adan belum aktif')
                    ->with('messageclass', 'error');
        }
        $modelTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $getAllHistoryTransaction = $modelTransaction->getHistoryTransactionCoin($dataUser->id);
//        dd($getAllHistoryTransaction);
        return view('member.pin.history-transaction')
                    ->with('getData', $getAllHistoryTransaction)
                    ->with('dataUser', $dataUser);
    }

    public function getStatusMember() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $downline = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $downline = '[' . $dataUser->id . ']';
        }
//        $getMyStructure = $modelMember->getMyDownlineAllStatus($downline, $dataUser->id);
        $getMyStructure = $modelMember->getAllDownlineSponsorNotActive($dataUser);
        $modelMemberPackage = New Memberpackage;
        $getCheckNewOrder = $modelMemberPackage->getCountMemberPackageInactive($dataUser);
        return view('member.sponsor.status-member')
                        ->with('getData', $getMyStructure)
                        ->with('dataOrder', $getCheckNewOrder)
                        ->with('dataUser', $dataUser);
    }

    public function getAddUpgrade() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type == 4) {
            return redirect()->route('mainDashboard')
                            ->with('message', 'Paket anda sudah yang tertinggi')
                            ->with('messageclass', 'danger');
        }
//        $date30 =  strtotime(date('Y-m-d', strtotime('+30 days', strtotime($dataUser->active_at))));
//        $dateNow = strtotime(date('Y-m-d 23:59:59'));
//        if($date30 < $dateNow){
//            return redirect()->route('mainDashboard')
//                        ->with('message', 'Masa berlaku melakukan upgrade telah habis')
//                        ->with('messageclass', 'danger');
//        }
        $modePackage = New Package;
        $getPackageUpgrade = $modePackage->getAllPackageUpgrade($dataUser);
        return view('member.package.upgrade-package')
                        ->with('headerTitle', 'Upgrade Package')
                        ->with('packageUpgrade', $getPackageUpgrade)
                        ->with('dataUser', $dataUser);
    }

    public function postAddUpgrade(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modelPackage = New Package;
        $modelMembership = New Membership;
        $modelMember = New Member;
        $modelPin = New Pin;
        $userPackage = $modelPackage->getMyPackage($dataUser);
        $total_pin_all = $userPackage->pin + $request->total_pin;
        $getNewPackage = $modelPackage->getMyPackagePin($total_pin_all);
        $dataUpgrade = array(
            'user_id' => $dataUser->id,
            'member_type_old' => $dataUser->member_type,
            'member_type_new' => $getNewPackage->id,
            'type' => 1
        );
        $modelMembership->getInsertMembership($dataUpgrade);
        $dataMemberUpdate = array(
            'package_id' => $getNewPackage->id,
            'member_type' => $getNewPackage->id,
            'upgrade_at' => date('Y-m-d H:i:s')
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataMemberUpdate);
        $getMylastPin = $modelPin->getMyLastPin($dataUser);
        $code = sprintf("%03s", $request->total_pin);
        $memberPin = array(
            'user_id' => $dataUser->id,
            'total_pin' => $request->total_pin,
            'setting_pin' => $getMylastPin->setting_pin,
            'pin_code' => $getMylastPin->pin_code . $code . $dataUser->id,
            'is_used' => 1,
            'used_at' => date('Y-m-d H:i:s'),
            'used_user_id' => $dataUser->id,
            'pin_status' => 1,
            'is_upgrade' => 1
        );
        $modelPin->getInsertMemberPin($memberPin);
        $modelBonusSetting = new Bonussetting;
        $modelBonus = new Bonus;
        $modelPlacemenHistory = New Placementhistory;
        $modelBinaryHistory = new Binaryhistory;
        $getBonusStart = $modelBonusSetting->getActiveBonusStart();
        $bonus_price = $getBonusStart->start_price * $request->total_pin;
        $getSafraPoin = $modelBonusSetting->getSafraPoin();
        $safra_price = $getSafraPoin / 100 * $bonus_price;
        $real_bonus_price = $bonus_price - $safra_price;
        $dataInsertBonus = array(
            'user_id' => $dataUser->sponsor_id,
            'from_user_id' => $dataUser->id,
            'type' => 1,
            'bonus_price' => $real_bonus_price,
            'bonus_date' => date('Y-m-d'),
            'poin_type' => 1,
            'total_pin' => $request->total_pin
        );
        $modelBonus->getInsertBonusMember($dataInsertBonus);
        $dataInsertSafra = array(
            'user_id' => $dataUser->sponsor_id,
            'from_user_id' => $dataUser->id,
            'type' => 1,
            'bonus_price' => $safra_price,
            'bonus_date' => date('Y-m-d'),
            'poin_type' => 2,
            'total_pin' => $request->total_pin
        );
        $modelBonus->getInsertBonusMember($dataInsertSafra);

        //bonus binary
        $getUplineId = $dataUser;
        $newMemberUpline = $getUplineId->upline_detail . ',[' . $getUplineId->id . ']';
        if ($getUplineId->upline_detail == null) {
            $newMemberUpline = '[' . $getUplineId->id . ']';
        }
        $arraySpId = explode(',', $newMemberUpline);
        $dataSp = array();
        $no = count($arraySpId) + 1;
        foreach ($arraySpId as $rowSp) {
            $no--;
            $rm1 = str_replace('[', '', $rowSp);
            $rm2 = str_replace(']', '', $rm1);
            $int = (int) $rm2;
            $dataSp[] = array(
                'level' => $no,
                'id' => $int
            );
        }
        $dataSpReverse = array_reverse($dataSp);
        foreach ($dataSpReverse as $rowUpline) {
            $getRowUpline = $modelMember->getCekIdPlacementAndSponsor($rowUpline['id']);
            if ($getRowUpline != null) {
                $kiri = 0;
                if ($getRowUpline->kiri_id != null) {
                    $downlineKiri = $getRowUpline->upline_detail . ',[' . $getRowUpline->id . ']' . ',[' . $getRowUpline->kiri_id . ']';
                    if ($getRowUpline->upline_detail == null) {
                        $downlineKiri = '[' . $getRowUpline->id . ']' . ',[' . $getRowUpline->kiri_id . ']';
                    }
                    $kiriLuar = $modelMember->getNewCountMyDownline($downlineKiri, $getRowUpline->id);
                    $kiriPas = $modelMember->getKaKiTerPendek($getRowUpline->kiri_id, $getRowUpline->id);
                    $kiri = $kiriLuar + $kiriPas;
                }
                $kanan = 0;
                if ($getRowUpline->kanan_id != null) {
                    $downlineKanan = $getRowUpline->upline_detail . ',[' . $getRowUpline->id . ']' . ',[' . $getRowUpline->kanan_id . ']';
                    if ($getRowUpline->upline_detail == null) {
                        $downlineKanan = '[' . $getRowUpline->id . ']' . ',[' . $getRowUpline->kanan_id . ']';
                    }
                    $kananLuar = $modelMember->getNewCountMyDownline($downlineKanan, $getRowUpline->id);
                    $kananPas = $modelMember->getKaKiTerPendek($getRowUpline->kanan_id, $getRowUpline->id);
                    $kanan = $kananLuar + $kananPas;
                }
                $getHistoryBinary = $modelBinaryHistory->getBinaryHistory($getRowUpline->id);
                $kiriCheck = $kiri - $getHistoryBinary->sum_total_kiri;
                $kananCheck = $kanan - $getHistoryBinary->sum_total_kanan;
                if ($kiriCheck > 0 && $kananCheck > 0) {
                    $pasangan = $kiriCheck;
                    if ($kiriCheck > $kananCheck) {
                        $pasangan = $kananCheck;
                    }
                    if ($kiriCheck < $kananCheck) {
                        $pasangan = $kiriCheck;
                    }
                    $getSetting = $modelBonusSetting->getActiveBonusTeam($getRowUpline->member_type);
                    $getSafraPoin = $modelBonusSetting->getSafraPoin();
                    $bonus_price = $getSetting->team_price * $pasangan;
                    $safra_price = $getSafraPoin / 100 * $bonus_price;
                    $real_bonus_price = $bonus_price - $safra_price;
                    $date = date('Y-m-d');
                    $dataInsertBonus = array(
                        'user_id' => $getRowUpline->id,
                        'type' => 2,
                        'bonus_price' => $real_bonus_price,
                        'bonus_date' => $date,
                        'poin_type' => 1,
                        'total_binary' => $pasangan,
                        'total_pin' => $pasangan
                    );
                    $modelBonus->getInsertBonusMember($dataInsertBonus);
                    $dataInsertSafra = array(
                        'user_id' => $getRowUpline->id,
                        'type' => 2,
                        'bonus_price' => $safra_price,
                        'bonus_date' => date('Y-m-d'),
                        'poin_type' => 2,
                        'total_binary' => $pasangan,
                        'total_pin' => $pasangan
                    );
                    $modelBonus->getInsertBonusMember($dataInsertSafra);
                    $dataInsertH = array(
                        'user_id' => $getRowUpline->id,
                        'total_left' => $pasangan,
                        'total_right' => $pasangan,
                        'binary_date' => $date
                    );
                    $modelBinaryHistory->getInsertBinaryHistory($dataInsertH);
                }
            }
        }

        return redirect()->route('mainDashboard')
                        ->with('message', 'Upgrade member berhasil')
                        ->with('messageclass', 'success');
    }

    public function getDetailMemberBinary(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->upline_id == null) {
            if ($dataUser->id > 4) {
                return redirect()->route('mainDashboard');
            }
        }
        $modelMember = New Member;
        $modelBinaryHistory = new Binaryhistory;
        $back = false;
        $downline = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $downline = '[' . $dataUser->id . ']';
        }
        if ($request->get_id != null) {
            if ($request->get_id != $dataUser->id) {
                $back = true;
                $dataUser = $modelMember->getCekIdDownline($request->get_id, $downline);
            }
        }
        if ($dataUser == null) {
            return redirect()->route('mainDashboard');
        }
        $kanan = 0;
        if ($dataUser->kanan_id != null) {
            $downlineKanan = $dataUser->upline_detail . ',[' . $dataUser->id . ']' . ',[' . $dataUser->kanan_id . ']';
            if ($dataUser->upline_detail == null) {
                $downlineKanan = '[' . $dataUser->id . ']' . ',[' . $dataUser->kanan_id . ']';
            }
            $kananLuar = $modelMember->getNewCountMyDownline($downlineKanan, $dataUser->id);
            $kananPas = $modelMember->getKaKiTerPendek($dataUser->kanan_id, $dataUser->id);
            $kanan = $kananLuar + $kananPas;
        }
        $kiri = 0;
        if ($dataUser->kiri_id != null) {
            $downlineKiri = $dataUser->upline_detail . ',[' . $dataUser->id . ']' . ',[' . $dataUser->kiri_id . ']';
            if ($dataUser->upline_detail == null) {
                $downlineKiri = '[' . $dataUser->id . ']' . ',[' . $dataUser->kiri_id . ']';
            }
            $kiriLuar = $modelMember->getNewCountMyDownline($downlineKiri, $dataUser->id);
            $kiriPas = $modelMember->getKaKiTerPendek($dataUser->kiri_id, $dataUser->id);
            $kiri = $kiriLuar + $kiriPas;
        }
        $getHistoryBinary = $modelBinaryHistory->getBinaryHistory($dataUser->id);
        $kiriCheck = $kiri - $getHistoryBinary->sum_total_kiri;
        $kananCheck = $kanan - $getHistoryBinary->sum_total_kanan;
        if ($kiriCheck > 0 && $kananCheck > 0) {
            $pasangan = $kiriCheck;
            if ($kiriCheck > $kananCheck) {
                $pasangan = $kananCheck;
            }
            if ($kiriCheck < $kananCheck) {
                $pasangan = $kiriCheck;
            }
        }
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        $dataUserDetail = (object) array(
                    'kanan' => $kanan,
                    'kiri' => $kiri,
                    'sisaKanan' => $kananCheck,
                    'sisaKiri' => $kiriCheck,
                    'omzetKiri' => $kiri * $getActivePinSetting->price,
                    'omzetKanan' => $kanan * $getActivePinSetting->price
        );
        return view('member.networking.detail-binary')
                        ->with('back', $back)
                        ->with('detailUser', $dataUserDetail)
                        ->with('dataUser', $dataUser);
    }

    public function getMemberListNews() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $getData = $modelNews->getListNews();
        return view('member.news.news_list')
                        ->with('getData', $getData)
                        ->with('headerTitle', 'Berita')
                        ->with('dataUser', $dataUser);
    }

    public function getMemberNewsByID($id) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $getData = $modelNews->getNewsByField('id', $id);
        if ($getData == null) {
            return redirect()->route('mainDashboard');
        }
//        if(!isset($_COOKIE['_content'])){
//            $value = $id;
//        } else {
//            $value = $_COOKIE['_content'].','.$id;
//        }
//        setcookie('_content', $value, time() + (86400 * 10), "/"); // 86400 = 1 day
        return view('member.news.news_detail')
                        ->with('getData', $getData)
                        ->with('dataUser', $dataUser);
    }

    public function getStatusLevel() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $downline = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $downline = '[' . $dataUser->id . ']';
        }
        $getMyStructure = $modelMember->getMyDownlineAllLevelActive($downline, $dataUser->id);
        $dataAll = array();
        if ($getMyStructure != null) {
            foreach ($getMyStructure as $row) {
                $placementdate = null;
                if ($row->upline_detail != null) {
                    $getLastPlacement = $modelMember->getMySponsorPlacementLastDate($row->id);
                    $placementdate = $getLastPlacement->created_at;
                }
                $dataAll[] = (object) array(
                            'id' => $row->id,
                            'user_code' => $row->user_code,
                            'email' => $row->email,
                            'hp' => $row->hp,
                            'is_active' => $row->is_active,
                            'active_at' => $row->active_at,
                            'upline_id' => $row->upline_id,
                            'upline_detail' => $row->upline_detail,
                            'package_id' => $row->package_id,
                            'paket_name' => $row->paket_name,
                            'pin' => $row->pin,
                            'placement_date' => $getLastPlacement->created_at
                );
            }
        }
        return view('member.sponsor.status-level')
                        ->with('getData', $dataAll)
                        ->with('dataUser', $dataUser);
    }

    public function getAllDS() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->package_id == null) {
            return redirect()->route('m_newPackage');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getData = $modelMember->getAllDirectorStockist();
        return view('member.sponsor.all-ds')
                        ->with('getData', $getData)
                        ->with('dataUser', $dataUser);
    }

    public function getEditPassword() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        return view('member.profile.my-password')
                        ->with('headerTitle', 'Password')
                        ->with('dataUser', $dataUser);
    }

    public function postEditPassword(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $dataUpdatePass = array(
            'password' => bcrypt($request->password),
        );
        $modelMember = New Member;
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdatePass);
        return redirect()->route('mainDashboard')
                        ->with('message', 'Berhasil edit password')
                        ->with('messageclass', 'success');
    }

    public function getMemberTestingCheck() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getDataAPI = $modelMember->getDataAPIMobilePulsa();
        $username = $getDataAPI->username;
        $apiKey = $getDataAPI->api_key;
        $sign = md5($username . $apiKey . 'pl');
        $json = '{
                    "commands" : "pricelist",
                    "username" : "' . $username . '",
                    "sign"     : "' . $sign . '", 
                    "status" : "active"
            }';
        $url = $getDataAPI->master_url . '/v1/legacy/index/pulsa/telkomsel';
        $cek = $modelMember->getAPIurlCheck($url, $json);
        $arrayData = json_decode($cek, true);
        dd($arrayData['data']);
    }

    public function getMySponsorTree(Request $request) {
        $dataUser = Auth::user();
        $sessionUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('m_newCoin')
                    ->with('message', 'akun anda belum aktif, silakan beli coin')
                    ->with('messageclass', 'error');
        }
        $modelMember = New Member;
        $back = false;
        if ($request->get_id != null) {
            if ($request->get_id < $sessionUser->id) {
                return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
            }
            if ($request->get_id != $dataUser->id) {
                $back = true;
                $dataUser = $modelMember->getUsers('id', $request->get_id);
            }
        }
        if ($dataUser == null) {
            return redirect()->route('m_mySponsorTree');
        }
        $getBinary = $modelMember->getStructureSponsor($dataUser);
        return view('member.networking.sponsor')
                        ->with('getData', $getBinary)
                        ->with('back', $back)
                        ->with('dataUser', $dataUser)
                        ->with('sessionUser', $sessionUser);
    }

    public function getAddMemberStockist() {
        $dataUser = Auth::user();
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 1) {
            return redirect()->route('mainDashboard');
        }
        //punya rek bank ga
        $modelBank = New Bank;
        $modelTrans = New Transaction;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        //cek trans masuk - trans keluar
        $cekPinIn = $modelTrans->getMasterStockistTransactionsIn($dataUser->id);
        $cekPinOut = $modelTrans->getMasterStockistTransactionsOut($dataUser->id);
        $cekSaldoPin = $cekPinIn->total_masuk - $cekPinOut->total_keluar;
        if ($cekSaldoPin <= 0) {
            return redirect()->route('m_newPin')
                            ->with('message', 'Saldo pin anda tidak mencukupi untuk daftar Stockist. Harap order pin')
                            ->with('messageclass', 'danger');
        }
        return view('member.sponsor.add-stockist')
                        ->with('headerTitle', 'Tambah Stockist baru')
                        ->with('dataUser', $dataUser);
    }

    public function postAddMemberStockist(Request $request) {
        $dataUser = Auth::user();
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 1) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $upline_detail = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $upline_detail = '[' . $dataUser->id . ']';
        }
        $dataInsertNewMember = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'hp' => $request->hp,
            'user_code' => $request->user_code,
            'is_active' => 1,
            'member_type' => 2,
            'upline_id' => $dataUser->id,
            'upline_detail' => $upline_detail,
            'active_at' => date('Y-m-d H:i:s'),
        );
        $modelMember->getInsertUsers($dataInsertNewMember);
        return redirect()->route('m_addMemberStockist')
                        ->with('message', 'Stockist baru berhasil dibuat')
                        ->with('messageclass', 'success');
    }

    public function getAddAnnualMemberPackage() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->total_sponsor != 0) {
            return redirect()->route('mainDashboard');
        }
        $modePackage = New Package;
        //punya rek bank ga
        $modelBank = New Bank;
        $modelTrans = New Transaction;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        //cek trans masuk - trans keluar - Pin Out aktifasi
        $cekPinIn = $modelTrans->getMasterStockistTransactionsIn($dataUser->id);
        $cekPinOut = $modelTrans->getMasterStockistTransactionsOut($dataUser->id);
        $cekPnAktivasi = $modePackage->getCekStockistPinKeluarAktivasi($dataUser->id);
        $cekSaldoPin = $cekPinIn->total_masuk - $cekPinOut->total_keluar - $cekPnAktivasi->total_keluar;
        //cek Pin Out aktifasi Opening member
        if ($cekSaldoPin <= 0) {
            return redirect()->route('m_newPin')
                            ->with('message', 'Saldo pin anda tidak mencukupi untuk daftar Stockist. Harap order pin')
                            ->with('messageclass', 'danger');
        }

        $data = $modePackage->getStockistPackage($dataUser->id);
        $getData = array();
        $saldo = 0;
        if ($data != null) {
            foreach ($data as $row) {
                $cekTransOut = $modelTrans->getStockistTransOutPackage($dataUser->id, $row->id);
                $jml_keluar = $modePackage->getCekStockistPinKeluarAktivasiPackage($dataUser->id, $row->id);
                $total_sisa = $row->total_qty - $jml_keluar->total_keluar - $cekTransOut->total_keluar;
                $saldo += $row->total_qty - $jml_keluar->total_keluar - $cekTransOut->total_keluar;
                if ($total_sisa < 0) {
                    $total_sisa = 0;
                }
                $getData[] = (object) array(
                            'qty' => $row->total_qty,
                            'name' => $row->name,
                            'code' => $row->code,
                            'image' => $row->image,
                            'id' => $row->id,
                            'jml_keluar' => $jml_keluar,
                            'total_sisa' => $total_sisa,
                );
            }
        }
//        dd($saldo);
        return view('member.sponsor.add-annual_package')
                        ->with('headerTitle', 'Tambah Member Baru')
                        ->with('getData', $getData)
                        ->with('tersedia', $saldo)
                        ->with('dataUser', $dataUser);
    }

    public function getAddMemberOpening() {
        $dataUser = Auth::user();
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->member_type != 1) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'forbiden area')
                    ->with('messageclass', 'error');
        }
        if ($dataUser->total_sponsor != 0) {
            return redirect()->route('mainDashboard')
                    ->with('message', 'anda sudah pernah mendaftarkan member')
                    ->with('messageclass', 'error');
        }
        //punya rek bank ga
        if ($dataUser->is_bank == 0) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'error');
        }
        //cek coin masuk - coin keluar - trans blm selesai
        $modelCoin = new Pin;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        if($cekSaldo <= 0){
            return redirect()->route('m_newCoin')
                    ->with('message', 'anda tidak memiliki saldo, harap membeli saldo kepada perusahaan')
                    ->with('messageclass', 'error');
        }
        //cek Pin Out aktifasi Opening member
        return view('member.sponsor.add-opening_member')
                        ->with('headerTitle', 'Tambah Member Baru')
                        ->with('dataUser', $dataUser);
    }

    public function postAddMemberOpening(Request $request) {
        $dataUser = Auth::user();
        $modelMember = New Member;
        $upline_detail = $dataUser->upline_detail . ',[' . $dataUser->id . ']';
        if ($dataUser->upline_detail == null) {
            $upline_detail = '[' . $dataUser->id . ']';
        }
        $dataInsertNewMember = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'hp' => $request->hp,
            'user_code' => $request->user_code,
            'member_type' => 3,
            'upline_id' => $dataUser->id,
            'upline_detail' => $upline_detail,
        );
        $getNew = $modelMember->getInsertUsers($dataInsertNewMember);
        
        //Activation
        $dataActivation = array(
            'user_id' => $getNew->lastID,
            'sponsor_id' => $dataUser->id,
        );
        $modelMember->getInsertActivationUser($dataActivation);
        
        //Update tambah total sponsor stockist hanya 1 saja
        $dataUpdateSponsor = array(
            'total_sponsor' => 1,
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdateSponsor);
        
        return redirect()->route('mainDashboard')
                        ->with('message', 'Member baru berhasil dibuat')
                        ->with('messageclass', 'success');
    }
    
    
    public function getAddCodePin() {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        return view('member.profile.pin_code')
                        ->with('dataUser', $dataUser);
    }

    public function postAddCodePin(Request $request) {
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $dataUpdatePinCoder = array(
            'pin_code' => $request->pin_code,
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $dataUpdatePinCoder);
        return redirect()->route('m_addCodePin')
                        ->with('message', 'Kode Pin berhasil ditambahkan')
                        ->with('messageclass', 'success');
    }
    
    public function getListStockistByMaster(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 1) {
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $modelTrans = New Transaction;
        $modelPackage = New Package;
        $modelMember = New Member;
        $getStockist = $modelMember->getAllMemberStockist();
        $package_id = 1; //khusus bronze
        $getData = array();
        if($getStockist != null){
            foreach($getStockist as $row){
                //khusus bronze
                $cekPinIn = $modelPackage->getStockistPinMasukPackageId($row->stockist_id, $package_id);
                $cekPinOut = $modelTrans->getStockistTransOutPackageId($row->stockist_id, $package_id);
                $cekPinAktivasi = $modelPackage->getStockistPinOutAktivasiPackageId($row->stockist_id, $package_id);
                $cekSaldoPin = $cekPinIn->total_masuk - $cekPinOut->total_keluar - $cekPinAktivasi->total_keluar;
                $getData[] = (object) array(
                    'stockist_id' => $row->stockist_id,
                    'stockist_name' => $row->stockist_name,
                    'stockist_user_id' => $row->stockist_user_id,
                    'total_aktifasi' => $row->total_aktifasi,
                    'saldo_bronze' => $cekSaldoPin
                );
            }
        }
//        dd($getData);
        return view('member.networking.list_stockist')
                        ->with('headerTitle', 'List Stockist')
                        ->with('getStockist', $getData)
                        ->with('dataUser', $dataUser);
    }
    
    public function postAddConfirmTransferPin(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type == 1) {
            return redirect()->route('mainDashboard');
        }
        $modelSettingTrans = New Transaction;
        $modePackage = New Package;
        $id_trans = $request->id_trans;
        if($request->pin_code == null) {
            return redirect()->route('m_listTransferPin')
                            ->with('message', 'Anda tidak mengisi kode pin')
                            ->with('messageclass', 'danger');
        }
        if($dataUser->pin_code != $request->pin_code){
            return redirect()->route('m_listTransferPin')
                            ->with('message', 'Kode pin tidak sama')
                            ->with('messageclass', 'danger');
        }
        $getData = $modelSettingTrans->getTransactionsTransferPin($id_trans, $dataUser->id);
        if ($getData == null) {
            return redirect()->route('m_listTransferPin')
                            ->with('message', 'Tidak ada data')
                            ->with('messageclass', 'danger');
        }
        foreach ($getData as $row) {
            $dataStock = array(
                'user_id' => $dataUser->id,
                'package_id' => $row->package_id,
                'amount' => $row->qty,
                'package_pin_id' => $row->id,
                'type' => 1
            );
            $modePackage->getInsertMemberStock($dataStock);
        }
        $dataUpdate = array(
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $modelSettingTrans->getUpdateTransaction('id', $id_trans, $dataUpdate);
        return redirect()->route('m_listTransferPin')
                        ->with('message', 'Konfirmasi transfer pin berhasil')
                        ->with('messageclass', 'success');
    }
    
    public function postRejectConfirmTransferPin(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type == 1) {
            return redirect()->route('mainDashboard');
        }
        $modePackage = New Package;
        $modelTrans = New Transaction;
        $id_trans = $request->id_trans;
        
        $getData = $modelTrans->getTransactionsRejectBuyer($id_trans);
        foreach ($getData as $row) {
            $modePackage->getDeleteStockPinByID($row->id);
        }
        $dataUpdate = array(
            'status' => 3,
            'deleted_at' => date('Y-m-d H:i:s'),
            'reason' => $request->reason
        );
        $modelTrans->getUpdateTransaction('id', $id_trans, $dataUpdate);
        return redirect()->route('m_listTransferPin')
                        ->with('message', 'Transaksi dibatalkan')
                        ->with('messageclass', 'success');
    }
    
    public function getClaimActivatedProduct(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        if($dataUser->is_claim_activated == 1){
            $cek = $modelMember->getCheckMemberClaimProdukAktifasi($dataUser->id);
            if($cek != null){
                return redirect()->route('m_claimAktifasiProduk', [$cek->id])
                        ->with('message', 'Anda pernah mengajukan claim aktifasi produk')
                        ->with('messageclass', 'warning');
            }
        }
        return view('member.sponsor.claim-product')
                        ->with('dataUser', $dataUser);
    }
    
    public function postClaimActivatedProduct(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_claim_activated == 1){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $dataInsert = array(
            'user_id' => $dataUser->id,
            'full_name' => $request->full_name,
            'hp' => $request->no_hp,
            'alamat' => $request->alamat,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kota' => $request->kota,
            'provinsi' => $request->provinsi,
        );
        $new = $modelMember->getInsertClaimProdukAktifasi($dataInsert);
        $updateCek = array(
            'is_claim_activated' => 1,
            'claim_activated_at' => date('Y-m-d H:i:s')
        );
        $modelMember->getUpdateUsers('id', $dataUser->id, $updateCek);
        return redirect()->route('m_claimAktifasiProduk', [$new->lastID])
                        ->with('message', 'Claim produk aktifasi berhasil, tunggu konfirmasi dari admin')
                        ->with('messageclass', 'success');
    }
    
    public function getActivatedProductID($id){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_claim_activated == 0){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getData = $modelMember->getMemberClaimProdukAktifasi($id, $dataUser->id);
        if($getData == null){
            return redirect()->route('mainDashboard');
        }
        return view('member.sponsor.detail-claimproduct')
                        ->with('getData', $getData)
                        ->with('dataUser', $dataUser);
    }
    
    public function getTransferBelanja(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'danger');
        }
        $modelSale = New Sale;
        $data = $modelSale->getListProdukSale();
//        dd($getData);
        return view('member.belanja.index')
                        ->with('headerTitle', 'Belanja Anda')
                        ->with('getData', $data)
                        ->with('dataUser', $dataUser);
    }
    
    public function postAddBelanja(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getCode = $modelSale->getCodeTransaction();
        $dataTrans = array(
            'user_id' => $dataUser->id,
            'invoice' => $getCode,
            'total_price' => $request->total_harga,
            'sale_date' => date('Y-m-d'),
            'buy_metode' => 2
        );
        $getInsert = $modelSale->getInsertTransaction($dataTrans);
        foreach($request->data as $row){
            $dataExplode = explode('__', $row);
            $dataItem = array(
                'user_id' => $dataUser->id,
                'sales_transaction_id' => $getInsert->lastID,
                'purchase_id' => $dataExplode[0],
                'amount' => $dataExplode[1],
                'item_price' => $dataExplode[2]
            );
            $modelSale->getInsertItemTransaction($dataItem);
        }
        return redirect()->route('m_detailBelanja', [$getInsert->lastID])
                            ->with('message', 'berhasil pilih produk belanja. Lakukan konfirmasi pembayaran')
                            ->with('messageclass', 'success');
    }
    
    public function getListBelanja(){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getData = $modelSale->getAllTransactionByUserId($dataUser->id);
//        dd($getData);
        return view('member.belanja.list')
                    ->with('headerTitle', 'List Belanja Anda')
                    ->with('getData', $getData)
                    ->with('dataUser', $dataUser);
    }
    
    public function getDetailBelanja($id){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $modelBank = New Bank;
        $getData = $modelSale->getDetailTransactionByUserId($id, $dataUser->id);
        if($getData == null){
            return redirect()->route('m_listBelanja')
                    ->with('message', 'tidak ada data')
                    ->with('messageclass', 'danger');
        }
//        dd($getData);
        $getSellerBank = $modelBank->getBankPerusahaan();
        return view('member.belanja.detail')
                    ->with('headerTitle', 'List Belanja Anda')
                    ->with('bankSeller', $getSellerBank)
                    ->with('getData', $getData[0])
                    ->with('dataUser', $dataUser);
    }
    
    public function postAddConfirmBelanja(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $modelBank = New Bank;
        $dataUpdateTrans = array(
            'bank_name' => $request->bank_name,
            'account_name' => $request->account_name,
            'account_no' => $request->account_no,
            'status' => 1,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $modelSale->getUpdateTransaction('id', $request->id_trans, $dataUpdateTrans);
        return redirect()->route('m_detailBelanja', [$request->id_trans])
                            ->with('message', 'berhasil konfirmasi pembayaran belanja')
                            ->with('messageclass', 'success');
    }
    
    public function postRejectConfirmBelanja(Request $request){
        $dataUser = Auth::user();
        $onlyUser = array(10);
        if (!in_array($dataUser->user_type, $onlyUser)) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->is_active == 0) {
            return redirect()->route('mainDashboard');
        }
        if ($dataUser->member_type != 3) {
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $dataUpdateTrans = array(
            'reason' => $request->reason,
            'status' => 3,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelSale->getUpdateTransaction('id', $request->id_trans, $dataUpdateTrans);
        return redirect()->route('m_detailBelanja', [$request->id_trans])
                            ->with('message', 'berhasil batal pembayaran belanja')
                            ->with('messageclass', 'success');
    }
    
    
    
    
    
    
    

}
