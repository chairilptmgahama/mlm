<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin;
use App\Model\Package;
use App\Model\Transaction;
use App\Model\Bank;
use App\Model\Pengiriman;
use App\Model\Member;
use App\Model\Transferwd;
use App\Model\Bonus;
use App\Model\Bonussetting;

class AjaxController extends Controller {

    public function __construct(){
        
    }
    
    public function getAdminById($type, $id){
        $dataUser = Auth::user();
        $getType = 0;
        $header = 'Empty';
        if($type == 1){
            $header = 'Edit';
            $getType = 1;
        }
        if($type == 2){
            $header = 'Delete';
            $getType = 2;
        }
        $modelAdmin = New Admin;
        $getData = null;
        if($id > 2){
            $getData = $modelAdmin->getAdminById($id);
        }
        return view('admin.ajax.admin')
                ->with('headerTitle', $header.' Admin')
                ->with('getData', $getData)
                ->with('type', $getType)
                ->with('dataUser', $dataUser);
    }
    
    public function getPackageById($id){
        $dataUser = Auth::user();
        $modelPackage = New Package;
        $getPackageId = $modelPackage->getPackageId($id);
        return view('admin.ajax.package')
                ->with('headerTitle', 'Edit Package')
                ->with('getData', $getPackageId)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekTransactionById($id, $user_id){
        $modelSettingTrans = New Transaction;
        $getData = $modelSettingTrans->getDetailTransactionSellerAdmin($id, $user_id);
        return view('admin.ajax.transaction')
                ->with('headerTitle', 'Cek Transaksi')
                ->with('getData', $getData);
    }
    
    public function getBankPerusahaan($id){
        $dataUser = Auth::user();
        $modelBank = new Bank;
        $getPerusahaanBank = $modelBank->getBankPerusahaanID($id);
        return view('admin.ajax.bank')
                ->with('headerTitle', 'Edit Bank Perusahaan')
                ->with('getData', $getPerusahaanBank)
                ->with('dataUser', $dataUser);
    }
    
    public function getKirimPaket($id, $user_id){
        $dataUser = Auth::user();
        $modelPengiriman = new Pengiriman;
        $getPengiriman = $modelPengiriman->getAdmPengirimanByID($id, $user_id);
        return view('admin.ajax.pengiriman')
                ->with('headerTitle', 'Confirm Pengiriman')
                ->with('getData', $getPengiriman)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekKirimPaket(Request $request){
        $dataUser = Auth::user();
        $id =$request->cekId;
        $user_id = $request->cekUserId;
        $kurir_name = $request->kurir_name;
        $no_resi = $request->no_resi;
        $modelMember = New Member;
        $getData = $modelMember->getAdminMemberClaimProdukAktifasi($id);
        $data = (object) array(
            'id' => $id,
            'user_id' => $user_id,
            'kurir_name' => $kurir_name,
            'no_resi' => $no_resi
        );
        return view('admin.ajax.pengiriman')
                ->with('headerTitle', 'Confirm Pengiriman')
                ->with('getData', $getData)
                ->with('data', $data)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminChangePasswordMember($id){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getData = $modelMember->getUsers('id', $id);
        return view('admin.ajax.change-passwd')
                ->with('headerTitle', 'Ubah Password Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekRejectWD($id, $type){
        $dataUser = Auth::user();
        $modelWD = new Transferwd;
        if($type != 5){
            $getData = $modelWD->getIDRequestWD($id);
        }
        if($type == 5){
            $getData = $modelWD->getAdminMemberBonusRewardByID($id);
        }
        return view('admin.ajax.reject-wd')
                ->with('headerTitle', 'Reject Withdrawal')
                ->with('getData', $getData)
                ->with('type', $type)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekDetailWD($id){
        $dataUser = Auth::user();
        $modelWD = new Transferwd;
        $getData = $modelWD->getIDRequestWD($id);
        return view('admin.ajax.detail-wd')
                ->with('headerTitle', 'Detail Withdrawal')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekDetailBonusReward($id){
        $dataUser = Auth::user();
        $modelBonusSetting = new Bonussetting;
        $getData = $modelBonusSetting->getAllBonusRewardByID($id);
        return view('admin.ajax.bonus-reward')
                ->with('headerTitle', 'Edit Bonus Reward')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekDetailBonusLevel($id){
        $dataUser = Auth::user();
        $modelBonusSetting = new Bonussetting;
        $getData = $modelBonusSetting->getAdminBonusLevelByID($id);
        return view('admin.ajax.bonus-level')
                ->with('headerTitle', 'Edit Bonus Level')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getSearchUsername(Request $request){
        $modelMember = New Member;
        $getData = null;
        if($request->name != null){
            $getData = $modelMember->getAdminSearchUsername($request->name);
        }
        return view('admin.ajax.get_name_autocomplete')
                        ->with('getData', $getData);
    }
    
    public function getAdminChangeDataMember($id){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getData = $modelMember->getUsers('id', $id);
        return view('admin.ajax.change-data')
                ->with('headerTitle', 'Ubah Data Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getAdminChangeBlockMember($id){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getData = $modelMember->getUsers('id', $id);
        return view('admin.ajax.change-block')
                ->with('headerTitle', 'Blokir Data Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekSafraTransactionById(Request $request){
        $dataUser = Auth::user();
        $user_id = $request->cekUserId;
        $id = $request->cekId;
        return view('admin.ajax.safra-transaction')
                ->with('id', $id)
                ->with('user_id', $user_id)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekDetailBonusPoolSharing($id){
        $dataUser = Auth::user();
        $modelBonusSetting = new Bonussetting;
        $getData = $modelBonusSetting->getAllBonusPoolSharingByID($id);
        return view('admin.ajax.bonus-pool')
                ->with('headerTitle', 'Edit Bonus Pool Sharing')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getCekConfirmSalesMember($id, Request $request){
        return view('admin.ajax.approve_belanja')
                    ->with('id', $id);
    }
    
    public function getCekRejectSalesMember($id, Request $request){
        return view('admin.ajax.reject_belanja')
                    ->with('id', $id);
    }
    
    
      
    
    

}
