<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Member;
use App\Model\Pinsetting;
use App\Model\Package;
use App\Model\Memberpackage;
use App\Model\Transaction;
use App\Model\Pin;
use App\Model\Transferwd;
use App\Model\Bonus;
use App\Model\Safra;
use App\Model\Bank;
use App\Model\Startwd;

class SaframemberController extends Controller {
    
    public function __construct(){
    }
    
    public function getMySaldoSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $modelWDStart = new Startwd;
        $modelSafra = New Safra;
        $cekDateStart =$modelWDStart->getStartEndWDMember($dataUser->id_user);
        $periodeWDMember = date('Y-m-d', strtotime($dataUser->active_at));
        if($cekDateStart != null){
            $periodeWDMember = $cekDateStart->start_date;
        }
        $totalWDRangeDate = $modelWD->getTotalWDRangeDate($dataUser->id, $periodeWDMember);
        $totalFeeRangeDate = $modelWD->getTotalWDFeeAdminRangeDate($dataUser->id, $periodeWDMember);
        $totalBonusSafraPoin = $modelBonus->getTotalBonusSafraPoin($dataUser);
        $totalWDSP = $modelWD->getTotalDiTransferByType($dataUser, 11);
        $safraOut = $modelSafra->getTotalSafraOut($dataUser->id);
//        $saldoSafra = $totalBonusSafraPoin - $safraOut;
        $dataAll = (object) array(
            'total_bonus' => $totalBonusSafraPoin,
            'total_wd' => $totalWDSP->total_wd,
            'total_tunda' => $totalWDSP->total_tunda,
            'stock_wd' => 10000000000, //$getMyPackage->stock_wd,
            'periodeWD' => $periodeWDMember,
            'fee_tunda' => $totalWDSP->total_tunda_admin_fee,
            'fee_tuntas' => $totalWDSP->total_wd_admin_fee,
            'total_wd_range' => $totalWDRangeDate + $totalFeeRangeDate,
            'belanjaSafra' => $safraOut,
            'saldo' => (int) ($totalBonusSafraPoin - ($totalWDSP->total_wd + $totalWDSP->total_tunda + $safraOut + $totalWDSP->total_tunda_admin_fee + $totalWDSP->total_wd_admin_fee)),
        );
        $totalSafra2 = $modelBonus->getSafraHistory($dataUser);
        return view('member.safra.saldo')
                ->with('dataAll', $dataAll)
                ->with('getData2', $totalSafra2)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyShopingSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        return view('member.safra.belanja')
                ->with('dataUser', $dataUser);
    }
    
    public function getMySummarySafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelBonus = new Bonus;
        $totalSafra = $modelBonus->getSafraHistory($dataUser);
        return view('member.safra.summary')
                ->with('getData', $totalSafra)
                ->with('dataUser', $dataUser);
    }
    
    public function getMemberListShopingSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelSafra = New Safra;
        $getData = $modelSafra->getListPurchase();
        return view('member.safra.purchase-list')
                    ->with('getData', $getData)
                    ->with('dataUser', $dataUser);
    }
    
    public function getMemberShoping($id){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelSafra = New Safra;
        $getData = $modelSafra->getPurchaseByID($id);
        return view('member.safra.purchase-view')
                    ->with('getData', $getData)
                    ->with('dataUser', $dataUser);
    }
    
    public function postMemberBuyShoping(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelSafra = New Safra;
        $safraCode = $modelSafra->getCodeSafra();
        $dataInsert = array(
            'user_id' => $dataUser->id,
            'safra_code' => 'WDS'.date('Ymd').$safraCode,
            'safra_total' => $request->potongan,
            'safra_date' => date('Y-m-d'),
            'transfer_total' => $request->bayar,
            'purchase_id' => $request->id_barang,
            'total_buy' => $request->total_buy
        );
        $modelSafra->getInsertSafraTransfer($dataInsert);
        return redirect()->route('m_myTransactionSafra')
                    ->with('message', 'Anda telah melakukan pilih barang yang akan dibeli. silakan transfer')
                    ->with('messageclass', 'success');
    }
    
    public function getMemberListTransactionSafra(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelSafra = New Safra;
        $getData =$modelSafra->getSafraTransaction($dataUser->id);
        return view('member.safra.transaction')
                    ->with('getData', $getData)
                    ->with('dataUser', $dataUser);
    }
    
    public function getMemberTransactionSafraID($id){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelSafra = New Safra;
        $modelBank = New Bank;
        $getData =$modelSafra->getSafraTransactionDetail($id, $dataUser->id);
        if($getData == null){
            return redirect()->route('m_myTransactionSafra')
                        ->with('message', 'Data tidak ditemukan')
                        ->with('messageclass', 'danger');
        }
        if($getData->bank_id != null){
            $getPerusahaanBank = $modelBank->getBankPerusahaanID($getData->bank_id);
        } else {
            $getPerusahaanBank = $modelBank->getBankPerusahaan();
        }
        return view('member.safra.transaction-detail')
                    ->with('getData', $getData)
                    ->with('bankPerusahaan', $getPerusahaanBank)
                    ->with('dataUser', $dataUser);
    }
    
    public function postMemberProsesSafraTransaction(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        if($request->alamat == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Alamat harus diisi')
                        ->with('messageclass', 'danger');
        }
        if($request->kecamatan == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Kecamatan harus diisi')
                        ->with('messageclass', 'danger');
        }
        if($request->kota == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Kota harus diisi')
                        ->with('messageclass', 'danger');
        }
        if($request->provinsi == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Provinsi harus dipilih')
                        ->with('messageclass', 'danger');
        }
        if($request->kode_pos == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Kode Pos harus diisi')
                        ->with('messageclass', 'danger');
        }
        $modelSafra = New Safra;
        $dataUpdate = array(
            'status' => 1,
            'bank_id' => $request->bank_id,
            'alamat_kirim' => $request->alamat,
            'kecamatan' => $request->kecamatan,
            'kota' => $request->kota,
            'provinsi' => $request->provinsi,
            'kode_pos' => $request->kode_pos,
        );
        $modelSafra->getUpdateSafraTransfer('id', $request->id_trans, $dataUpdate);
        return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Berhasil, tunggu konfirmasi dari admin')
                        ->with('messageclass', 'success');
    }
    
    public function postMemberRejectsSafraTransaction(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        if($request->reason == null){
            return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Alasan harus diisi')
                        ->with('messageclass', 'danger');
        }
        $modelSafra = New Safra;
        $dataUpdate = array(
            'status' => 3,
            'reason' => $request->reason,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelSafra->getUpdateSafraTransfer('id', $request->id_trans, $dataUpdate);
        return redirect()->route('m_myTransactionSafraID', [$request->id_trans])
                        ->with('message', 'Transaksi dibatalkan')
                        ->with('messageclass', 'success');
    }
    
    public function getRequestWDSafraPoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
//        $modePackage = New Package;
        $modelSafra = New Safra;
        $modelWDStart = new Startwd;
        $cekDateStart =$modelWDStart->getStartEndWDMember($dataUser->id_user);
        $periodeWDMember = date('Y-m-d', strtotime($dataUser->active_at));
        if($cekDateStart != null){
            $periodeWDMember = $cekDateStart->start_date;
        }
        $totalWDRangeDate = $modelWD->getTotalWDRangeDate($dataUser->id, $periodeWDMember);
        $totalFeeRangeDate = $modelWD->getTotalWDFeeAdminRangeDate($dataUser->id, $periodeWDMember);
        $totalBonusSafraPoin = $modelBonus->getTotalBonusSafraPoin($dataUser);
        $totalWDSP = $modelWD->getTotalDiTransferByType($dataUser, 11);
//        $getMyPackage = $modePackage->getMyPackage($dataUser);
        $getNewFee = $modelWD->getNewFee($dataUser);
        $safraOut = $modelSafra->getTotalSafraOut($dataUser->id);
        $dataAll = (object) array(
            'total_bonus' => $totalBonusSafraPoin,
            'total_wd' => $totalWDSP->total_wd,
            'total_tunda' => $totalWDSP->total_tunda,
            'stock_wd' => 10000000000, //$getMyPackage->stock_wd,
            'periodeWD' => $periodeWDMember,
            'fee_tunda' => $totalWDSP->total_tunda_admin_fee,
            'fee_tuntas' => $totalWDSP->total_wd_admin_fee,
            'total_wd_range' => $totalWDRangeDate + $totalFeeRangeDate,
            'newFee' => $getNewFee,
            'belanjaSafra' => $safraOut ,
            'saldo' => (int) ($totalBonusSafraPoin - ($totalWDSP->total_wd + $totalWDSP->total_tunda + $safraOut + $totalWDSP->total_tunda_admin_fee + $totalWDSP->total_wd_admin_fee)),
        );
        return view('member.wd.bonus-safra-poin')
                ->with('dataAll', $dataAll)
                ->with('dataUser', $dataUser);
    }
    
    public function postRequestWDSafraPoin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelWD = new Transferwd;
        $codeWD = $modelWD->getCodeWD();
        $rand = rand(10, 99);
        $dataInsertWD = array(
            'user_id' => $dataUser->id,
            'user_bank' => $request->user_bank,
            'wd_code' => 'WDSP'.date('Ymd').$codeWD.$rand,
            'type' => 11,
            'wd_total' => $request->wd_get,
            'wd_date' => date('Y-m-d'),
            'admin_fee' => $request->admin_fee,
        );
        $modelWD->getInsertWD($dataInsertWD);
        return redirect()->route('m_myReportSafraPoin')
                    ->with('message', 'Withdrawal safra poin berhasil dibuat')
                    ->with('messageclass', 'success');
    }
    
    public function getMyReportSafraPoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelBonus = New Bonus;
        $modelWD = New Transferwd;
        $getData = $modelWD->getReportMemberSafraPoin($dataUser);
        return view('member.report.safra-poin')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    
}
