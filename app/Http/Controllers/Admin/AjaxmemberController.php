<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Pinsetting;
use App\Model\Package;
use App\Model\Member;
use App\Model\Validation;
use App\Model\Bank;
use App\Model\Pengiriman;
use App\Model\Pin;
use App\Model\Memberpackage;
use App\Model\Binaryhistory;
use App\Model\News;
use App\Model\Bonus;
use App\Model\Transferwd;
use App\Model\Startwd;
use App\Model\Transaction;
use App\Model\Bonussetting;
use App\Model\Sale;

class AjaxmemberController extends Controller {

    public function __construct(){
    }
    
    public function postCekAddSponsor(Request $request){
        $maxHp = 5;
        $maxEmail = 5;
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $canInsert = $modelValidasi->getCheckNewSponsor($request);
        if($canInsert->can == false){
            return view('member.ajax.confirm_add_sponsor')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        $modelMember = New Member;
        $getCheck = $modelMember->getCheckEmailPhoneUsercode($request->email, $request->hp, $request->user_code);
        if($getCheck->cekEmail >= $maxEmail){
            $canInsert = (object) array('can' => false,  'pesan' => 'Email Sudah terpakai '.$maxEmail.' kali');
            return view('member.ajax.confirm_add_sponsor')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if($getCheck->cekHP >= $maxHp){
            $canInsert = (object) array('can' => false,  'pesan' => 'No HP Sudah terpakai '.$maxHp.' kali');
            return view('member.ajax.confirm_add_sponsor')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if($getCheck->cekCode > 0){
            $canInsert = (object) array('can' => false,  'pesan' => 'Username sudah terpakai');
            return view('member.ajax.confirm_add_sponsor')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        $data = (object) array(
            'name' => $request->name,
            'email' => $request->email,
            'hp' => $request->hp,
            'username' => $request->user_code,
            'password' => $request->password,
            'package_name' => $request->name_package,
            'package_id' => $request->package_id
        );
        return view('member.ajax.confirm_add_sponsor')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function postCekAddProfile(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelMember = New Member;
        $canInsert = $modelValidasi->getCheckNewProfile($request);
        if($canInsert->can == false){
            return view('member.ajax.confirm_add_profile')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        $data = (object) array(
            'full_name' => $request->full_name,
            'gender' => $request->gender,
            'kecamatan' => $request->kecamatan,
            'alamat' => $request->alamat,
            'provinsi' => $request->provinsi,
            'kode_pos' => $request->kode_pos,
            'kota' => $request->kota,
            'wallet' => $request->wallet,
        );
        return view('member.ajax.confirm_add_profile')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekAddPackage($id_paket){
        $modePackage = New Package;
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        $getDetailPackage = $modePackage->getPackageId($id_paket);
        return view('member.ajax.confirm_add_package')
                        ->with('getData', $getDetailPackage)
                        ->with('pinSetting', $getActivePinSetting);
    }
    
    public function postCekAddPin(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modePackage = New Package;
        $canInsert = $modelValidasi->getCheckAddPin($request, $dataUser);
        if($canInsert->can == false){
            return view('member.ajax.confirm_add_pin')
                        ->with('check', $canInsert)
                        ->with('disc', null)
                        ->with('data', null);
        }
        $getDetailPackage = $modePackage->getPackageId($dataUser->package_id);
        $disc = 0;
        if($request->total_pin >= 100){
            $disc = 3; //6; //3
        }
        if($request->total_pin >= 50 && $request->total_pin < 99){
            $disc = 1; //2.5; //3
        }
        if($request->total_pin >= 25 && $request->total_pin < 49){
            $disc = 0; //1; //3
        }
        if($request->total_pin < 25){ //100
            if($request->total_pin >= $getDetailPackage->pin){
                $disc = $getDetailPackage->discount;
            }
        }
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        $hargaAwal = $getActivePinSetting->price * $request->total_pin;
        $discAwal = $hargaAwal * $disc / 100;
        $harga = $hargaAwal - $discAwal;
        $data = (object) array(
            'total_pin' => $request->total_pin,
            'harga' => $harga,
            'disc' => $disc,
            'harga_awal' => $hargaAwal
        );
        return view('member.ajax.confirm_add_pin')
                        ->with('check', $canInsert)
                        ->with('disc', $disc)
                        ->with('data', $data);
    }
    
    public function getCekOrderPin(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $modelSettingPin = New Pinsetting;
        if ($dataUser->member_type == 1) {
            $dataSettingCoin = $modelSettingPin->getActiveCoinSetting(1);
        }
        if ($dataUser->member_type == 3) {
            $dataSettingCoin = $modelSettingPin->getActiveCoinSetting(2);
        }
        $minBuy = $dataSettingCoin->buy_min;
        if($dataUser->is_active == 1){
            $minBuy = 10;
        }
        if($dataUser->member_type == 1) {
            $minBuy = $dataSettingCoin->buy_min;
        }
        if($request->total_coin < $minBuy){
            $canInsert = (object) array('can' => false, 'pesan' => 'minimal coin yang harus di beli adalah '.$minBuy);
            return view('member.ajax.confirm_add_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $harga = $dataSettingCoin->buy_price * $request->total_coin;
        $fee = $harga * $dataSettingCoin->buy_fee / 100;
        $data = (object) array(
            'total_coin' => $request->total_coin,
            'harga' => $harga,
            'fee' => $fee,
            'seller_id' => $request->s,
            'setting_id' => $request->p
        );
        return view('member.ajax.confirm_add_pin')
                        ->with('check', $canInsert)
                        ->with('data', $data);
    }
    
    public function getCekJualPin(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->total_coin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda tidak mengisi nominal koin');
            return view('member.ajax.confirm_jual_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $modelSettingPin = New Pinsetting;
        $modelTransaction = New Transaction;
        $modelCoin = New Pin;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
//        $coinPosting = $modelTransaction->getCoinPostingByUserID($dataUser->id);
//        $sisaBebas = $cekSaldo - $coinPosting->coin_posting;
        $cek = $cekSaldo - $request->total_coin;
        if($cek < 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'saldo anda tidak cukup untuk melakukan transaksi jual coin');
            return view('member.ajax.confirm_jual_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $dataSettingCoin = $modelSettingPin->getActiveCoinSettingId($request->p);
        if($dataSettingCoin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Data tidak ditemukan');
            return view('member.ajax.confirm_jual_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $harga = $dataSettingCoin->sell_price * $request->total_coin;
        $admin_fee = $harga * $dataSettingCoin->sell_fee / 100;
        $data = (object) array(
            'total_coin' => $request->total_coin,
            'price' => $harga,
            'admin_fee' => $admin_fee,
            'exchanger_id' => $request->s,
            'setting_id' => $dataSettingCoin->id
        );
        return view('member.ajax.confirm_jual_pin')
                        ->with('check', $canInsert)
                        ->with('data', $data);
    }
    
    public function getCekPostingPin(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->total_coin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda tidak mengisi nominal koin');
            return view('member.ajax.confirm_posting_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $modelSettingPin = New Pinsetting;
        $modelCoin = New Pin;
        $getSetting = $modelSettingPin->getActiveCoinPostingSetting();
        if($getSetting == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Data tidak ditemukan');
            return view('member.ajax.confirm_posting_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        if($request->total_coin < $getSetting->min_posting){
            $canInsert = (object) array('can' => false, 'pesan' => 'Coin terlalu kecil, Minimal coin untuk posting adalah '.$getSetting->min_posting);
            return view('member.ajax.confirm_posting_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        $cek = $cekSaldo - $request->total_coin;
        if($cek < 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'saldo anda tidak cukup untuk melakukan posting coin');
            return view('member.ajax.confirm_posting_pin')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $data = (object) array(
            'total_coin' => $request->total_coin,
            'phase' => $request->phase,
            'persentase' => $getSetting->persentase
        );
        return view('member.ajax.confirm_posting_pin')
                        ->with('check', $canInsert)
                        ->with('setting', $getSetting)
                        ->with('data', $data);
    }
    
    public function postCekAddPostingCoin(Request $request){
        $dataUser = Auth::user();
        //cek posting coin hari ini
        $modeTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $dateRange = $modelCoin->getRange24Hours();
        $cekLastCoin24 = $modelCoin->getPostingCoinLast24Hours($request->id, $dataUser->id, $dateRange);
        if($cekLastCoin24 != null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Dalam 24 jam terakhir, anda sudah melakukan posting coin');
            return view('member.ajax.req_posting')
                        ->with('check', $canInsert)
                        ->with('data', $request->id);
        }
        return view('member.ajax.req_posting')
                        ->with('check', $canInsert)
                        ->with('data', $request->id);
    }
    
    public function postCekClaimPostingCoin(Request $request){
        $dataUser = Auth::user();
        //cek posting coin hari ini
        $modeTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $getData = $modeTransaction->getPostingByID($request->id, $dataUser->id);
        if($getData == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Tidak ada data');
            return view('member.ajax.claim_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $dateRange = $modelCoin->getRange24Hours();
        $cekLastCoin24 = $modelCoin->getCoinPostingAvailableByID($request->id, $dataUser->id, $dateRange);
        if($cekLastCoin24 == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Tidak ada data');
            return view('member.ajax.claim_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $cekSetting = $modelCoinsetting->getIDCoinPostingSetting($getData->posting_setting_id);
        $cekSaldo = $cekLastCoin24->coin_masuk - $cekLastCoin24->coin_keluar;
        $minConvert = 0.01;
        if($cekSetting->min_convert > 0){
            $minConvert = $cekSetting->min_convert;
        }
        if(($cekSaldo - $minConvert) < 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimal convert ke Wallet '.$minConvert.' coin');
            return view('member.ajax.claim_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
//        dd($cekLastCoin24);
        $data = (object) array(
            'coin_masuk' => $cekLastCoin24->coin_masuk,
            'coin_keluar' => $cekLastCoin24->coin_keluar,
            'posting_id' => $request->id
        );
        return view('member.ajax.claim_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', $data);
    }
    
    public function postCekFinnishPostingCoin(Request $request){
        $dataUser = Auth::user();
        //cek posting coin hari ini
        $modeTransaction = New Transaction;
        $modelCoinsetting = New Pinsetting;
        $modelCoin = New Pin;
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $getData = $modeTransaction->getPostingByID($request->id, $dataUser->id);
        if($getData == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Tidak ada data');
            return view('member.ajax.tuntas_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $fase = $modelCoin->getPhaseHari();
        $totalHari = $fase * $getData->phase;
        $dateStart = $getData->created_at;
        $dateNow = date('Y-m-d H:i:s');
        $diff = abs(strtotime($dateNow) - strtotime($dateStart));  
        $years = floor($diff / (365*60*60*24));  
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
        $days = (int) floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24));
        if(($days + 1) <=  $totalHari){
            $canInsert = (object) array('can' => false, 'pesan' => 'Posting anda belum tuntas');
            return view('member.ajax.tuntas_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $dateRange = $modelCoin->getRange24Hours();
        $getDataPosting = $modeTransaction->getPostingByID($request->id, $dataUser->id);
        if($getDataPosting == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Tidak ada data');
            return view('member.ajax.tuntas_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        if($getDataPosting->status == 1){
            $canInsert = (object) array('can' => false, 'pesan' => 'Postingan ini sudah tuntas');
            return view('member.ajax.tuntas_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $cekLastCoin24 = $modelCoin->getCoinPostingAvailableByID($getDataPosting->id, $dataUser->id, $dateRange);
        $saldoCoinPosting = $cekLastCoin24->coin_masuk - $cekLastCoin24->coin_keluar;
        $total_posting = $getDataPosting->total_coin;
        $cekSaldoLast = $total_posting + $saldoCoinPosting;
        
        $data = (object) array(
            'cekSaldo' => $cekSaldoLast,
            'posting_id' => $request->id
        );
        return view('member.ajax.tuntas_coin_posting')
                        ->with('check', $canInsert)
                        ->with('data', $data);
    }
    
    public function postAddBuyerTransaction(Request $request){
        $dataUser = Auth::user();
        $modelTrans = New Transaction;
        $modelBank = New Bank;
        if($dataUser->member_type == 1){
            $getTrans = $modelTrans->getDetailTransactionsMember($request->id_trans, $dataUser);
            $getSellerBank = $modelBank->getBankPerusahaanID($request->id_bank);
        } 
        if($dataUser->member_type == 3){
            $getTrans = $modelTrans->getDetailTransactionsBuyer($request->id_trans, $dataUser);
            $getSellerBank = $modelBank->getSelectBankMemberID($request->id_bank);
        }
        $data = (object) array('id_trans' => $request->id_trans);
        return view('member.ajax.confirm_add_transaction')
                        ->with('bankSeller', $getSellerBank)
                        ->with('getData', $getTrans)
                        ->with('dataUser', $dataUser)
                        ->with('data', $data);
    }
    
    public function postRejectBuyerTransaction(Request $request){
        $data = (object) array('id_trans' => $request->id_trans);
        return view('member.ajax.confirm_reject_transaction')
                        ->with('data', $data);
    }
    
    public function postAddSellerTransaction(Request $request){
        $dataUser = Auth::user();
        $modelTrans = New Transaction;
        $modelCoin = New Pin;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        $data = (object) array('id_trans' => $request->id_trans);
        if($cekSaldo <= 0){
            return view('member.ajax.confirm_add_seller-transaction')
                        ->with('getData', null)
                        ->with('dataUser', $dataUser)
                        ->with('data', $data);
        }
        $getTrans = $modelTrans->getDetailTransactionsSeller($request->id_trans, $dataUser);
        return view('member.ajax.confirm_add_seller-transaction')
                        ->with('getData', $getTrans)
                        ->with('dataUser', $dataUser)
                        ->with('data', $data);
    }
    
    public function postRejectSellerTransaction(Request $request){
        $data = (object) array('id_trans' => $request->id_trans);
        return view('member.ajax.confirm_reject_seller-transaction')
                        ->with('data', $data);
    }
    
    public function getCekConfirmOrderPackage(Request $request){
        $dataUser = Auth::user();
        $data = (object) array('id_paket' => $request->id_paket);
        $modelPin = new Pin;
        $modelMemberPackage = New Memberpackage;
        $getData = $modelMemberPackage->getDetailMemberPackageInactive($request->id_paket, $dataUser);
        $sisaPin = $modelPin->getTotalPinMember($dataUser);
        $sum_pin_masuk = 0;
        $sum_pin_keluar = 0;
        if($sisaPin->sum_pin_masuk != null){
            $sum_pin_masuk = $sisaPin->sum_pin_masuk;
        }
        if($sisaPin->sum_pin_keluar != null){
            $sum_pin_keluar = $sisaPin->sum_pin_keluar;
        }
        $total = $sum_pin_masuk - $sum_pin_keluar;
        $totalPinOrder = $getData->total_pin;
        $lanjut = false;
        if($total >= $totalPinOrder){
            $lanjut = true;
        }
        return view('member.ajax.confirm_order')
                        ->with('getData', $getData)
                        ->with('lanjut', $lanjut)
                        ->with('data', $data);
    }
    
    public function getCekConfirmKirimPaket(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelPengiriman = new Pengiriman;
        $modelPin = new Pin;
        $canInsert = $modelValidasi->getCheckPengiriman($request);
        if($canInsert->can == false){
            return view('member.ajax.confirm_add_pengiriman')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        $cekPin = $modelPin->getTotalPinMember2($dataUser);
        $getTotalPinTerkirimDanTuntas = $modelPengiriman->getCekPinPengirimanPaketAll($dataUser);
        $sum_pin_masuk = 0;
        $sum_pin_keluar = 0;
        if($cekPin->sum_pin_masuk != null){
            $sum_pin_masuk = $cekPin->sum_pin_masuk;
        }
        if($cekPin->sum_pin_keluar != null){
            $sum_pin_keluar = $cekPin->sum_pin_keluar;
        }
        $sum_pin_masuk_terima = 0;
        if($cekPin->sum_pin_masuk_terima != null){
            $sum_pin_masuk_terima = $cekPin->sum_pin_masuk_terima;
        }
        $sum_pin_keluar_terima = 0;
        if($cekPin->sum_pin_keluar_terima != null){
            $sum_pin_keluar_terima = $cekPin->sum_pin_keluar_terima;
        }
        $sum_pin_keluar_ditransfer = 0;
        if($cekPin->sum_pin_keluar_ditransfer != null){
            $sum_pin_keluar_ditransfer = $cekPin->sum_pin_keluar_ditransfer;
        }
        $saldo_pin_kirim_paket = $sum_pin_masuk - $getTotalPinTerkirimDanTuntas->total_pin_proses_dan_tuntas - $sum_pin_keluar_ditransfer - $sum_pin_masuk_terima;
        $pin_paket_diterima = $getTotalPinTerkirimDanTuntas->total_pin_tuntas + $sum_pin_masuk_terima - $sum_pin_keluar_terima;
        if($request->total_pin > $saldo_pin_kirim_paket){
            $canInsert = (object) array('can' => false,  'pesan' => 'Sisa Saldo Kirim Paket tidak cukup untuk mengajukan kirim paket');
        }
        $kelipatan = $request->total_pin * 4;
        $day_cream = 0;
        if($request->day_cream != null){
            $day_cream = $request->day_cream;
        }
        $night_cream = 0;
        if($request->night_cream != null){
            $night_cream = $request->night_cream;
        }
        $face_toner = 0;
        if($request->face_toner != null){
            $face_toner = $request->face_toner;
        }
        $facial_wash = 0;
        if($request->facial_wash != null){
            $facial_wash = $request->facial_wash;
        }
        $total_item = $day_cream + $night_cream + $face_toner + $facial_wash;
        $data = (object) array(
            'total_pin' => $request->total_pin,
            'alamat_kirim' => $request->alamat_kirim,
            'day_cream' => $day_cream,
            'night_cream' => $night_cream,
            'face_toner' => $face_toner,
            'facial_wash' => $facial_wash,
        );
        return view('member.ajax.confirm_add_pengiriman')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekAddBank(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $canInsert = $modelValidasi->getCheckAddBank($request);
        $data = (object) array(
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'account_name' => $request->name_rek
        );
        return view('member.ajax.confirm_add_bank')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getActivateBank($id){
        $dataUser = Auth::user();
        $modelBank = New Bank;
        $getCek = $modelBank->getBankMemberID($id, $dataUser);
        return view('member.ajax.confirm_activate_bank')
                        ->with('getData', $getCek)
                        ->with('dataUser', $dataUser);
    }
        
    public function getCekUpgrade($id_paket){
        $dataUser = Auth::user();
        $modePackage = New Package;
        $modelPin = New Pin;
        $getDetailPackage = $modePackage->getPackageId($id_paket);
        $getMyPackage = $modePackage->getMyPackage($dataUser);
        $total_sisa_pin = $getDetailPackage->pin - $getMyPackage->pin;
        $cekPin =$modelPin->getTotalPinMember($dataUser);
        $sisaPin = $cekPin->sum_pin_masuk - $cekPin->sum_pin_keluar;
        $dataCek = (object) array(
            'total_sisa_pin' => $total_sisa_pin,
            'sisa_pin' => $sisaPin
        );
        $modelValidasi = New Validation;
        $canInsert = $modelValidasi->getCekPinForUpgrade($dataCek);
        return view('member.ajax.confirm_upgrade_package')
                        ->with('canInsert', $canInsert)
                        ->with('total_pin', $total_sisa_pin)
                        ->with('dataPackage', $getDetailPackage)
                        ->with('dataMyPackage', $getMyPackage);
    }
    
    public function getCekPlacementKiriKanan($id, $type){
        $posisi = 'kanan_id';
        if($type == 1){
            $posisi = 'kiri_id';
        }
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $dataUser = Auth::user();
        $modelMember = New Member;
        $getUplineId = $dataUser;
        if($id != $dataUser->id){
            $getUplineId = $modelMember->getUsers('id', $id);
        }
        if($getUplineId->$posisi != null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Posisi placement yang anda pilih telah terisi, pilih posisi yang lain');
        }
        $getDataDataCalon = $modelMember->getAllMemberToPlacement($dataUser);
        $jml = count($getDataDataCalon);
        if($jml == 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Tidak ada data member yang akan di placement');
        }
        return view('member.ajax.confirm_add_placement')
                        ->with('dataCalon', $getDataDataCalon)
                        ->with('check', $canInsert)
                        ->with('upline_id', $getUplineId->id)
                        ->with('type', $type)
                        ->with('dataUser', $dataUser);
    }
    
    public function getSearchUserCode(Request $request){
        $dataUser=  Auth::user();
        $modelMember = New Member;
        $downline = $dataUser->upline_detail.',['.$dataUser->id.']';
        if($dataUser->upline_detail == null){
            $downline = '['.$dataUser->id.']';
        }
        $getDownlineUsername = null;
        if($request->name != null){
            $getDownlineUsername = $modelMember->getMyDownlineUsername($downline, $request->name);
        }
//        dd($getDownlineUsername);
        return view('member.ajax.get_name_autocomplete')
                        ->with('getData', $getDownlineUsername)
                        ->with('dataUser', $dataUser);
    }
    
    public function getSearchUserCodeAll(Request $request){
        $dataUser=  Auth::user();
        $modelMember = New Member;
        $getDownlineUsername = null;
        if($request->name != null){
            $getDownlineUsername = $modelMember->getSearchUsernameAll($dataUser, $request->name);
        }
        return view('member.ajax.get_name_autocomplete')
                        ->with('getData', $getDownlineUsername)
                        ->with('dataUser', $dataUser);
    }
    
    public function getAjaxDetailMemberBinary(Request $request){
        $dataUser = Auth::user();
        $modelMember = New Member;
        $modelBinaryHistory = new Binaryhistory;
        $back = false;
        $downline = $dataUser->upline_detail.',['.$dataUser->id.']';
        if($dataUser->upline_detail == null){
            $downline = '['.$dataUser->id.']';
        }
        if($request->get_id != null){
            if($request->get_id != $dataUser->id){
                $back = true;
                $dataUser = $modelMember->getCekIdDownline($request->get_id, $downline);
            }
        }
        if($dataUser == null){
            return redirect()->route('mainDashboard');
        }
        $kanan = 0;
        if($dataUser->kanan_id != null){
            $downlineKanan = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$dataUser->kanan_id.']';
            if($dataUser->upline_detail == null){
                $downlineKanan = '['.$dataUser->id.']'.',['.$dataUser->kanan_id.']';
            }
            $kananLuar = $modelMember->getNewCountMyDownline($downlineKanan, $dataUser->id);
            $kananPas = $modelMember->getKaKiTerPendek($dataUser->kanan_id, $dataUser->id);
            $kanan = $kananLuar + $kananPas;
        }
        $kiri = 0;
        if($dataUser->kiri_id != null){
            $downlineKiri = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$dataUser->kiri_id.']';
            if($dataUser->upline_detail == null){
                $downlineKiri = '['.$dataUser->id.']'.',['.$dataUser->kiri_id.']';
            }
            $kiriLuar = $modelMember->getNewCountMyDownline($downlineKiri, $dataUser->id);
            $kiriPas = $modelMember->getKaKiTerPendek($dataUser->kiri_id, $dataUser->id);
            $kiri = $kiriLuar + $kiriPas;
        }
        $getHistoryBinary = $modelBinaryHistory->getBinaryHistory($dataUser->id);
        $kiriCheck = $kiri - $getHistoryBinary->sum_total_kiri;
        $kananCheck = $kanan - $getHistoryBinary->sum_total_kanan;
        if($kiriCheck > 0 && $kananCheck > 0){
            $pasangan = $kiriCheck;
            if($kiriCheck > $kananCheck){
                $pasangan = $kananCheck;
            }
            if($kiriCheck < $kananCheck){
                $pasangan = $kiriCheck;
            }
        }
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        $dataUserDetail = (object) array(
            'kanan' => $kanan,
            'kiri' => $kiri,
            'sisaKanan' => $kananCheck,
            'sisaKiri' => $kiriCheck,
            'omzetKiri' => $kiri * $getActivePinSetting->price,
            'omzetKanan' => $kanan * $getActivePinSetting->price
        );
        return view('member.ajax.detail-binary')
                        ->with('detailUser', $dataUserDetail)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekDetailNews($id){
        $dataUser = Auth::user();
        $modelNews = New News;
        $getData = $modelNews->getNewsByField('id', $id);
        return view('member.ajax.detail-news')
                        ->with('getData', $getData)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekReqWDBonusSponsor(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $modelBank = New Bank;
        $modePackage = New Package;
        $modelWDStart = new Startwd;
        $cekDateStart =$modelWDStart->getStartEndWDMember($dataUser->id_user);
        $periodeWDMember = date('Y-m-d', strtotime($dataUser->active_at));
        if($cekDateStart != null){
            $periodeWDMember = $cekDateStart->start_date;
        }
        if(!is_numeric($request->input_jml_wd)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Jumlah nominal WD harus dalam angka');
            return view('member.ajax.confirm_add_wd_sp')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $totalBonus = $request->input_jml_wd;
        $totalBonusAll = $modelBonus->getTotalBonusSponsor($dataUser);
        if($totalBonusAll == 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda tidak memiliki bonus Sponsor');
            return view('member.ajax.confirm_add_wd_sp')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $totalWD = $modelWD->getTotalDiTransferByType($dataUser, 1);
        $totalWDRangeDate = $modelWD->getTotalWDRangeDate($dataUser->id, $periodeWDMember);
        $totalFeeRangeDate = $modelWD->getTotalWDFeeAdminRangeDate($dataUser->id, $periodeWDMember);
        $getMyActiveBank = $modelBank->getBankMemberActive($dataUser);
        $getNewFee = $modelWD->getNewFee($dataUser);
//        $getMyPackage = $modePackage->getMyPackage($dataUser);
        $id_bank = null;
        if($getMyActiveBank != null){
            $id_bank = $getMyActiveBank->id;
        }
//        $admin_fee = 10;
        $biaya_admin = $getNewFee; //floor(($totalBonus * $admin_fee) / 100);
        $totalBonusDidapat = floor($totalBonus - $biaya_admin);
        $dataAll = (object) array(
            'req_wd' => (int) $totalBonus,
            'wd_get' => $totalBonusDidapat,
            'total_bonus_get' => $totalBonusAll,
            'total_wd' => $totalWD->total_wd,
            'total_wd_range' => $totalWDRangeDate + $totalFeeRangeDate,
            'total_tunda' => $totalWD->total_tunda,
            'saldo' => (int) ($totalBonusAll - ($totalWD->total_wd + $totalWD->total_tunda )),
            'admin_fee' => $biaya_admin,
            'bank' => $id_bank,
            'kuota_wd' => 10000000000, //$getMyPackage->stock_wd,
            'wd_fee' => $totalWD->total_wd_admin_fee,
            'wd_tunda_fee' => $totalWD->total_tunda_admin_fee,
            'total_sponsor' => $dataUser->total_sponsor,
            'total_placement' => $dataUser->total_placement
        );
//        dd($dataAll);
        $canInsert = $modelValidasi->getCheckWDSponsor($dataAll);
         return view('member.ajax.confirm_add_wd_sp')
                        ->with('check', $canInsert)
                        ->with('data', $dataAll);
    }
    
    public function getCekReqWDBonusBinary(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $modelBank = New Bank;
        $modePackage = New Package;
        $modelWDStart = new Startwd;
        $cekDateStart =$modelWDStart->getStartEndWDMember($dataUser->id_user);
        $periodeWDMember = date('Y-m-d', strtotime($dataUser->active_at));
        if($cekDateStart != null){
            $periodeWDMember = $cekDateStart->start_date;
        }
        if(!is_numeric($request->input_jml_wd)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Jumlah nominal WD harus dalam angka');
            return view('member.ajax.confirm_add_wd_binary')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $totalBonus = $request->input_jml_wd;
//        $totalBonusAll = $modelBonus->getTotalBonusBinary($dataUser);
        $totalBonusAll = $modelBonus->getTotalBonusBinaryNew($dataUser);
        if($totalBonusAll == 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda tidak memiliki bonus Pasangan');
            return view('member.ajax.confirm_add_wd_binary')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $totalWD = $modelWD->getTotalDiTransferByType($dataUser, 2);
        $totalWDRangeDate = $modelWD->getTotalWDRangeDate($dataUser->id, $periodeWDMember);
        $totalFeeRangeDate = $modelWD->getTotalWDFeeAdminRangeDate($dataUser->id, $periodeWDMember);
        $getMyActiveBank = $modelBank->getBankMemberActive($dataUser);
//        $getMyPackage = $modePackage->getMyPackage($dataUser);
        $getNewFee = $modelWD->getNewFee($dataUser);
        $id_bank = null;
        if($getMyActiveBank != null){
            $id_bank = $getMyActiveBank->id;
        }
//        $admin_fee = 10;
        $biaya_admin = $getNewFee; //floor(($totalBonus * $admin_fee) / 100);
        $totalBonusDidapat = floor($totalBonus - $biaya_admin);
        $dataAll = (object) array(
            'req_wd' => (int) $totalBonus,
            'wd_get' => $totalBonusDidapat,
            'total_bonus_get' => $totalBonusAll,
            'total_wd' => $totalWD->total_wd,
            'total_wd_range' => $totalWDRangeDate + $totalFeeRangeDate,
            'total_tunda' => $totalWD->total_tunda,
            'saldo' => (int) ($totalBonusAll - ($totalWD->total_wd + $totalWD->total_tunda )),
            'admin_fee' => $biaya_admin,
            'bank' => $id_bank,
            'kuota_wd' => 10000000000, //$getMyPackage->stock_wd,
            'wd_fee' => $totalWD->total_wd_admin_fee,
            'wd_tunda_fee' => $totalWD->total_tunda_admin_fee,
            'total_sponsor' => $dataUser->total_sponsor,
            'total_placement' => $dataUser->total_placement
        );
//        dd($dataAll);
        $canInsert = $modelValidasi->getCheckWD($dataAll);
         return view('member.ajax.confirm_add_wd_binary')
                        ->with('check', $canInsert)
                        ->with('data', $dataAll);
    }
    
    public function getCekReqClaimReward($id){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelBonusSetting = New Bonussetting;
        $modelMember = new Member;
        $modelWD = new Transferwd;
        $modelBank = New Bank;
        $getCekClaim = $modelWD->getMemberBonusRewardByUser($dataUser, $id);
        if($getCekClaim != null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda pernah claim reward bonus ini');
            return view('member.ajax.confirm_claim_reward')
                        ->with('check', $canInsert)
                        ->with('data', null);
        }
        $getData = $modelBonusSetting->getAllBonusRewardByID($id);
        $kanan = 0;
        if($dataUser->kanan_id != null){
            $downlineKanan = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$dataUser->kanan_id.']';
            if($dataUser->upline_detail == null){
                $downlineKanan = '['.$dataUser->id.']'.',['.$dataUser->kanan_id.']';
            }
            $kananLuar = $modelMember->getNewCountMyDownlineReward($downlineKanan);
            $kananPas = $modelMember->getKaKiTerPendekReward($dataUser->kanan_id);
            $kanan = $kananLuar + $kananPas;
        }
        $kiri = 0;
        if($dataUser->kiri_id != null){
            $downlineKiri = $dataUser->upline_detail.',['.$dataUser->id.']'.',['.$dataUser->kiri_id.']';
            if($dataUser->upline_detail == null){
                $downlineKiri = '['.$dataUser->id.']'.',['.$dataUser->kiri_id.']';
            }
            $kiriLuar = $modelMember->getNewCountMyDownlineReward($downlineKiri);
            $kiriPas = $modelMember->getKaKiTerPendekReward($dataUser->kiri_id);
            $kiri = $kiriLuar + $kiriPas;
        }
        $getMyActiveBank = $modelBank->getBankMemberActive($dataUser);
        $dataAll = (object) array(
            'kanan' => $kanan,
            'kiri' => $kiri,
            'getData' => $getData,
            'bank' => $getMyActiveBank
        );
        $canInsert = $modelValidasi->getCheckClaimReward($dataAll);
         return view('member.ajax.confirm_claim_reward')
                        ->with('check', $canInsert)
                        ->with('data', $dataAll);
    }
    
    public function getCekBuyShoping(Request $request){
        $dataUser = Auth::user();
        $idPurchase = $request->id_barang;
        $quantity = $request->total_buy;
        $modelSafra = New Safra;
        $modelBonus = new Bonus;
        $modelPackage = New Package;
        
        $getDataBarang = $modelSafra->getPurchaseByID($idPurchase);
        $totalMySafra = $modelBonus->getTotalSafra($dataUser);
        $getMyPackage = $modelPackage->getPackageId($dataUser->package_id);
        $safraOut = $modelSafra->getTotalSafraOut($dataUser->id);
        $saldoSafra = $totalMySafra->total_safra - $safraOut;
        $safraDiscount = ($quantity * $getDataBarang->main_price * $getMyPackage->safra_discount) / 100;
        if($saldoSafra < $safraDiscount){
            $safraDiscount = $saldoSafra;
        }
        $priceDibayar = ($quantity * $getDataBarang->main_price) - $safraDiscount;
        $data = (object) array(
            'dibayar' => $priceDibayar,
            'potongan' => $safraDiscount,
            'dataBarang' => $getDataBarang,
            'total_buy' => $quantity
        );
        return view('member.ajax.confirm_buy_barang')
                        ->with('data', $data);
    }
    
    public function getCekEditPassword(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->password == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password harus diisii');
            return view('member.ajax.confirm_edit_password')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert);
        }
        if(strpos($request->repassword, ' ') !== false){
            $canInsert = (object) array('can' => false, 'pesan' => 'Ketik ulang password harus diisi');
            return view('member.ajax.confirm_edit_password')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert);
        }
        if($request->password != $request->repassword){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password tidak sama');
            return view('member.ajax.confirm_edit_password')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert);
        }
        if(strlen($request->password) < 6){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password terlalu pendek, minimal 6 karakter');
            return view('member.ajax.confirm_edit_password')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert);
        }
        $data = (object) array(
            'password' => $request->password
        );
        return view('member.ajax.confirm_edit_password')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    
    public function postCekAddStockist(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $canInsert = $modelValidasi->getCheckNewSponsor($request);
        $modelMember = New Member;
        $getCheck = $modelMember->getCheckEmailPhoneUsercode($request->email, $request->hp, $request->user_code);
        if($getCheck->cekCode == 1){
            $canInsert = (object) array('can' => false,  'pesan' => 'Username sudah terpakai');
        }
        $data = (object) array(
            'name' => $request->name,
            'email' => $request->email,
            'hp' => $request->hp,
            'username' => $request->user_code,
            'password' => $request->password,
        );
        return view('member.ajax.confirm_add_stockist')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function postAddOpeningMember(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $canInsert = $modelValidasi->getCheckNewSponsor($request);
        $modelMember = New Member;
        $getCheck = $modelMember->getCheckEmailPhoneUsercode($request->email, $request->hp, $request->user_code);
        if($getCheck->cekCode == 1){
            $canInsert = (object) array('can' => false,  'pesan' => 'Username sudah terpakai');
        }
        $data = (object) array(
            'name' => $request->name,
            'email' => $request->email,
            'hp' => $request->hp,
            'username' => $request->user_code,
            'password' => $request->password,
            'package_id' => $request->package_id,
        );
        return view('member.ajax.confirm_add_openingmember')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekPinCode(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->pin_code == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi kode pin');
            return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if($request->re_pin_code == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi ketik ulang kode pin');
            return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if($request->pin_code != $request->re_pin_code){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kode pin dan ketik ulang harus sama persis');
            return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if(!is_numeric($request->pin_code)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kode Pin harus menggunakan angka');
            return $canInsert;
        }
        if(strlen($request->pin_code) < 6){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kode Pin tidak boleh kurang dari 6 digit');
            return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        if(strlen($request->pin_code) > 6){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kode Pin tidak boleh lebih dari 6 digit');
            return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', null)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
        }
        $data = (object) array(
            'pin_code' => $request->pin_code
        );
        return view('member.ajax.confirm_pin_code')
                        ->with('dataRequest', $data)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekTransferCoin(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->to_id == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'anda tidak memilih data member tujuan');
            return view('member.ajax.confirm_transfer_pin')
                            ->with('dataRequest', null)
                            ->with('check', $canInsert)
                            ->with('dataUser', $dataUser);
        }
        $modelMember = new Member;
        $modelCoin = New Pin;
        $to_id = $request->to_id;
        $qty = $request->qty;
        $to_member = $modelMember->getUsers('id', $to_id);
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
        if($cekSaldo <= 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'message', 'anda tidak memiliki saldo, harap membeli saldo kepada perusahaan');
            return view('member.ajax.confirm_transfer_pin')
                            ->with('dataRequest', null)
                            ->with('check', $canInsert)
                            ->with('dataUser', $dataUser);
        }
        $sisa = $cekSaldo - $qty;
        if($sisa < 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'message', 'anda tidak memiliki saldo, harap membeli saldo kepada perusahaan');
            return view('member.ajax.confirm_transfer_pin')
                            ->with('dataRequest', null)
                            ->with('check', $canInsert)
                            ->with('dataUser', $dataUser);
        }
        return view('member.ajax.confirm_transfer_pin')
                        ->with('dataRequest', $qty)
                        ->with('to_member', $to_member)
                        ->with('check', $canInsert)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekExploreStockist($type, Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3, 10, 11);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return dd('forbiden');
        }
        $modelMember = new Member;
        $getData = null;
        if($type == 'stockist'){
            if($request->name != null){
                if(strlen($request->name) >= 3){
                    $getData = $modelMember->getSearchMemberStockist($dataUser->id, $request->name);
                }
            }
        }
        return view('member.ajax.cek_suggestion')
                        ->with('getData', $getData)
                        ->with('type', $type)
                        ->with('dataUser', $dataUser);
    }
    
    public function getCekExploreMember($type, Request $request){
        $dataUser = Auth::user();
        $modelMember = new Member;
        $getData = null;
        if($type == 'member'){
            if($request->name != null){
                if(strlen($request->name) >= 3){
                    $getData = $modelMember->getSearchAllMember($request->name);
                }
            }
        }
        return view('member.ajax.cek_suggestion')
                        ->with('getData', $getData)
                        ->with('type', $type)
                        ->with('dataUser', $dataUser);
    }
    
    public function postAddTransferPin(Request $request){
        $dataUser = Auth::user();
        $modelTrans = New Transaction;
        $getTrans = $modelTrans->getTransferPinMasukDetail($request->id_trans, $dataUser->id);
        $data = (object) array('id_trans' => $request->id_trans);
        return view('member.ajax.confirm_add_transferpin')
                        ->with('getData', $getTrans)
                        ->with('dataUser', $dataUser)
                        ->with('data', $data);
    }
    
    public function postRejectTransferPin(Request $request){
        $data = (object) array('id_trans' => $request->id_trans);
        return view('member.ajax.confirm_reject_transferpin')
                        ->with('data', $data);
    }
    
    public function getCekClaimBonusPHU(Request $request){
        $dataUser = Auth::user();
        $modelBonus = new Bonus;
        $modelBonusSetting = new Bonussetting;
        $getData = $modelBonus->getCekMemberHaveBonusFly($dataUser->id, $request->b_id);
        $phuSetting = $modelBonusSetting->getActiveBonusClaimPHU();
        return view('member.ajax.confirm_claim_phu')
                        ->with('phuSetting', $phuSetting)
                        ->with('bonus_id', $request->b_id)
                        ->with('getData', $getData);
        
    }
    
    public function getCekClaimActivatedProduct(Request $request){
        $dataUser = Auth::user();
        $modelValidasi = New Validation;
        $modelMember = New Member;
        $canInsert = $modelValidasi->getCheckKirimProduk($request);
        if($canInsert->can == false){
            return view('member.ajax.confirm_claim_produkaktifasi')
                        ->with('check', $canInsert)
                        ->with('getData', null);
        }
        $data = (object) array(
            'full_name' => $request->full_name,
            'no_hp' => $request->no_hp,
            'kecamatan' => $request->kecamatan,
            'alamat' => $request->alamat,
            'provinsi' => $request->provinsi,
            'kelurahan' => $request->kelurahan,
            'kota' => $request->kota,
            'id' => $dataUser->id
        );
        return view('member.ajax.confirm_claim_produkaktifasi')
                        ->with('check', $canInsert)
                        ->with('dataRequest', $data);
    }
    
    public function getCekClaimBonusPoinID($id){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $modelBonus = New Bonus;
        $modelBonusSetting = New Bonussetting;
        $modelWD = New Transferwd;
        $totBonusPoin = $modelBonus->getSumBonusNew($dataUser->id, 2);
        $getSettingPoin = $modelBonusSetting->getActiveBonusClaimPoinID($id);
        $totWDPoin = $modelWD->getSumWDPoin($dataUser->id);
        $saldo = $totBonusPoin - $totWDPoin;
        $jml_poin = $getSettingPoin->jml_poin;
        if($jml_poin > $saldo){
            $canInsert = (object) array('can' => false, 'pesan' => 'Saldo anda tidak cukup untuk claim poin');
            return view('member.ajax.confirm_claim_poin')
                        ->with('check', $canInsert)
                        ->with('dataRequest', null);
        }
        return view('member.ajax.confirm_claim_poin')
                        ->with('check', $canInsert)
                        ->with('saldo', $saldo)
                        ->with('dataRequest', $getSettingPoin);
    }
    
    public function getAjaxBelanja(Request $request){
        $dataUser = Auth::user();
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $cekArray = json_decode($request->cart_list, true);
        if(empty($cekArray)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum memilih barang belanja');
            return view('member.ajax.confirm_belanja')
                    ->with('dataRequest', null)
                    ->with('check', $canInsert)
                    ->with('dataUser', $dataUser);
        }
        return view('member.ajax.confirm_belanja')
                    ->with('dataRequest', $cekArray)
                    ->with('check', $canInsert)
                    ->with('dataUser', $dataUser);
    }
    
    public function getAjaxConfirmBelanja(Request $request){
        $dataUser = Auth::user();
        $modelSale = New Sale;
        $modelBank = New Bank;
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $bankPerusahaan = $modelBank->getBankPerusahaanID($request->id_bank);
        $getData = $modelSale->getDetailTransactionByUserId($request->id_trans, $dataUser->id);
        return view('member.ajax.confirm_transfer_belanja')
                    ->with('dataRequest', $getData[0])
                    ->with('check', $canInsert)
                    ->with('bank', $bankPerusahaan)
                    ->with('dataUser', $dataUser);
    }
    
    public function getAjaxRejectBelanja(Request $request){
        $dataUser = Auth::user();
        $modelSale = New Sale;
        $modelBank = New Bank;
        $canInsert = (object) array('can' => true, 'pesan' => '');
        $getData = $modelSale->getDetailTransactionByUserId($request->id_trans, $dataUser->id);
        return view('member.ajax.confirm_reject_belanja')
                    ->with('dataRequest', $getData[0])
                    ->with('check', $canInsert)
                    ->with('dataUser', $dataUser);
    }
    
    
    

    
    
    
    
    
}
