<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Model\Admin;
use App\Model\Pinsetting;
use App\Model\Package;
use App\Model\Transaction;
use App\Model\Pin;
use App\Model\Bank;
use App\Model\Member;
use App\Model\Masterpin;
use App\Model\Pengiriman;
use App\Model\Bonussetting;
use App\Model\Membership;
use App\Model\Repeatorder;
use App\Model\News;
use App\Model\Transferwd;
use App\Model\Bonus;
use App\Model\Sale;

class MasterAdminController extends Controller {

    public function __construct(){

    }

    public function getAddAdmin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelAdmin = New Admin;
        $getAllAdmin = $modelAdmin->getAllUserAdmin($dataUser);
        return view('admin.user.create-user')
                ->with('headerTitle', 'Admin')
                ->with('getAllAdmin', $getAllAdmin)
                ->with('dataUser', $dataUser);
    }

    public function postAddAdmin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($request->email == null || $request->user_code == null || $request->password == null || $request->repassword == null){
            return redirect()->route('addCrew')
                ->with('message', 'Data-data harus diisi')
                ->with('messageclass', 'danger');
        }
        if($request->password != $request->repassword){
            return redirect()->route('addCrew')
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        if(strpos($request->user_code, ' ') !== false){
            return redirect()->route('addCrew')
                ->with('message', 'Username tidak boleh ada spasi')
                ->with('messageclass', 'danger');
        }
         $modelAdmin = New Admin;
         $cekUsername = $modelAdmin->getCekNewUsername($request->user_code);
         if($cekUsername != null){
            return redirect()->route('addCrew')
                ->with('message', 'Gunakan username yang lain')
                ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'user_code' => $request->user_code,
            'is_active' => 1,
            'user_type' => $request->user_type,
            'name' => $request->user_code,
        );
        $modelAdmin->getInsertUser($dataInsert);
        return redirect()->route('addCrew')
                ->with('message', 'Create New Admin Success')
                ->with('messageclass', 'success');
    }

    public function postEditRemoveAdmin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelAdmin = New Admin;
        if($request->type == 1){
            if($request->email == null || $request->name == null || $request->user_code == null || $request->password == null || $request->repassword == null){
                return redirect()->route('addCrew')
                    ->with('message', 'Data harus diisi')
                    ->with('messageclass', 'danger');
            }
            if($request->password != $request->repassword){
                return redirect()->route('addCrew')
                    ->with('message', 'Password tidak sama')
                    ->with('messageclass', 'danger');
            }
            if(strpos($request->user_code, ' ') !== false){
                return redirect()->route('addCrew')
                    ->with('message', 'Username tidak boleh ada spasi')
                    ->with('messageclass', 'danger');
            }
             $cekUsername = $modelAdmin->getCekNewUsername($request->user_code);
             if($cekUsername != null){
                return redirect()->route('addCrew')
                    ->with('message', 'Gunakan username yang lain')
                    ->with('messageclass', 'danger');
            }
            $dataUpdate = array(
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'name' => $request->name,
                'user_code' => $request->user_code,
            );
            $modelAdmin->getUpdateMember('id', $request->cekId, $dataUpdate);
            return redirect()->route('addCrew')
                    ->with('message', 'Update Data Admin Success')
                    ->with('messageclass', 'success');
        }
        if($request->type == 2){
            $dataUpdate = array(
                'is_login' => 0
            );
            $modelAdmin->getUpdateMember('id', $request->cekId, $dataUpdate);
            return redirect()->route('addCrew')
                    ->with('message', 'Delete Data Admin Success')
                    ->with('messageclass', 'success');
        }

    }

    //Create Exchanger
    public function getAddExchanger(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelAdmin = New Admin;
        $getData = $modelAdmin->getAllUserExchanger();
        return view('admin.user.create-exchanger')
                ->with('headerTitle', 'Exchanger')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postAddExchanger(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($request->email == null || $request->user_code == null || $request->password == null || $request->repassword == null || $request->name == null || $request->hp == null){
            return redirect()->route('addExchanger')
                ->with('message', 'Data-data harus diisi')
                ->with('messageclass', 'danger');
        }
        if($request->password != $request->repassword){
            return redirect()->route('addExchanger')
                ->with('message', 'Password tidak sama')
                ->with('messageclass', 'danger');
        }
        if(strpos($request->user_code, ' ') !== false){
            return redirect()->route('addExchanger')
                ->with('message', 'Username tidak boleh ada spasi')
                ->with('messageclass', 'danger');
        }
         $modelAdmin = New Admin;
         $cekUsername = $modelAdmin->getCekNewUsername($request->user_code);
         if($cekUsername != null){
            return redirect()->route('addExchanger')
                ->with('message', 'Gunakan username yang lain')
                ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'user_code' => $request->user_code,
            'hp' => $request->hp,
            'is_active' => 1,
            'user_type' => 10,
            'member_type' => 1,
            'active_at' => date('Y-m-d H:i:s')
        );
        $modelAdmin->getInsertUser($dataInsert);
        return redirect()->route('addExchanger')
                ->with('message', 'Berhasil buat exchanger baru')
                ->with('messageclass', 'success');
    }

    //Setting
    public function getListCoinSetting(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getData = $modelSettingCoin->getAdminCoinSetting();
        return view('admin.pin.list-pinsetting')
                ->with('headerTitle', 'Coin Setting')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getCoinSetting($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getCoinSetting = $modelSettingCoin->getActiveCoinSettingId($id);
        return view('admin.pin.pin-setting')
                ->with('headerTitle', 'Coin Setting')
                ->with('data', $getCoinSetting)
                ->with('dataUser', $dataUser);
    }

    public function postCoinSetting(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $price = $request->price;
        $modelSettingCoin = New Pinsetting;
        $remove = array(
            'is_active' => 0
        );
        $modelSettingCoin->getUpdateCoinSetting($request->id, $remove);
        $dataInsert = array(
            'type' => $request->type,
            'is_active' => 1,
            'buy_price' => $request->buy_price,
            'sell_price' => $request->sell_price,
            'buy_fee' => $request->buy_fee,
            'sell_fee' => $request->sell_fee,
            'buy_min' => $request->buy_min,
            'sell_min' => $request->sell_min
        );
        $modelSettingCoin->getInsertCoinSetting($dataInsert);
        return redirect()->route('listSettingCoin')
                ->with('message', 'Ganti Harga Coin Berhasil')
                ->with('messageclass', 'success');
    }

    public function getListCoinSettingBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getData = $modelSettingCoin->getAdminCoinSettingApp();
        return view('admin.setting.bonus-setting')
                ->with('headerTitle', 'Bonus Setting')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getCoinSettingBonus($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getCoinSetting = $modelSettingCoin->getActiveCoinSettingApp($id);
        return view('admin.setting.edit-bonus-setting')
                ->with('headerTitle', 'Edit Bonus Setting')
                ->with('getData', $getCoinSetting)
                ->with('dataUser', $dataUser);
    }

    public function postCoinSettingBonus(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $remove = array(
            'is_active' => 0
        );
        $modelSettingCoin->getUpdateCoinSettingApp($request->id, $remove);
        $dataInsert = array(
            'is_active' => 1,
            'bonus_sponsor' => $request->bonus_sponsor,
            'bonus_profit' => $request->bonus_profit,
            'max_user_profit' => $request->max_user_profit,
        );
        $modelSettingCoin->getInsertCoinSettingApp($dataInsert);
        return redirect()->route('listSettingCoinBonus')
                ->with('message', 'Berhasil edit')
                ->with('messageclass', 'success');
    }

    public function getListCoinSettingPosting(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getData = $modelSettingCoin->getAllActiveCoinPostingSetting();
        return view('admin.setting.coin-setting-posting')
                ->with('headerTitle', 'Coin Posting Setting')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getCoinSettingPosting($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $getData = $modelSettingCoin->getActiveCoinPostingSetting($id);
        return view('admin.setting.editcoin-setting-posting')
                ->with('headerTitle', 'Coin Posting Setting')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postCoinSettingPosting(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingCoin = New Pinsetting;
        $remove = array(
            'is_active' => 0
        );
        $modelSettingCoin->getUpdateCoinSettingPosting($request->id, $remove);
        $dataInsert = array(
            'is_active' => 1,
            'persentase' => $request->persentase,
            'min_posting' => $request->min_posting,
            'min_convert' => $request->min_convert,
        );
        $modelSettingCoin->getInsertCoinSettingPosting($dataInsert);
        return redirect()->route('listSettingCoinPosting')
                ->with('message', 'Berhasil edit')
                ->with('messageclass', 'success');
    }

    public function getAllPackage(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPackage = New Package;
        $getAllPackage = $modelPackage->getAllPackage();
        return view('admin.package.package-list')
                ->with('headerTitle', 'Package')
                ->with('package', $getAllPackage)
                ->with('dataUser', $dataUser);
    }

    public function postUpdatePackage(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPackage = New Package;
        $dataUpdate = array(
            'name' => $request->name,
            'code' => $request->code,
            'image' => $request->image,
            'price' => $request->price,
            'stockist_price' => $request->stockist_price,
            'm_stockist_price' => $request->m_stockist_price
        );
        $modelPackage->getUpdatePackage($request->cekId, $dataUpdate);
        return redirect()->route('allPackage')
                ->with('message', 'Update Package '.$request->name.' berhasil')
                ->with('messageclass', 'success');
    }

    public function getListTransactions(Request $request){
        $status = $request->s;
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSettingTrans = New Transaction;
        $getAllTransaction = $modelSettingTrans->getTransactionsSellerByAdmin();
        return view('admin.pin.list-transaction')
                ->with('headerTitle', 'Transaksi')
                ->with('getData', $getAllTransaction)
                ->with('dataUser', $dataUser);
    }

    public function postConfirmTransaction(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $id = $request->cekId;
        $user_id = $request->cekMemberId;
        $modelSettingTrans = New Transaction;
        $modelPin = New Pin;
        $modelMasterpin = New Masterpin;
        $getData = $modelSettingTrans->getDetailTransactionSellerAdmin($id, $user_id);
        if($getData == null){
            return redirect()->route('adm_listTransaction')
                ->with('message', 'Data tidak ditemukan')
                ->with('messageclass', 'danger');
        }
        $dataUpdate = array(
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $modelSettingTrans->getUpdateTransaction('id', $id, $dataUpdate);
        if($getData->type == 1){
            
            //coin exhcanger bertambah
            $dataInsertCoinMember = array(
                'user_id' => $user_id,
                'transaction_id' => $id,
                'type' => 1,
                'qty' => $getData->total_coin,
                'price' => 0
            );
            $modelPin->getInsertMemberCoin($dataInsertCoinMember);

            //Coin Perusahaan Berkurang
            $dataInsertCoinKurang = array(
                'total_coin' => $getData->total_coin,
                'type' => 2,
                'transaction_id' => $id
            );
            $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang);
            $message = 'Berhasil konfirmasi exchanger beli coin';
        }
        
        if($getData->type == 3){
            //coin exhcanger berkurang
            $dataInsertCoinMember = array(
                'user_id' => $user_id,
                'transaction_id' => $id,
                'type' => 2,
                'qty' => $getData->total_coin,
                'price' => 0
            );
            $modelPin->getInsertMemberCoin($dataInsertCoinMember);

            //Coin Perusahaan bertambah
            $dataInsertCoinKurang = array(
                'total_coin' => $getData->total_coin,
                'type' => 1,
                'transaction_id' => $id
            );
            $modelMasterpin->getInsertMasterCoin($dataInsertCoinKurang);
            $message = 'Berhasil konfirmasi exchanger jual coin';
        }
        

        return redirect()->route('adm_listTransaction')
                ->with('message', $message)
                ->with('messageclass', 'success');

    }

    public function getBankPerusahaan(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBank = new Bank;
        $getPerusahaanBank = $modelBank->getBankPerusahaan();
        return view('admin.bank.list-bank')
                ->with('headerTitle', 'Bank Perusahaan')
                ->with('getData', $getPerusahaanBank)
                ->with('dataUser', $dataUser);
    }

    public function postBankPerusahaan(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBank = new Bank;
        $getPerusahaanBank = $modelBank->getBankPerusahaanID($request->id);
        $dataUpdate = array(
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'account_name' => $request->account_name,
        );
        $modelBank->getUpdateBank('id', $request->id, $dataUpdate);
        return redirect()->route('adm_bankPerusahaan')
                ->with('message', 'Berhasil update bank perusahaan')
                ->with('messageclass', 'success');
    }

    public function getAddBankPerusahaan(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.bank.add-bank')
                ->with('headerTitle', 'Bank Perusahaan')
                ->with('dataUser', $dataUser);
    }

    public function postAddBankPerusahaan(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBank = new Bank;
        $dataInsert = array(
            'user_id' => 2,
            'bank_name' => $request->bank_name,
            'account_no' => $request->account_no,
            'account_name' => $request->account_name,
            'bank_type' => 1,
            'active_at' => date('Y-m-d H:i:s')
        );
        $modelBank->getInsertBank($dataInsert);
        return redirect()->route('adm_bankPerusahaan')
                ->with('message', 'Berhasil tambah bank perusahaan')
                ->with('messageclass', 'success');
    }

    public function getListKirimPaket(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPengiriman = new Pengiriman;
        $getAllPengiriman = $modelPengiriman->getAdmPengiriman();
        return view('admin.pin.kirim-paket')
                ->with('headerTitle', 'Kirim Paket')
                ->with('getData', $getAllPengiriman)
                ->with('dataUser', $dataUser);
    }

    public function getKirimPaketByID($id, $user_id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPengiriman = new Pengiriman;
        $getPengiriman = $modelPengiriman->getAdmPengirimanByID($id, $user_id);
        return view('admin.pin.kirim-paket-detail')
                ->with('headerTitle', 'Konfirmasi Pengiriman')
                ->with('getData', $getPengiriman)
                ->with('dataUser', $dataUser);
    }

    public function postConfirmKirimPaket(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPengiriman = new Pengiriman;
        $getPengiriman = $modelPengiriman->getAdmPengirimanByID($request->cekId, $request->cekUserId);
        if($getPengiriman == null){
            return redirect()->route('adm_listKirimPaket')
                    ->with('message', 'Data tidak ditemukan')
                    ->with('messageclass', 'danger');
        }
        $dataUpdate = array(
            'status' => 1,
            'kirim_at' => date('Y-m-d H:i:s'),
            'kurir_name' => $request->kurir_name,
            'no_resi' => $request->no_resi
        );
        $modelPengiriman->getUpdatePengiriman($getPengiriman->id, $dataUpdate);
        return redirect()->route('adm_listKirimPaket')
                    ->with('message', 'Paket sudah dikirim')
                    ->with('messageclass', 'success');
    }

    public function getBonusStart(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonusSetting = new Bonussetting;
        $getBonusStart =$modelBonusSetting->getActiveBonusStart();
        return view('admin.setting.bonus-start')
                ->with('headerTitle', 'Setting Bonus Sponsor')
                ->with('getData', $getBonusStart)
                ->with('dataUser', $dataUser);
    }

    public function postBonusStart(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonusSetting = new Bonussetting;
        $dataUpdate = array(
            'is_active' => 0,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelBonusSetting->getUpdateBonusStart('is_active', 1, $dataUpdate);
        $dataInsert = array(
            'start_price' => $request->start_price,
            'created_by' => $dataUser->id
        );
        $modelBonusSetting->getInsertBonusStart($dataInsert);
        return redirect()->route('adm_bonusStart')
                    ->with('message', 'Edit bonus sponsor berhasil')
                    ->with('messageclass', 'success');
    }

    public function getAddMasterCoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMasterCoin = new Masterpin;
        $getTotalCoin = $modelMasterCoin->getTotalCoinPerusahaan();
//        $cek = $modelMasterCoin->getHistoryMasterCoinTransaction();
        return view('admin.pin.master-pin')
                ->with('headerTitle', 'Input Kuota Coin')
                ->with('getData', $getTotalCoin)
                ->with('dataUser', $dataUser);
    }

    public function getHistoryMasterCoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMasterCoin = new Masterpin;
        $history   = $modelMasterCoin->getHistoryMasterCoinTransaction();
//        $sumDebit  = $modelMasterCoin->getSumDebitMasterCoin();
//        $sumCredit = $modelMasterCoin->getSumCreditMasterCoin();
        return view('admin.report.history_transaction_coin')
                ->with('headerTitle', 'History Transaksi Coin')
                ->with('getData', $history)
//                ->with('sumDebit', $sumDebit)
//                ->with('sumCredit', $sumCredit)
                ->with('dataUser', $dataUser);
    }

    public function postAddMasterCoin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMasterPin = New Masterpin;
        $dataInsertMaster = array(
            'total_coin' => $request->nominal,
            'type' => 1,
        );
        $modelMasterPin->getInsertMasterCoin($dataInsertMaster);
        return redirect()->route('addMasterCoin')
                ->with('message', 'Input kuota coin berhasil. Kuota coin diperusahaan telah bertambah')
                ->with('messageclass', 'success');
    }

    public function getListNews(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $getData = $modelNews->getListNews();
        return view('admin.news.news_list')
                ->with('headerTitle', 'Berita')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAddNews(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.news.news_create')
                ->with('headerTitle', 'New Berita ')
                ->with('dataUser', $dataUser);
    }

    public function postAddNews(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $dataInsert = array(
            'created_by' => $dataUser->id,
            'title' => $request->title,
            'image' => $request->image_url,
            'full_desc' => $request->full_desc
        );
        $modelNews->getInsertNews($dataInsert);
        return redirect()->route('adm_listNews')
                ->with('message', 'Berita berhasil dibuat')
                ->with('messageclass', 'success');
    }

    public function getNewsByID($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $getData = $modelNews->getNewsByField('id', $id);
        return view('admin.news.news_edit')
                ->with('headerTitle', 'Berita ')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postEditNews(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelNews = New News;
        $dataUpdate = array(
            'created_by' => $dataUser->id,
            'title' => $request->title,
            'image' => $request->image_url,
            'full_desc' => $request->full_desc,
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $modelNews->getUpdateNews('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_listNews')
                ->with('message', 'Berita berhasil di-edit')
                ->with('messageclass', 'success');
    }

    public function postRemoveNews(Request $request){

    }

    public function getListMember(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $dateNow = date('Y-m-d');
        $getMonth = (object) array(
            'dateSkr' => null,
            'startDay' => date('Y-m-d',strtotime("-10 days")),
            'endDay' => $dateNow
        );
        if($request->start_date != null && $request->end_date != null){
            $getMonth = (object) array(
                'dateSkr' => 1,
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
        }
        $getData = $modelMember->getAllMemberForAdminByDate($getMonth);
        return view('admin.member.member-list')
                ->with('search_name', 0)
                ->with('headerTitle', 'Member')
                ->with('getData', $getData)
                ->with('getDate', $getMonth)
                ->with('dataUser', $dataUser);
    }

    public function postAdminChangePasswordMember(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($request->password != $request->repassword){
            return redirect()->route('adm_listMember')
                    ->with('message', 'Password dn ktik ulang password tidak sama')
                    ->with('messageclass', 'danger');
        }
        if(strlen($request->password) < 6){
            return redirect()->route('adm_listMember')
                    ->with('message', 'Password terlalu pendek, minimal 6 karakter')
                    ->with('messageclass', 'danger');
        }
        $modelMember = New Member;
        $dataUpdatePass = array(
            'password' => bcrypt($request->password),
        );
        $modelMember->getUpdateUsers('id', $request->cekId, $dataUpdatePass);
        return redirect()->route('adm_listMember')
                    ->with('message', 'Berhasil')
                    ->with('messageclass', 'success');
    }

    public function postAdminChangeDataMember(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getCheck = $modelMember->getCheckEmailPhoneUsercodeAdmin($request->cekId, $request->email, $request->hp, $request->user_code);
        if($getCheck->cekCode == 1){
            return redirect()->route('adm_listMember')
                    ->with('message', 'Username sudah terpakai')
                    ->with('messageclass', 'danger');
        }
        $getData = $modelMember->getUsers('id', $request->cekId);
        $full_name = null;
        if($getData->full_name != null){
            $full_name = $request->full_name;
        }
        $dataUpdate = array(
            'user_code' => $request->user_code,
            'email' => $request->email,
            'hp' => $request->hp,
            'full_name' => $full_name
        );
        $modelMember->getUpdateUsers('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_listMember')
                    ->with('message', 'Berhasil')
                    ->with('messageclass', 'success');
    }

    public function postAdminChangeBlockMember(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $dataUpdate = array(
            'is_login' => 0,
        );
        $modelMember->getUpdateUsers('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_listMember')
                    ->with('message', 'Berhasil Blokir Member')
                    ->with('messageclass', 'success');
    }

    public function getAllWDSponsor(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminMemberBonusSponsor();
        return view('admin.report.list_wd_sponsor')
                ->with('headerTitle', 'Request Withdrawal Bonus Sponsor')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllClaimPHU(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminMemberClaimPHU();
        return view('admin.report.list_claim_phu')
                ->with('headerTitle', 'Request Claim PHU')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }



    public function postCheckWD(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $type = $request->type;
        $redirect = 'adm_listWDSponsor';
        if($type == 1){
            $redirect = 'adm_listWDSponsor';
        }
        if($type == 2){
            $redirect = 'adm_listClaimPHU';
        }
        if($request->id == null){
            return redirect()->route($redirect)
                    ->with('message', 'Data tidak ditemukan, tidak memilih checklist untuk di submit')
                    ->with('messageclass', 'danger');
        }
        $modelWD = new Transferwd;
        $getRowID = $request->id;
        foreach($getRowID as $getID){
            if($type == 1){
                $dataUpdate = array(
                    'status' => 1,
                    'transfer_at' => date('Y-m-d H:i:s')
                );
                $modelWD->getUpdateWD('id', $getID, $dataUpdate);
            }
            if($type == 2){
                $dataUpdate = array(
                    'status' => 1,
                    'transfer_at' => date('Y-m-d H:i:s')
                );
                $modelWD->getUpdateClaimPHU('id', $getID, $dataUpdate);
            }
        }
        return redirect()->route($redirect)
                    ->with('message', 'Konfirmasi Transfer WD berhasil')
                    ->with('messageclass', 'success');
    }

    public function postRejectWD(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $type = $request->type;
        $redirect = 'adm_listWDSponsor';
        if($type == 1){
            $redirect = 'adm_listWDSponsor';
        }
        if($type == 2){
            $redirect = 'adm_listClaimPHU';
        }
        $modelWD = new Transferwd;
        $getID = $request->cekId;
        $alesan = $request->reason;
        if($type == 1){
            $getData = $modelWD->getIDRequestWD($getID);
            if($getData == null){
                return redirect()->route($redirect)
                        ->with('message', 'Gagal, data tidak ditemukan')
                        ->with('messageclass', 'danger');
            }
            $dataUpdate = array(
                'status' => 2,
                'reason' => $alesan,
                'deleted_at' => date('Y-m-d H:i:s')
            );
            $modelWD->getUpdateWD('id', $getID, $dataUpdate);
        }
        if($type == 2){
            $dataUpdate = array(
                'status' => 2,
                'reason' => $alesan,
                'deleted_at' => date('Y-m-d H:i:s')
            );
            $modelWD->getUpdateClaimPHU('id', $getID, $dataUpdate);
        }
        return redirect()->route($redirect)
                    ->with('message', 'Data Berhasil di reject direject')
                    ->with('messageclass', 'success');
    }

    public function getAllHistoryWDSponsor(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusSponsor();
        return view('admin.report.history_wd_sponsor')
                ->with('headerTitle', 'History WD Bonus Sponsor')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllHistoryClaimPHU(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryClaimPHU();
        return view('admin.report.history_claim_phu')
                ->with('headerTitle', 'History Claim PHU')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getMemberBonusPHU(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonus = new Bonus;
        $getData = $modelBonus->getAdminBonusFly();
        return view('admin.report.member_bonus_phu')
                ->with('headerTitle', 'Member bonus PHU')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAddBonusReward(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.setting.add_bonus_reward')
                ->with('headerTitle', 'Bonus Reward')
                ->with('dataUser', $dataUser);
    }

    public function postAddBonusReward(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $dataInsert = array(
            'total_kiri' => $request->total_kiri,
            'total_kanan' => $request->total_kanan,
            'reward_detail' => $request->reward_detail,
            'reward_price' => $request->reward_price,
            'created_by' => $dataUser->id
        );
        $modelBonusSetting = new Bonussetting;
        $modelBonusSetting->getInsertBonusReward($dataInsert);
        return redirect()->route('adm_bonusReward')
                    ->with('message', 'Tambah setting bonus reward')
                    ->with('messageclass', 'success');
    }

    public function getBonusReward(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonusSetting = new Bonussetting;
        $getData =$modelBonusSetting->getAllBonusReward();
        return view('admin.setting.bonus-reward')
                ->with('headerTitle', 'Bonus Reward')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postBonusReward(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $dataUpdate = array(
            'total_kiri' => $request->total_kiri,
            'total_kanan' => $request->total_kanan,
            'reward_detail' => $request->reward_detail,
            'reward_price' => $request->reward_price,
        );
        $modelBonusSetting = new Bonussetting;
        $modelBonusSetting->getUpdateBonusReward('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_bonusReward')
                    ->with('message', 'Edit bonus rewards berhasil')
                    ->with('messageclass', 'success');
    }

    public function getAllWDClaimReward(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminMemberBonusReward();
        return view('admin.report.list_wd_reward')
                ->with('headerTitle', 'Claim Request Reward')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getBonusLevel(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonusSetting = new Bonussetting;
        $getData = $modelBonusSetting->getAdminBonusLevel();
        return view('admin.setting.bonus-level')
                ->with('headerTitle', 'Bonus Level')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postBonusLevel(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $dataUpdate = array(
            'ro_price' => $request->ro_price
        );
        $modelBonusSetting = new Bonussetting;
        $modelBonusSetting->getUpdateBonusLevel('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_bonusLevel')
                    ->with('message', 'Edit bonus level berhasil')
                    ->with('messageclass', 'success');
    }

    public function getAllHistoryWDBinary(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusBinary();
        return view('admin.report.history_wd_binary')
                ->with('headerTitle', 'History WD Bonus Binary')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllHistoryWDReward(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusReward();
        return view('admin.report.history_reward')
                ->with('headerTitle', 'History Bonus Reward')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllHistoryWDRO(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusRO();
        return view('admin.report.history_wd_ro')
                ->with('headerTitle', 'History WD Bonus RO')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllHistoryWDLevel(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusLevel();
        return view('admin.report.history_wd_level')
                ->with('headerTitle', 'History WD Bonus Level')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAllHistoryWDSafraPoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminHistoryBonusSafraPoin();
        return view('admin.report.history_wd_safrapoin')
                ->with('headerTitle', 'History WD Bonus Safra Poin')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getMemberRequestKirim(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.pin.member_req_kirim')
                ->with('headerTitle', 'Member Request')
                ->with('dataUser', $dataUser);
    }

    public function postMemberRequestKirim(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPengiriman = new Pengiriman;
        $modelPin = new Pin;
        $modelMember = New Member;
        $getDataUser = $modelMember->getUsers('id', $request->get_id);
        $dataPin = $modelPin->getTotalPinMember2($getDataUser);
        $getTotalPinTerkirimDanTuntas = $modelPengiriman->getCekPinPengirimanPaketAll($getDataUser);
        $sum_pin_masuk = 0;
        $sum_pin_keluar = 0;
        if($dataPin->sum_pin_masuk != null){
            $sum_pin_masuk = $dataPin->sum_pin_masuk;
        }
        if($dataPin->sum_pin_keluar != null){
            $sum_pin_keluar = $dataPin->sum_pin_keluar;
        }
        $sum_pin_terpakai = 0;
        if($dataPin->sum_pin_terpakai != null){
            $sum_pin_terpakai = $dataPin->sum_pin_terpakai;
        }
        $sum_pin_masuk_terima = 0;
        if($dataPin->sum_pin_masuk_terima != null){
            $sum_pin_masuk_terima = $dataPin->sum_pin_masuk_terima;
        }
        $sum_pin_keluar_terima = 0;
        if($dataPin->sum_pin_keluar_terima != null){
            $sum_pin_keluar_terima = $dataPin->sum_pin_keluar_terima;
        }
        $sum_pin_keluar_ditransfer = 0;
        if($dataPin->sum_pin_keluar_ditransfer != null){
            $sum_pin_keluar_ditransfer = $dataPin->sum_pin_keluar_ditransfer;
        }
        $saldo_pin_aktifasi = $sum_pin_masuk - $sum_pin_keluar;
        $saldo_pin_transfer = $sum_pin_masuk - $sum_pin_keluar;
        $saldo_pin_kirim_paket = $sum_pin_masuk - $getTotalPinTerkirimDanTuntas->total_pin_proses_dan_tuntas - $sum_pin_keluar_ditransfer - $sum_pin_masuk_terima;
        $pin_paket_diterima = $getTotalPinTerkirimDanTuntas->total_pin_tuntas + $sum_pin_masuk_terima - $sum_pin_keluar_terima;
        if($request->total_pin > $saldo_pin_kirim_paket){
            return redirect()->route('adm_memberReqKirim')
                    ->with('message', 'Sisa Saldo Kirim Paket tidak cukup untuk mengajukan kirim paket')
                    ->with('messageclass', 'danger');
        }
        $dataInsert = array(
            'user_id' => $request->get_id,
            'total_pin' => $request->total_pin,
            'alamat_kirim' => $request->alamat_kirim,
            'status' => 1,
            'kirim_at' => date('Y-m-d H:i:s'),
            'kurir_name' => $request->kurir_name,
            'no_resi' => $request->no_resi
        );
        $modelPengiriman->getInsertPengiriman($dataInsert);
        return redirect()->route('adm_memberReqKirim')
                    ->with('message', 'Request Paket sudah dikirim')
                    ->with('messageclass', 'success');
    }

    public function getGlobalReportBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonus = New Bonus;
        $getData = $modelBonus->getAdminGlobalBonus();
        return view('admin.report.global_bonus')
                ->with('headerTitle', 'Global Report')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getGlobalReportSales(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPin = new Pin;
        $getData = $modelPin->getAdminGLobalSales();
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        return view('admin.report.global_sales')
                ->with('headerTitle', 'Global Report')
                ->with('getData', $getData)
                ->with('pin_price', $getActivePinSetting->price)
                ->with('dataUser', $dataUser);
    }

    public function getGlobalReportMemberTransaction(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPin = new Pin;
        $getData = $modelPin->getAdminGLobalMemberTransaction();
        $modelSettingPin = New Pinsetting;
        $getActivePinSetting = $modelSettingPin->getActivePinSetting();
        return view('admin.report.global_member_trans')
                ->with('headerTitle', 'Global Report')
                ->with('getData', $getData)
                ->with('pin_price', $getActivePinSetting->price)
                ->with('dataUser', $dataUser);
    }

    public function getlistPinMember(){
         $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelPin = new Pin;
        $getData = $modelPin->getAdminPinMember();
        return view('admin.member.member-pin')
                ->with('headerTitle', 'Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getlistBonusMember(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonus = New Bonus;
        $getData = $modelBonus->getAdminBonusMember();
        return view('admin.member.member-bonus')
                ->with('headerTitle', 'Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getlistSaldoMember(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonus = New Bonus;
        $getData = $modelBonus->getAdminSaldoMember();
        return view('admin.member.member-saldo')
                ->with('headerTitle', 'Member')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getListSalesPurchase(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getData = $modelSale->getListPurchase();
        return view('admin.shop.purchase-list')
                ->with('headerTitle', 'Purchase')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAddSalesPurchase(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.shop.purchase-create')
                ->with('headerTitle', 'Purchase')
                ->with('dataUser', $dataUser);
    }

    public function postAddSalesPurchase(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $dataInsert = array(
            'name' => $request->name,
            'full_desc' => $request->full_desc,
            'image' => $request->image,
            'price' => $request->price,
        );
        $modelSale->getInsertPurchase($dataInsert);
        return redirect()->route('adm_listSalesPurchases')
                    ->with('message', 'Purchase berhasil dibuat')
                    ->with('messageclass', 'success');

    }

    public function getSalesPurchaseByID($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getData = $modelSale->getPurchaseByID($id);
        return view('admin.shop.purchase-edit')
                ->with('headerTitle', 'Purchase')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postEditSalesPurchase(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $dataUpdate = array(
            'name' => $request->name,
            'full_desc' => $request->full_desc,
            'image' => $request->image,
            'price' => $request->price,
            'updated_at' => date('Y-m-d H:i:s')
        );
        $modelSale->getUpdatePurchase('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_listSalesPurchases')
                    ->with('message', 'Purchase berhasil diedit')
                    ->with('messageclass', 'success');
    }

    public function postRemoveSalesPurchase(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSafra = New Safra;
    }







    public function getListMemberSales(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getData = $modelSale->getAllTransactionSalesAdmin();
        return view('admin.shop.list-transaction')
                ->with('headerTitle', 'Transaksi Penjualan')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getMembeSalesByID($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $getData = $modelSale->getAllTransactionSalesAdminByID($id);
        return view('admin.shop.detail-transaction')
                ->with('headerTitle', 'Detail Sales')
                ->with('getData', $getData[0])
                ->with('dataUser', $dataUser);
    }

    public function getConfirmMemberSalesByID(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $modeBonus = New Bonus;

        $dataUpdateTrans = array(
            'status' => 2,
            'tuntas_at' => date('Y-m-d H:i:s')
        );
        $modelSale->getUpdateTransaction('id', $request->cekId, $dataUpdateTrans);
        //bonus member masuk
        $getData = $modelSale->getAdminTransactionByID($request->cekId);
        $bonusPoin = 1;
        if ($getData->total_price >= 300000 && $getData->total_price < 500000) {
            $bonusPoin = 2;
        }
        if ($getData->total_price >= 500000 && $getData->total_price < 700000) {
            $bonusPoin = 3;
        }
        if ($getData->total_price >= 700000) {
            $bonusPoin = 4;
        }
        $dataBonusPoinBelanja = array(
            'user_id' => $getData->user_id,
            'type' => 4,
            'bonus_price' => $bonusPoin,
            'bonus_date' => date('Y-m-d H:i:s'),
        );
        $modeBonus->getInsertBonusMember($dataBonusPoinBelanja);
        return redirect()->route('adm_salesByID', [$request->cekId])
                    ->with('message', 'Belanja berhasil diapprove')
                    ->with('messageclass', 'success');
    }

    public function getRejectMemberSalesByID(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSale = New Sale;
        $dataUpdateTrans = array(
            'status' => 3,
            'reason' => $request->reason,
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $modelSale->getUpdateTransaction('id', $request->cekId, $dataUpdateTrans);
        return redirect()->route('adm_salesByID', [$request->cekId])
                    ->with('message', 'Belanja berhasil direject')
                    ->with('messageclass', 'success');
    }




    public function postMemberBuyPurchase(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelSafra = New Safra;
        $dataUpdate = array(
            'status' => 2,
            'tuntas_at' => date("Y-m-d H:i:s")
        );
        $modelSafra->getUpdateSafraTransfer('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_listShopBuy')
                    ->with('message', 'Barang penjualan safra berhasil dikirim')
                    ->with('messageclass', 'success');
    }

    public function getAllWDSafraPoin(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminMemberWDSafraPoin();
        return view('admin.report.list_wd_safrapoin')
                ->with('headerTitle', 'Request Withdrawal Safra Poin')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getAddBonusPoolSharing(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        return view('admin.setting.add_bonus_pool')
                ->with('headerTitle', 'Bonus pool Sharing')
                ->with('dataUser', $dataUser);
    }

    public function postAddBonusPoolSharing(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $dataInsert = array(
            'is_active' => 1,
            'created_by' => $dataUser->id,
            'name' => $request->name,
            'min_omzet' => $request->min_omzet,
            'max_omzet' => $request->max_omzet,
            'persentase' => $request->persentase
        );
        $modelBonusSetting = new Bonussetting;
        $modelBonusSetting->getInsertBonusPool($dataInsert);
        return redirect()->route('adm_bonusPoolSharing')
                    ->with('message', 'Tambah setting bonus reward')
                    ->with('messageclass', 'success');
    }

    public function getBonusPoolSharing(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelBonusSetting = new Bonussetting;
        $getData =$modelBonusSetting->getAllBonusPoolSharing();
        return view('admin.setting.bonus-pool')
                ->with('headerTitle', 'Bonus Pool Sharing')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postBonusPoolSharing(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $dataUpdate = array(
            'name' => $request->name,
            'min_omzet' => $request->min_omzet,
            'max_omzet' => $request->max_omzet,
            'persentase' => $request->persentase
        );
        $modelBonusSetting = new Bonussetting;
        $modelBonusSetting->getUpdateBonusPool('id', $request->cekId, $dataUpdate);
        return redirect()->route('adm_bonusPoolSharing')
                    ->with('message', 'Edit bonus Pool Sharing')
                    ->with('messageclass', 'success');
    }

    public function postAdminsearchListMember(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $dateNow = date('Y-m-d');
        $getMonth = (object) array(
            'dateSkr' => null,
            'startDay' => $dateNow,
            'endDay' => $dateNow
        );
        $cekLenght = strlen($request->name);
        if($cekLenght < 3){
            return redirect()->route('adm_listMember')
                    ->with('message', 'Minimal pencarian harus 3 karakter (huruf).')
                    ->with('messageclass', 'danger');
        }
        $getData = $modelMember->getAllMemberForAdminSearchName($request->name);
        return view('admin.member.member-list')
                ->with('headerTitle', 'Member')
                ->with('search_name', 1)
                ->with('getData', $getData)
                ->with('getDate', $getMonth)
                ->with('dataUser', $dataUser);
    }

    public function getListTopTenMemberOmzetMonthly(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $start_day = date("Y-m-01");
        $end_day = date("Y-m-t");
        $dataThisMonth = (object) array(
            'startDay' => $start_day,
            'endDay' => $end_day
        );
        if($request->start_date != null && $request->end_date != null){
            $dataThisMonth = (object) array(
                'startDay' => $request->start_date,
                'endDay' => $request->end_date
            );
        }
        $takeTop = 10;
        $getDataTop = $modelMember->getTopMemberOmzetByPlacement($dataThisMonth, $takeTop);
        $dataDetail = array();
        if($getDataTop != null){
            foreach($getDataTop as $row){
                $downlineKanan = $row->upline_detail.',['.$row->owner_id.']'.',['.$row->kanan_id.']';
                if($row->upline_detail == null){
                    $downlineKanan = '['.$row->id.']'.',['.$row->kanan_id.']';
                }
                $kananLuar = $modelMember->getNewCountMyDownlineTopTen($downlineKanan, $row->owner_id, $dataThisMonth);
                $kananPas = 0;
                if($row->kanan_id != null){
                    $kananPas = $modelMember->getKaKiTerPendekTopTen($row->kanan_id, $row->owner_id, $dataThisMonth);
                }
                $kanan = $kananLuar + $kananPas;

                $downlineKiri = $row->upline_detail.',['.$row->owner_id.']'.',['.$row->kiri_id.']';
                if($row->upline_detail == null){
                    $downlineKiri = '['.$row->id.']'.',['.$row->kiri_id.']';
                }
                $kiriLuar = $modelMember->getNewCountMyDownlineTopTen($downlineKiri, $row->owner_id, $dataThisMonth);
                $kiriPas = 0;
                if($row->kiri_id != null){
                    $kiriPas = $modelMember->getKaKiTerPendekTopTen($row->kiri_id, $row->owner_id, $dataThisMonth);
                }
                $kiri = $kiriLuar + $kiriPas;
                $dataDetail[] = (object) array(
                    'user_code' => $row->user_code,
                    'id' => $row->owner_id,
                    'total_pin' => $row->total_pin,
                    'kanan' => $kanan,
                    'kiri' => $kiri,
                );
            }
        }
        return view('admin.member.member-top')
                ->with('headerTitle', 'Member Top Ten Omzet')
                ->with('getData', $dataDetail)
                ->with('getDate', $dataThisMonth)
                ->with('dataUser', $dataUser);
    }

    public function getListMemberBonusSponsor(){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $getMemberBonusSP = $modelMember->getAllMemberProbablyGetBonus();
        $dataAll = array();
        foreach($getMemberBonusSP as $row){
            $totalBonusSP = $modelBonus->getTotalBonusSponsor($row);
            $totalWDSP = $modelWD->getTotalDiTransferByType($row, 1);
            $dataAll[] = (object) array(
                'id' => $row->id,
                'user_code' => $row->user_code,
                'total_bonus' => $totalBonusSP,
                'total_wd' => $totalWDSP->total_wd,
                'total_tunda' => $totalWDSP->total_tunda,
                'fee_tunda' => $totalWDSP->total_tunda_admin_fee,
                'fee_tuntas' => $totalWDSP->total_wd_admin_fee,
                'saldo' => (int) ($totalBonusSP - ($totalWDSP->total_wd + $totalWDSP->total_tunda + $totalWDSP->total_tunda_admin_fee + $totalWDSP->total_wd_admin_fee)),
            );
        }
        return view('admin.member.bonus-sp')
                ->with('headerTitle', 'Member Bonus Sponsor')
                ->with('getData', $dataAll)
                ->with('dataUser', $dataUser);
    }

    public function getListMemberBonusBinary(){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $getMemberBonusSP = $modelMember->getAllMemberProbablyGetBonus();
        $dataAll = array();
        foreach($getMemberBonusSP as $row){
            $totalBonusBinary = $modelBonus->getTotalBonusBinaryNew($row);
            $totalWDBinary = $modelWD->getTotalDiTransferByType($row, 2);
            $dataAll[] = (object) array(
                'id' => $row->id,
                'user_code' => $row->user_code,
                'total_bonus' => $totalBonusBinary,
                'total_wd' => $totalWDBinary->total_wd,
                'total_tunda' => $totalWDBinary->total_tunda,
                'fee_tunda' => $totalWDBinary->total_tunda_admin_fee,
                'fee_tuntas' => $totalWDBinary->total_wd_admin_fee,
                'saldo' => (int) ($totalBonusBinary - ($totalWDBinary->total_wd + $totalWDBinary->total_tunda + $totalWDBinary->total_tunda_admin_fee + $totalWDBinary->total_wd_admin_fee)),
            );
        }
        return view('admin.member.bonus-binary')
                ->with('headerTitle', 'Member Bonus Binary')
                ->with('getData', $dataAll)
                ->with('dataUser', $dataUser);
    }

    public function getListMemberBonusLevel(){
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 1500);
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $getMemberBonusSP = $modelMember->getAllMemberProbablyGetBonus();
        $dataAll = array();
        foreach($getMemberBonusSP as $row){
            $totalBonusLevel = $modelBonus->getTotalBonusLevel($row);
            $totalWDLevel = $modelWD->getTotalDiTransferByType($row, 3);
            $dataAll[] = (object) array(
                'id' => $row->id,
                'user_code' => $row->user_code,
                'total_bonus' => $totalBonusLevel,
                'total_wd' => $totalWDLevel->total_wd,
                'total_tunda' => $totalWDLevel->total_tunda,
                'fee_tunda' => $totalWDLevel->total_tunda_admin_fee,
                'fee_tuntas' => $totalWDLevel->total_wd_admin_fee,
                'saldo' => (int) ($totalBonusLevel - ($totalWDLevel->total_wd + $totalWDLevel->total_tunda + $totalWDLevel->total_tunda_admin_fee + $totalWDLevel->total_wd_admin_fee)),
            );
        }
        return view('admin.member.bonus-level')
                ->with('headerTitle', 'Member Bonus Level')
                ->with('getData', $dataAll)
                ->with('dataUser', $dataUser);
    }

    public function getAllRequestPulsa(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelWD = new Transferwd;
        $getData = $modelWD->getAdminMemberRequestPulsa();
        return view('admin.report.list_req_pulsa')
                ->with('headerTitle', 'Request Withdrawal Bonus Sponsor')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getListClaimPrudukAktifasi(){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getData = $modelMember->getListAdminMemberClaimProdukAktifasi();
        return view('admin.member.list_claim_produkaktifasi')
                ->with('headerTitle', 'Claim Produk Aktifasi')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function getClaimPrudukAktifasiID($id){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $getData = $modelMember->getAdminMemberClaimProdukAktifasi($id);
        return view('admin.member.detail_claim_produkaktifasi')
                ->with('headerTitle', 'Claim Produk Aktifasi')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }

    public function postClaimPrudukAktifasi(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(1, 2);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = New Member;
        $update = array(
            'kurir_name' => $request->kurir_name,
            'no_resi' => $request->no_resi,
            'status' => 1,
            'kirim_at' => date('Y-m-d H:i:s')
        );
        $modelMember->getUpdateClaimProdukAktifasi('id', $request->cekId, $update);
        return redirect()->route('adm_listClaimPrudukAktifasi')
                    ->with('message', 'Edit pengiriman claim produk aktifasi berhasil')
                    ->with('messageclass', 'success');

    }
























    ###################

    public function getTestEmail(){
        $dataEmail = array(
            'name' => 'Chairil Hakim',
            'email' => 'chairil.ptmgahama@gmail.com',
            'password' => '123456',
            'hp' => '087811112222',
            'user_code' => 'test123',
            'sponsor_name' => 'Akmal'
        );
        $emailSend = 'chairil@ioc.sc';
        Mail::send('member.email.email', $dataEmail, function($message) use($emailSend){
            $message
                    ->to($emailSend, 'Member Registration')
                    ->subject('Welcome');
        });
        dd('done');
    }



}
