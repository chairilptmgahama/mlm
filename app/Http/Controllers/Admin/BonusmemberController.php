<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Member;
use App\Model\Pinsetting;
use App\Model\Package;
use App\Model\Memberpackage;
use App\Model\Transaction;
use App\Model\Pin;
use App\Model\Bonussetting;
use App\Model\Transferwd;
use App\Model\Bonus;
use App\Model\Startwd;
use App\Model\Bank;

class BonusmemberController extends Controller {
    
    public function __construct(){
    }
    
    public function getMySummaryBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->package_id == null){
            return redirect()->route('m_newPackage');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->upline_id == null){
            if($dataUser->id > 4){
                return redirect()->route('mainDashboard');
            }
        }
        $modelBonus = new Bonus;
        $modelWD = new Transferwd;
        $totalBonus = $modelBonus->getTotalBonus($dataUser);
        $totalWD = $modelWD->getTotalDiTransfer($dataUser);
        $bonusType = $modelBonus->getBonusTotalPerType($dataUser);
        $dataAll = (object) array(
            'total_bonus' => $totalBonus->total_bonus,
            'total_wd' => $totalWD->total_wd,
            'total_tunda' => $totalWD->total_tunda,
            'fee_tunda' => $totalWD->total_tunda_admin_fee,
            'fee_tuntas' => $totalWD->total_wd_admin_fee
        );
        return view('member.bonus.summary')
                ->with('dataAll', $dataAll)
                ->with('bonusType', $bonusType)
                ->with('dataUser', $dataUser);
    }
    
    public function getMySponsorBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'danger');
        }
        $modelBonus = New Bonus;
        $getData = $modelBonus->getBonusSponsor($dataUser);
        return view('member.bonus.sponsor')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyPoinBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'danger');
        }
        $modelBonus = New Bonus;
        $modelBonusSetting = New Bonussetting;
        $modelWD = New Transferwd;
        $totBonusPoin = $modelBonus->getSumBonusNew($dataUser->id, 2);
        $getSettingBenusPoin = $modelBonusSetting->getActiveBonusClaimPoin();
        $totWDPoin = $modelWD->getSumWDPoin($dataUser->id);
        $getAllWDPoin = $modelWD->getUserWDPoinAll($dataUser->id);
        $saldo = $totBonusPoin - $totWDPoin;
        $dataPoin = null;
        $no = 0;
        if($getSettingBenusPoin != null){
            foreach($getSettingBenusPoin as $row){
                $no++;
                $boleh = false;;
                $dataHistory = $modelWD->getUserWDPoin($row->id, $dataUser->id);
                if($dataHistory == null){
                    if($saldo >= $row->jml_poin){
                        $boleh = true;
                    }
                }
                $dataPoin[] = (object) array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'jml_poin' => $row->jml_poin,
                    'can_claim' => $boleh
                );
            }
        }
        return view('member.bonus.poin')
                ->with('totBonusPoin', $totBonusPoin)
                ->with('totWDPoin', $totWDPoin)
                ->with('dataPoin', $dataPoin)
                ->with('getAllWDPoin', $getAllWDPoin)
                ->with('dataUser', $dataUser);
    }
    
    public function getMyFlyBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'danger');
        }
        $modelBonus = New Bonus;
        $getData = $modelBonus->getBonusFly($dataUser);
        return view('member.bonus.fly')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postClaimBonusPHU(Request $request){
        $dataUser = Auth::user();
        $modelBonus = New Bonus;
        $modelWD = new Transferwd;
        if($request->phu_claim_id == 0){
            return redirect()->route('m_myBonusFly')
                    ->with('message', 'Anda tidak memilih claim PHU')
                    ->with('messageclass', 'danger');
        }
        $getData = $modelBonus->getCekMemberHaveBonusFly($dataUser->id, $request->bonus_id);
        if($getData == null){
            return redirect()->route('m_myBonusFly')
                    ->with('message', 'Tidak ada data')
                    ->with('messageclass', 'danger');
        }
        $phu_claim_id = $request->phu_claim_id;
        $dataInsertWD = array(
            'user_id' => $dataUser->id,
            'claim_phu_id' => $phu_claim_id,
            'claim_date' => date('Y-m-d')
        );
        $getID = $modelWD->getInsertClaimPHU($dataInsertWD);
        $dataUpdateBonusPHU = array(
            'is_phu_claim' => 1,
            'phu_claim_id' => $getID->lastID,
            'claim_phu_at' => date('Y-m-d H:i:s')
        );
        $modelBonus->getUpdateBonusPHU('id', $request->bonus_id, $dataUpdateBonusPHU);
        return redirect()->route('m_myBonusFly')
                    ->with('message', 'Berhasil Claim PHU')
                    ->with('messageclass', 'success');
    }
    
    public function getMyReportSponsorBonus(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        $modelWD = New Transferwd;
        $getData = $modelWD->getReportMemberBonusSponsor($dataUser);
        return view('member.report.bonus-sponsor')
                ->with('getData', $getData)
                ->with('dataUser', $dataUser);
    }
    
    public function postClaimBonusPoin(Request $request){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        if($dataUser->is_active == 0){
            return redirect()->route('mainDashboard');
        }
        $modelBank = New Bank;
        $modelBonusSetting = New Bonussetting;
        $modelWD = New Transferwd;
        $cekBank = $modelBank->getBankMemberActive($dataUser->id);
        if ($cekBank == null) {
            return redirect()->route('m_myBank')
                            ->with('message', 'Anda belum punya rekening bank. Harap tambahkan data rekening')
                            ->with('messageclass', 'danger');
        }
        if ($dataUser->pin_code == null) {
            return redirect()->route('m_addCodePin')
                            ->with('message', 'Anda belum punya kode pin. Harap tambahkan kode pin anda')
                            ->with('messageclass', 'danger');
        }
        $getSettingPoin = $modelBonusSetting->getActiveBonusClaimPoinID($request->idPoin);
        $dataInsert = array(
            'user_id' => $dataUser->id,
            'claim_poin_id' => $request->idPoin,
            'total_poin' => $getSettingPoin->jml_poin,
            'claim_date' => date('Y-m-d')
        );
        $modelWD->getInsertClaimPoin($dataInsert);
        return redirect()->route('m_myBonusPoin')
                    ->with('message', 'Request claim poin berhasil')
                    ->with('messageclass', 'success');
    }
    
    
}

