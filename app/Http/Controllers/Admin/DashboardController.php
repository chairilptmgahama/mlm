<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Pin;
use App\Model\Member;
use App\Model\Masterpin;
use App\Model\Bonus;
use App\Model\Transferwd;
use App\Model\Package;
use App\Model\Pinsetting;
use App\Model\Transaction;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {

    public function __construct(){
        
    }
    
    public function getDashboard(){
        $dataUser = Auth::user();
        if(in_array($dataUser->user_type, array(10))){
            return redirect()->route('mainDashboard');
        }
        $onlyUser  = array(1, 2, 3);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelMember = new Member;
        $modelMasterCoin = new Masterpin;
        $getTotalCoin = $modelMasterCoin->getTotalCoinPerusahaan();
        $getAllmember = $modelMember->getAllMember();
        return view('admin.home.admin-dashboard')
                    ->with('headerTitle', 'Dashboard')
                    ->with('getTotalCoin', $getTotalCoin)
                    ->with('totalMember', $getAllmember)
                    ->with('dataUser', $dataUser);
        
    }
    
    public function getMemberDashboard(){
        $dataUser = Auth::user();
        $onlyUser  = array(10);
        if(!in_array($dataUser->user_type, $onlyUser)){
            return redirect()->route('mainDashboard');
        }
        $modelCoin = New Pin;
        $modelMember = new Member;
        $modelTransaction = New Transaction;
        $modelMasterCoin = new Masterpin;
        $getTotalSponsorBlmAktif = null;
        $coinPosting = null;
        $cekCoinTersedia = $modelCoin->getCoinAvailable($dataUser->id);
        $getBalanceCoin = $modelMasterCoin->getBalanceCoinGlobal();
        $getCoinBonusMining = $modelCoin->getCoinPostingGlobalAvailableAllPosting();
        $getCoinMining = $modelTransaction->getCoinPostingGlobal();
        $getCoinGlobalBebas = $modelCoin->getCoinGlobalAvailable();
        $totalBonusPosting = null;
        $sp_name = null;
        $view = 'dashboard-stockist';
        if($dataUser->member_type == 3){
            $view = 'dashboard';
            $getTotalSponsorBlmAktif = $modelMember->getCountDownlineSponsorNonActive($dataUser->id);
            $coinPosting = $modelTransaction->getCoinPostingByUserID($dataUser->id);
            $dateRange = $modelCoin->getRange24Hours();
            $totalBonusPosting = $modelCoin->getCoinPostingAvailableAllPosting($dataUser->id, $dateRange);
            $sp_name = 'Exchanger';
            if($dataUser->sponsor_id != null){
                $sponsor = $modelMember->getUsers('id', $dataUser->sponsor_id);
                $sp_name = $sponsor->name;
            }
        }
//        dd($getCoinGlobalBebas);
        return view('member.home.'.$view)
                    ->with('headerTitle', 'Dashboard')
                    ->with('cekCoinTersedia', $cekCoinTersedia)
                    ->with('coinPosting', $coinPosting)
                    ->with('totalBonusPosting', $totalBonusPosting)
                    ->with('getBalanceCoin', $getBalanceCoin)
                    ->with('totalSponsotBlmAktif', $getTotalSponsorBlmAktif)
                    ->with('getCoinBonusMining', $getCoinBonusMining)
                    ->with('getCoinMining', $getCoinMining)
                    ->with('getCoinGlobalBebas', $getCoinGlobalBebas)
                    ->with('sp_name', $sp_name)
                    ->with('dataUser', $dataUser);
    } 
    

}
