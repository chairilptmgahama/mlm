<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Model\Admin;

class HomeController extends Controller {
    use AuthenticatesUsers;
    
    public function __construct(){
        
    }
    
    public function getFront(){
        return redirect()->route('memberLogin');
    }
    
    public function getAdminLogin(){
        $dataUser = Auth::user();
        if($dataUser != null){
            return redirect()->route('admDashboard');
        }
        return view('admin.login.login');
    }
    
    public function getMemberLogin(){
        $dataUser = Auth::user();
        if($dataUser != null){
            return redirect()->route('admDashboard');
        }
        return view('member.login-member');
    }
    
    public function postAdminLogin(Request $request){
        $email = $request->admin_email;
        $password = $request->admin_password;
        $userdata = array('user_code' => $email, 'password'  => $password, 'is_login' => 1);
        if($this->guard()->attempt($userdata)){
            $request->session()->regenerate();
            return redirect()->route('admDashboard');
        }
        return redirect()->route('memberLogin')
                ->with('message', 'Login gagal')
                ->with('messageclass', 'error');
    }
    
    public function getAdminLogout(Request $request) {
        $dataUser = Auth::user();
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect()->route('memberLogin')
                ->with('message', 'Logout Sukses')
                ->with('messageclass', 'success');
    }
    
    public function getMaintenance(){
        $dataUser = Auth::user();
        return view('member.home.maintenance')
                        ->with('dataUser', $dataUser);
    }


}
