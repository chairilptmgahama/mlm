<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class News extends Model {
    
    public function getInsertNews($data){
        try {
            $lastInsertedID = DB::table('news')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateNews($fieldName, $name, $data){
        try {
            DB::table('news')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListNews(){
        $sql = DB::table('news')
                    ->where('publish', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getNewsByField($fieldName, $name){
        $sql = DB::table('news')
                    ->where($fieldName, '=', $name)
                    ->where('publish', '=', 1)
                    ->first();
        return $sql;
    }
    
    
}