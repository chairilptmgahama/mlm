<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;


class Safra extends Model {
    
    public function getInsertPurchase($data){
        try {
            $lastInsertedID = DB::table('purchase')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdatePurchase($fieldName, $name, $data){
        try {
            DB::table('purchase')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInsertSafraTransfer($data){
        try {
            $lastInsertedID = DB::table('transfer_safra')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateSafraTransfer($fieldName, $name, $data){
        try {
            DB::table('transfer_safra')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListPurchase(){
        $sql = DB::table('purchase')
                    ->where('publish', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getPurchaseByID($id){
        $sql = DB::table('purchase')
                    ->where('publish', '=', 1)
                    ->where('id', '=', $id)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getTotalSafraOut($id){
        $sql = DB::table('transfer_safra')
                    ->selectRaw('sum(case when status != 2 then safra_total else 0 end) as total_safra')
                    ->where('user_id', '=', $id)
                    ->first();
        $total = 0;
        if($sql->total_safra != null){
            $total = $sql->total_safra;
        }
        return $total;
    }
    
    public function getCodeSafra(){
        $getTransCount = DB::table('transfer_safra')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $code = sprintf("%04s", $tmp);
        return $code;
    }
    
    public function getSafraTransaction($id){
        $sql = DB::table('transfer_safra')
                    ->where('user_id', '=', $id)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getSafraTransactionDetail($id, $user_id){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id')
                    ->where('transfer_safra.id', '=', $id)
                    ->where('transfer_safra.user_id', '=', $user_id)
                    ->first();
        return $sql;
    }
    
    public function getAdminafraTransactionDetail(){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->join('users', 'transfer_safra.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id, users.user_code, users.full_name, transfer_safra.user_id, users.hp')
                    ->whereIn('transfer_safra.status', array(1, 2))
                    ->get();
        return $sql;
    }
    
     public function getAdminafraTransactionDetailID($id){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->join('users', 'transfer_safra.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id, users.user_code, users.full_name, transfer_safra.user_id, users.hp')
                    ->where('transfer_safra.id', '=', $id)
                    ->whereIn('transfer_safra.status', array(1, 2))
                    ->first();
        return $sql;
    }
    
}
