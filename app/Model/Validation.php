<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Validation extends Model {
    
    public function getCheckNewSponsor($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->email == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Email tidak boleh kosong');
            return $canInsert;
        }
        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $canInsert = (object) array('can' => false, 'pesan' => 'Format email salah');
            return $canInsert;
          }
        if($request->name == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nama tidak boleh kosong');
            return $canInsert;
        }
        if($request->password == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password tidak boleh kosong');
            return $canInsert;
        }
        if($request->user_code == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Username tidak boleh kosong');
            return $canInsert;
        }
        if($request->hp == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'No. Handphone tidak boleh kosong');
            return $canInsert;
        }
        if($request->password != $request->repassword){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password tidak sama');
            return $canInsert;
        }
        if(strlen($request->name) > 50){
            $canInsert = (object) array('can' => false, 'pesan' => 'Username tidak boleh lebih dari 50 karakter');
            return $canInsert;
        }
        if(strlen($request->email) > 90){
            $canInsert = (object) array('can' => false, 'pesan' => 'Email tidak boleh lebih dari 90 karakter');
            return $canInsert;
        }
        if(strlen($request->password) < 6){
            $canInsert = (object) array('can' => false, 'pesan' => 'Password terlalu pendek, minimal 6 karakter');
            return $canInsert;
        }
        if(!is_numeric($request->hp)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nomor HP menggunakan harus menggunakan angka');
            return $canInsert;
        }
        $cekHP = substr($request->hp, 0, 2);
        if($cekHP != '08'){
            $canInsert = (object) array('can' => false, 'pesan' => 'Awalan Nomor HP menggunakan harus menggunakan angka 08');
            return $canInsert;
        }
        if(strlen($request->hp) < 9){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nomor HP terlalu pendek minimal 8 digit');
            return $canInsert;
        }
        if(strlen($request->hp) > 13){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nomor HP terlalu panjang, maksimal 13 angka');
            return $canInsert;
        }
        if(strlen($request->user_code) > 20){
            $canInsert = (object) array('can' => false, 'pesan' => 'Username tidak boleh lebih dari 20 karakter');
            return $canInsert;
        }
        if(strpos($request->user_code, ' ') !== false){
            $canInsert = (object) array('can' => false, 'pesan' => 'Username tidak boleh ada spasi');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckNewProfile($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->full_name == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nama lengkap harus diisi, sesuai dengan nama pada rekening Bank');
            return $canInsert;
        }
        if($request->alamat == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Alamat harus diisi');
            return $canInsert;
        }
        if($request->provinsi == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pilih Provinsi');
            return $canInsert;
        }
        if($request->kota == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kota harus diisi');
            return $canInsert;
        }
        if($request->kode_pos == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kode pos harus diisi');
            return $canInsert;
        }
        if($request->wallet == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Wallet Vidy Coin harus diisi');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckAddPin($request, $data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->total_pin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin harus diisi');
            return $canInsert;
        }
        if(!is_numeric($request->total_pin)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin harus dalam angka');
            return $canInsert;
        }
        if($request->total_pin <= 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin harus diatas 0');
            return $canInsert;
        }
        if($data->member_status == 2){
            if($request->total_pin < 100){
                $canInsert = (object) array('can' => false, 'pesan' => 'Anda Pernah Membeli 100 pin atau lebih, maka anda harus membeli pin minimal 100');
                return $canInsert;
            }
        }
        return $canInsert;
    }
    
    public function getCheckAddBank($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->account_no == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum memilih nomor rekening');
            return $canInsert;
        }
        if($request->bank_name == 'none'){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum memilih bank');
            return $canInsert;
        }
        if(!is_numeric($request->account_no)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nomor Rekening harus dalam angka');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckPengiriman($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->total_pin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Pin harus diisi');
            return $canInsert;
        }
        if($request->alamat_kirim == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Alamat Kirim harus diisi');
            return $canInsert;
        }
        if(!is_numeric($request->total_pin)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Pin harus dalam angka');
            return $canInsert;
        }
        $kelipatan = $request->total_pin * 4;
        $day_cream = 0;
        if($request->day_cream != null){
            $day_cream = $request->day_cream;
        }
        $night_cream = 0;
        if($request->night_cream != null){
            $night_cream = $request->night_cream;
        }
        $face_toner = 0;
        if($request->face_toner != null){
            $face_toner = $request->face_toner;
        }
        $facial_wash = 0;
        if($request->facial_wash != null){
            $facial_wash = $request->facial_wash;
        }
        $total_item = $day_cream + $night_cream + $face_toner + $facial_wash;
        if($kelipatan != $total_item){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total item harus berjumlah '.$kelipatan.' ');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckTransferPin($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->total_pin == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Pin harus diisi');
            return $canInsert;
        }
        if(!is_numeric($request->total_pin)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Pin harus dalam angka');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCekPinForUpgrade($data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($data->total_sisa_pin <= 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Pin harus lebis besar dari 0');
            return $canInsert;
        }
        if($data->sisa_pin < $data->total_sisa_pin){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin anda tidak cukup untuk melakukan upgrade, silakan klik <a href="/m/add/pin">DISINI </a>untuk beli pin');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckRO($request, $getTotalPin, $data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if(!is_numeric($request->total_pin)){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin harus dalam angka');
            return $canInsert;
        }
        if($request->total_pin <= 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin harus diatas 0');
            return $canInsert;
        }
        $sum_pin_masuk = 0;
        $sum_pin_keluar = 0;
        if($getTotalPin->sum_pin_masuk != null){
            $sum_pin_masuk = $getTotalPin->sum_pin_masuk;
        }
        if($getTotalPin->sum_pin_keluar != null){
            $sum_pin_keluar = $getTotalPin->sum_pin_keluar;
        }
        $total = $sum_pin_masuk - $sum_pin_keluar;
        if($total < $request->total_pin){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pin yang anda masukan tidak cukup untuk melakukan repeat order, silakan klik <a href="/m/add/pin">DISINI </a>untuk beli pin');
            return $canInsert;
        }
        return $canInsert;
    }
    
    public function getCheckWD($data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
//        if($data->total_sponsor < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member');
//            return $canInsert;
//        }
//        if($data->total_placement < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member yang sudah diplacement');
//            return $canInsert;
//        }
        if($data->bank == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi data profile dan data bank');
            return $canInsert;
        }
        if($data->req_wd < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd -  $data->admin_fee) < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pengajuan withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if(($data->req_wd - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal melebihi dari total bonus');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd_range) - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd + $data->total_tunda + $data->wd_fee + $data->wd_tunda_fee) - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi Total Bonus');
            return $canInsert;
        }
        
        return $canInsert;
    }
    
    public function getCheckWDSponsor($data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
//        if($data->total_sponsor < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member');
//            return $canInsert;
//        }
//        if($data->total_placement < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member yang sudah diplacement');
//            return $canInsert;
//        }
        if($data->bank == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi data profile dan data bank');
            return $canInsert;
        }
        if($data->req_wd < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd -  $data->admin_fee) < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pengajuan withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if(($data->req_wd - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal melebihi dari total bonus');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd_range) - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd + $data->total_tunda + $data->wd_fee + $data->wd_tunda_fee) - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi Total Bonus');
            return $canInsert;
        }
        
        return $canInsert;
    }
    
    public function getCheckWDSafra($data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
//        if($data->total_sponsor < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member');
//            return $canInsert;
//        }
//        if($data->total_placement < 2){
//            $canInsert = (object) array('can' => false, 'pesan' => 'bonus diperoleh jika sudah mensponsoing DUA member yang sudah diplacement');
//            return $canInsert;
//        }
        if($data->bank == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi data profile dan data bank');
            return $canInsert;
        }
        if($data->req_wd < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd -  $data->admin_fee) < 50000){
            $canInsert = (object) array('can' => false, 'pesan' => 'Minimum withdraw adalah Rp. 50.000 dengan biaya admin (fee) Rp. 10.000 untuk pengguna Non-BCA dan gratis untuk pengguna BCA');
            return $canInsert;
        }
        if(($data->req_wd - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pengajuan withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if(($data->req_wd - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal melebihi dari total bonus');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd_range + $data->belanjaSafra) - $data->kuota_wd) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi kuota WD');
            return $canInsert;
        }
        if((($data->req_wd + $data->total_wd + $data->total_tunda + $data->wd_fee + $data->wd_tunda_fee + $data->belanjaSafra) - $data->total_bonus_get) > 0){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nominal withdrawal tidak boleh melebihi Total Bonus');
            return $canInsert;
        }
        
        return $canInsert;
    }
    
    public function getCheckClaimReward($data){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($data->bank == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Anda belum mengisi data profile dan data bank');
            return $canInsert;
        }
        if($data->kiri < $data->getData->total_kiri){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Kiri Anda saat ini tidak mencukup untuk claim reward');
            return $canInsert;
        }
//        if($data->kiri > $data->getData->total_kiri){
//            $canInsert = (object) array('can' => false, 'pesan' => 'Total Kiri Anda saat ini kelebihan untuk claim reward');
//            return $canInsert;
//        }
        if($data->kanan < $data->getData->total_kanan){
            $canInsert = (object) array('can' => false, 'pesan' => 'Total Kanan Anda saat ini tidak mencukup untuk claim reward');
            return $canInsert;
        }
//        if($data->kanan > $data->getData->total_kanan){
//            $canInsert = (object) array('can' => false, 'pesan' => 'Total Kanan Anda saat ini kelebihan untuk claim reward');
//            return $canInsert;
//        }
        return $canInsert;
    }
    
    public function getCheckKirimProduk($request){
        $canInsert = (object) array('can' => true, 'pesan' => '');
        if($request->full_name == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Nama lengkap harus diisi');
            return $canInsert;
        }
        if($request->no_hp == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'No. HP harus diisi');
            return $canInsert;
        }
        if($request->alamat == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Alamat harus diisi');
            return $canInsert;
        }
        if($request->kelurahan == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kelurahan harus diisi');
            return $canInsert;
        }
        if($request->kecamatan == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kecamatan harus diisi');
            return $canInsert;
        }
        if($request->kota == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Kota/Kabupaten harus diisi');
            return $canInsert;
        }
        if($request->provinsi == null){
            $canInsert = (object) array('can' => false, 'pesan' => 'Pilih Provinsi');
            return $canInsert;
        }
        return $canInsert;
    }
    
}
