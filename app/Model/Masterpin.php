<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Masterpin extends Model {

    public function getInsertMasterCoin($data){
        try {
            $lastInsertedID = DB::table('master_coin')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }

    public function getUpdateMasterCoin($fieldName, $name, $data){
        try {
            DB::table('master_coin')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }

    public function getTotalCoinPerusahaan(){
        $sql = DB::table('master_coin')
                    ->selectRaw('
                        sum(case when type = 1 then total_coin end) as sum_coin_masuk,
                        sum(case when type = 2 then total_coin end) as sum_coin_keluar_terjual,
                        sum(case when type = 3 then total_coin end) as sum_coin_keluar_terposting,
                        sum(case when type = 4 then total_coin end) as sum_coin_keluar_bonus_sp,
                        sum(case when type = 5 then total_coin end) as sum_coin_keluar_bonus_posting
                    ')
                    ->first();
        $coinMasuk = 0;
        $coinKeluarJual = 0;
        $coinKeluarPosting = 0;
        $coinKeluarBnsSp = 0;
        $coinKeluarBnsPost = 0;
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar_terjual != null){
            $coinKeluarJual = $sql->sum_coin_keluar_terjual;
        }
        if($sql->sum_coin_keluar_terposting != null){
            $coinKeluarPosting = $sql->sum_coin_keluar_terposting;
        }
        if($sql->sum_coin_keluar_bonus_sp != null){
            $coinKeluarBnsSp = $sql->sum_coin_keluar_bonus_sp;
        }
        if($sql->sum_coin_keluar_bonus_posting != null){
            $coinKeluarBnsPost = $sql->sum_coin_keluar_bonus_posting;
        }
        $data = (object) array(
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluarJual,
            'coin_keluar_posting' => $coinKeluarPosting,
            'coin_keluar_bns_sponsor' => $coinKeluarBnsSp,
            'coin_keluar_bns_posting' => $coinKeluarBnsPost
        );
        return $data;
    }

    public function getHistoryMasterCoinTransaction(){
        // 1 coin masuk
        // 2 coin keluar terjual
        // 3 coin keluar terposting
        // 4 coin keluar bonus sponsor
        // 5 coin keluar bonus profit prodting
        $sql = DB::table('master_coin')
                    ->selectRaw('transaction.id, transaction.transaction_code, transaction.type as trans_type, transaction.status as trans_status, '
                            . 'master_coin.total_coin, master_coin.type as coin_type, master_coin.created_at,'
                            . 'users.user_code, users.name, users.member_type')
                    ->leftJoin('transaction', 'transaction.id', '=', 'master_coin.transaction_id')
                    ->leftJoin('users', 'transaction.user_id', '=', 'users.id')
                    ->orderBy('master_coin.id', 'ASC')
                    ->get();
        return $sql;
    }

    public function getSumDebitMasterCoin(){
        // 1 coin masuk
        // 2 coin keluar terjual
        // 3 coin keluar terposting
        // 4 coin keluar bonus sponsor
        // 5 coin keluar bonus profit prodting
        $sql = DB::table('master_coin')
                    ->selectRaw('SUM(master_coin.total_coin) as total_coin, master_coin.type as coin_type')
                    ->where('master_coin.type', '=', 1)
                    ->groupBy('master_coin.type')
                    ->get();
        return $sql;
    }

    public function getSumCreditMasterCoin(){
        // 1 coin masuk
        // 2 coin keluar terjual
        // 3 coin keluar terposting
        // 4 coin keluar bonus sponsor
        // 5 coin keluar bonus profit prodting
        $sql = DB::table('master_coin')
                    ->selectRaw('SUM(master_coin.total_coin) as total_coin, master_coin.type as coin_type')
                    ->where('master_coin.type', '>', 1)
                    ->groupBy('master_coin.type')
                    ->get();
        return $sql;
    }

    public function getBalanceCoinGlobal(){
        $sql = DB::table('master_coin')
                    ->selectRaw('sum(total_coin) as sum_coin_balance')
                    ->where('type', '!=', 1)
                    ->first();
        return $sql;
    }


}
