<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Admin extends Model {
    
    public function getInsertUser($data){
        try {
            DB::table('users')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateMember($fieldName, $name, $data){
        try {
            DB::table('users')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllUserAdmin($dataUser){
        if($dataUser->user_type == 1){
            $sql = DB::table('users')
                    ->selectRaw('id, email, user_type, user_code')
                    ->whereIn('user_type', array(2, 3))
                    ->where('is_login', '=', 1)
                    ->get();
        } else {
            $sql = DB::table('users')
                    ->selectRaw('id, email, user_type')
                    ->where('user_type', '=', 3)
                    ->get();
        }
        return $sql;
    }
    
    public function getCekNewUsername($username){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_code', '=', $username)
                    ->first();
        return $sql;
    }
    
    public function getAdminById($id){
        $sql = DB::table('users')
                    ->selectRaw('id, email, user_type, user_code')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getAllUserExchanger(){
        $sql = DB::table('users')
                ->selectRaw('id, email, user_type, user_code, name, hp, active_at, created_at')
                ->where('member_type', '=', 1)
                ->where('is_active', '=', 1)
                ->orderBy('id', 'DESC')
                ->get();
        return $sql;
    }
    
}

