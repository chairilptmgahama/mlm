<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Bonus extends Model {
    
    public function getInsertBonusMember($data){
        try {
            $lastInsertedID = DB::table('bonus_member')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getTotalBonus($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('poin_type', '=', 1)
                    ->where('type', '<', 10)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        $return = (object) array(
            'total_bonus' => $total_bonus
        );
        return $return;
    }
    
    public function getTotalSafra($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_safra')
                    ->where('user_id', '=', $data->id)
                    ->where('poin_type', '=', 2)
                    ->first();
        $total_bonus = 0;
        if($sql->total_safra != null){
            $total_bonus = $sql->total_safra;
        }
        $return = (object) array(
            'total_safra' => $total_bonus
        );
        return $return;
    }
    
    public function getBonusSponsor($data){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.from_user_id', '=', 'users.id')
                    ->join('package', 'bonus_member.package_id', '=', 'package.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, users.user_code, package.name as package_name, bonus_member.created_at')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 1)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusPoin($data){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.from_user_id', '=', 'users.id')
                    ->join('package', 'bonus_member.package_id', '=', 'package.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, users.user_code, package.name as package_name, bonus_member.created_at')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 2)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusFly($data){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.from_user_id', '=', 'users.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, users.user_code, bonus_member.created_at, bonus_member.fly_id,'
                            . 'bonus_member.is_phu_claim, bonus_member.phu_claim_id, bonus_member.id, bonus_member.user_id')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 3)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getCekMemberHaveBonusFly($id, $bonus_id){
        $sql = DB::table('bonus_member')
                    ->selectRaw('bonus_price, bonus_date, created_at, fly_id, phu_claim_id')
                    ->where('user_id', '=', $id)
                    ->where('id', '=', $bonus_id)
                    ->where('type', '=', 3)
                    ->where('is_phu_claim', '=', 0)
                    ->first();
        return $sql;
    }
    
    public function getAdminBonusFly(){
        $sql = DB::table('bonus_member')
                    ->join('users as b', 'bonus_member.user_id', '=', 'b.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, bonus_member.created_at, bonus_member.fly_id,'
                            . 'bonus_member.is_phu_claim, bonus_member.phu_claim_id, bonus_member.id, bonus_member.user_id,'
                            . 'b.user_code, b.name, b.active_at')
                    ->where('bonus_member.type', '=', 3)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusLevel($data){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.from_user_id', '=', 'users.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, users.name, users.user_code, bonus_member.level_id')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 3)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusRO($data){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.from_user_id', '=', 'users.id')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, users.name, users.user_code, bonus_member.total_pin')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 4)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusBinary($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('bonus_member.bonus_price, bonus_member.bonus_date, '
                            . 'bonus_member.total_pin, bonus_member.total_binary')
                    ->where('bonus_member.user_id', '=', $data->id)
                    ->where('bonus_member.type', '=', 2)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->whereNotNull('bonus_member.total_binary')
                    ->orderBy('bonus_member.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusTotalPerType($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(case when type = 1 then bonus_price end) as total_bonus_start,'
                            . 'sum(case when type = 2 then bonus_price end) as total_bonus_binary,'
                            . 'sum(case when type = 3 then bonus_price end) as total_bonus_level,'
                            . 'sum(case when type = 4 then bonus_price end) as total_bonus_ro')
                    ->where('user_id', '=', $data->id)
                    ->where('poin_type', '=', 1)
                    ->first();
        $total_bonus_start = 0;
        if($sql->total_bonus_start != null){
            $total_bonus_start = $sql->total_bonus_start;
        }
        $total_bonus_binary = 0;
        if($sql->total_bonus_binary != null){
            $total_bonus_binary = $sql->total_bonus_binary;
        }
        $total_bonus_level = 0;
        if($sql->total_bonus_level != null){
            $total_bonus_level = $sql->total_bonus_level;
        }
        $total_bonus_ro = 0;
        if($sql->total_bonus_ro != null){
            $total_bonus_ro = $sql->total_bonus_ro;
        }
        $return = (object) array(
            'total_bonus_start' => $total_bonus_start,
            'total_bonus_binary' => $total_bonus_binary,
            'total_bonus_level' => $total_bonus_level,
            'total_bonus_ro' => $total_bonus_ro
        );
        return $return;
    }
    
    public function getCronBonusSponsor(){
        $today = date('Y-m-d');
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->join('bank', 'users.id', '=', 'bank.user_id')
                    ->selectRaw('users.id as id_user,  users.user_code, '
                            . 'sum(bonus_member.bonus_price) as total_bonus, '
                            . 'bank.id id_bank')
                    ->where('bonus_member.type', '=', 1)
                    ->where('bonus_member.is_wd_sp', '=', 0)
                    ->where('bank.is_active', '=', 1)
                    ->whereDate('bonus_member.bonus_date', '<', $today)
                    ->groupBy('users.user_code')
                    ->groupBy('bank.id')
                    ->groupBy('users.id')
                    ->orderBy('users.id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getCronBonusBinary(){
        $today = date('Y-m-d');
        $backDate = date('Y-m-d', strtotime($today."-25 days"));
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->join('bank', 'users.id', '=', 'bank.user_id')
                    ->join('package', 'users.package_id', '=', 'package.id')
                    ->selectRaw('users.id as id_user, sum(bonus_member.bonus_price) as total_bonus, users.user_code, '
                            . 'bank.id id_bank, package.stock_wd, sum(bonus_member.total_pin) as pin_total, users.active_at')
                    ->where('bonus_member.type', '=', 2)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->where('bank.is_active', '=', 1)
                    ->whereDate('bonus_member.bonus_date', '>=', $backDate)
                    ->whereDate('bonus_member.bonus_date', '<', $today)
                    ->groupBy('users.user_code')
                    ->groupBy('bank.id')
                    ->groupBy('users.id')
                    ->groupBy('package.stock_wd')
                    ->groupBy('users.active_at')
                    ->orderBy('users.id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getTotalBonusSponsor($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 1)
                    ->where('poin_type', '=', 1)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getTotalBonusBinary($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 2)
                    ->where('poin_type', '=', 1)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getTotalBonusLevel($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 3)
                    ->where('poin_type', '=', 1)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getTotalBonusRO($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 4)
                    ->where('poin_type', '=', 1)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getTotalBonusSafraPoin($data){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $data->id)
                    ->where('poin_type', '=', 2)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getAdminGlobalBonus(){
        $sql = DB::table('bonus_member')
                    ->selectRaw('bonus_date, 
                            sum(case when type = 1 then bonus_price else 0 end) as tot_bonus_sp,
                            sum(case when type = 2 then bonus_price else 0 end) as tot_bonus_pasangan,
                            sum(case when type = 3 then bonus_price else 0 end) as tot_bonus_level,
                            sum(case when type = 4 then bonus_price else 0 end) as tot_bonus_ro,
                            sum(bonus_price) as total_all_bonus
                    ')
                    ->groupBy('bonus_date')
                    ->orderBy('bonus_date', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminBonusMember(){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->selectRaw('users.id, users.user_code, users.hp,
		sum(case when bonus_member.type = 1 then bonus_member.bonus_price else 0 end) as tot_bonus_sp,
		sum(case when bonus_member.type = 2 then bonus_member.bonus_price else 0 end) as tot_bonus_pasangan,
		sum(case when bonus_member.type = 3 then bonus_member.bonus_price else 0 end) as tot_bonus_level,
		sum(case when bonus_member.type = 4 then bonus_member.bonus_price else 0 end) as tot_bonus_ro
                    ')
                    ->where('bonus_member.poin_type', '=', 1)
                    ->groupBy('users.id')
                    ->groupBy('users.user_code')
                    ->groupBy('users.hp')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminSaldoMember(){
        $query = "SELECT all_member.id, all_member.user_code, all_member.hp, 
		sum(all_member.tot_bonus_all) as total_all_bonus,
		sum(all_member.tot_bonus_safra) as total_safra_bonus,
		sum(all_member.tot_wd_tuntas) as total_tuntas_wd,
		sum(all_member.tot_wd_proses) as total_proses_wd,
		sum(all_member.tot_fee_tuntas) as total_tuntas_fee,
		sum(all_member.tot_fee_proses) as total_proses_fee
	FROM (
	SELECT users.id, users.user_code, users.hp, 
		sum(case when bonus_member.poin_type = 1 then bonus_member.bonus_price else 0 end) as tot_bonus_all,
		sum(case when bonus_member.poin_type = 2 then bonus_member.bonus_price else 0 end) as tot_bonus_safra,
		0 as tot_wd_tuntas, 0 as tot_wd_proses, 0 as tot_fee_tuntas, 0 as tot_fee_proses
	FROM users
	JOIN bonus_member ON users.id = bonus_member.user_id
	WHERE users.user_type = 10
	GROUP BY users.id, users.user_code, users.hp
	UNION
	SELECT users.id, users.user_code, users.hp, 
		0 as tot_bonus_all, 0 as tot_bonus_safra,
		sum(case when transfer_wd.status = 1 then transfer_wd.wd_total else 0 end) as tot_wd_tuntas,
		sum(case when transfer_wd.status = 0 then transfer_wd.wd_total else 0 end) as tot_wd_proses,
		sum(case when transfer_wd.status = 1 then transfer_wd.admin_fee else 0 end) as tot_fee_tuntas,
		sum(case when transfer_wd.status = 0 then transfer_wd.admin_fee else 0 end) as tot_fee_proses
	FROM users
	JOIN transfer_wd ON users.id = transfer_wd.user_id
	WHERE users.user_type = 10
	GROUP BY users.id, users.user_code, users.hp
	) as all_member
	GROUP BY all_member.id, all_member.user_code, all_member.hp";
        $sql = DB::select($query);
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getInsertRekapBonus($data){
        try {
            $lastInsertedID = DB::table('rekap_bonus')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getBonusBinaryByDate(){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->join('bonus_team', 'users.member_type', '=', 'bonus_team.id')
                    ->selectRaw('bonus_member.user_id, sum(bonus_member.bonus_price) as total_get_bonus, '
                            . 'sum(bonus_member.total_pin) as toal_get_pin, '
                            . 'DATE(bonus_member.bonus_date) as day,'
                            . 'bonus_team.max_day')
                    ->where('bonus_member.type', '=', 2)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->whereDate('bonus_member.bonus_date', '>=', '2019-09-17')
                    ->whereNotNull('bonus_member.total_binary')
                    ->groupBy('bonus_member.bonus_date')
                    ->groupBy('bonus_member.user_id')
                    ->groupBy('bonus_team.max_day')
                    ->orderBy('bonus_member.user_id', 'ASC')
                    ->orderBy('bonus_member.bonus_date', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getBonusBinaryYesterday($dateYesterday){
        $sql = DB::table('bonus_member')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->join('bonus_team', 'users.member_type', '=', 'bonus_team.id')
                    ->selectRaw('bonus_member.user_id, sum(bonus_member.bonus_price) as total_get_bonus, '
                            . 'sum(bonus_member.total_pin) as toal_get_pin, '
                            . 'DATE(bonus_member.bonus_date) as day,'
                            . 'bonus_team.max_day')
                    ->where('bonus_member.type', '=', 2)
                    ->where('bonus_member.poin_type', '=', 1)
                    ->whereDate('bonus_member.bonus_date', '=', $dateYesterday)
                    ->whereNotNull('bonus_member.total_binary')
                    ->groupBy('bonus_member.bonus_date')
                    ->groupBy('bonus_member.user_id')
                    ->groupBy('bonus_team.max_day')
                    ->orderBy('bonus_member.user_id', 'ASC')
                    ->orderBy('bonus_member.bonus_date', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getRekapBonusBinary($id, $date){
        $return = DB::table('rekap_bonus')
                    ->selectRaw('id')
                    ->where('user_id', '=', $id)
                    ->whereDate('rekap_date', '=', $date)
                    ->where('type', '=', 2)
                    ->where('status', '=', 0)
                    ->first();
        return $return;
    }
    
    public function getSumBonusNew($user_id, $type){
        $sql = DB::table('bonus_member')
                    ->selectRaw('sum(bonus_price) as total_bonus')
                    ->where('user_id', '=', $user_id)
                    ->where('type', '=', $type)
                    ->first();
        $total_bonus = 0;
        if($sql->total_bonus != null){
            $total_bonus = $sql->total_bonus;
        }
        return $total_bonus;
    }
    
    public function getCountMemberGetBonusPHU($paket){
        $sql = DB::table('bonus_member')
                    ->selectRaw('bonus_member.id')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->where('bonus_member.type', '=', 3)
                    ->where('users.package_id', '=', $paket)
                    ->count();
        return $sql;
    }
    
    public function getMaxPHUUser(){
        $maxPHU = 6;
        return $maxPHU;
    }
    
    public function getAdminFee(){
        $data = 5;
        return $data;
    }
    
    public function getMaxWD(){
        $data = 100000;
        return $data;
    }
    
    public function getIDMemberGetBonusPHU(){
        $sql = DB::table('bonus_member')
                    ->selectRaw('bonus_member.user_id')
                    ->where('bonus_member.type', '=', 3)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getMemberTodayPHU(){
        $date = date('Y-m-d');
        $sql = DB::table('bonus_member')
                    ->selectRaw('users.id, users.user_code, users.email, users.name, bonus_member.bonus_date, bonus_member.bonus_price')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->where('bonus_member.type', '=', 3)
                    ->whereDate('bonus_member.bonus_date', '=', $date)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getMembersTodayPHU(){
        $date = date('Y-m-d');
        $sql = DB::table('bonus_member')
                    ->selectRaw('users.id, users.user_code, users.email, users.name, bonus_member.bonus_date, bonus_member.bonus_price')
                    ->join('users', 'bonus_member.user_id', '=', 'users.id')
                    ->where('bonus_member.type', '=', 3)
                    ->whereDate('bonus_member.bonus_date', '=', $date)
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getUpdateBonusSponsor($user_id, $data){
        $today = date('Y-m-d');
        try {
            DB::table('bonus_member')
                    ->where('user_id', '=', $user_id)
                    ->where('type', '=', 1)
                    ->where('is_wd_sp', '=', 0)
                    ->whereDate('bonus_date', '<', $today)
                    ->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateBonusPHU($fieldName, $name, $data){
        try {
            DB::table('bonus_member')->where($fieldName, '=', $name)->where('type', '=', 3)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
   
    
}
