<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Pin extends Model {
    
    public function getInsertMemberCoin($data){
        try {
            $lastInsertedID = DB::table('member_coin')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateMemberCoin($fieldName, $name, $data){
        try {
            DB::table('member_coin')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getCoinAvailable($id){
        $sql = DB::table('member_coin')
                    ->selectRaw('
		sum(case when type = 1 then qty end) as sum_coin_masuk,
		sum(case when type = 2 then qty end) as sum_coin_keluar
                    ')
                    ->where('user_id', '=', $id)
                    ->first();
        $sqlTrans = DB::table('transaction')
                    ->selectRaw('sum(total_coin) as coin_blm_tuntas')
                    ->where('status', '<', 2)
                    ->where('type', '=', 3) //->whereIn('type', array(1, 2)) //sementara cek pakai ini dl
                    ->where('user_id', '=', $id) //->where('seller_id', '=', $id)
                    ->first();
//        $sqlTransJual = DB::table('transaction')
//                    ->selectRaw('sum(total_coin) as coin_blm_tuntas')
//                    ->whereIn('status', array(0, 1))
//                    ->where('type', '=', 3)
//                    ->where('seller_id', '=', $id)
//                    ->first();
        $coinGantung = 0;
        $coinMasuk = 0;
        $coinKeluar = 0;
        if($sqlTrans->coin_blm_tuntas != null){
            $coinGantung = $sqlTrans->coin_blm_tuntas;
        }
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar != null){
            $coinKeluar = $sql->sum_coin_keluar;
        }
        $data = (object) array(
            'coin_blm_tuntas' => $coinGantung,
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluar
        );
        return $data;
    }
    
    public function getTotalPinAdmin(){
        $sql = DB::table('member_coin')
                    ->selectRaw('
		sum(case when type_pin = 1 then total_pin end) as sum_pin_masuk,
		sum(case when type_pin = 2 then total_pin end) as sum_pin_keluar
                    ')
                    ->first();
        return $sql;
    }
    
    public function getTotalPinMember($data){
        $sql = DB::table('member_pin')
                    ->selectRaw('
		sum(case when is_used = 0 then total_pin else 0 end) as sum_pin_masuk,
		sum(case when is_used = 1 then total_pin else 0 end) as sum_pin_keluar,
                                    sum(case when is_used = 1 and pin_status = 1 then total_pin else 0 end) as sum_pin_terpakai,
                                    sum(case when is_used = 0 and is_terima = 1 then total_pin else 0 end) as sum_pin_masuk_terima,
                                    sum(case when is_used = 1 and pin_status = 2 and is_terima = 1 then total_pin else 0 end) as sum_pin_keluar_terima,
                                    sum(case when is_used = 1 and pin_status = 2 then total_pin else 0 end) as sum_pin_keluar_ditransfer
                    ')
                    ->where('user_id', '=', $data->id)
                    ->first();
        return $sql;
    }
    
    public function getTotalPinMember2($data){
        $sql = DB::table('member_pin')
                    ->selectRaw('
		sum(case when is_used = 0 then total_pin else 0 end) as sum_pin_masuk,
		sum(case when is_used = 1 then total_pin else 0 end) as sum_pin_keluar,
                                    sum(case when is_used = 1 and pin_status = 1 then total_pin else 0 end) as sum_pin_terpakai,
                                    sum(case when is_used = 0 and is_terima = 1 then total_pin else 0 end) as sum_pin_masuk_terima,
                                    sum(case when is_used = 1 and pin_status = 2 and is_terima = 1 then total_pin else 0 end) as sum_pin_keluar_terima,
                                    sum(case when is_used = 1 and pin_status = 2  and is_terima = 0 then total_pin else 0 end) as sum_pin_keluar_ditransfer
                    ')
                    ->where('user_id', '=', $data->id)
                    ->first();
        return $sql;
    }
    
    public function getMyLastPin($data){
        $sql = DB::table('member_pin')
                    ->selectRaw('setting_pin, pin_code')
                    ->where('user_id', '=', $data->id)
                    ->where('is_used', '=', 0)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getMyHistoryPin($data){
        $sql = DB::table('member_pin')
                    ->leftJoin('users as u1', 'member_pin.used_user_id', '=', 'u1.id')
                    ->leftJoin('users as u2', 'member_pin.transfer_user_id', '=', 'u2.id')
                    ->leftJoin('users as u3', 'member_pin.transfer_from_user_id', '=', 'u3.id')
                    ->selectRaw('member_pin.total_pin, member_pin.pin_status, member_pin.is_used, member_pin.used_at, '
                            . 'member_pin.created_at, member_pin.transaction_code, member_pin.is_ro, member_pin.is_upgrade, '
                            . 'u1.name as name_activation, '
                            . 'u2.name as name_transfer_to, '
                            . 'u3.name as name_transfer_from')
                    ->where('member_pin.user_id', '=', $data->id)
                    ->orderBy('member_pin.id', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getMyTotalPinPengiriman($data){
        $sql = DB::table('member_pin')
                    ->selectRaw('sum(total_pin) as pin_tersedia')
                    ->where('user_id', '=', $data->id)
                    ->where('is_used', '=', 0)
                    ->first();
        return $sql;
    }
    
    public function getTotalPinMemberRO($id, $dateStart){
        $dateEnd = date('Y-m-d', strtotime($dateStart.'+30 days'));
        $sql = DB::table('member_pin')
                ->selectRaw('sum(total_pin) as pin_total')
                ->where('user_id', '=', $id)
                ->where('is_used', '=', 1)
                ->where('is_ro', '=', 1)
                ->whereDate('used_at', '>=', $dateStart)
                ->whereDate('used_at', '<', $dateEnd)
                ->first();
        return $sql;
    }
    
    public function getAdminGLobalSales(){
        $sql = DB::table('member_pin')
                    ->selectRaw(' date(created_at) as date_sales,
                        sum(case when is_used = 1 and pin_status = 1 and used_user_id is not null then total_pin else 0 end) as total_pin_aktifasi,
                        sum(case when is_used = 1 and pin_status = 1 and is_ro = 1 then total_pin else 0 end) as total_pin_ro,
                        sum(case when is_used = 1 and pin_status = 1 and is_upgrade = 1 then total_pin else 0 end) as total_pin_upgrade
                    ')
                    ->where('is_used', '=', 1)
                    ->groupBy('date_sales')
                    ->orderBy('date_sales', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminGLobalMemberTransaction(){
        $sql = DB::table('member_pin')
                    ->join('users', 'member_pin.user_id', '=', 'users.id')
                    ->selectRaw('
                        date(member_pin.created_at) as tgl_trans, member_pin.user_id, users.user_code, users.hp,
                        sum(case when member_pin.is_used = 1 and member_pin.pin_status = 1 and member_pin.used_user_id is not null then total_pin else 0 end) as total_pin_aktifasi,
                        sum(case when member_pin.is_used = 1 and member_pin.pin_status = 1 and member_pin.is_ro = 1 then total_pin else 0 end) as total_pin_ro,
                        sum(case when member_pin.is_used = 1 and member_pin.pin_status = 1 and member_pin.is_upgrade = 1 then total_pin else 0 end) as total_pin_upgrade
                    ')
                    ->where('is_used', '=', 1)
                    ->groupBy('tgl_trans')
                    ->groupBy('member_pin.user_id')
                    ->groupBy('users.user_code')
                    ->groupBy('users.hp')
                    ->orderBy('tgl_trans', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminPinMember(){
        $query = "SELECT all_member_pin.id, all_member_pin.user_code, all_member_pin.hp,
		sum(all_member_pin.sum_pin_masuk) as sum_pin_masuk, 
                                    sum(all_member_pin.sum_pin_keluar) as sum_pin_keluar, 
                                    sum(all_member_pin.sum_pin_terpakai) as sum_pin_terpakai,
		sum(all_member_pin.sum_pin_masuk_terima) as sum_pin_masuk_terima, 
                                    sum(all_member_pin.sum_pin_keluar_terima) as sum_pin_keluar_terima, 
                                    sum(all_member_pin.sum_pin_keluar_ditransfer) as sum_pin_keluar_ditransfer,
		sum(all_member_pin.total_pin_tuntas) as total_pin_tuntas, 
                                    sum(all_member_pin.total_pin_proses_dan_tuntas) as total_pin_proses_dan_tuntas
	FROM ( 
	SELECT users.id, users.user_code, users.hp,
		sum(case when member_pin.is_used = 0 then member_pin.total_pin else 0 end) as sum_pin_masuk,
		sum(case when member_pin.is_used = 1 then member_pin.total_pin else 0 end) as sum_pin_keluar,
		sum(case when member_pin.is_used = 1 and member_pin.pin_status = 1 then member_pin.total_pin else 0 end) as sum_pin_terpakai,
		sum(case when member_pin.is_used = 0 and member_pin.is_terima = 1 then member_pin.total_pin else 0 end) as sum_pin_masuk_terima,
		sum(case when member_pin.is_used = 1 and member_pin.pin_status = 2 and member_pin.is_terima = 1 then member_pin.total_pin else 0 end) as sum_pin_keluar_terima,
		sum(case when member_pin.is_used = 1 and member_pin.pin_status = 2  and member_pin.is_terima = 0 then member_pin.total_pin else 0 end) as sum_pin_keluar_ditransfer,
		0 as total_pin_tuntas, 0 as total_pin_proses_dan_tuntas
	FROM users
	JOIN member_pin ON users.id = member_pin.user_id
	WHERE users.user_type = 10
	GROUP BY users.id, users.user_code, users.hp
	UNION 
	SELECT users.id, users.user_code, users.hp, 
			0 as sum_pin_masuk, 0 as sum_pin_keluar, 0 as sum_pin_terpakai, 
			0 as sum_pin_masuk_terima, 0 as sum_pin_keluar_terima, 0 as sum_pin_keluar_ditransfer,
			sum(case when status = 1 then total_pin else 0 end) as total_pin_tuntas, 
			sum(case when status != 2 then total_pin else 0 end) as total_pin_proses_dan_tuntas
	FROM users
	JOIN pengiriman_paket ON users.id = pengiriman_paket.user_id
	WHERE users.user_type = 10
	GROUP BY users.id, users.user_code, users.hp
	) as all_member_pin
	GROUP BY all_member_pin.id, all_member_pin.user_code, all_member_pin.hp";
        $sql = DB::select($query);
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getInsertPostingCoin($data){
        try {
            $lastInsertedID = DB::table('posting_coin')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdatePostingCoin($fieldName, $name, $data){
        try {
            DB::table('posting_coin')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getPostingCoinLast24Hours($posting_id, $user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('id, qty, active_at, created_at')
                    ->where('type', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->where('posting_id', '=', $posting_id)
                    ->where('active_at', '>', $data->start)
                    ->where('active_at', '<', $data->end)
                    ->first();
        return $sql;
    }
    
    public function getTotalPostingCoinBefore24Hours($user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('sum(qty) as total_coin_posting')
                    ->where('type', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->where('active_at', '<', $data->start)
                    ->first();
        $coinMasuk = 0;
        if($sql->total_coin_posting != null){
            $coinMasuk = $sql->total_coin_posting;
        }
        $return = (object) array(
            'coin_masuk' => $coinMasuk,
        );
        return $return;
    }
    
    public function getPostingCoinHistory($posting_id, $user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('id, qty, active_at, created_at, persentase')
                    ->where('type', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->where('posting_id', '=', $posting_id)
                    ->where('active_at', '<', $data->start)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTotalPostingCoinBefore24HoursByID($posting_id, $user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('sum(qty) as total_coin_posting')
                    ->where('type', '=', 1)
                    ->where('user_id', '=', $user_id)
                    ->where('posting_id', '=', $posting_id)
                    ->where('active_at', '<', $data->start)
                    ->first();
        $coinMasuk = 0;
        if($sql->total_coin_posting != null){
            $coinMasuk = $sql->total_coin_posting;
        }
        $return = (object) array(
            'coin_masuk' => $coinMasuk,
        );
        return $return;
    }
    
    public function getCoinPostingAvailableByID($posting_id, $user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('
		sum(case when type = 1 and active_at < \''.$data->start.'\'  then qty end) as sum_coin_masuk,
		sum(case when type = 2 then qty end) as sum_coin_keluar
                    ')
                    ->where('user_id', '=', $user_id)
                    ->where('posting_id', '=', $posting_id)
                    ->first();
        
        $coinMasuk = 0;
        $coinKeluar = 0;
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar != null){
            $coinKeluar = $sql->sum_coin_keluar;
        }
        $return = (object) array(
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluar
        );
        return $return;
    }
    
    public function getCoinPostingAvailableAllPosting($user_id, $data){
        $sql = DB::table('posting_coin')
                    ->selectRaw('
		sum(case when type = 1 and active_at < \''.$data->start.'\'  then qty end) as sum_coin_masuk,
		sum(case when type = 2 then qty end) as sum_coin_keluar
                    ')
                    ->where('user_id', '=', $user_id)
                    ->first();
        
        $coinMasuk = 0;
        $coinKeluar = 0;
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar != null){
            $coinKeluar = $sql->sum_coin_keluar;
        }
        $return = (object) array(
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluar
        );
        return $return;
    }
    
    public function getArrayPhasePersentase($data){
        $batasHari = $data->hari * $data->phase;
        $batasPersen = $data->persentase * $data->phase;
        $rumus = $batasPersen/$batasHari;
        $randomPenjumlah = array(0, 0.1, -0.12, 0.121, 0.14, 0.17, 0, -0.1, -0.12, -0.121, -0.14, -0.17, -0.2, 0.2, -0.2, 0.16, -0.1, 0.1, 0.2, -0.2, 0.19, 0.18);
        $array = array();
        for ($i = 1; $i <= $batasHari; $i++) {
            $getIndex = rand(0, 20);
            $getPersen = $rumus + $randomPenjumlah[$getIndex];
            $array[] = number_format($getPersen, 2);
        }
        return $array;
    }
    
    public function getPhaseHari(){
        $data = 30;
        return $data;
    }
    
    public function getRange24Hours(){
        $dateNow = date('Y-m-d H:i:s');
        $last24Hour = date('Y-m-d H:i:s', strtotime('-24 hours', strtotime($dateNow))); //10 minutes
        $dateRange = (object) array(
            'start' => $last24Hour,
            'end' => $dateNow
        );
        return $dateRange;
    }
    
    public function getCoinGlobalAvailable(){
        $sql = DB::table('member_coin')
                    ->selectRaw('
		sum(case when type = 1 then qty end) as sum_coin_masuk,
		sum(case when type = 2 then qty end) as sum_coin_keluar
                    ')
                    ->first();
        $sqlTrans = DB::table('transaction')
                    ->selectRaw('sum(total_coin) as coin_blm_tuntas')
                    ->where('status', '<', 2)
                    ->where('type', '=', 3)
                    ->first();
        $coinGantung = 0;
        $coinMasuk = 0;
        $coinKeluar = 0;
        if($sqlTrans->coin_blm_tuntas != null){
            $coinGantung = $sqlTrans->coin_blm_tuntas;
        }
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar != null){
            $coinKeluar = $sql->sum_coin_keluar;
        }
        $data = (object) array(
            'coin_blm_tuntas' => $coinGantung,
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluar
        );
        return $data;
        return $data;
    }
    
    public function getCoinPostingGlobalAvailableAllPosting(){
        $sql = DB::table('posting_coin')
                    ->selectRaw('
		sum(case when type = 1 then qty end) as sum_coin_masuk,
		sum(case when type = 2 then qty end) as sum_coin_keluar
                    ')
                    ->first();
        
        $coinMasuk = 0;
        $coinKeluar = 0;
        if($sql->sum_coin_masuk != null){
            $coinMasuk = $sql->sum_coin_masuk;
        }
        if($sql->sum_coin_keluar != null){
            $coinKeluar = $sql->sum_coin_keluar;
        }
        $return = (object) array(
            'coin_masuk' => $coinMasuk,
            'coin_keluar' => $coinKeluar
        );
        return $return;
    }
    
    
}
