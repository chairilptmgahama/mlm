<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Package extends Model {
    
    public function getInsertPackage($data){
        try {
            DB::table('package')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdatePackage($id, $data){
        try {
            DB::table('package')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getPackageId($id){
        $sql = DB::table('package')
                    ->where('id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getPackageIdDshboard($id){
        $sql = DB::table('package')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getAllPackage(){
        $sql = DB::table('package')
                    ->where('is_active', '=', 1)
                    ->orderBy('id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAllPackageUpgrade($data){
        $sql = DB::table('package')
                    ->selectRaw('id, name, short_desc, pin, stock_wd, discount, safra_discount, img_package')
                    ->where('id', '>', $data->package_id)
                    ->whereNull('deleted_at')
                    ->orderBy('pin')
                    ->get();
        return $sql;
    }
    
    public function getMyPackage($id){
        $sql = DB::table('package')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getMyPackagePin($total_pin){
        $sql = DB::table('package')
                    ->selectRaw('id, name, short_desc, pin, stock_wd, discount, safra_discount')
                    ->where('pin', '=', $total_pin)
                    ->first();
        return $sql;
    }
    
    public function getInsertMemberPackagePin($data){
        try {
            $lastInsertedID = DB::table('package_pin')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getInsertMemberStock($data){
        try {
            $lastInsertedID = DB::table('stock_pin')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getDeleteStockPin($id, $userID){
        try {
            DB::table('stock_pin')->where('package_pin_id', '=', $id)->where('user_id', '=', $userID)->delete();
            $result = (object) array('status' => true, 'message' => null, 'lastID' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getDeleteStockPinByID($id){
        try {
            DB::table('stock_pin')->where('package_pin_id', '=', $id)->delete();
            $result = (object) array('status' => true, 'message' => null, 'lastID' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getStockistPackage($user_id){
        $sql = DB::table('transaction')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->join('package_pin', 'transaction.id', '=', 'package_pin.transaction_id')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->selectRaw('sum(package_pin.qty) as total_qty, '
                            . 'package.name, package.code, package.price, package.image, package.stockist_price,'
                            . 'package.m_stockist_price, package.id')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->whereNull('package_pin.deleted_at')
                    ->groupBy('package.name')
                    ->groupBy('package.code')
                    ->groupBy('package.image')
                    ->groupBy('package.price')
                    ->groupBy('package.stockist_price')
                    ->groupBy('package.m_stockist_price')
                    ->groupBy('package.id')
                    ->orderBy('package.id')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getStockistByPackageId($user_id, $package_id){
        $sql = DB::table('transaction')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->join('package_pin', 'transaction.id', '=', 'package_pin.transaction_id')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->selectRaw('sum(package_pin.qty) as total_qty, '
                            . 'package.name, package.code, package.price, package.image, package.stockist_price,'
                            . 'package.m_stockist_price, package.id')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->where('package_pin.package_id', '=', $package_id)
                    ->whereNull('package_pin.deleted_at')
                    ->groupBy('package.name')
                    ->groupBy('package.code')
                    ->groupBy('package.image')
                    ->groupBy('package.price')
                    ->groupBy('package.stockist_price')
                    ->groupBy('package.m_stockist_price')
                    ->groupBy('package.id')
                    ->orderBy('package.id')
                    ->first();
        return $sql;
    }
    
    public function getSumStock($stockist_id, $package_id){
        $query = " 
            SELECT sum(data_stock.amount) as jml_keluar
            FROM (
                SELECT 
                        amount
                FROM stock_pin
                WHERE user_id = $stockist_id
                AND package_id = $package_id
                AND type = 2
                GROUP BY user_id, amount
            ) as data_stock ";
        $sql = DB::select($query);
        $return = 0;
        if($sql[0]->jml_keluar != null){
            $return = $sql[0]->jml_keluar;
        }
        return $return;
    }
    
    public function getHistoryPinMasuk($user_id){
        $sql = DB::table('transaction')
                    ->join('package_pin', 'transaction.id', '=', 'package_pin.transaction_id')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->selectRaw('package.name, package.image, '
                            . 'package.id, package_pin.qty, package_pin.created_at,'
                            . 'transaction.type')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->whereNull('package_pin.deleted_at')
                    ->orderBy('package_pin.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getHistoryPinKeluar($user_id){
        $sql = DB::table('activation_user')
                    ->join('package', 'package.id', '=', 'activation_user.package_id')
                    ->join('users', 'activation_user.user_id', '=', 'users.id')
                    ->selectRaw('package.name, package.image, '
                            . 'package.id, activation_user.created_at, users.user_code')
                    ->where('activation_user.sponsor_id', '=', $user_id)
                    ->orderBy('activation_user.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getHistoryPinKeluarStockist($user_id){
        $sql = DB::table('activation_user')
                    ->join('package', 'package.id', '=', 'activation_user.package_id')
                    ->join('users', 'activation_user.user_id', '=', 'users.id')
                    ->selectRaw('package.name, package.image, '
                            . 'package.id, activation_user.created_at, users.user_code')
                    ->where('activation_user.stockist_id', '=', $user_id)
                    ->orderBy('activation_user.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getHistoryPinKeluarPenjualan($user_id){
        $sql = DB::table('transaction')
                    ->join('package_pin', 'transaction.id', '=', 'package_pin.transaction_id')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->selectRaw('package.name, package.image, transaction.status, transaction.type, '
                            . 'package.id, package_pin.qty, package_pin.created_at, users.user_code')
                    ->where('transaction.status', '<', 3)
                    ->where('transaction.seller_id', '=', $user_id)
//                    ->whereNull('package_pin.deleted_at')
                    ->orderBy('package_pin.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function generateKey() {
        
        $charsetlower = "abcdefghijklmnopqrstuvwxyz";
        $key_lower = '';
        for ($i = 0; $i < 3; $i++) {
            $key_lower .= $charsetlower[(mt_rand(0, strlen($charsetlower) - 1))];
        }
        
        $charsetnumber = "1234567890";
        $key_number = '';
        for ($i = 0; $i < 2; $i++) {
            $key_number .= $charsetnumber[(mt_rand(0, strlen($charsetnumber) - 1))];
        }

        $charset = $key_lower.$key_number;
        $key = date('Ymd').'_'.str_shuffle($charset);
        return $key;
        
    }
    
    public function getCekStockistPinKeluarAktivasi($user_id){
        $sql = DB::table('activation_user')
                    ->join('stock_pin', 'stock_pin.activate_user_id', '=', 'activation_user.id')
                    ->selectRaw('sum(stock_pin.amount) as total_keluar')
                    ->where('activation_user.stockist_id', '=', $user_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getCekStockistPinKeluarAktivasiPackage($user_id, $package_id){
        $sql = DB::table('activation_user')
                    ->join('stock_pin', 'stock_pin.activate_user_id', '=', 'activation_user.id')
                    ->selectRaw('sum(stock_pin.amount) as total_keluar')
                    ->where('activation_user.stockist_id', '=', $user_id)
                    ->where('activation_user.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getStockistPinMasukPackageId($user_id, $package_id){
        $sql = DB::table('transaction')
                    ->join('package_pin', 'transaction.id', '=', 'package_pin.transaction_id')
                    ->selectRaw('sum(package_pin.qty) as total_masuk')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->where('package_pin.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_masuk != null){
            $qty = $sql->total_masuk;
        }
        $data = (object) array(
            'total_masuk' => $qty
        );
        return $data;
    }
    
    public function getStockistPinOutAktivasiPackageId($user_id, $package_id){
        $sql = DB::table('activation_user')
                    ->join('stock_pin', 'stock_pin.activate_user_id', '=', 'activation_user.id')
                    ->selectRaw('sum(stock_pin.amount) as total_keluar')
                    ->where('activation_user.stockist_id', '=', $user_id)
                    ->where('activation_user.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getCodeActivation($package_id){
        $sql = DB::table('activation_user')
                    ->selectRaw('id')
                    ->where('package_id', '=', $package_id)
                    ->count();
        $tmp = $sql + 1;
        $code = sprintf("%09s", $tmp);
        $dataArray = (object) array(
            'code' => $code,
            'total_new' => $tmp
        );
        return $dataArray;
    }
    
    public function getMemberPinKeluarAktivasiPackageId($user_id, $package_id){
        $sql = DB::table('activation_user')
                    ->join('stock_pin', 'stock_pin.activate_user_id', '=', 'activation_user.id')
                    ->selectRaw('sum(stock_pin.amount) as total_keluar')
                    ->where('activation_user.sponsor_id', '=', $user_id)
                    ->where('activation_user.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getCekMemberPinKeluarAktivasi($user_id){
        $sql = DB::table('activation_user')
                    ->join('stock_pin', 'stock_pin.activate_user_id', '=', 'activation_user.id')
                    ->selectRaw('sum(stock_pin.amount) as total_keluar')
                    ->where('activation_user.sponsor_id', '=', $user_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    
}
