<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Transaction extends Model {
    
    public function getInsertTransaction($data){
        try {
            $lastInsertedID = DB::table('transaction')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateTransaction($fieldName, $name, $data){
        try {
            DB::table('transaction')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getCodeTransaction($type){
        $charsetlower = "abcdefghijklmnopqrstuvwxyz";
        $key_lower = '';
        for ($i = 0; $i < 6; $i++) {
            $key_lower .= $charsetlower[(mt_rand(0, strlen($charsetlower) - 1))];
        }
        
        $charsetnumber = "1234567890";
        $key_number = '';
        for ($i = 0; $i < 3; $i++) {
            $key_number .= $charsetnumber[(mt_rand(0, strlen($charsetnumber) - 1))];
        }
        $code = $key_lower;
        $rand = rand(101, 999);
        $kode = 'OXC-';
        if($type == 2){
            $kode = 'TXC-';
        }
        if($type == 3){
            $kode = 'SXC-';
        }
        if($type == 4){
            $kode = 'PsXC-';
        }
        if($type == 5){
            $kode = 'PfXC-';
        }
        if($type == 6){
            $kode = 'BPXC-';
        }
        if($type == 7){
            $kode = 'BSpXC-';
        }
        if($type == 8){
            $kode = 'BPsXC-';
        }
        return $kode.date('Ymd').$code;
    }
    
    public function getTransactionsMember($data){
        $sql = DB::table('transaction')
                    ->where('user_id', '=', $data->id)
                    ->get();
        return $sql;
    }
    
    public function getTransactionsBuyer($data){
        $sql = DB::table('transaction')
                    ->where('user_id', '=', $data->id)
//                    ->where('type', '=', 1)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTransactionsSeller($data){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, '
                                . 'transaction.transaction_code, transaction.price, transaction.status, transaction.total_coin, transaction.admin_fee,'
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, transaction.type')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->where('transaction.seller_id', '=', $data->id)
//                    ->where('transaction.type', '=', 1)
                    ->orderBy('transaction.id', 'DESC')
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTransactionsByID($id){
        $sql = DB::table('transaction')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getDetailTransactionsMember($id, $data){
        $sql = DB::table('transaction')
                    ->where('id', '=', $id)
                    ->where('user_id', '=', $data->id)
                    ->first();
        return $sql;
    }
    
    public function getDetailTransactionsBuyer($id, $data){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                            . 'transaction.transaction_code, transaction.price, transaction.status, transaction.seller_id, transaction.admin_fee, '
                            . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, transaction.type, '
                            . 'transaction.bank_name, transaction.account_no, transaction.account_name, transaction.total_coin, transaction.coin_setting_id')
                    ->join('users', 'transaction.seller_id', '=', 'users.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.user_id', '=', $data->id)
                    ->first();
        return $sql;
    }
    
    public function getDetailTransactionsSeller($id, $data){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                            . 'transaction.transaction_code, transaction.price, transaction.status, transaction.total_coin, transaction.admin_fee,'
                            . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, transaction.type, '
                            . 'transaction.bank_name, transaction.account_no, transaction.account_name, transaction.coin_setting_id')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.seller_id', '=', $data->id)
                    ->first();
        return $sql;
    }
    
    public function getHistoryTransactionCoin($user_id){
        $sql = DB::table('member_coin')
                    ->selectRaw('transaction.id, transaction.transaction_code, transaction.type as trans_type, transaction.status as trans_status, '
                            . 'member_coin.qty, member_coin.type as coin_type, member_coin.bonus_type, member_coin.created_at,'
                            . 'users.user_code, users.name, transaction.coin_setting_id')
                    ->join('transaction', 'transaction.id', '=', 'member_coin.transaction_id')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->where('member_coin.user_id', '=', $user_id)
                    ->where('transaction.status', '=', 2)
                    ->orderBy('member_coin.id', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getDetailTransactionsBuyerPackagePin($id, $data){
        $sql = DB::table('package_pin')
                    ->selectRaw('package_pin.qty, package.name, package.code,'
                            . 'package_pin.price, package_pin.discount')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->where('package_pin.transaction_id', '=', $id)
                    ->where('package_pin.user_id', '=', $data->id)
                    ->get();
        return $sql;
    }
    
    public function getDetailTransactionsSellerPackagePin($id){
        $sql = DB::table('package_pin')
                    ->selectRaw('package_pin.qty, package.name, package.code,'
                            . 'package.price, package.stockist_price, package.m_stockist_price')
                    ->join('package', 'package.id', '=', 'package_pin.package_id')
                    ->where('package_pin.transaction_id', '=', $id)
                    ->get();
        return $sql;
    }
    
    public function getDetailTransactionsAdmin($id, $user_id){
        $sql = DB::table('transaction')
                        ->join('users', 'transaction.user_id', '=', 'users.id')
                        ->join('bank', 'transaction.bank_perusahaan_id', '=', 'bank.id')
                        ->selectRaw('users.name, users.hp, users.user_code, '
                                . 'transaction.transaction_code, transaction.type, transaction.total_pin, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, '
                                . 'bank.bank_name, bank.account_name, bank.account_no')
                        ->where('transaction.id', '=', $id)
                        ->where('transaction.user_id', '=', $user_id)
                        ->where('transaction.status', '=', 1)
                        ->first();
        return $sql;
    }
    
    public function getTransactionsByAdmin($status){
        if($status == null){
            $sql = DB::table('transaction')
                        ->join('users', 'transaction.user_id', '=', 'users.id')
                        ->selectRaw('users.name, users.hp, '
                                . 'transaction.transaction_code, transaction.type, transaction.total_pin, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id')
                        ->where('transaction.status', '<', 2)
                        ->get();
        } else {
            $sql = DB::table('transaction')
                        ->join('users', 'transaction.user_id', '=', 'users.id')
                        ->selectRaw('users.name, users.hp, '
                                . 'transaction.transaction_code, transaction.type, transaction.total_pin, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id')
                        ->where('transaction.status', '=', $status)
                        ->get();
        }
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTransactionsSellerByAdmin(){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, '
                                . 'transaction.transaction_code, transaction.price, transaction.status, transaction.type, '
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, transaction.total_coin')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->where('transaction.seller_id', '=', 1)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getDetailTransactionSellerAdmin($id, $user_id){
        $sql = DB::table('transaction')
                        ->join('users', 'transaction.user_id', '=', 'users.id')
                        ->selectRaw('users.hp, users.user_code, '
                                . 'transaction.transaction_code, transaction.type, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.unique_digit, transaction.user_id, transaction.id, transaction.total_coin ')
                        ->where('transaction.id', '=', $id)
                        ->where('transaction.user_id', '=', $user_id)
                        ->where('transaction.status', '=', 1)
                        ->first();
        return $sql;
    }
    
    public function getTransactionsConfirm($id, $user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('package.name, package.image, '
                            . 'package_pin.id, package_pin.qty, package.id as id_package')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->join('package', 'package_pin.package_id', '=', 'package.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.seller_id', '=', $user_id)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getMasterStockistTransactionsIn($user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_masuk')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->first();
        $qty = 0;
        if($sql->total_masuk != null){
            $qty = $sql->total_masuk;
        }
        $data = (object) array(
            'total_masuk' => $qty
        );
        return $data;
    }
    
    public function getMasterStockistTransactionsOut($user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_keluar')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '!=', 3)
                    ->where('transaction.seller_id', '=', $user_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getStockistTransOutPackage($user_id, $id_package){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_keluar')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '!=', 3)
                    ->where('transaction.seller_id', '=', $user_id)
                    ->where('package_pin.package_id', '=', $id_package)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getStockistTransOutPackageId($user_id, $package_id){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_keluar')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '!=', 3)
                    ->where('transaction.seller_id', '=', $user_id)
                    ->where('package_pin.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_keluar != null){
            $qty = $sql->total_keluar;
        }
        $data = (object) array(
            'total_keluar' => $qty
        );
        return $data;
    }
    
    public function getMemberTransactionsIn($user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_masuk')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->first();
        $qty = 0;
        if($sql->total_masuk != null){
            $qty = $sql->total_masuk;
        }
        $data = (object) array(
            'total_masuk' => $qty
        );
        return $data;
    }
    
    public function getMemberTransInPackagId($user_id, $package_id){
        $sql = DB::table('transaction')
                    ->selectRaw('sum(package_pin.qty) as total_masuk')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.status', '=', 2)
                    ->where('transaction.user_id', '=', $user_id)
                    ->where('package_pin.package_id', '=', $package_id)
                    ->first();
        $qty = 0;
        if($sql->total_masuk != null){
            $qty = $sql->total_masuk;
        }
        $data = (object) array(
            'total_masuk' => $qty
        );
        return $data;
    }
    
    public function getTransferPinKeluar($id){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                                . 'transaction.transaction_code, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.id, package_pin.qty, package.name as package_name')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->join('package', 'package_pin.package_id', '=', 'package.id')
                    ->where('transaction.seller_id', '=', $id)
                    ->where('transaction.type', '=', 2)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTransferPinKeluarDetail($id, $user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                                . 'transaction.transaction_code, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.id')
                    ->join('users', 'transaction.user_id', '=', 'users.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.seller_id', '=', $user_id)
                    ->where('transaction.type', '=', 2)
                    ->first();
        return $sql;
    }
    
    public function getTransferPinMasuk($id){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                                . 'transaction.transaction_code, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.id, package_pin.qty, package.name as package_name')
                    ->join('users', 'transaction.seller_id', '=', 'users.id')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->join('package', 'package_pin.package_id', '=', 'package.id')
                    ->where('transaction.user_id', '=', $id)
                    ->where('transaction.type', '=', 2)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getTransferPinMasukDetail($id, $user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('users.hp, users.user_code, users.name, '
                                . 'transaction.transaction_code, transaction.price, transaction.status,'
                                . 'transaction.created_at, transaction.id')
                    ->join('users', 'transaction.seller_id', '=', 'users.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.user_id', '=', $user_id)
                    ->where('transaction.type', '=', 2)
                    ->first();
        return $sql;
    }
    
    public function getTransactionsTransferPin($id, $user_id){
        $sql = DB::table('transaction')
                    ->selectRaw('package_pin.id, package_pin.qty, package_pin.package_id')
                    ->join('package_pin', 'package_pin.transaction_id', '=', 'transaction.id')
                    ->where('transaction.id', '=', $id)
                    ->where('transaction.user_id', '=', $user_id)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getInsertPostingTranscation($data){
        try {
            $lastInsertedID = DB::table('posting')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdatePostingTranscation($fieldName, $name, $data){
        try {
            DB::table('posting')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListPosting($user_id){
        $sql = DB::table('posting')
                    ->where('user_id', '=', $user_id)
                    ->get();
        $cek = null;
        if(count($sql) > 0){
            $cek = $sql;
        }
        return $cek;
    }
    
    public function getPostingByID($id, $user_id){
        $sql = DB::table('posting')
                    ->where('id', '=', $id)
                    ->where('user_id', '=', $user_id)
                    ->first();
        return $sql;
    }
    
    public function getCoinPostingByUserID($user_id){
        $sql = DB::table('posting')
                    ->selectRaw('sum(total_coin) as total_coin_posting')
                    ->where('user_id', '=', $user_id)
                    ->first();
        $coinPosting = 0;
        if($sql->total_coin_posting != null){
            $coinPosting = $sql->total_coin_posting;
        }
        $data = (object) array(
            'coin_posting' => $coinPosting,
        );
        return $data;
    }
    
    public function getCoinPostingGlobal(){
        $sql = DB::table('posting')
                    ->selectRaw('sum(total_coin) as total_coin_posting')
                    ->first();
        $coinPosting = 0;
        if($sql->total_coin_posting != null){
            $coinPosting = $sql->total_coin_posting;
        }
        $data = (object) array(
            'coin_posting' => $coinPosting,
        );
        return $data;
    }
    
    
}
