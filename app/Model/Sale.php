<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;


class Sale extends Model {
    
    public function getInsertPurchase($data){
        try {
            $lastInsertedID = DB::table('s_purchase')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdatePurchase($fieldName, $name, $data){
        try {
            DB::table('s_purchase')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getListPurchase(){
        $sql = DB::table('s_purchase')
                    ->where('is_active', '=', 1)
                    ->orderBy('id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getPurchaseByID($id){
        $sql = DB::table('s_purchase')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getListProdukSale(){
        $sql = DB::table('s_purchase')
                    ->where('is_active', '=', 1)
                    ->orderBy('name', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getInsertTransaction($data){
        try {
            $lastInsertedID = DB::table('s_transaction')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateTransaction($fieldName, $name, $data){
        try {
            DB::table('s_transaction')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getTransactionByID($id, $user_id){
        $sql = DB::table('s_transaction')
                    ->where('user_id', '=', $user_id)
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getAdminTransactionByID($id){
        $sql = DB::table('s_transaction')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getCodeTransaction(){
        $getTransCount = DB::table('s_transaction')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $code = sprintf("%04s", $tmp);
        return 'PJ'.date('dmY').$code;
    }
    
    public function getAllTransactionByUserId($user_id){
        $sql = DB::table('s_transaction')
                    ->selectRaw('s_transaction.invoice, s_transaction.total_price, s_transaction.sale_date, s_transaction.status, s_transaction.id, '
                            . 's_item_transaction.amount, s_item_transaction.item_price, '
                            . 's_purchase.name')
                    ->join('s_item_transaction', 's_item_transaction.sales_transaction_id', '=', 's_transaction.id')
                    ->join('s_purchase', 's_purchase.id', '=', 's_item_transaction.purchase_id')
                    ->where('s_transaction.user_id', '=', $user_id)
                    ->orderBy('s_transaction.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        $dataAll = null;
        if($return != null){
            $group = array();
            foreach($return as $row) {
                $group[$row->id][] = array(
                    'invoice' => $row->invoice,
                    'total_price' => $row->total_price,
                    'sale_date' => $row->sale_date,
                    'status' => $row->status,
                    'id_transaction' => $row->id,
                    'amount' => $row->amount,
                    'item_price' => $row->item_price,
                    'name' => $row->name
                );
            }
            $dataAll = array();
            foreach($group as $row1){
                $item = array();
                foreach($row1 as $rowItem){
                    $item[] = array(
                        'amount' => $rowItem['amount'],
                        'item_price' => $rowItem['item_price'],
                        'name' => $rowItem['name']
                    );
                }
                $dataAll[] = array(
                    'invoice' => $row1[0]['invoice'],
                    'total_price' => $row1[0]['total_price'],
                    'sale_date' => $row1[0]['sale_date'],
                    'status' => $row1[0]['status'],
                    'id_transaction' => $row1[0]['id_transaction'],
                    'item_transaction' => $item
                );
            }
        }
        return $dataAll;
    }
    
    public function getDetailTransactionByUserId($id, $user_id){
        $sql = DB::table('s_transaction')
                    ->selectRaw('s_transaction.invoice, s_transaction.total_price, s_transaction.sale_date, s_transaction.status, s_transaction.id, '
                            . 's_transaction.bank_name, s_transaction.account_no, s_transaction.account_name, '
                            . 's_item_transaction.amount, s_item_transaction.item_price, '
                            . 's_purchase.name')
                    ->join('s_item_transaction', 's_item_transaction.sales_transaction_id', '=', 's_transaction.id')
                    ->join('s_purchase', 's_purchase.id', '=', 's_item_transaction.purchase_id')
                    ->where('s_transaction.id', '=', $id)
                    ->where('s_transaction.user_id', '=', $user_id)
                    ->orderBy('s_transaction.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        $dataAll = null;
        if($return != null){
            $group = array();
            foreach($return as $row) {
                $group[$row->id][] = array(
                    'invoice' => $row->invoice,
                    'total_price' => $row->total_price,
                    'sale_date' => $row->sale_date,
                    'status' => $row->status,
                    'id_transaction' => $row->id,
                    'bank_name' => $row->bank_name,
                    'account_no' => $row->account_no,
                    'account_name' => $row->account_name,
                    
                    'amount' => $row->amount,
                    'item_price' => $row->item_price,
                    'name' => $row->name
                );
            }
            $dataAll = array();
            foreach($group as $row1){
                $item = array();
                foreach($row1 as $rowItem){
                    $item[] = array(
                        'amount' => $rowItem['amount'],
                        'item_price' => $rowItem['item_price'],
                        'name' => $rowItem['name']
                    );
                }
                $dataAll[] = array(
                    'invoice' => $row1[0]['invoice'],
                    'total_price' => $row1[0]['total_price'],
                    'sale_date' => $row1[0]['sale_date'],
                    'status' => $row1[0]['status'],
                    'id_transaction' => $row1[0]['id_transaction'],
                    'bank_name' => $row1[0]['bank_name'],
                    'account_no' => $row1[0]['account_no'],
                    'account_name' => $row1[0]['account_name'],
                    'item_transaction' => $item
                );
            }
        }
        return $dataAll;
    }
    
    public function getInsertItemTransaction($data){
        try {
            $lastInsertedID = DB::table('s_item_transaction')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateItemTransaction($fieldName, $name, $data){
        try {
            DB::table('s_item_transaction')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllTransactionSalesAdmin(){
        $sql = DB::table('s_transaction')
                    ->selectRaw('s_transaction.invoice, s_transaction.total_price, s_transaction.sale_date, s_transaction.status, s_transaction.id, '
                            . 's_transaction.bank_name, s_transaction.account_no, s_transaction.account_name, '
                            . 's_item_transaction.amount, s_item_transaction.item_price, '
                            . 's_purchase.name, users.user_code, users.name as name_user, users.hp')
                    ->join('s_item_transaction', 's_item_transaction.sales_transaction_id', '=', 's_transaction.id')
                    ->join('s_purchase', 's_purchase.id', '=', 's_item_transaction.purchase_id')
                    ->join('users', 'users.id', '=', 's_item_transaction.user_id')
                    ->where('s_transaction.status', '<=', 1)
                    ->orderBy('s_transaction.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        $dataAll = null;
        if($return != null){
            $group = array();
            foreach($return as $row) {
                $group[$row->id][] = array(
                    'invoice' => $row->invoice,
                    'total_price' => $row->total_price,
                    'sale_date' => $row->sale_date,
                    'status' => $row->status,
                    'id_transaction' => $row->id,
                    'bank_name' => $row->bank_name,
                    'account_no' => $row->account_no,
                    'account_name' => $row->account_name,
                    'user_code' => $row->user_code,
                    'name_user' => $row->name_user,
                    'hp' => $row->hp,
                    
                    'amount' => $row->amount,
                    'item_price' => $row->item_price,
                    'name' => $row->name
                );
            }
            $dataAll = array();
            foreach($group as $row1){
                $item = array();
                foreach($row1 as $rowItem){
                    $item[] = array(
                        'amount' => $rowItem['amount'],
                        'item_price' => $rowItem['item_price'],
                        'name' => $rowItem['name']
                    );
                }
                $dataAll[] = array(
                    'invoice' => $row1[0]['invoice'],
                    'total_price' => $row1[0]['total_price'],
                    'sale_date' => $row1[0]['sale_date'],
                    'status' => $row1[0]['status'],
                    'id_transaction' => $row1[0]['id_transaction'],
                    'bank_name' => $row1[0]['bank_name'],
                    'account_no' => $row1[0]['account_no'],
                    'account_name' => $row1[0]['account_name'],
                    'user_code' => $row1[0]['user_code'],
                    'name_user' => $row1[0]['name_user'],
                    'hp' => $row1[0]['hp'],
                    'item_transaction' => $item
                );
            }
        }
        return $dataAll;
    }
    
    public function getAllTransactionSalesAdminByID($id){
        $sql = DB::table('s_transaction')
                    ->selectRaw('s_transaction.invoice, s_transaction.total_price, s_transaction.sale_date, s_transaction.status, s_transaction.id, '
                            . 's_transaction.bank_name, s_transaction.account_no, s_transaction.account_name, '
                            . 's_item_transaction.amount, s_item_transaction.item_price, '
                            . 's_purchase.name, users.user_code, users.name as name_user, users.hp')
                    ->join('s_item_transaction', 's_item_transaction.sales_transaction_id', '=', 's_transaction.id')
                    ->join('s_purchase', 's_purchase.id', '=', 's_item_transaction.purchase_id')
                    ->join('users', 'users.id', '=', 's_item_transaction.user_id')
                    ->where('s_transaction.id', '=', $id)
                    ->orderBy('s_transaction.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        $dataAll = null;
        if($return != null){
            $group = array();
            foreach($return as $row) {
                $group[$row->id][] = array(
                    'invoice' => $row->invoice,
                    'total_price' => $row->total_price,
                    'sale_date' => $row->sale_date,
                    'status' => $row->status,
                    'id_transaction' => $row->id,
                    'bank_name' => $row->bank_name,
                    'account_no' => $row->account_no,
                    'account_name' => $row->account_name,
                    'user_code' => $row->user_code,
                    'name_user' => $row->name_user,
                    'hp' => $row->hp,
                    
                    'amount' => $row->amount,
                    'item_price' => $row->item_price,
                    'name' => $row->name
                );
            }
            $dataAll = array();
            foreach($group as $row1){
                $item = array();
                foreach($row1 as $rowItem){
                    $item[] = array(
                        'amount' => $rowItem['amount'],
                        'item_price' => $rowItem['item_price'],
                        'name' => $rowItem['name']
                    );
                }
                $dataAll[] = array(
                    'invoice' => $row1[0]['invoice'],
                    'total_price' => $row1[0]['total_price'],
                    'sale_date' => $row1[0]['sale_date'],
                    'status' => $row1[0]['status'],
                    'id_transaction' => $row1[0]['id_transaction'],
                    'bank_name' => $row1[0]['bank_name'],
                    'account_no' => $row1[0]['account_no'],
                    'account_name' => $row1[0]['account_name'],
                    'user_code' => $row1[0]['user_code'],
                    'name_user' => $row1[0]['name_user'],
                    'hp' => $row1[0]['hp'],
                    'item_transaction' => $item
                );
            }
        }
        return $dataAll;
    }
    
    
    
    
    
    public function getCodeSafra(){
        $getTransCount = DB::table('transfer_safra')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $code = sprintf("%04s", $tmp);
        return $code;
    }
    
    public function getSafraTransaction($id){
        $sql = DB::table('transfer_safra')
                    ->where('user_id', '=', $id)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getSafraTransactionDetail($id, $user_id){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id')
                    ->where('transfer_safra.id', '=', $id)
                    ->where('transfer_safra.user_id', '=', $user_id)
                    ->first();
        return $sql;
    }
    
    public function getAdminafraTransactionDetail(){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->join('users', 'transfer_safra.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id, users.user_code, users.full_name, transfer_safra.user_id, users.hp')
                    ->whereIn('transfer_safra.status', array(1, 2))
                    ->get();
        return $sql;
    }
    
     public function getAdminafraTransactionDetailID($id){
        $sql = DB::table('transfer_safra')
                    ->join('purchase', 'purchase.id', '=', 'transfer_safra.purchase_id')
                    ->join('users', 'transfer_safra.user_id', '=', 'users.id')
                    ->selectRaw('purchase.purchase_name, purchase.main_price, transfer_safra.safra_code, transfer_safra.status, '
                            . 'transfer_safra.safra_date, transfer_safra.safra_total, transfer_safra.transfer_total, transfer_safra.total_buy, '
                            . 'transfer_safra.alamat_kirim, transfer_safra.provinsi, transfer_safra.kota, transfer_safra.kecamatan, transfer_safra.kode_pos, '
                            . 'transfer_safra.bank_id, transfer_safra.id, users.user_code, users.full_name, transfer_safra.user_id, users.hp')
                    ->where('transfer_safra.id', '=', $id)
                    ->whereIn('transfer_safra.status', array(1, 2))
                    ->first();
        return $sql;
    }
    
}
