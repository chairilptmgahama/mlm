<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Bonussetting extends Model {
    
    //Bonus Start (Sponsor) 
    public function getInsertBonusSetting($data){
        try {
            $lastInsertedID = DB::table('setting_app')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateBonusSetting($fieldName, $name, $data){
        try {
            DB::table('setting_app')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    //Bonus Reward
    public function getInsertBonusClaimPHU($data){
        try {
            $lastInsertedID = DB::table('claim_phu_setting')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateBonusClaimPHU($fieldName, $name, $data){
        try {
            DB::table('claim_phu_setting')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getActiveBonusClaimPHU(){
        $sql = DB::table('claim_phu_setting')
                    ->where('is_active', '=', 1)
                    ->get();
        return $sql;
    }
    
    public function getActiveBonusClaimPHUID($id){
        $sql = DB::table('claim_phu_setting')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getActiveBonusSetting(){
        $sql = DB::table('setting_app')
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getActiveBonusSponsor(){
        $sql = DB::table('bonus_start')
                    ->selectRaw('start_price')
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    
    
    public function getActiveBonusTeam($type){
        $sql = DB::table('bonus_team')
                    ->where('is_active', '=', 1)
                    ->where('member_type', '=', $type)
                    ->first();
        return $sql;
    }
    
    public function getAllBonusReward(){
        $sql = DB::table('bonus_reward')
                    ->where('is_active', '=', 1)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAllBonusRewardByID($id){
        $sql = DB::table('bonus_reward')
                    ->where('id', '=', $id)
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getInsertBonusClaimPoin($data){
        try {
            $lastInsertedID = DB::table('claim_poin_setting')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateBonusClaimPoin($fieldName, $name, $data){
        try {
            DB::table('claim_poin_setting')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getActiveBonusClaimPoin(){
        $sql = DB::table('claim_poin_setting')
                    ->where('is_active', '=', 1)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getActiveBonusClaimPoinID($id){
        $sql = DB::table('claim_poin_setting')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    
    
    
}
