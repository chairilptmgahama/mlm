<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Startwd extends Model {
    
    public function getInsertStartWD($data){
        try {
            $lastInsertedID = DB::table('start_wd')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateStartWD($fieldName, $name, $data){
        try {
            DB::table('start_wd')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getStartEndWDMember($id){
        $sql = DB::table('start_wd')
                    ->where('user_id', '=', $id)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    
}