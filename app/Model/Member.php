<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Member extends Model {
    
    public function getInsertUsers($data){
        try {
            $lastInsertedID = DB::table('users')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateUsers($fieldName, $name, $data){
        try {
            DB::table('users')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllMember(){
        $sql = DB::table('users')
                    ->selectRaw('sum(case when member_type = 1 then 1 else 0 end) as total_exchanger,'
                            . 'sum(case when member_type = 3 and is_active = 0 then 1 else 0 end) as total_m_inactive,'
                            . 'sum(case when member_type = 3 and is_active = 1 then 1 else 0 end) as total_m_active')
                    ->where('user_type', '=', 10)
                    ->first();
        $exchanger = 0;
        $inactive = 0;
        $active = 0;
        if($sql->total_exchanger != null){
            $exchanger = $sql->total_exchanger;
        }
        if($sql->total_m_inactive != null){
            $inactive = $sql->total_m_inactive;
        }
        if($sql->total_m_active != null){
            $active = $sql->total_m_active;
        }
        $data = (object) array(
            'total_exchanger' => $exchanger,
            'total_m_inactive' => $inactive,
            'total_active' => $active
        );
        return $data;
    }
    
    public function getUsers($where, $data){
        $sql = DB::table('users')->where($where, '=', $data)->first();
        return $sql;
    }
    
    public function getCheckEmailPhoneUsercode($mail, $phone, $usercode){
        $sqlEmail = DB::table('users')->selectRaw('id')->where('email', '=', $mail)->where('user_type', '=', 10)->count();
        $sqlHP = DB::table('users')->selectRaw('id')->where('hp', '=', $phone)->where('user_type', '=', 10)->count();
        $sqlCode = DB::table('users')->selectRaw('id')->where('user_code', '=', $usercode)->where('user_type', '=', 10)->count();
        $data = (object) array(
            'cekEmail' => $sqlEmail, 'cekHP' => $sqlHP, 'cekCode' => $sqlCode
        );
        return $data;
    }
    
    public function getCheckEmailPhoneUsercodeAdmin($id, $mail, $phone, $usercode){
        $sqlCode = DB::table('users')->selectRaw('id')->where('user_code', '=', $usercode)->where('user_type', '=', 10)->where('id', '!=', $id)->count();
        $data = (object) array(
            'cekEmail' => 0, 'cekHP' => 0, 'cekCode' => $sqlCode
        );
        return $data;
    }
    
    public function getCountLastMember(){
        $getCount = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->whereDate('created_at', date('Y-m-d'))
                    ->count();
        $tmp = $getCount+13;
         $getCode = 'safra'.date('dmy').sprintf("%04s", $tmp);
        return $getCode;
    }
    
    public function getCheckKTP($ktp){
        $sql = DB::table('users')->selectRaw('id')->where('ktp', '=', $ktp)->where('user_type', '=', 10)->count();
        return $sql;
    }
    
    public function getAllDownlineSponsor($data){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('sponsor_id', '=', $data->id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
        $getData = null;
        if(count($sql) > 0){
            $getData = $sql;
        }
        return $getData;
    }
    
    public function getCountDownlineSponsor($user_id){
        $getCount = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('sponsor_id', '=', $user_id)
                    ->count();
        return $getCount;
    }
    
    public function getCountDownlineSponsorNonActive($user_id){
        $getCount = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('is_active', '=', 0)
                    ->where('sponsor_id', '=', $user_id)
                    ->count();
        return $getCount;
    }
    
    public function getAllDownlineSponsorNew($data){
        $sql = DB::table('users')
                    ->selectRaw('users.id, users.email, users.user_code, users.hp, users.active_at, placement_history.created_at,'
                            . 'users.is_active')
                    ->join('placement_history', 'placement_history.user_id', '=', 'users.id')
//                    ->where('users.user_type', '=', 10)
                    ->where('users.sponsor_id', '=', $data->id)
                    ->where('placement_history.owner_id', '=', $data->id)
                    ->get();
        $getData = null;
        if(count($sql) > 0){
            $getData = $sql;
        }
        return $getData;
    }
    
    public function getMySponsorPlacementLastDate($id){
        $sql = DB::table('placement_history')
                    ->where('user_id', '=', $id)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getAllDownlineSponsorNotActive($data){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('sponsor_id', '=', $data->id)
                    ->where('is_active', '=', 0)
                    ->get();
        $getData = null;
        if(count($sql) > 0){
            $getData = $sql;
        }
        return $getData;
    }
    
    public function getAllMemberToPlacement($data){
        $sql = DB::table('users')
                    ->where('sponsor_id', '=', $data->id)
                    ->whereNull('upline_id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->get();
        return $sql;
    }
    
    public function getCekMemberToPlacement($id, $data){
        $sql = DB::table('users')
                    ->where('id', '=', $id)
                    ->where('sponsor_id', '=', $data->id)
                    ->whereNull('upline_id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->first();
        return $sql;
    }
    
    public function getCekKananKiriFreeKakiKecil($id, $data){
        $sql = DB::table('users')
                    ->where('id', '=', $id)
                    ->where('upline_id', '=', $data->id)
                    ->where('user_type', '=', 10)
                    ->where(function ($query) {
                        $query->whereNull('kiri_id')
                                    ->orWhereNull('kanan_id');
                    })
                    ->first();
        return $sql;
    }
    
    public function getCekKananKiriFreeKakiPanjang($uplineDetail, $id){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
//                    ->where('upline_id', '=', $id)
                    ->where('upline_detail', 'LIKE', $uplineDetail.'%')
                    ->where(function ($query) {
                        $query->whereNull('kiri_id')
                                    ->orWhereNull('kanan_id');
                    })
                    ->orderBy('id', 'ASC')
                    ->first();
        return $sql;
    }
    
    //binary ver 1.00
    public function getBinary($data){
        $sql1 = DB::table('users')
                    ->where('id', '=', $data->id)
                    ->where('user_type', '=', 10)
                    ->first();
        $sql2 = $sql3 = $sql4 = $sql5 = $sql6 = $sql7 = null;
        if($sql1->kiri_id != null){
            $sql2 = DB::table('users')
                    ->where('id', '=', $sql1->kiri_id)
                    ->where('user_type', '=', 10)
                    ->first();
        }
        if($sql1->kanan_id != null){
            $sql3 = DB::table('users')
                    ->where('id', '=', $sql1->kanan_id)
                    ->where('user_type', '=', 10)
                    ->first();
        }
        
        if($sql2 != null){
            if($sql2->kiri_id != null){
                $sql4 = DB::table('users')
                        ->where('id', '=', $sql2->kiri_id)
                        ->where('user_type', '=', 10)
                        ->first();
            }
            if($sql2->kanan_id != null){
                $sql5 = DB::table('users')
                        ->where('id', '=', $sql2->kanan_id)
                        ->where('user_type', '=', 10)
                        ->first();
            }
        }
        
        if($sql3 != null){
            if($sql3->kiri_id != null){
                $sql6 = DB::table('users')
                        ->where('id', '=', $sql3->kiri_id)
                        ->where('user_type', '=', 10)
                        ->first();
            }
            if($sql3->kanan_id != null){
                $sql7 = DB::table('users')
                        ->where('id', '=', $sql3->kanan_id)
                        ->where('user_type', '=', 10)
                        ->first();
            }
        }
        $dataReturn = array($sql1, $sql2, $sql3, $sql4, $sql5, $sql6, $sql7);
        return $dataReturn;
    }
    
    public function getMyDownline($downline){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('upline_detail', 'LIKE', $downline.'%')
                    ->orderBy('id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getCountMyDownline($downline){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->selectRaw('sum(package.pin) total_pin')
                    ->where('users.user_type', '=', 10)
                    ->where('users.upline_detail', 'LIKE', $downline.'%')
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getCountMemberActivate($downline, $status){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', $status)
                    ->where('upline_detail', 'LIKE', $downline.'%')
                    ->count();
        return $sql;
    }
    
    public function getMyDownlineAllStatus($downline, $id){
        $sql = DB::table('users')
                    ->selectRaw('users.id, users.name, users.email, users.hp, users.user_code, users.active_at, users.is_active, '
                            . 'users.upline_id, users.upline_detail, '
                            . 'users.package_id, package.name as paket_name')
                    ->leftJoin('package', 'package.id', '=', 'users.package_id')
                    ->where('users.user_type', '=', 10)
                    ->where('users.sponsor_id', '=', $id)
                    ->orWhere('users.upline_detail', 'LIKE', $downline.'%')
                    ->orderBy('users.id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getMyDownlineUsername($downline, $username){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('upline_detail', 'LIKE', $downline.'%')
                    ->where('user_code', 'LIKE', '%'.$username.'%')
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getCekIdDownline($id, $downline){
        $sql = DB::table('users')
                    ->where('id', '=', $id)
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('upline_detail', 'LIKE', $downline.'%')
                    ->orderBy('id', 'ASC')
                    ->first();
        return $sql;
    }
    
    public function getCountMemberNonActivate($arraySpId){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 0)
                    ->whereIn('sponsor_id', $arraySpId)
                    ->whereNull('package_id')
                    ->count();
        return $sql;
    }
    
    public function getCountSponsorDownNonActivate($id){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 0)
                    ->where('sponsor_id', '=', $id)
                    ->whereNull('package_id')
                    ->count();
        return $sql;
    }
    
    public function getCekAllDownline($downline){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('upline_detail', 'LIKE', $downline.'%')
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = array();
        if(count($sql) > 0){
            foreach($sql as $row){
                $return[] = $row->id;
            }
        }
        return $return;
    }
    
    public function getMemberHaveTwoSponsor(){
        $sql = DB::table('users')
                    ->selectRaw('id, upline_detail, kanan_id, kiri_id, member_type')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('total_sponsor', '>=', 2)
                    ->whereNotNull('kiri_id')
                    ->whereNotNull('kanan_id')
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getCekIdPlacementAndSponsor($id){
        $sql = DB::table('users')
                    ->where('id', '=', $id)
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
//                    ->where('total_sponsor', '>=', 2)
//                    ->where('total_placement', '>=', 2)
                    ->orderBy('id', 'ASC')
                    ->first();
        return $sql;
    }
    
    public function getNewCountMyDownline($downline, $owner_id){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->join('placement_history', 'users.id', '=', 'placement_history.user_id')
                    ->selectRaw('sum(package.pin) as total_pin')
                    ->where('users.user_type', '=', 10)
                    ->where('users.upline_detail', 'LIKE', $downline.'%')
                    ->where('placement_history.owner_id', '=', $owner_id)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getKaKiTerPendek($id, $owner_id){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->join('placement_history', 'users.id', '=', 'placement_history.user_id')
                    ->selectRaw('sum(package.pin) as total_pin')
                    ->where('users.id', '=', $id)
                    ->where('placement_history.owner_id', '=', $owner_id)
                    ->where('users.user_type', '=', 10)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getDetailCountMyDownline($downline){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->selectRaw('sum(package.pin) as total_pin')
                    ->where('users.user_type', '=', 10)
                    ->where('users.upline_detail', 'LIKE', $downline.'%')
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getDetailKaKiTerPendek($id){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->selectRaw('sum(package.pin) as total_pin')
                    ->where('users.id', '=', $id)
                    ->where('users.user_type', '=', 10)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getAllMemberForAdmin(){
        $sql = DB::table('users as a')
                    ->selectRaw('a.user_code, a.email, a.hp, a.active_at, a.id, a.total_sponsor, b.user_code as usercode_sponsor,'
                            . 'a.is_login, package.name as paket_name')
                    ->leftJoin('users as b', 'b.id', '=', 'a.sponsor_id')
                    ->leftJoin('package', 'package.id', '=', 'a.package_id')
                    ->where('a.is_active', '=', 1)
                    ->where('a.user_type', '=', 10)
                    ->orderBy('a.id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getSearchUsernameAll($data, $username){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('id', '!=', $data->id)
                    ->where('user_code', 'LIKE', '%'.$username.'%')
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getMyDownlineAllLevelActive($downline, $id){
        $sql = DB::table('users')
                    ->selectRaw('users.id, users.name, users.email, users.hp, users.user_code, users.active_at, users.is_active, '
                            . 'users.upline_id, users.upline_detail, users.full_name, '
                            . 'users.package_id, package.name as paket_name, package.pin')
                    ->leftJoin('package', 'package.id', '=', 'users.package_id')
                    ->where('users.user_type', '=', 10)
                    ->where('users.is_active', '=', 1)
                    ->where('users.sponsor_id', '=', $id)
                    ->orWhere('users.upline_detail', 'LIKE', $downline.'%')
                    ->orderBy('users.id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAllDirectorStockist(){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('member_status', '=', 2)
                    ->where('id', '>=', 6)
                    ->whereNotIn('id', array(117))
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
     public function getUsersCodeEmail($user_code, $email){
        $sql = DB::table('users')->where('user_code', '=', $user_code)->where('email', '=', $email)->first();
        return $sql;
    }
    
    public function getAdminSearchUsername( $username){
        $sql = DB::table('users')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('user_code', 'LIKE', '%'.$username.'%')
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getNewCountMyDownlineReward($downline){
        $sql = DB::table('users')
                    ->join('member_pin', 'users.id', '=', 'member_pin.user_id')
                    ->selectRaw('sum(member_pin.total_pin) as total_pin')
                    ->where('users.user_type', '=', 10)
                    ->where('member_pin.is_ro', '=', 1)
                    ->where('users.upline_detail', 'LIKE', $downline.'%')
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getKaKiTerPendekReward($id){
        $sql = DB::table('users')
                    ->join('member_pin', 'users.id', '=', 'member_pin.user_id')
                    ->selectRaw('sum(member_pin.total_pin) as total_pin')
                    ->where('users.id', '=', $id)
                    ->where('member_pin.is_ro', '=', 1)
                    ->where('users.user_type', '=', 10)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getAllMemberForAdminByDate($date){
//        $latestPosts = DB::table('posts')
//                   ->select('user_id', DB::raw('MAX(created_at) as last_post_created_at'))
//                   ->where('is_published', true)
//                   ->groupBy('user_id');
        $sql = DB::table('users as a')
                    ->selectRaw('a.user_code, a.name, a.email, a.hp, a.active_at, a.created_at, a.id, a.total_sponsor, b.user_code as usercode_sponsor, '
                            . 'a.full_name, a.gender, a.alamat, a.kota, a.kecamatan, a.provinsi, a.is_profile, '
                            . 'a.is_login, '
                            . 'bank.bank_name, bank.account_no, bank.account_name')
                    ->leftJoin('users as b', 'b.id', '=', 'a.sponsor_id')
                    ->leftJoin('bank', function($join){
                        $join->on('bank.user_id', '=', 'a.id')
                               ->where('bank.is_active', '=', 1);
                    })
                    ->whereDate('a.created_at', '>=', $date->startDay)
                    ->whereDate('a.created_at', '<=', $date->endDay)
                    ->where('a.is_active', '=', 1)
                    ->where('a.user_type', '=', 10)
                    ->where('a.member_type', '=', 3)
                    ->orderBy('a.active_at', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getAllMemberForAdminSearchName($name){
        $sql = DB::table('users as a')
                    ->selectRaw('a.user_code, a.name, a.email, a.hp, a.active_at, a.id, a.total_sponsor, b.user_code as usercode_sponsor, '
                            . 'a.full_name, a.gender, a.alamat, a.kota, a.kecamatan, a.provinsi, a.is_profile,'
                            . 'a.is_login, package.name as paket_name, '
                            . 'bank.bank_name, bank.account_no, bank.account_name')
                    ->leftJoin('users as b', 'b.id', '=', 'a.sponsor_id')
                    ->leftJoin('package', 'package.id', '=', 'a.package_id')
                    ->leftJoin('bank', function($join){
                        $join->on('bank.user_id', '=', 'a.id')
                               ->where('bank.is_active', '=', 1);
                    })
                    ->where('a.is_active', '=', 1)
                    ->where('a.user_type', '=', 10)
                    ->where('a.member_type', '=', 3)
                    ->where('a.user_code', 'LIKE', '%'.$name.'%')
                    ->orderBy('a.active_at', 'DESC')
                    ->get();
        return $sql;
    }
    
    public function getTopMemberOmzetByPlacement($date, $take){
        $sql = DB::table('placement_history')
                    ->selectRaw('placement_history.owner_id, a.user_code, a.upline_detail, a.kanan_id, a.kiri_id, '
                            . 'sum(b.package_id) as total_pin')
                    ->join('users as a', 'a.id', '=', 'placement_history.owner_id')
                    ->join('users as b', 'b.id', '=', 'placement_history.user_id')
                    ->where('a.id', '>', 10)
                    ->whereDate('placement_history.created_at', '>=', $date->startDay)
                    ->whereDate('placement_history.created_at', '<=', $date->endDay)
                    ->groupBy('placement_history.owner_id')
                    ->groupBy('a.user_code')
                    ->groupBy('a.email')
                    ->groupBy('a.upline_detail')
                    ->groupBy('a.kanan_id')
                    ->groupBy('a.kiri_id')
                    ->orderBy('total_pin', 'DESC')
                    ->take($take)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
     public function getNewCountMyDownlineTopTen($downline, $owner_id, $date){
        $sql = DB::table('users')
                    ->join('placement_history', 'users.id', '=', 'placement_history.user_id')
                    ->selectRaw('sum(users.package_id) as total_pin')
                    ->where('users.user_type', '=', 10)
                    ->where('users.upline_detail', 'LIKE', $downline.'%')
                    ->where('placement_history.owner_id', '=', $owner_id)
                    ->whereDate('placement_history.created_at', '>=', $date->startDay)
                    ->whereDate('placement_history.created_at', '<=', $date->endDay)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getKaKiTerPendekTopTen($id, $owner_id, $date){
        $sql = DB::table('users')
                    ->join('package', 'package.id', '=', 'users.package_id')
                    ->join('placement_history', 'users.id', '=', 'placement_history.user_id')
                    ->selectRaw('sum(package.pin) as total_pin')
                    ->where('users.id', '=', $id)
                    ->where('placement_history.owner_id', '=', $owner_id)
                    ->where('users.user_type', '=', 10)
                    ->whereDate('placement_history.created_at', '>=', $date->startDay)
                    ->whereDate('placement_history.created_at', '<=', $date->endDay)
                    ->first();
        $return = 0;
        if($sql->total_pin != null){
            $return = $sql->total_pin;
        }
        return $return;
    }
    
    public function getAllMemberProbablyGetBonus(){
        $sql = DB::table('users')
                    ->selectRaw('id, user_code')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('total_sponsor', '>', 0)
                    ->whereNotNull('kiri_id')
                    ->whereNotNull('kanan_id')
//                    ->where('id', '=', 58)
                    ->orderBy('id', 'ASC')
                    ->get();
        return $sql;
    }
    
    public function getAPIurlCheck($url, $json){
        $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);       
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,15);          
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $json_response = curl_exec($ch);
        curl_close($ch);
        
        if(empty($json_response)){
            return null;
        } else {
            return $json_response;
        }
    }
    
    public function getDataAPIMobilePulsa(){
        $data = (object) array(
            'username' => '085782312323',
            'api_key' => '1125e4e4b92d3424',
            'master_url' => 'https://testprepaid.mobilepulsa.net'
        );
        return $data;
    }
    
    public function getSellerIdfromUpline($arrayUplineId, $type){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('user_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->where('member_type', '=', $type)
                    ->whereIn('id', $arrayUplineId)
                    ->orderBy('id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getInsertActivationUser($data){
        try {
            $lastInsertedID = DB::table('activation_user')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getTotalUpperMember($id, $paket){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('id', '<=', $id)
                    ->where('package_id', '=', $paket)
                    ->count();
        return $sql;
    }
    
    public function getIdBonusPHU($paket, $offset){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('package_id', '=', $paket)
                    ->offset($offset)
                    ->first();
        return $sql;
    }
    
    public function getIdBonusPHUNew($paket, $offset){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('package_id', '=', $paket)
//                    ->whereNull('history_phu')
                    ->offset($offset)
                    ->orderBy('id', 'ASC')
                    ->first();
        return $sql;
    }
    
    public function memberGetPHU($paket, $code){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('package_id', '=', $paket)
                    ->where('code', '=', $code)
                    ->first();
        return $sql;
    }
    
    public function getFirstThreeIDRememberPHU($paket, $limit){
        $sql = DB::table('users')
                    ->selectRaw('id, name, email, user_code, hp')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('is_phu', '=', 0)
                    ->where('package_id', '=', $paket)
                    ->limit($limit)
                    ->get();
        return $sql;
    }
    
    public function getAllMemberPackage($paket){
        $sql = DB::table('users')
                    ->selectRaw('id')
                    ->where('is_active', '=', 1)
                    ->where('user_type', '=', 10)
                    ->where('member_type', '=', 3)
                    ->where('package_id', '=', $paket)
                    ->count();
        return $sql;
    }
    
    public function getAllMemberStockist(){
        $sql = DB::table('users as a')
                    ->selectRaw('a.id as stockist_id, a.name as stockist_name, a.user_code as stockist_user_id, count(b.id) as total_aktifasi')
                    ->leftJoin('users as b', 'a.id', '=', 'b.upline_id')
                    ->where('a.member_type', '=', 2)
                    ->groupBy('a.id')
                    ->groupBy('a.name')
                    ->groupBy('a.user_code')
                    ->orderBy('a.id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getSearchMemberStockist($id, $search){
        $sql = DB::table('users as a')
                    ->selectRaw('a.id as stockist_id, a.name as stockist_name, a.user_code as stockist_user_code')
                    ->where('a.member_type', '>', 1)
                    ->where('a.is_active', '=', 1)
                    ->where('a.user_type', '=', 10)
                    ->where('a.id', '!=', $id)
                    ->where('a.name', 'LIKE', '%'.$search.'%')
                    ->orWhere('a.user_code', 'LIKE', '%'.$search.'%')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getSearchAllMember($search){
        $sql = DB::table('users as a')
                    ->selectRaw('a.id, a.name, a.user_code, a.is_active')
                    ->where('a.is_active', '=', 1)
                    ->where('a.member_type', '=', 3)
                    ->where('a.user_type', '=', 10)
                    ->where('a.user_code', 'LIKE', '%'.$search.'%')
                    ->orWhere('a.name', 'LIKE', '%'.$search.'%')
                    ->where('a.is_active', '=', 1)
                    ->where('a.member_type', '=', 3)
                    ->where('a.user_type', '=', 10)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getStructureSponsor($data){
        $sql = DB::table('users')
                    ->where('sponsor_id', '=', $data->id)
                    ->where('user_type', '=', 10)
                    ->orderBy('id', 'ASC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    
    
    
}

