<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Pinsetting extends Model {
    
    public function getInsertCoinSetting($data){
        try {
            DB::table('coin_setting')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCoinSetting($id, $data){
        try {
            DB::table('coin_setting')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getActiveCoinSetting($type){
        $sql = DB::table('coin_setting')
                    ->where('type', '=', $type)
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getActiveCoinSettingId($id){
        $sql = DB::table('coin_setting')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getAdminCoinSetting(){
        $sql = DB::table('coin_setting')
                    ->where('is_active', '=', 1)
                    ->orderBy('type', 'ASC')
                    ->get();
        return $sql;
    }
    
    ################################
    
    public function getInsertCoinSettingPosting($data){
        try {
            DB::table('setting_posting')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCoinSettingPosting($id, $data){
        try {
            DB::table('setting_posting')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAllActiveCoinPostingSetting(){
        $sql = DB::table('setting_posting')
                    ->where('is_active', '=', 1)
                    ->get();
        return $sql;
    }
    
    public function getActiveCoinPostingSetting(){
        $sql = DB::table('setting_posting')
                    ->where('is_active', '=', 1)
                    ->first();
        return $sql;
    }
    
    public function getIDCoinPostingSetting($id){
        $sql = DB::table('setting_posting')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    #######################
    
    public function getInsertCoinSettingApp($data){
        try {
            DB::table('setting_app')->insert($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getUpdateCoinSettingApp($id, $data){
        try {
            DB::table('setting_app')->where('id', '=', $id)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getAdminCoinSettingApp(){
        $sql = DB::table('setting_app')
                    ->where('is_active', '=', 1)
                    ->get();
        return $sql;
    }
    
    public function getActiveCoinSettingApp($id){
        $sql = DB::table('setting_app')
                    ->where('is_active', '=', 1)
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    public function getIDCoinSettingApp($id){
        $sql = DB::table('setting_app')
                    ->where('id', '=', $id)
                    ->first();
        return $sql;
    }
    
    
}
