<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Validator;

class Transferwd extends Model {
    
    public function getInsertWD($data){
        try {
            $lastInsertedID = DB::table('transfer_wd')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateWD($fieldName, $name, $data){
        try {
            DB::table('transfer_wd')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getTotalDiTransfer($data){
        $sql = DB::table('transfer_wd')
                    ->selectRaw('sum(case when status = 1 then wd_total else 0 end) total_wd, '
                            . 'sum(case when status = 0 then wd_total else 0 end) total_tunda,'
                            . 'sum(case when status = 0 then admin_fee else 0 end) total_tunda_admin_fee,'
                            . 'sum(case when status = 1 then admin_fee else 0 end) total_wd_admin_fee')
                    ->where('user_id', '=', $data->id)
                    ->first();
        $total_wd = 0;
        if($sql->total_wd != null){
            $total_wd = $sql->total_wd;
        }
        $total_tunda = 0;
        if($sql->total_tunda != null){
            $total_tunda = $sql->total_tunda;
        }
        $total_tunda_admin_fee = 0;
        if($sql->total_tunda_admin_fee != null){
            $total_tunda_admin_fee = $sql->total_tunda_admin_fee;
        }
        $total_wd_admin_fee = 0;
        if($sql->total_wd_admin_fee != null){
            $total_wd_admin_fee = $sql->total_wd_admin_fee;
        }
        $return = (object) array(
            'total_wd' => $total_wd,
            'total_tunda' => $total_tunda,
            'total_wd_admin_fee' => $total_wd_admin_fee,
            'total_tunda_admin_fee' => $total_tunda_admin_fee
        );
        return $return;
    }
    
    public function getTotalWD($id){
        $sql = DB::table('transfer_wd')
                    ->selectRaw('sum(wd_total) as total_wd')
                    ->where('status', '!=', 2)
                    ->where('user_id', '=', $id)
                    ->first();
        $total_wd = 0;
        if($sql->total_wd != null){
            $total_wd = $sql->total_wd;
        }
        return $total_wd;
    }
    
    public function getTotalWDRangeDate($id, $date){
        $dateEnd = date('Y-m-d', strtotime($date.'+30 days'));
        $sql = DB::table('transfer_wd')
                    ->selectRaw('sum(case when status != 2 then wd_total else 0 end) as total_wd')
                    ->where('user_id', '=', $id)
                    ->whereDate('wd_date', '>=', $date)
                    ->whereDate('wd_date', '<', $dateEnd)
                    ->first();
        $total_wd = 0;
        if($sql->total_wd != null){
            $total_wd = $sql->total_wd;
        }
        return $total_wd;
    }
    
    public function getTotalWDFeeAdminRangeDate($id, $date){
        $dateEnd = date('Y-m-d', strtotime($date.'+30 days'));
        $sql = DB::table('transfer_wd')
                    ->selectRaw('sum(case when status != 2 then admin_fee else 0 end) as total_fee')
                    ->where('user_id', '=', $id)
                    ->whereDate('wd_date', '>=', $date)
                    ->whereDate('wd_date', '<', $dateEnd)
                    ->first();
        $total_fee = 0;
        if($sql->total_fee != null){
            $total_fee = $sql->total_fee;
        }
        return $total_fee;
    }
    
    public function getCodeWD(){
        $getTransCount = DB::table('transfer_wd')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $code = sprintf("%04s", $tmp);
        return $code;
    }
    
    public function getReportMemberBonusSponsor($data){
        $sql = DB::table('transfer_wd')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 1)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getReportMemberBonusBinary($data){
        $sql = DB::table('transfer_wd')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 2)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getReportMemberBonusLevel($data){
        $sql = DB::table('transfer_wd')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 3)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getReportMemberBonusRO($data){
        $sql = DB::table('transfer_wd')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 4)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getReportMemberSafraPoin($data){
        $sql = DB::table('transfer_wd')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', 11)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getTotalDiTransferByType($data, $type){
        $sql = DB::table('transfer_wd')
                    ->selectRaw('sum(case when status = 1 then wd_total else 0 end) total_wd, '
                            . 'sum(case when status = 0 then wd_total else 0 end) total_tunda, '
                            . 'sum(case when status = 0 then admin_fee else 0 end) total_tunda_admin_fee,'
                            . 'sum(case when status = 1 then admin_fee else 0 end) total_wd_admin_fee')
                    ->where('user_id', '=', $data->id)
                    ->where('type', '=', $type)
                    ->first();
        $total_wd = 0;
        if($sql->total_wd != null){
            $total_wd = $sql->total_wd;
        }
        $total_tunda = 0;
        if($sql->total_tunda != null){
            $total_tunda = $sql->total_tunda;
        }
        $total_tunda_admin_fee = 0;
        if($sql->total_tunda_admin_fee != null){
            $total_tunda_admin_fee = $sql->total_tunda_admin_fee;
        }
        $total_wd_admin_fee = 0;
        if($sql->total_wd_admin_fee != null){
            $total_wd_admin_fee = $sql->total_wd_admin_fee;
        }
        $return = (object) array(
            'total_wd' => $total_wd,
            'total_tunda' => $total_tunda,
            'total_wd_admin_fee' => $total_wd_admin_fee,
            'total_tunda_admin_fee' => $total_tunda_admin_fee
        );
        return $return;
    }
    
    public function getAdminMemberBonusSponsor(){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee')
                    ->where('transfer_wd.status', '=', 0)
                    ->where('transfer_wd.type', '=', 1)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminHistoryBonusSponsor(){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee, transfer_wd.status')
                    ->where('transfer_wd.type', '=', 1)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberClaimPHU(){
        $sql = DB::table('claim_phu_reward')
                    ->join('users', 'claim_phu_reward.user_id', '=', 'users.id')
                    ->join('bank', 'bank.user_id', '=', 'users.id')
                    ->join('claim_phu_setting', 'claim_phu_reward.claim_phu_id', '=', 'claim_phu_setting.id')
                    ->selectRaw('users.user_code, bank.bank_name, bank.account_no, bank.account_name, users.hp, users.name, users.id as is_user,'
                            . 'claim_phu_reward.claim_date, claim_phu_reward.id, claim_phu_reward.status, '
                            . 'claim_phu_setting.name as phu_reward_name')
                    ->where('claim_phu_reward.status', '=', 0)
                    ->where('bank.is_active', '=', 1)
                    ->orderBy('claim_phu_reward.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminHistoryClaimPHU(){
        $sql = DB::table('claim_phu_reward')
                    ->join('users', 'claim_phu_reward.user_id', '=', 'users.id')
                    ->join('bank', 'bank.user_id', '=', 'users.id')
                    ->join('claim_phu_setting', 'claim_phu_reward.claim_phu_id', '=', 'claim_phu_setting.id')
                    ->selectRaw('users.user_code, users.hp, users.name, users.id as is_user, '
                            . 'claim_phu_reward.claim_date, claim_phu_reward.id, claim_phu_reward.status, '
                            . 'claim_phu_setting.name as phu_reward_name, '
                            . 'bank.bank_name, bank.account_no, bank.account_name')
                    ->where('bank.is_active', '=', 1)
                    ->orderBy('claim_phu_reward.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberBonusLevel(){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee')
                    ->where('transfer_wd.status', '=', 0)
                    ->where('transfer_wd.type', '=', 3)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberBonusRO(){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee')
                    ->where('transfer_wd.status', '=', 0)
                    ->where('transfer_wd.type', '=', 4)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberWDSafraPoin(){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee')
                    ->where('transfer_wd.status', '=', 0)
                    ->where('transfer_wd.type', '=', 11)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getIDRequestWD($id){
        $sql = DB::table('transfer_wd')
                    ->join('users', 'transfer_wd.user_id', '=', 'users.id')
                    ->join('bank', 'transfer_wd.user_bank', '=', 'bank.id')
                    ->selectRaw('transfer_wd.id, users.user_code, users.hp, bank.bank_name, bank.account_no, bank.account_name,'
                            . 'transfer_wd.wd_code, transfer_wd.wd_total, transfer_wd.wd_date, transfer_wd.admin_fee, users.full_name,'
                            . 'transfer_wd.reason, transfer_wd.status')
                    ->where('transfer_wd.id', '=', $id)
                    ->where('transfer_wd.status', '=', 0)
                    ->orderBy('transfer_wd.id', 'DESC')
                    ->first();
        return $sql;
    }
    
    public function getInsertClaimReward($data){
        try {
            $lastInsertedID = DB::table('claim_reward')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateClaimReward($fieldName, $name, $data){
        try {
            DB::table('claim_reward')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getMemberBonusReward($data){
        $sql = DB::table('claim_reward')
                    ->join('bonus_reward', 'bonus_reward.id', '=', 'claim_reward.reward_id')
                    ->selectRaw('claim_reward.total_kiri, claim_reward.total_kanan, claim_reward.claim_date, claim_reward.status,'
                            . 'bonus_reward.reward_price, bonus_reward.reward_detail, claim_reward.reason')
                    ->where('claim_reward.user_id', '=', $data->id)
                    ->orderBy('claim_reward.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberBonusReward(){
        $sql = DB::table('claim_reward')
                    ->join('bonus_reward', 'bonus_reward.id', '=', 'claim_reward.reward_id')
                    ->join('bank', 'claim_reward.user_bank', '=', 'bank.id')
                    ->join('users', 'claim_reward.user_id', '=', 'users.id')
                    ->selectRaw('claim_reward.id, claim_reward.total_kiri, claim_reward.total_kanan, claim_reward.claim_date, claim_reward.status,'
                            . 'bonus_reward.reward_price, bonus_reward.reward_detail, claim_reward.reason,'
                            . 'bank.bank_name, bank.account_no, bank.account_name,'
                            . 'users.user_code, users.hp')
                    ->where('claim_reward.status', '=', 0)
                    ->orderBy('claim_reward.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberBonusRewardByID($id){
        $sql = DB::table('claim_reward')
                    ->join('bonus_reward', 'bonus_reward.id', '=', 'claim_reward.reward_id')
                    ->join('bank', 'claim_reward.user_bank', '=', 'bank.id')
                    ->join('users', 'claim_reward.user_id', '=', 'users.id')
                    ->selectRaw('claim_reward.id, claim_reward.total_kiri, claim_reward.total_kanan, claim_reward.claim_date, claim_reward.status,'
                            . 'bonus_reward.reward_price, bonus_reward.reward_detail, claim_reward.reason,'
                            . 'bank.bank_name, bank.account_no, bank.account_name,'
                            . 'users.user_code, users.hp')
                    ->where('claim_reward.id', '=', $id)
                    ->where('claim_reward.status', '=', 0)
                    ->first();
        return $sql;
    }
    
    public function getMemberBonusRewardByUser($data, $reward_id){
        $sql = DB::table('claim_reward')
                    ->selectRaw('claim_reward.id')
                    ->where('claim_reward.user_id', '=', $data->id)
                    ->where('claim_reward.reward_id', '=', $reward_id)
                    ->first();
        return $sql;
    }
    
    public function getNewFee($data){
        $sql = DB::table('bank')
                    ->selectRaw('id, bank_name, account_no, account_name, is_active, active_at')
                    ->where('user_id', '=', $data->id)
                    ->where('bank_type', '=', 10)
                    ->where('is_active', '=', 1)
                    ->first();
        $feeNew = 0;
        if($sql != null){
            if($sql->bank_name != 'BCA'){
                $feeNew = 10000;
            }
        }
        return $feeNew;
    }
    
    public function getInsertWDPulsa($data){
        try {
            $lastInsertedID = DB::table('wd_pulsa')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateWDPulsa($fieldName, $name, $data){
        try {
            DB::table('wd_pulsa')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getCodeWDPulsa(){
        $getTransCount = DB::table('wd_pulsa')->selectRaw('id')->whereDate('created_at', date('Y-m-d'))->count();
        $tmp = $getTransCount+1;
        $code = sprintf("%04s", $tmp);
        return $code;
    }
    
    public function getReportMemberPulsa($data){
        $sql = DB::table('wd_pulsa')
                    ->where('user_id', '=', $data->id)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getAdminMemberRequestPulsa(){
        $sql = DB::table('wd_pulsa')
                    ->join('users', 'wd_pulsa.user_id', '=', 'users.id')
                    ->selectRaw('wd_pulsa.id, users.user_code,'
                            . 'wd_pulsa.wd_code, wd_pulsa.no_hp, wd_pulsa.pulsa_op, wd_pulsa.nominal_pulsa, wd_pulsa.price_pulsa, '
                            . 'wd_pulsa.admin_fee, wd_pulsa.pulsa_date')
                    ->where('wd_pulsa.status', '=', 0)
                    ->orderBy('wd_pulsa.id', 'DESC')
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = $sql;
        }
        return $return;
    }
    
    public function getInsertClaimPHU($data){
        try {
            $lastInsertedID = DB::table('claim_phu_reward')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateClaimPHU($fieldName, $name, $data){
        try {
            DB::table('claim_phu_reward')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getInsertClaimPoin($data){
        try {
            $lastInsertedID = DB::table('claim_poin_reward')->insertGetId($data);
            $result = (object) array('status' => true, 'message' => null, 'lastID' => $lastInsertedID);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message, 'lastID' => null);
        }
        return $result;
    }
    
    public function getUpdateClaimPoin($fieldName, $name, $data){
        try {
            DB::table('claim_poin_reward')->where($fieldName, '=', $name)->update($data);
            $result = (object) array('status' => true, 'message' => null);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $result = (object) array('status' => false, 'message' => $message);
        }
        return $result;
    }
    
    public function getSumWDPoin($user_id){
        $sql = DB::table('claim_poin_reward')
                    ->selectRaw('sum(total_poin) as total_poin')
                    ->where('user_id', '=', $user_id)
                    ->where('status', '!=', 2)
                    ->first();
        $data = 0;
        if($sql->total_poin != null){
            $data = $sql->total_poin;
        }
        return $data;
    }
    
    public function getUserWDPoin($id, $user_id){
        $sql = DB::table('claim_poin_reward')
                    ->where('id', '=', $id)
                    ->where('user_id', '=', $user_id)
                    ->where('status', '!=', 2)
                    ->first();
        return $sql;
    }
    
    public function getUserWDPoinAll($user_id){
        $sql = DB::table('claim_poin_reward')
                    ->where('user_id', '=', $user_id)
                    ->where('status', '!=', 2)
                    ->get();
        $return = null;
        if(count($sql) > 0){
            $return = array();
            foreach($sql as $row){
                $return[] = $row->claim_poin_id;
            }
        }
        return $return;
    }
   
    
}
