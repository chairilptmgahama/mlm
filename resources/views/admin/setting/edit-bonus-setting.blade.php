@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="row clearfix">

        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Edit</h5>
                </div>
                <div class="card-body">
                        <form class="login100-form validate-form" method="post" action="/adm/coin-setting-bonus">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Bonus Sponsor (%)</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="bonus_sponsor" required="true" autocomplete="off" value="{{$getData->bonus_sponsor}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Bonus Profit (%)</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="bonus_profit" required="true" autocomplete="off" value="{{$getData->bonus_profit}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Max User Profit Bonus Sponsor</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="max_user_profit" required="true" autocomplete="off" value="{{$getData->max_user_profit}}">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$getData->id}}">
                        <div class="row">
                            <div class="update ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
@stop
