@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>Persentase</th>
                                        <th>Min. Posting</th>
                                        <th>Min. Convert Wallet</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        @foreach($getData as $row)
                                            <tr>
                                                <td>{{$row->persentase}}%</td>
                                                <td>{{$row->min_posting}}</td>
                                                <td>{{$row->min_convert}}</td>
                                                <td>
                                                    <a class="text-primary" href="{{ URL::to('/') }}/adm/coin-setting-posting/{{$row->id}}">edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop