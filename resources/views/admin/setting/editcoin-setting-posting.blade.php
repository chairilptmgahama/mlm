@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="row clearfix">

        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Edit</h5>
                </div>
                <div class="card-body">
                        <form class="login100-form validate-form" method="post" action="/adm/coin-setting-posting">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Persentase</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="persentase" required="true" autocomplete="off" value="{{$getData->persentase}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Min. Mining</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="min_posting" required="true" autocomplete="off" value="{{$getData->min_posting}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Min. Convert</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="min_convert" required="true" autocomplete="off" value="{{$getData->min_convert}}">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$getData->id}}">
                        <div class="row">
                            <div class="update ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
@stop
