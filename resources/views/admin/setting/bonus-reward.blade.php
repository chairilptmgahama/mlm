@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/add/bonus-reward">Tambah Reward</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Poin Kiri</th>
                                        <th>Poin Kanan</th>
                                        <th>Rewards</th>
                                        <th>Nominal Reward</th>
                                        <th>###</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                          ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->total_kiri}}</td>
                                                <td>{{$row->total_kanan}}</td>
                                                <td>{{$row->reward_detail}}</td>
                                                <td>{{number_format($row->reward_price, 0, ',', '.')}}</td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#popUp"  href="{{ URL::to('/') }}/ajax/adm/cek/bonus-reward/{{$row->id}}" class="text-primary">edit</a>
                                                        &nbsp;&nbsp;
<!--                                                        <a rel="tooltip" title="Remove" data-toggle="modal" data-target="#rmNews" class="text-danger" href="#"><i class="nc-icon nc-simple-remove"></i></a>-->
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
@stop

@section('javascript')
<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
        } );
    } );
</script>
@stop
