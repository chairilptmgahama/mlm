@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Tambah</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/adm/add/bonus-reward">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Total Kiri</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal1" name="total_kiri" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Total Kanan</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal2" name="total_kanan" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Rewards</label>
                                        <input type="text" class="form-control" name="reward_detail" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nominal Rewards</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal3" name="reward_price" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('asset_admin/css/simditor.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/module.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/hotkeys.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/simditor.js') }}"></script>
<script>
  $(".allownumericwithoutdecimal1").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
 $(".allownumericwithoutdecimal2").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
  $(".allownumericwithoutdecimal3").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
</script>
@stop
