@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Tambah</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/adm/add/bonus-pool">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" name="name" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Min. Omzet</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal1" name="min_omzet" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Max. Omzet</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal2" name="max_omzet" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Persentase</label>
                                        <input type="text" class="form-control" name="persentase" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('asset_admin/css/simditor.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/module.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/hotkeys.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/simditor.js') }}"></script>
<script>
  $(".allownumericwithoutdecimal1").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
 $(".allownumericwithoutdecimal2").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
  $(".allownumericwithoutdecimal3").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });
</script>
@stop
