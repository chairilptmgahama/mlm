@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="row clearfix">
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <?php
                $type = 'Exchanger - Perusahaan';
                if($data->type == 2){
                    $type = 'Member - Exchanger';
                }
                ?>
                <div class="body">
                    <p class="text-warning" style="font-weight:bold;">{{ $type }}</p>
                    <h3>{{ number_format($data->sell_price, 0, ',', '.') }} / {{$data->sell_fee}}%
                        <i class="icon-briefcase text-warning float-right"></i>
                    </h3>
                    <h4>
                        <p class="card-title"></p>
                    </h4>
                    <span>Harga Beli / Fee</span>
                    <hr>
                    <h3>{{ number_format($data->buy_price, 0, ',', '.') }} / {{$data->buy_fee}}%</h3>
                    <span>Harga Jual / Fee<br></span>
                    <hr>
                    <h3>
                        <p class="card-title">{{$data->sell_min}} / {{$data->buy_min}}</p>
                    </h3>
                    <span>Minimal Jual / Beli<br></span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-yellow m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Edit Harga Coin</h5>
                </div>
                <div class="card-body">
                        <form class="login100-form validate-form" method="post" action="/adm/coin-setting">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Harga Jual</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="sell_price" required="true" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fee Jual</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="sell_fee" required="true" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Harga Beli</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="buy_price" required="true" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fee Beli</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="buy_fee" required="true" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Minimal Jual</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="sell_min" required="true" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Minimal Beli</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="buy_min" required="true" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{$data->id}}">
                        <input type="hidden" name="type" value="{{$data->type}}">
                        <div class="row">
                            <div class="update ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</div>
@stop

@section('javascript')
<script>
        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

</script>
@stop
