@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="row clearfix">
        <div class="col-lg-8 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format( $getData->coin_masuk - $getData->coin_keluar - $getData->coin_keluar_posting, 4, ',', '.') }} <small><sup>xone</sup></small>
                    <img src="{{ URL('image/logo_xone.png') }}" class="text-primary float-right" style="height:48px"></h3>
                    <span>Saldo Coin</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-yellow m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-6 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($getData->coin_keluar, 4, ',', '.') }} <small><sup>xone</sup></small>
                    <i class="icon-cloud-download text-success float-right"></i></h3>
                    <span>Total Coin Terjual</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-green m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($getData->coin_keluar_posting, 4, ',', '.') }} <small><sup>xone</sup></small>
                    <i class="icon-cloud-download text-danger float-right"></i></h3>
                    <span>Total Coin Terposting</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-purple m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
        <!--
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Tambah Coin Perusahaan</h5>
                </div>
                <div class="card-body">
                    @if ( Session::has('message') )
                        <div class="widget-content mt10 mb10 mr15">
                            <div class="alert alert-{{ Session::get('messageclass') }}">
                                <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                {{  Session::get('message')    }}
                            </div>
                        </div>
                    @endif
                        <form class="login100-form validate-form" method="post" action="/adm/add/master-coin">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nominal</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal" name="nominal" required="true" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="update ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div
        -->
    </div>

</div>
@stop

@section('javascript')
<script>
        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

</script>
@stop
