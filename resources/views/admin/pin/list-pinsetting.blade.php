@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Type</th>
                                        <th>Harga Jual</th>
                                        <th>Fee Jual</th>
                                        <th>Harga Beli</th>
                                        <th>Fee Beli</th>
                                        <th>Minimal Beli</th>
                                        <th>Minimal Jual</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $status = 'proses transfer';
                                            $type = 'Exchanger - Perusahaan';
                                            if($row->type == 2){
                                                $type = 'Member - Exchanger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->sell_price, 0, ',', ',')}}</td>
                                                <td>{{$row->sell_fee}}%</td>
                                                <td>{{number_format($row->buy_price, 0, ',', ',')}}</td>
                                                <td>{{$row->buy_fee}}%</td>
                                                <td>{{$row->buy_min}}</td>
                                                <td>{{$row->sell_min}}</td>
                                                <td>
                                                    <a class="text-primary" href="{{ URL::to('/') }}/adm/coin-setting/{{$row->id}}">edit</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop