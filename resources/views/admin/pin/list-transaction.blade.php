@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Username</th>
                                        <th>No. HP</th>
                                        <th>Kode Trans.</th>
                                        <th>Type</th>
                                        <th>Total Coin</th>
                                        <th>Total Harga</th>
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $price = $row->price + $row->unique_digit;
                                            $status = 'proses transfer';
                                            $label = 'info';
                                            if($row->status == 1){
                                                $status = 'konfirmasi';
                                                $label = 'warning';
                                            }
                                            if($row->status == 2){
                                                $status = 'tuntas';
                                                $label = 'success';
                                            }
                                            $type = 'Beli';
                                            if($row->type == 3){
                                                $type = 'Jual';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->user_code}}</td>
                                                <td>{{$row->hp}}</td>
                                                <td>{{$row->transaction_code}}</td>
                                                <td>{{$type}}</td>
                                                <td>{{number_format($row->total_coin, 0, ',', ',')}}</td>
                                                <td>{{number_format($price, 0, ',', ',')}}</td>
                                                <td><span class="badge badge-{{$label}}">{{$status}}</span></td>
                                                <td>
                                                    @if($row->status == 1)
                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/ajax/adm/cek/transaction/{{$row->id}}/{{$row->user_id}}">confirm</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop