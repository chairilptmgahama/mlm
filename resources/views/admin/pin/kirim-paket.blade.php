@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No. HP</th>
                                        <th>Total Pin</th>
                                        <th>Day Cream</th>
                                        <th>Night Cream</th>
                                        <th>Face Toner</th>
                                        <th>Facial Wash</th>
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $status = 'proses kirim';
                                            $label = 'info';
                                            if($row->status == 1){
                                                $status = 'terkirim';
                                                $label = 'success';
                                            }
                                            if($row->status == 2){
                                                $status = 'batal';
                                                $label = 'danger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->hp}}</td>
                                                <td>{{number_format($row->total_pin, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->day_cream, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->night_cream, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->face_toner, 0, ',', ',')}}</td>
                                                <td>{{number_format($row->facial_wash, 0, ',', ',')}}</td>
                                                <td><span class="badge badge-pill badge-{{$label}}">{{$status}}</span></td>
                                                <td>
                                                    <!--<a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/ajax/adm/kirim-paket/{{$row->id}}/{{$row->user_id}}">detail</a>-->
                                                    <a class="text-primary" href="{{ URL::to('/') }}/adm/kirim-paket/{{$row->id}}/{{$row->user_id}}">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop