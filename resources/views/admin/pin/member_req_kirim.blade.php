@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Kirim Paket Member</h5>
                        <p class="text-muted">Member kirim paket oleh admin</p>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/adm/member/req/kirim">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Member</label>
                                        <input type="text" class="form-control" id="get_id" autocomplete="off">
                                        <input type="hidden" name="get_id" id="id_get_id">
                                        <ul class="typeahead dropdown-menu form-control" style="max-height: 200px; overflow: auto;border: 1px solid #ddd;width: 98%;margin-left: 11px;" id="get_id-box"></ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jml Pin</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal2" name="total_pin" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat Kirim</label>
                                        <textarea class="form-control" id="alamat_kirim" rows="2" name="alamat_kirim" autocomplete="off" required=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Kurir</label>
                                        <input type="text" class="form-control" name="kurir_name" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. Resi</label>
                                        <input type="text" class="form-control" name="no_resi" required="" autocomplete="off" required="">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('asset_admin/css/simditor.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/module.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/hotkeys.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/simditor.js') }}"></script>
<script>
  $(".allownumericwithoutdecimal2").on("keypress keyup blur",function (event) {
    $(this).val($(this).val().replace(/[^\d].+/, ""));
     if ((event.which < 48 || event.which > 57)) {
         event.preventDefault();
     }
 });

    $(document).ready(function(){
        $("#get_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/ajax/adm/cek/username" + "?name=" + $(this).val() ,
                success: function(data){
                    $("#get_id-box").show();
                    $("#get_id-box").html(data);
                }
            });
        });
    });
    function selectUsername(val) {
        var valNew = val.split("____");
        $("#get_id").val(valNew[1]);
        $("#id_get_id").val(valNew[0]);
        $("#get_id-box").hide();
    }
</script>
@stop
