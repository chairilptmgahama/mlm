@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                        <a class="btn btn-info btn-fill btn-sm" href="{{ URL::to('/') }}/adm/add/news">Tambah Berita</a>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>Gambar</th>
                                        <th>Judul</th>
                                        <th>Tgl</th>
                                        <th>###</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php $no++;?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td><img src="{{$row->image}}" style="max-width: 150px;"></td>
                                                <td>{{$row->title}}</td>
                                                <td><?php  echo (date('d M Y', strtotime($row->created_at))) ?></td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/adm/news/{{$row->id}}">edit</a>
                                                        &nbsp;&nbsp;
                                                        <!--<a rel="tooltip" title="Remove" data-toggle="modal" data-target="#rmNews" class="text-danger" href="#"><i class="nc-icon nc-simple-remove"></i></a>-->
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="rmNews" tabindex="-1" role="dialog" aria-labelledby="crewModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
