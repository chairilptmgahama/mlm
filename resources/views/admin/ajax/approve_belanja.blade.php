<div class="modal-content">
<form class="login100-form validate-form" method="post" action="/adm/member/confirm/sales">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Reject</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <h4 class="text-success" style="text-align: center;">Konfirmasi belanja member!!!</h4>
                    </div>
                </div>
                <input type="hidden" name="cekId" value="{{$id}}" >
            </div>
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">Confirm</button>
        </div>
    </div>
</form>
</div>