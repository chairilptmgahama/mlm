@if($data->kurir_name != null && $data->no_resi != null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-danger" style="display: block;text-align: center;">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/adm/claim/activation/product">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" disabled="" value="{{$getData->member_name}} ({{$getData->user_code}})">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group">
                        <label>Penerima</label>
                        <input type="text" class="form-control" disabled="" value="{{$getData->full_name}}">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>No. HP</label>
                        <input type="text" class="form-control" disabled="" value="{{$getData->hp}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Alamat Kirim</label>
                        <textarea class="form-control" id="alamat_kirim" disabled="" rows="2">{{$getData->alamat}} {{$getData->kelurahan}} {{$getData->kecamatan}} {{$getData->kota}} {{$getData->provinsi}}</textarea>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama Kurir</label>
                        <input type="text" class="form-control" name="kurir_name" readonly="" value="{{$getData->kurir_name}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>No. Resi</label>
                        <input type="text" class="form-control" name="no_resi"readonly="" value="{{$getData->no_resi}}">
                    </div>
                </div>
            </div>
            <input type="hidden" name="cekId" id="cekId" value="{{$data->id}}" >
            <input type="hidden" name="cekUserId" id="cekUserId" value="{{$data->user_id}}" >
        </form>    
    </div>
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-default btn-link" id="tutupModal" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
            <button type="button" class="btn btn-danger btn-link" id="submit" onclick="confirmSubmit()">Submit</button>
        </div>
    </div>
</div>
@endif

@if($data->kurir_name == null || $data->no_resi == null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> Pengisian data harus lengkap </h4>
    </div>
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-default btn-link" id="tutupModal" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
    </div>
</div>

@endif