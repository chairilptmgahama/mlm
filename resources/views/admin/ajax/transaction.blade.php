<form class="login100-form validate-form" method="post" action="/adm/confirm/transaction">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <?php
        $price = $getData->price + $getData->unique_digit;
        $status = 'proses transfer';
        if($getData->status == 1){
            $status = 'konfirmasi';
        }
        $type = 'Beli';
        if($getData->type == 3){
            $type = 'Jual';
        }
    ?>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>UserID</label>
                        <input type="text" class="form-control" readonly="" value="{{$getData->user_code}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Handphone</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->hp}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Kode Transaksi</label>
                        <input type="text" readonly=""  class="form-control" value="{{$getData->transaction_code}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Type</label>
                        <input type="text" readonly=""  class="form-control" value="{{$type}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Total harga</label>
                        <input type="text" readonly=""  class="form-control" value="Rp. {{number_format($price, 0, ',', ',')}}">
                    </div>
                </div>
                <input type="hidden" name="cekId" value="{{$getData->id}}" >
                <input type="hidden" name="cekMemberId" value="{{$getData->user_id}}" >
            </div>
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary waves-effect waves-light">Confirm</button>
    </div>
</form>   