<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Pembelian</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <form id="form-add" method="POST" action="/adm/safra/buy">
            {{ csrf_field() }}
            <h4 class="text-dark" style="text-align: center;">Konfirmasi pembelian barang oleh member</h4>
            <input type="hidden" name="cekId" id="cekId" value="{{$id}}" >
            <input type="hidden" name="cekUserId" id="cekUserId" value="{{$user_id}}" >
        </form>    
    </div>
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-default btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
            <button type="button" class="btn btn-danger btn-link" id="submit" onclick="confirmSubmit()">Submit</button>
        </div>
    </div>
</div>