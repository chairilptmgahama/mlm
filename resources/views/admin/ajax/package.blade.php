<form class="login100-form validate-form" method="post" action="/adm/package">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        @if($getData != null)

            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Package Name</label>
                        <input type="text" class="form-control" name="name"  value="{{$getData->name}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kode</label>
                        <input type="text" class="form-control" name="code"  value="{{$getData->code}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Gambar</label>
                        <input type="text" class="form-control" name="image" value="{{$getData->image}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" class="form-control" name="price" value="{{number_format( $getData->price, 0, ',', '')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Harga Stockist</label>
                        <input type="text" class="form-control" name="stockist_price" value="{{number_format( $getData->stockist_price, 0, ',', '')}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Harga Master Stockist</label>
                        <input type="text" class="form-control" name="m_stockist_price" value="{{number_format( $getData->m_stockist_price, 0, ',', '')}}">
                    </div>
                </div>
            </div>
            <input type="hidden" name="cekId" value="{{$getData->id}}" >
            
        @else 
            No Data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">Submit</button>
        </div>
    </div>
</form>   