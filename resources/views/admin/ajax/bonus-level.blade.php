<form class="login100-form validate-form" method="post" action="/adm/bonus-level">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Level</label>
                        <input type="text" class="form-control" value="{{$getData->level}}" disabled="">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Bonus Price</label>
                        <input type="text" name="ro_price" class="form-control" value="{{$getData->ro_price}}" required="">
                    </div>
                </div>
            </div>
            <div class="row">
                <input type="hidden" name="cekId" value="{{$getData->id}}" >
            </div>
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">Confirm</button>
        </div>
        @endif
    </div>
</form>   