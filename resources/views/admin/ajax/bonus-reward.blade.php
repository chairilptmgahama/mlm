<form class="login100-form validate-form" method="post" action="/adm/bonus-reward">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Total Kiri</label>
                        <input type="text" name="total_kiri" class="form-control" value="{{$getData->total_kiri}}" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Total Kanan</label>
                        <input type="text" name="total_kanan" class="form-control" value="{{$getData->total_kanan}}" required="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Rewards</label>
                        <input type="text" class="form-control" name="reward_detail" required="" autocomplete="off" required="" value="{{$getData->reward_detail}}">
                    </div>
                </div>
                <input type="hidden" name="cekId" value="{{$getData->id}}" >
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nominal Reward</label>
                        <input type="text" class="form-control" name="reward_price" required="" autocomplete="off" required="" value="{{$getData->reward_price}}">
                    </div>
                </div>
            </div>
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">Confirm</button>
        </div>
        @endif
    </div>
</form>   