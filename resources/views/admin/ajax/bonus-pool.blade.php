<form class="login100-form validate-form" method="post" action="/adm/bonus-pool">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$headerTitle}}</h5>
    </div>
    <div class="modal-body">
        {{ csrf_field() }}
        @if($getData != null)
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="name" required="" autocomplete="off" required="" value="{{$getData->name}}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Persentase</label>
                        <input type="text" class="form-control" name="persentase" required="" autocomplete="off" required="" value="{{$getData->persentase}}">
                    </div>
                </div>
                <input type="hidden" name="cekId" value="{{$getData->id}}" >
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Min. Omzet</label>
                        <input type="text" name="min_omzet" class="form-control" value="{{$getData->min_omzet}}" required="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Max. Omzet</label>
                        <input type="text" name="max_omzet" class="form-control" value="{{$getData->max_omzet}}" required="">
                    </div>
                </div>
            </div>
        @else 
            Tidak ada data
        @endif
    </div>
    
    <div class="modal-footer">
        <div class="left-side">
            <button type="button" class="btn btn-danger btn-link" data-dismiss="modal">Tutup</button>
        </div>
        <div class="divider"></div>
        @if($getData != null)
        <div class="right-side">
            <button type="submit" class="btn btn-info btn-link">Confirm</button>
        </div>
        @endif
    </div>
</form>   