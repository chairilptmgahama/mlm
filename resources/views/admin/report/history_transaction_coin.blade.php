@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Transaksi Coin</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Transaksi</th>
                                        <th>Type</th>
                                        <th>User Id</th>
                                        <th>Kredit</th>
                                        <th>Debit</th>
                                        <th>Saldo Akhir</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $in = 0;
                                        $out = 0;
                                        $saldo = 0;
                                        $sisa_saldo = 0;
                                        // dd($getData);
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $type_trans = 'Coin Admin';
                                            if($row->coin_type == 2){
                                                $type_trans = 'Exchanger Beli';
                                            }
                                            if($row->coin_type == 3){
                                                $type_trans = 'Bonus Profit';
                                            }
                                            if($row->coin_type == 4){
                                                $type_trans = 'Bonus Sponsor';
                                            }
                                            if($row->coin_type == 5){
                                                $type_trans = 'Bonus Profit Share';
                                            }

                                            if ($row->coin_type == 1) {
                                                $in = $row->total_coin;
                                                $out = 0;
                                                $saldo_row = $row->total_coin;
                                                $saldo = $saldo + $saldo_row;
                                            } else {
                                                $in = 0;
                                                $out = $row->total_coin;
                                                $saldo_row = $row->total_coin;
                                                $saldo = $saldo - $saldo_row;
                                            }
                                            $sisa_saldo = $saldo;
                                        ?>
                                            <tr>
                                                <td>{{ date('d M Y H:i', strtotime($row->created_at)) }}</td>
                                                <td>{{ $row->transaction_code }}</td>
                                                <td>{{ $type_trans }}</td>
                                                <td>{{ $row->user_code  }}</td>
                                                <td>{{ number_format($in, 4, ',', '.') }}</td>
                                                <td>{{ number_format($out, 4, ',', '.') }}</td>
                                                <td>{{ number_format($sisa_saldo, 4, ',', '.') }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@stop


@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        var myTableRow =  $('#myTable').DataTable( {
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                dom: 'Bfrtip',
                "deferRender": true,
                columnDefs: [{
                    orderable: false,
                    targets: 0,
                }],
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export_xls' ,
                   }
                ],
                searching: false,
                 pagingType: "full_numbers",
                 "paging":   true,
                 "info":     false,
                 "ordering": false,
        } );


    } );


</script>
@stop