@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                             <form method="post" name="emailCompose" id="emailCompose" action="/adm/check/wd">
                                 {{ csrf_field() }}
                                 <input type="hidden" name="type" value="5" >
                             <p class="form-group">
                                <button type="submit" class="btn btn-primary btn-sm" id="formCheck">Submit Tuntas</button>
                             </p>
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                         <th>
                                             <input type="checkbox" name="select_all" value="1" id="example-select-all"> All
                                         </th>
                                         <th>Action</th>
                                        <th>No</th>
                                        <th>UserID</th>
                                        <th>No. HP</th>
                                        <th>Tgl. Claim</th>
                                        <th>Reward </th>
                                        <th>Kiri</th>
                                        <th>Kanan</th>
                                        <th>Bank</th>
                                        <th>No. Rek</th>
                                        <th>Nama. Rek</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                        ?>
                                            <tr>
                                                <td><input type="checkbox" name="id[]" value="{{$row->id}}"></td>
                                                <td>
                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="btn btn-danger btn-sm" href="{{ URL::to('/') }}/ajax/adm/cek/reject-wd/{{$row->id}}/5">reject</a>
                                                </td>
                                                <td>{{$no}}</td>
                                                <td>{{$row->user_code}}</td>
                                                <td>{{$row->hp}}</td>
                                                <td>{{date('d M Y', strtotime($row->claim_date))}}</td>
                                                <td>{{$row->reward_detail}} ({{number_format($row->reward_price, 0, ',', ',')}})</td>
                                                <td>{{$row->total_kiri}}</td>
                                                <td>{{$row->total_kanan}}</td>
                                                <td>{{$row->bank_name}}</td>
                                                <td>{{$row->account_no}}</td>
                                                <td>{{$row->account_name}}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             </form>
                             <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/buttons.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).ready(function() {
        var myTableRow =  $('#myTable').DataTable( {
                columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
                dom: 'Bfrtip',
                "deferRender": true,
                columnDefs: [{
                    orderable: false,
                    targets: 0,
                }],
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'export_xls' ,
                   }
                ],
                searching: false,
                 pagingType: "full_numbers",
                 "paging":   true,
                 "info":     false,
                 "ordering": true,
        } );
        $('#myTable #example-select-all').change(function() {
                var checked = $(this).is(":checked");
                $("input", myTableRow.rows({search:'applied'}).nodes()).each(function(){
                        if(checked){
                                $(this).attr("checked", true);
                        }
                        else {
                                $(this).attr("checked", false);
                        }
                });
        });
        $("form").submit(function() {
                $(myTableRow.rows({search:'applied'}).nodes()).find('input[type="checkbox"]:checked').appendTo('#emailCompose');
        });

    } );

</script>
@stop