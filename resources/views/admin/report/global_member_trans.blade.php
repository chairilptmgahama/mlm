@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Member Transaction</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Tgl. Sales</th>
                                        <th>Member USERID</th>
                                        <th>Jml.PIN Aktifasi</th>
                                        <th>Jml.PIN RO</th>
                                        <th>Jml. PIN Upgrade</th>
                                        <th>Total Jml. Omzet</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $total = $row->total_pin_aktifasi + $row->total_pin_ro + $row->total_pin_upgrade;
                                            $omzet = $pin_price * $total;
                                        ?>
                                            @if($total > 0)
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{date('d M Y', strtotime($row->tgl_trans))}}</td>
                                                    <td>{{$row->user_code}}</td>
                                                    <td>{{number_format($row->total_pin_aktifasi, 0, ',', ',')}}</td>
                                                    <td>{{number_format($row->total_pin_ro, 0, ',', ',')}}</td>
                                                    <td>{{number_format($row->total_pin_upgrade, 0, ',', ',')}}</td>
                                                    <td>{{number_format($omzet, 0, ',', ',')}}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export_global_sales'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: 0 },
                 ]
        } );
    } );
</script>
@stop