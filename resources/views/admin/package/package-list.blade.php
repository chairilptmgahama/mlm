@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Kode</th>
                                        <th>Gambar</th>
                                        <th>Harga Member</th>
                                        <th>Harga Stockist</th>
                                        <th>Harga Master Stockist</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($package != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($package as $row)
                                        <?php
                                            $no++;
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->code}}</td>
                                                <td><img data-name="product_image" src="{{$row->image}}" alt="..." style="width: 100px;"></td>
                                                <td>{{number_format( $row->price, 0, ',', ',')}}</td>
                                                <td>{{number_format( $row->stockist_price, 0, ',', ',')}}</td>
                                                <td>{{number_format( $row->m_stockist_price, 0, ',', ',')}}</td>
                                                <td><a rel="tooltip"  data-toggle="modal" data-target="#editPackage" class="text-primary" href="{{ URL::to('/') }}/ajax/adm/package/{{$row->id}}">edit</a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                             <div class="modal fade" id="editPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
    $("#editPackage").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop