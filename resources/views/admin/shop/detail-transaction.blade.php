@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-body">
                        <h3 class="text-center font-weight-bold mb-1">&nbsp;</h3>
                        <div class="row pb-2 p-2">
                            <div class="col-md-6">
                                <p class="mb-0"><strong>Invoice No.</strong>: {{$getData['invoice']}}</p>
                                <p><strong>Invoice Date</strong>: {{$getData['sale_date']}}</p>
                                <p class="mb-0"><strong>Name</strong>: {{$getData['name_user']}}</p>
                                <p class="mb-0"><strong>User ID</strong>: {{$getData['user_code']}}</p>
                                <p class="mb-0"><strong>HP</strong>: {{$getData['hp']}}</p>
                            </div>
                            <?php
                            $status = 'proses transfer';
                            $label = 'info';
                            if($getData['status'] == 1){
                                $status = 'proses admin';
                                $label = 'warning';
                            }
                            if($getData['status'] == 2){
                                $status = 'tuntas';
                                $label = 'success';
                            }
                            ?>
                            <div class="col-md-6 text-right">
                                <p class="mb-0"><strong>Status</strong>: <span class="badge badge-pill badge-{{$label}}">{{$status}}</span></p>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase small font-weight-bold">No.</th>
                                        <th class="text-uppercase small font-weight-bold">Nama Barang</th>
                                        <th class="text-uppercase small font-weight-bold">Qty</th>
                                        <th class="text-uppercase small font-weight-bold">Harga Barang</th>
                                        <th class="text-uppercase small font-weight-bold">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @foreach($getData['item_transaction'] as $row)
                                    <?php $no++; ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row['name']}}</td>
                                        <td>{{$row['amount']}}</td>
                                        <td>{{$row['item_price']}}</td>
                                        <td>{{$row['amount'] * $row['item_price']}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot class="font-weight-bold small">
                                 <tr>
                                        <td colspan="4" style="font-size: 14px;">Grand Total</td>
                                        <td style="font-size: 14px;">{{$getData['total_price']}}</td>
                                 </tr>
                                 @if($getData['bank_name'] != null)
                                 <tr>
                                        <td colspan="8" style="font-size: 15px;">
                                            {{$getData['bank_name']}} - {{$getData['account_no']}} ({{$getData['account_name']}})
                                        </td>
                                 </tr>
                                 @endif
                                </tfoot>
                          </table>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <a type="submit" class="btn btn-dark-outline" href="{{ URL::to('/') }}/adm/list/member/sales">Kembali</a>
                                    @if($getData['status'] == 1)
                                    <button type="submit" class="btn btn-danger" id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                                    <button type="submit" class="btn btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Konfirmasi</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="confirmDetail">
                            </div>
                        </div>
                        <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="rejectDetail">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @if($getData['status'] == 1)
        <script>
               function inputSubmit(){
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/ajax/adm/cek/confirm/belanja/{{$getData['id_transaction']}}",
                         success: function(url){
                             $("#confirmDetail" ).empty();
                             $("#confirmDetail").html(url);
                         }
                     });
               }

               function rejectSubmit(){
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/ajax/adm/cek/reject/belanja/{{$getData['id_transaction']}}",
                         success: function(url){
                             $("#rejectDetail" ).empty();
                             $("#rejectDetail").html(url);
                         }
                     });
               }

                function confirmSubmit(){
                    var dataInput = $("#form-add").serializeArray();
                    $('#form-add').submit();
                    $('#form-add').remove();
                    $('#loading').show();
                    $('#tutupModal').remove();
                    $('#submit').remove();
                }
        </script>
    @endif
@stop
