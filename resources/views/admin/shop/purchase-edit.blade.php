@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Edit Barang</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <form class="login100-form validate-form" method="post" action="/adm/edit/sales/purchase">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" class="form-control" name="name" required="" autocomplete="off" value="{{$getData->name}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal" name="price" required="" autocomplete="off" value="{{number_format($getData->price, 0, ',', '')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Link Gambar</label>
                                        <input type="text" class="form-control" name="image" required="" autocomplete="off" value="{{$getData->image}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Keterangan</label>
                                        <textarea class="form-control textarea" name="full_desc" id="editor1" required="" >{{$getData->full_desc}}</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <input type="hidden" name="cekId" value="{{$getData->id}}">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-round">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('asset_admin/css/simditor.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/module.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/hotkeys.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('asset_admin/js/simditor.js') }}"></script>
<script>
  $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
     });
</script>
<script>
   var editor = new Simditor({
        textarea: $('#editor1')
  });
</script>
@stop
