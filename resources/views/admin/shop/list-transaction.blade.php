@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>UserID</th>
                                        <th>HP</th>
                                        <th>Kode Trans.</th>
                                        <th>Tgl</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $status = 'proses transfer';
                                            $label = 'info';
                                            if($row['status'] == 1){
                                                $status = 'proses admin';
                                                $label = 'warning';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row['user_code']}}</td>
                                                <td>{{$row['hp']}}</td>
                                                <td>{{$row['invoice']}}</td>
                                                <td>{{date('d M Y', strtotime($row['sale_date']))}}</td>
                                                <td>{{number_format($row['total_price'], 0, ',', ',')}}</td>
                                                <td><span class="badge badge-pill badge-{{$label}}">{{$status}}</span></td>
                                                <td>
                                                    <a rel="tooltip"  class="text-primary" href="{{ URL::to('/') }}/adm/member/sales/{{$row['id_transaction']}}">detail</a>
                                                    <?php // {{ URL::to('/') }}/adm/member/sales/{{$row['id_transaction']}} ?>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop