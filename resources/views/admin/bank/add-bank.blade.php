@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Tambah Bank Perusahaan</h5>
                    </div>
                    <div class="card-body">
                         <form class="login100-form validate-form" method="post" action="/adm/add/bank">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Bank</label>
                                        <select class="form-control" name="bank_name" id="bank_name">
                                            <option value="none">- Pilih Bank -</option>
                                            <option value="BCA">BCA</option>
                                            <option value="CIMB Niaga">CIMB</option>
                                            <option value="Bank Permata">Bank Permata</option>
                                            <option value="Bank Mandiri">Bank Mandiri</option>
                                            <option value="BRI">BRI</option>
                                            <option value="BNI">BNI</option>
                                            <option value="BTN">BTN</option>
                                            <option value="BII">BII</option>
                                            <option value="Bank Panin">Bank Panin</option>
                                            <option value="Bank NISP">Bank NISP</option>
                                            <option value="Citibank">Citibank</option>
                                            <option value="Bank Danamon">Bank Danamon</option>
                                            <option value="Bank Mega">Bank Mega</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Rekening</label>
                                        <input type="text" class="form-control" name="account_no" required="true" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Rekening</label>
                                        <input type="text" class="form-control" name="account_name" required="true" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

</script>
@stop
