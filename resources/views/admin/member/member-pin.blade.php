@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Data Pin</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>UserID</th>
                                        <th>Saldo Pin Aktifasi</th>
                                        <th>Saldo Pin Transfer</th>
                                        <th>Saldo Pin Kirim Paket</th>
                                        <th>Saldo Pin/Paket Diterima</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $dataPin)
                                        <?php
                                            $no++;
                                            $sum_pin_masuk = 0;
                                            $sum_pin_keluar = 0;
                                            if($dataPin->sum_pin_masuk != null){
                                                $sum_pin_masuk = $dataPin->sum_pin_masuk;
                                            }
                                            if($dataPin->sum_pin_keluar != null){
                                                $sum_pin_keluar = $dataPin->sum_pin_keluar;
                                            }
                                            $sum_pin_terpakai = 0;
                                            if($dataPin->sum_pin_terpakai != null){
                                                $sum_pin_terpakai = $dataPin->sum_pin_terpakai;
                                            }
                                            $sum_pin_masuk_terima = 0;
                                            if($dataPin->sum_pin_masuk_terima != null){
                                                $sum_pin_masuk_terima = $dataPin->sum_pin_masuk_terima;
                                            }
                                            $sum_pin_keluar_terima = 0;
                                            if($dataPin->sum_pin_keluar_terima != null){
                                                $sum_pin_keluar_terima = $dataPin->sum_pin_keluar_terima;
                                            }
                                            $sum_pin_keluar_ditransfer = 0;
                                            if($dataPin->sum_pin_keluar_ditransfer != null){
                                                $sum_pin_keluar_ditransfer = $dataPin->sum_pin_keluar_ditransfer;
                                            }
                                            $saldo_pin_aktifasi = $sum_pin_masuk - $sum_pin_keluar;
                                            $saldo_pin_transfer = $sum_pin_masuk - $sum_pin_keluar;
                                            $saldo_pin_kirim_paket = $sum_pin_masuk - $dataPin->total_pin_proses_dan_tuntas - $sum_pin_keluar_ditransfer - $sum_pin_masuk_terima;
                                            $pin_paket_diterima = $dataPin->total_pin_tuntas + $sum_pin_masuk_terima - $sum_pin_keluar_terima;
                                        ?>
                                            <tr>
                                                <td>{{$dataPin->user_code}}</td>
                                                <td>{{$saldo_pin_aktifasi}}</td>
                                                <td>{{$saldo_pin_transfer}}</td>
                                                <td>{{$saldo_pin_kirim_paket}}</td>
                                                <td>{{$pin_paket_diterima}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/simplePagination.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export_member_pin'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: 0 },
                 ],
                 "order": [[ 1, "desc" ]],
        } );
    } );
</script>
@stop
