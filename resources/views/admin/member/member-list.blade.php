@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-user">
                    <div class="card-header">
                        <h5 class="card-title">Search By Date</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="get" action="/adm/list/member">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Awal</label>
                                            <input type="text" class="form-control datepickerStart" name="start_date">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tanggal Akhir</label>
                                            <input type="text" class="form-control datepickerEnd" name="end_date">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="cari" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-header">
                        <h5 class="card-title">Search By Username</h5>
                    </div>
                    <div class="card-body" style="min-height: auto;">
                        <form class="login100-form validate-form" method="post" action="/adm/search-list/member">
                            {{ csrf_field() }}
                            <div id="addPenjualan">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Search name</label>
                                            <input type="text" class="form-control" name="name" placeholder="Minimal 3 karakter">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                             <button type="submit" class="form-control btn btn-sm btn-info " title="cari" style="margin: 0;">Cari</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <?php
                            $text = date('d M Y', strtotime($getDate->startDay)).' - '.date('d M Y', strtotime($getDate->endDay));
                            if($search_name == 0){
                                if($getDate->dateSkr != null){
                                    $text = date('d M Y', strtotime($getDate->startDay)).' - '.date('d M Y', strtotime($getDate->endDay));
                                }
                            }
                        ?>
                        <h5 class="card-title">List {{$text}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>UserID</th>
                                        <th>Nama</th>
                                        <th>no. HP</th>
                                        <th>Tgl. join</th>
                                        <th>Tgl. Aktif</th>
                                        <th>Sponsor</th>
                                        <th>Nama Lengkap</th>
                                        <th>Gender</th>
                                        <th>Alamat</th>
                                        <th>Bank</th>
                                        <th>###</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $sponsor = 'Exchanger';
                                            if($row->usercode_sponsor != null){
                                                $sponsor = $row->usercode_sponsor;
                                            }
                                            $gender = '';
                                            if($row->is_profile != null){
                                                $gender = 'Laki-laki';
                                                if($row->gender == 2){
                                                    $gender = 'Perempuan';
                                                }
                                            }
                                            $bank = '';
                                            if($row->bank_name != null){
                                                $bank = $row->bank_name.'. '.$row->account_no.' - '.$row->account_name;
                                            }
                                            $active_date = '--:--';
                                            if($row->active_at != null){
                                                $active_date = date('d M Y H:i', strtotime($row->active_at));
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->user_code}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->hp}}</td>
                                                <td>{{$active_date}}</td>
                                                <td>{{date('d M Y H:i', strtotime($row->created_at))}}</td>
                                                <td>{{$sponsor}}</td>
                                                <td>{{$row->full_name}}</td>
                                                <td>{{$gender}}</td>
                                                <td>{{$row->alamat}} {{$row->kecamatan}} {{$row->kota}} {{$row->provinsi}}</td>
                                                <td>{{$bank}} </td>
                                                <td class="td-actions text-left" >
                                                    <div class="table-icons">
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#popUp"  href="{{ URL::to('/') }}/ajax/adm/change-passwd/member/{{$row->id}}" class="text-warning">passwd</a>
                                                        <?php
                                                        /*
                                                        &nbsp;&nbsp;
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#popUp1"  href="{{ URL::to('/') }}/ajax/adm/change-data/member/{{$row->id}}" class="text-primary">data</a>
                                                        &nbsp;&nbsp;
                                                        <a rel="tooltip"  data-toggle="modal" data-target="#popUp2"  href="{{ URL::to('/') }}/ajax/adm/change-block/member/{{$row->id}}" class="text-danger">blokir</a>
                                                         */
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                             <div class="modal fade" id="popUp1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                             <div class="modal fade" id="popUp2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/simplePagination.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{asset('js/bootstrap-selectpicker.js') }}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#popUp1").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $("#popUp2").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export_member_{{$text}}'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: 0 },
                 ]
        } );
    } );

     $('.datepickerStart').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            },
            maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
        });
        $('.datepickerEnd').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
              time: "fa fa-clock-o",
              date: "fa fa-calendar",
              up: "fa fa-chevron-up",
              down: "fa fa-chevron-down",
              previous: 'fa fa-chevron-left',
              next: 'fa fa-chevron-right',
              today: 'fa fa-screenshot',
              clear: 'fa fa-trash',
              close: 'fa fa-remove'
            },
            maxDate: '<?php echo date('Y-m-d',strtotime("1 days")); ?>',
        });
</script>
@stop
