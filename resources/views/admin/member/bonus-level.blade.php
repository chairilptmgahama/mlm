@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table table-striped nowrap" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No.</th>
                                        <th>UserID</th>
                                        <th>Total Bonus (Rp.)</th>
                                        <th>Saldo Bonus (Rp.)</th>
                                        <th>Diproses</th>
                                        <th>Ditransfer</th>
                                     </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $wd_tuntas = $row->total_wd + $row->fee_tuntas;
                                            $wd_proses = $row->total_tunda + $row->fee_tunda;
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->user_code}}</td>
                                                <td>{{number_format($row->total_bonus, 0)}}</td>
                                                <td>{{number_format($row->saldo, 0)}}</td>
                                                <td>{{number_format($wd_proses, 0)}}</td>
                                                <td>{{number_format($wd_tuntas, 0)}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'export_member_bonus_binary'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: 0 },
                 ]
        } );
    } );

</script>
@stop
