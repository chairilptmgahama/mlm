@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">List</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="widget-content mt10 mb10 mr15">
                                <div class="alert alert-{{ Session::get('messageclass') }}">
                                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                                    {{  Session::get('message')    }}
                                </div>
                            </div>
                        @endif
                         <div class="table-responsive">
                            <table class="table" id="myTable">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Status</th>
                                        <th>Nama User</th>
                                        <th>Penerima</th>
                                        <th>No. HP</th>
                                        <th>Tgl. Claim</th>
                                        <th>Tgl. Kirim</th>
                                        <th>###</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if($getData != null)
                                        <?php
                                        $no = 0;
                                        ?>
                                        @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $status = 'proses kirim';
                                            $label = 'info';
                                            $kirim = '--:--';
                                            if($row->status == 1){
                                                $status = 'terkirim';
                                                $label = 'success';
                                                $kirim = date('d M Y H:i', strtotime($row->kirim_at));
                                            }
                                            if($row->status == 2){
                                                $status = 'batal';
                                                $label = 'danger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td><span class="badge badge-pill badge-{{$label}}">{{$status}}</span></td>
                                                <td>{{$row->member_name}} ({{$row->user_code}})</td>
                                                <td>{{$row->full_name}}</td>
                                                <td>{{$row->hp}}</td>
                                                <td>{{date('d M Y H:i', strtotime($row->created_at))}}</td>
                                                <td>{{$kirim}}</td>
                                                <td>
                                                    <a class="text-primary" href="{{ URL::to('/') }}/adm/claim/activation/product/{{$row->id}}">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/simplePagination.css') }}">
@stop

@section('javascript')
<script type="text/javascript" language="javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/jszip.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                        {
                            extend: 'excelHtml5',
                            title: 'claim_produk_aktifasi'
                        }
                    ],
                searching: false,
                pageLength: 10,
                columnDefs: [
                    { orderable: false, targets: 0 },
                 ]
        } );
    });
</script>
@stop
