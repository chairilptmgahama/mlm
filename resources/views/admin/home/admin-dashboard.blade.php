@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">

    <div class="row clearfix">
        <div class="col-lg-8 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3><a href="{{ URL::to('/') }}/adm/list/history-coin">
                    {{ number_format($getTotalCoin->coin_masuk - ($getTotalCoin->coin_keluar + $getTotalCoin->coin_keluar_posting + $getTotalCoin->coin_keluar_bns_sponsor + $getTotalCoin->coin_keluar_bns_posting), 4, ',', '.') }} <small><sup>xone</sup></small>
                    </a>
                    <img src="{{ URL('image/logo_xone.png') }}" class="text-primary float-right" style="height:48px"></h3>
                    <span>SALDO COIN</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-blue m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($totalMember->total_exchanger, 0, ',', '.') }}
                    <i class="icon-shuffle text-warning float-right"></i></h3>
                    <span>Total Exchanger</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-yellow m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($totalMember->total_active, 0, ',', '.') }}
                    <i class="icon-users text-success float-right"></i></h3>
                    <span>Total Member Aktif</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-green m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($totalMember->total_m_inactive, 0, ',', '.') }}
                    <i class="icon-user-unfollow text-danger float-right"></i></h3>
                    <span>Total Member Belum Aktif</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-purple m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
    </div>

</div>

@stop