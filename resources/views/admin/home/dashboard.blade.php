@extends('layout.admin.master')
@section('title', $headerTitle)

@section('content')
<div class="main-panel">
    <div class="row clearfix">
        @if($dataUser->is_active == 1)
            @if($dataOrder > 0)
                <div class="alert alert-warning alert-dismissible fade show">
                    <span>
                        <b> New Order Package (Total {{$dataOrder}}) - </b> Check this <a href="{{ URL::to('/') }}/m/list/order-package">link</a>
                    </span>
                </div>
            @endif
        @endif
        @if($dataUser->is_active == 0)
            @if($dataOrder > 0)
                <div class="alert alert-warning alert-dismissible fade show">
                    <span>
                        <b> Waiting conformation from your sponsor </b>
                    </span>
                </div>
            @endif
        @endif
        @if ( Session::has('message') )
            <div class="widget-content mt10 mb10 mr15">
                <div class="alert alert-{{ Session::get('messageclass') }}">
                    <button class="close" type="button" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
                    {{  Session::get('message')    }}
                </div>
            </div>
        @endif
    </div>

    <div class="row clearfix">
        <?php
            $sum_pin_masuk = 0;
            $sum_pin_keluar = 0;
            if($dataPin->sum_pin_masuk != null){
                $sum_pin_masuk = $dataPin->sum_pin_masuk;
            }
            if($dataPin->sum_pin_keluar != null){
                $sum_pin_keluar = $dataPin->sum_pin_keluar;
            }
            $total = $sum_pin_masuk - $sum_pin_keluar;
        ?>
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($total, 0, ',', '.') }}
                    <i class="icon-shuffle text-warning float-right"></i></h3>
                    <span>Total Pin</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-yellow m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card overflowhidden">
                <div class="body">
                    <h3>{{ number_format($sum_pin_keluar, 0, ',', '.') }}
                    <i class="icon-users text-success float-right"></i></h3>
                    <span>Total Pin Terjual</span>
                </div>
                <div class="progress progress-xs progress-transparent custom-color-green m-b-0">
                    <div class="progress-bar" data-transitiongoal="100" style="width: 100%;" aria-valuenow="100"></div>
                </div>
            </div>
        </div>
    </div>

</div>

@stop