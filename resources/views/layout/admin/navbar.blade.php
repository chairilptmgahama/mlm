<?php
    if ($dataUser->member_type == 1){
        $type = 'Super Admin';
    } else if ($dataUser->member_type == 2){
        $type = 'Master Admin';
    } else {
        $type = 'Admin';
    }

    $hidden = '';
    if ($dataUser->user_type >= 3) {
        $hidden = 'hidden';
    }
?>
<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>

        <div class="navbar-brand">
            <a href="#"><img src="{{url('/')}}/image/xone.jpg" alt="Xone Logo" class="img-responsive logo"></a>
        </div>

        <div class="navbar-right">
            <form id="navbar-search" class="navbar-form search-form">
                <h6>{{$dataUser->user_code}} <span class="badge badge-success m-l-10 hidden-sm-down">{{$type}}</span></h6>
            </form>

            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-grid"></i></a>
                        <ul class="dropdown-menu user-menu menu-icon">
                            <li class="menu-heading">Member</li>
                            <li><a href="{{ URL::to('/') }}/adm/list/member"><i class="icon-users m-r-10"></i>All Member</a></li>
                            <li {{ $hidden }}><a href="{{ URL::to('/') }}/adm/add-exchanger"><i class="icon-user m-r-10"></i>Exchanger</a></li>
                            <li {{ $hidden }} class="menu-heading">Setting</li>
                            <li {{ $hidden }}><a href="{{ URL::to('/') }}/adm/list/coin-setting"><i class="icon-tag m-r-10"></i>Coin</a></li>
                            <li {{ $hidden }}><a href="{{ URL::to('/') }}/adm/add-admin"><i class="icon-star m-r-10"></i>Admin</a></li>
                            <hr>
                            <li><a href="{{ URL::to('/') }}/admin_logout"><i class="icon-power m-r-10 text-danger"></i>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
