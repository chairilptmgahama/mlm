<?php
$hidden = '';
$hidden_admin = '';

if ($dataUser->user_type >= 3) {
    $hidden = 'hidden';
}

if ($dataUser->user_type >= 2) {
    $hidden_admin = 'hidden';
}

?>

<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="{{ URL('image/logo_xone.png') }}" class="rounded-circle user-photo" alt="User Profile Picture">
            <div class="dropdown">
                <span>Selamat Datang,</span>
                <a href="#" class="user-name"><strong>{{$dataUser->name}}</strong></a>
            </div>

            @yield('sidebar-user')

        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">Menu</a></li>
        </ul>

        <!-- Tab panes -->

        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">

                        <li>
                            <a href="{{ URL::to('/') }}/adm/dashboard" ><i class="icon-home"></i> <span>Dashboard</span></a>
                        </li>

                        <li @if(Route::currentRouteName() == 'adm_listMember' || Route::currentRouteName() == 'addExchanger') class="active" @endif>
                            <a href="#Member" class="has-arrow"><i class="icon-users"></i> <span>Member</span></a>
                            <ul>
                                <li @if(Route::currentRouteName() == 'adm_listMember') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/member">All Member</a></li>
                                <li {{ $hidden }} @if(Route::currentRouteName() == 'addExchanger') class="active" @endif><a href="{{ URL::to('/') }}/adm/add-exchanger">Exchanger</a></li>
                            </ul>
                        </li>

                        <li {{ $hidden }} @if(Route::currentRouteName() == 'addMasterCoin' || Route::currentRouteName() == 'adm_listTransaction') || Route::currentRouteName() == 'adm_listHistoryCoin') class="active" @endif>
                            <a href="#Xone" class="has-arrow"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="height:26px;padding-right:16px;margin-left:-5px"> <span>Xone Coin</span></a>
                            <ul>
                                <li @if(Route::currentRouteName() == 'addMasterCoin') class="active" @endif><a href="{{ URL::to('/') }}/adm/add/master-coin">Coin Perusahaan</a></li>
                                <li @if(Route::currentRouteName() == 'adm_listTransaction') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/transactions">Transaksi Order</a></li>
                                <li @if(Route::currentRouteName() == 'adm_listHistoryCoin') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/history-coin">Saldo Coin</a></li>
                            </ul>
                        </li>

                        <li {{ $hidden }} @if(Route::currentRouteName() == 'listSettingCoin' || Route::currentRouteName() == 'addCrew') class="active" @endif>
                            <a href="#Setting" class="has-arrow"><i class="icon-settings"></i> <span>Setting</span></a>
                            <ul>
                                <li @if(Route::currentRouteName() == 'listSettingCoin') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/coin-setting">Coin</a></li>
                                <li @if(Route::currentRouteName() == 'listSettingCoinBonus') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/coin-setting-bonus">Bonus</a></li>
                                <li @if(Route::currentRouteName() == 'listSettingCoinPosting') class="active" @endif><a href="{{ URL::to('/') }}/adm/list/coin-setting-posting">Mining</a></li>
                                <li {{ $hidden_admin }}  @if(Route::currentRouteName() == 'addCrew') class="active" @endif ><a href="{{ URL::to('/') }}/adm/add-admin">Admin</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ URL::to('/') }}/admin_logout"><i class="icon-logout"></i> <span>Logout</span></a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>

    </div>
</div>
