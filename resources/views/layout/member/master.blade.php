<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="icon" href="{{url('/')}}/image/ico_logo_xone.png" type="image/x-icon"> <!-- Favicon-->
        <title>Xone Coin - Member</title>
        <meta name="description" content="Xone Coin">
        <meta name="author" content="Xone Coin">
        <meta name="tag" content="One Cryptocurrency Community">
        @yield('meta')
        <link rel="stylesheet" href="{{ asset('lucid/vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/vendor/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/vendor/morrisjs/morris.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('lucid/vendor/toastr/toastr.min.css') }}">
        @yield('styles')
        <link rel="stylesheet" href="{{ asset('lucid/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/css/color_skins.css') }}">

    </head>

    <body class="theme-blue">

        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img src="{{url('/')}}/image/logo_xone.jpg" alt="Xone Coin" class="fa-pulse" style="width: 100px;"></div>
            </div>
        </div>

        <div id="wrapper">
            @include('layout.member.navbar')
            @include('layout.member.sidebar')
            <div id="main-content">
                <div class="container-fluid">
                    <div class="block-header">
                        <div class="row">
                            <div class="col-lg-5 col-md-8 col-sm-12">
                                <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i class="fa fa-arrow-left"></i></a> @yield('title')</h2>
                            </div>
                        </div>
                    </div>

                    @yield('content')

                </div>
            </div>
        </div>

        <script src="{{ asset('lucid/bundles/libscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/bundles/vendorscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/bundles/morrisscripts.bundle.js') }}"></script><!-- Morris Plugin Js -->
        <script src="{{ asset('lucid/bundles/knob.bundle.js') }}"></script>
        <script src="{{ asset('lucid/js/index4.js') }}"></script>
        <script src="{{ asset('lucid/bundles/mainscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/vendor/toastr/toastr.js') }}"></script>
        @if ( Session::has('message') )
        <script>
            $(function() {
                toastr.options.timeOut = "true";
                toastr.options.closeButton = true;
                toastr.options.positionClass = 'toast-top-center';
                toastr.options.timeOut = 2000;
                toastr.{{ Session::get('messageclass') }}('{{ Session::get("message") }}');
                //error, warning, success
            });
        </script>
        @endif
        <script src="{{ asset('js/chat.js') }}"></script>
        @yield('javascript')
    </body>
</html>
