<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="icon" href="/image/ico_logo_xone.png" type="image/x-icon"> <!-- Favicon-->
        <title>Xone Coin</title>
        <meta name="description" content="@yield('meta_description', config('app.name'))">
        <meta name="author" content="Xone Coin">


        <link rel="stylesheet" href="{{ asset('lucid/vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/vendor/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/css/color_skins.css') }}">
        <link rel="stylesheet" href="{{ asset('lucid/vendor/toastr/toastr.min.css') }}">
        @yield('styles')

    </head>
    <body>

        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img src="{{ URL('image/logo_xone.png') }}" alt="Xone Coin" class="fa-pulse" style="width: 100px;"></div>
            </div>
        </div>

        <div id="wrapper">

            @yield('content')

        </div>

        <script src="{{ asset('lucid/bundles/libscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/bundles/vendorscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/bundles/mainscripts.bundle.js') }}"></script>
        <script src="{{ asset('lucid/vendor/toastr/toastr.js') }}"></script>
        @if ( Session::has('message') )
        <script>
            $(function() {
                toastr.options.timeOut = "true";
                toastr.options.closeButton = true;
                toastr.options.positionClass = 'toast-top-left';
                toastr.options.timeOut = 4000;
                toastr.{{ Session::get('messageclass') }}('{{ Session::get("message") }}');
                //error, warning, success
            });
        </script>
        @endif
        @yield('javascript')
    </body>
</html>
