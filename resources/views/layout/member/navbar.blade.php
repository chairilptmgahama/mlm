<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>

        <div class="navbar-brand">
            <a href="#"><img src="{{url('/')}}/image/xone.jpg" alt="Xone Logo" class="img-responsive logo"></a>
        </div>

        <div class="navbar-right">
            <form id="navbar-search" class="navbar-form search-form">
                <?php
                    $type = 'Exchanger';
                    if($dataUser->member_type == 3){
                        $type = 'Member';
                    }
                ?>
                <h6>{{$dataUser->user_code}} <span class="badge badge-warning m-l-10 hidden-sm-down">{{$type}}</span></h6>
            </form>

            <div id="navbar-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-grid"></i></a>
                        <ul class="dropdown-menu user-menu menu-icon">
                            <li class="menu-heading">Account</li>
                            <li><a href="{{ URL::to('/') }}/m/profile"><i class="icon-user m-r-10"></i>Profile</a></li>
                            <li><a href="{{ URL::to('/') }}/m/bank"><i class="icon-wallet m-r-10"></i>Bank</a></li>
                            <li><a href="{{ URL::to('/') }}/m/add/code/pin"><i class="icon-key m-r-10"></i>Kode pin</a></li>
                            <li><a href="{{ URL::to('/') }}/m/edit/password"><i class="icon-lock m-r-10"></i>Password</a></li>
                            <hr>
                            <li><a href="{{ URL::to('/') }}/admin_logout"><i class="icon-power m-r-10 text-danger"></i>Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
