<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="{{ URL('image/logo_xone.png') }}" class="rounded-circle user-photo" alt="User Profile Picture">
            <div class="dropdown">
                <span>Selamat Datang,</span>
                <a href="#" class="user-name"><strong>{{$dataUser->name}}</strong></a>
            </div>

            @yield('sidebar-user')

        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#">Menu</a></li>
        </ul>

        <!-- Tab panes -->

        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">

                        <li>
                            <a href="{{ URL::to('/') }}/m/dashboard"><i class="icon-home"></i> <span>Dashboard</span></a>
                        </li>

                        @if($dataUser->member_type == 3)
                        <li @if(Route::currentRouteName() == 'm_newSponsor' || Route::currentRouteName() == 'm_mySponsor' || Route::currentRouteName() == 'm_mySponsorTree') class="active" @endif>
                            <a href="#Network" class="has-arrow"><i class="icon-share"></i> <span>Network</span></a>
                            <ul>
                                <li @if(Route::currentRouteName() == 'm_newSponsor') class="active" @endif><a href="{{ URL::to('/') }}/m/add/sponsor">Register</a></li>
                                <li @if(Route::currentRouteName() == 'm_mySponsor') class="active" @endif><a href="{{ URL::to('/') }}/m/my/sponsor">Sponsor</a></li>
                                <li @if(Route::currentRouteName() == 'm_mySponsorTree') class="active" @endif><a href="{{ URL::to('/') }}/m/my/sponsor-tree">Sponsor Tree</a></li>
                            </ul>
                        </li>

                        <li @if(Route::currentRouteName() == 'm_newCoin' || Route::currentRouteName() == 'm_sellCoin' || Route::currentRouteName() == 'm_listBuyerTransactions' || Route::currentRouteName() == 'm_postingCoin'
                             || Route::currentRouteName() == 'm_listPosting' || Route::currentRouteName() == 'm_postingByID' || Route::currentRouteName() == 'm_buyerTransactionID' || Route::currentRouteName() == 'm_myHistoryMyCoin') class="active" @endif>
                            <a href="#Xone" class="has-arrow"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="height:30px;padding-right:13px;margin-left:-5px"> <span>Xone Coin</span></a>
                            <ul>
                                <li @if(Route::currentRouteName() == 'm_newCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/add/coin">Beli</a></li>
                                <li @if(Route::currentRouteName() == 'm_sellCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/sell/coin">Jual</a></li>
                                <li @if(Route::currentRouteName() == 'm_myHistoryMyCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/history/my-coin">History</a></li>
                                <li @if(Route::currentRouteName() == 'm_listBuyerTransactions' || Route::currentRouteName() == 'm_buyerTransactionID') class="active" @endif><a href="{{ URL::to('/') }}/m/list/buyer/transactions">Transaksi</a></li>
                                <li @if(Route::currentRouteName() == 'm_postingCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/posting/coin">Mining</a></li>
                                <li @if(Route::currentRouteName() == 'm_listPosting' || Route::currentRouteName() == 'm_postingByID') class="active" @endif><a href="{{ URL::to('/') }}/m/list/posting">List Mining</a></li>
                            </ul>
                        </li>
                        @endif


                        @if($dataUser->member_type == 1)
                            @if($dataUser->total_sponsor == 0)
                            <li @if(Route::currentRouteName() == 'm_addMemberOpening') class="active" @endif>
                                <a href="#Network" class="has-arrow"><i class="icon-share"></i> <span>Network</span></a>
                                <ul><li @if(Route::currentRouteName() == 'm_addMemberOpening') class="active" @endif><a href="{{ URL::to('/') }}/m/add/annual-member">Register</a></li>
                                </ul>
                            </li>
                            @endif
                        <li @if(Route::currentRouteName() == 'm_newCoin' || Route::currentRouteName() == 'm_sellCoin' || Route::currentRouteName() == 'm_listBuyerTransactions' || Route::currentRouteName() == 'm_addTransferCoin'
                             || Route::currentRouteName() == 'm_listBuyerTransactions' || Route::currentRouteName() == 'm_listSellerTransactions' || Route::currentRouteName() == 'm_buyerTransactionID'
                             || Route::currentRouteName() == 'm_sellerTransactionID' || Route::currentRouteName() == 'm_myHistoryMyCoin') class="active" @endif>
                            <a href="#Xone" class="has-arrow"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="height:30px;padding-right:13px;margin-left:-5px"> <span>Xone Coin</span></a>
                            <ul><li @if(Route::currentRouteName() == 'm_newCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/add/coin">Beli</a></li>
                                <li><a href="{{ URL::to('/') }}/m/coming/soon">Jual</a></li>
                                <li @if(Route::currentRouteName() == 'm_myHistoryMyCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/history/my-coin">History</a></li>
                                <li @if(Route::currentRouteName() == 'm_addTransferCoin') class="active" @endif><a href="{{ URL::to('/') }}/m/add/transfer-coin">Transfer</a></li>
                                <li @if(Route::currentRouteName() == 'm_listBuyerTransactions' || Route::currentRouteName() == 'm_buyerTransactionID' ) class="active" @endif><a href="{{ URL::to('/') }}/m/list/buyer/transactions">Transaksi Order</a></li>
                                <li @if(Route::currentRouteName() == 'm_listSellerTransactions' || Route::currentRouteName() == 'm_sellerTransactionID') class="active" @endif><a href="{{ URL::to('/') }}/m/list/seller/transactions">Konfirmasi Order</a></li>
                            </ul>
                        </li>
                        @endif

                        <li>
                            <a href="{{ URL::to('/') }}/admin_logout"><i class="icon-logout"></i> <span>Logout</span></a>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>

    </div>
</div>
