<header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed header-blue">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="{{ URL::to('/') }}/m/profile" class="b-brand">
            <b>{{$dataUser->name}}</b>
        </a>
        </a>
    </div>
</header>