@extends('layout.member.master')
@section('title', 'Bank')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Tambah Bank</h2>
            </div>
            <div class="body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Nama Rekening (sesuai dengan Nama Profil)</label>
                        <input type="text" class="form-control" id="name_rek" autocomplete="off">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-5">
                        <label for="name">Nomor Rekening</label>
                        <input type="text" class="form-control" id="account_no" name="account_no" autocomplete="off">
                    </div>
                    <div class="form-group col-md-7">
                        <label for="bank_name">Nama Bank</label>
                        <select class="form-control" name="bank_name" id="bank_name">
                            <option value="none">- Pilih Bank -</option>
                            <option value="BCA">BCA</option>
                            <option value="CIMB">CIMB</option>
                            <option value="Bank Permata">Bank Permata</option>
                            <option value="Bank Mandiri">Bank Mandiri</option>
                            <option value="BRI">BRI</option>
                            <option value="BNI">BNI</option>
                            <option value="BTN">BTN</option>
                            <option value="BII">BII</option>
                            <option value="Bank Panin">Bank Panin</option>
                            <option value="Bank NISP">Bank NISP</option>
                            <option value="Citibank">Citibank</option>
                            <option value="Bank Danamon">Bank Danamon</option>
                            <option value="Bank Mega">Bank Mega</option>
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn  btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                &nbsp;&nbsp;
                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="header">
                <h2>List Bank</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Lengkap</th>
                                <th>No. Rekening</th>
                                <th>Nama Bank</th>
                                <th>Status</th>
                                <th>###</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($getData != null)
                                <?php $no = 0; ?>
                                @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $status = 'Aktif';
                                        $color = 'success';
                                        if($row->is_active == 0){
                                            $status = 'Tidak Aktif';
                                            $color = 'danger';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->account_name}}</td>
                                        <td>{{$row->account_no}}</td>
                                        <td>{{$row->bank_name}}</td>
                                        <td>
                                            <label class="badge badge-{{$color}}">{{$status}}</label>
                                        </td>
                                        <td>
                                            @if($row->is_active == 0)
                                                <a rel="tooltip"  data-toggle="modal" data-target="#activateBank" class="text-primary" href="{{ URL::to('/') }}/m/activate/bank/{{$row->id}}">aktifkan</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="modal fade" id="activateBank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript')
<script>
       function inputSubmit(){
           var account_no = $("#account_no").val();
           var bank_name = $("#bank_name").val();
           var name_rek = $("#name_rek").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/add-bank?account_no="+account_no+"&bank_name="+bank_name+"&name_rek="+name_rek ,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#form-add').remove();
            $('#loading').show();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

</script>
<script type="text/javascript">
    $("#activateBank").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-dialog").load(link.attr("href"));
    });
    
    function activateSubmit(){
        var dataInput = $("#form-insert").serializeArray();
        $('#form-insert').submit();
        $('#form-insert').remove();
        $('#loading').show();
        $('#tutupModal').remove();
        $('#submit').remove();
    }
</script>
@stop