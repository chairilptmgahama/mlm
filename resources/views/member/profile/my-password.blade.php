@extends('layout.member.master')
@section('title', 'Password')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Ubah Password</h2>
            </div>
            <div class="body">
                
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" autocomplete="off">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="repassword">Ketik Ulang Password</label>
                        <input type="password" class="form-control" id="repassword" name="repassword" autocomplete="off">
                    </div>
                </div>
                <button type="submit" class="btn  btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                &nbsp;&nbsp;
                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@stop

@section('javascript')
<script>
       function inputSubmit(){
           var password = $("#password").val();
           var repassword = $("#repassword").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/edit-password?&password="+password+"&repassword="+repassword,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#form-add').remove();
            $('#loading').show();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

</script>
@stop