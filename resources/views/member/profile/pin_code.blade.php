@extends('layout.member.master')
@section('title', 'Kode pin')

@section('content')

<div class="row clearfix">
    @if($dataUser->pin_code == null)
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <h2>Buat Kode Pin</h2>
            </div>
            <div class="body">
                
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Kode Pin</label>
                        <input type="text" class="form-control allownumericwithoutdecimal" id="pin_code" maxlength="6" autocomplete="off" placeholder="max 6 digit">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Ketik Ulang Kode Pin</label>
                        <input type="text" class="form-control allownumericwithoutdecimal" id="re_pin_code" maxlength="6" autocomplete="off" placeholder="max 6 digit">
                    </div>
                </div>
                <button type="submit" class="btn  btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                &nbsp;&nbsp;
                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($dataUser->pin_code != null)
    <div class="col-lg-6 col-md-12">
        <div class="card weather2">                        
            <div class="body city-selected">
                <div class="row">
                    <div class="col-12">
                        <div class="city"><span>Kode Pin Transaksi:</span></div>
                        <div class="night">&nbsp;</div>
                    </div>
                    <div class="info col-7">
                        <div class="temp"><h4>{{$dataUser->pin_code}}</h4></div>                                    
                    </div>
                    <div class="icon col-5">
                        <i class="icon-key"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

@stop

@section('javascript')
    @if($dataUser->pin_code == null)
        <script>
            function inputSubmit(){
                var pin_code = $("#pin_code").val();
                var re_pin_code = $("#re_pin_code").val();
                 $.ajax({
                     type: "GET",
                     url: "{{ URL::to('/') }}/m/cek/pin_code?pin_code="+pin_code+"&re_pin_code="+re_pin_code,
                     success: function(url){
                         $("#confirmDetail" ).empty();
                         $("#confirmDetail").html(url);
                     }
                 });
             }

             function confirmSubmit(){
                 var dataInput = $("#form-add").serializeArray();
                 $('#form-add').submit();
                 $('#form-add').remove();
                 $('#loading').show();
                 $('#tutupModal').remove();
                 $('#submit').remove();
             }
             $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
                 $(this).val($(this).val().replace(/[^\d].+/, ""));
                  if ((event.which < 48 || event.which > 57)) {
                      event.preventDefault();
                  }
              });
        </script>
    @endif
@stop