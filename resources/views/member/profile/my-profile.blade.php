@extends('layout.member.master')
@section('title', 'Profil')

@section('content')

<div class="row clearfix">

    <?php
        $gender = 'Pria';
        if($dataUser->gender == 2){
            $gender = 'Wanita';
        }
    ?>
    <div class="col-lg-4 col-md-12">
        <div class="card profile-header">
            <div class="body">
                <div class="profile-image" style="margin-bottom: 10px;"> <i class="icon-user-following fa-3x text-success" style="border: 1px solid #efefef; border-radius: 50px; padding: 13px;"></i> </div>
                <div>
                    <h4 class="m-b-0"><strong>{{$dataUser->full_name}}</strong></h4>
                    <span>{{$dataUser->user_code}}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Info</h2>
<!--                <ul class="header-dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>-->
            </div>
            <div class="body">
                <small class="text-muted">Alamat: </small>
                <p>{{$dataUser->alamat}}, {{$dataUser->kecamatan}}, {{$dataUser->kota}}, {{$dataUser->provinsi}}</p>
                <hr>
                <small class="text-muted">Email: </small>
                <p>{{$dataUser->email}}</p>                            
                <hr>
                <small class="text-muted">HP: </small>
                <p>{{$dataUser->hp}}</p>
                <hr>
                <small class="text-muted">Gender: </small>
                <p class="m-b-0">{{$gender}}</p>
                <hr>
                <small class="text-muted">Wallet: </small>
                <p class="m-b-0">{{$dataUser->wallet}}</p>
                <hr>
            </div>
        </div>
    </div>
</div>

@stop


<?php
/*









@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                    $gender = 'Pria';
                                    if($dataUser->gender == 2){
                                        $gender = 'Wanita';
                                    }
                                ?>
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="name">Nama Lengkap (sesuai dengan Nama pada Rekening Bank)</label>
                                        <input type="text" class="form-control" id="code" aria-describedby="code" value="{{$dataUser->full_name}}" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="name">Gender</label>
                                        <input type="text" class="form-control" id="name" aria-describedby="name" value="{{$gender}}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="name">Alamat Lengkap</label>
                                        <input type="text" class="form-control" id="code" aria-describedby="code" value="{{$dataUser->alamat}}" autocomplete="off">
                                    </div>
                                    
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="name">Kecamatan</label>
                                        <input type="text" class="form-control" id="name" aria-describedby="name" value="{{$dataUser->kecamatan}}" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="name">Kota/Kabupaten</label>
                                        <input type="text" class="form-control" id="code" aria-describedby="code" value="{{$dataUser->kota}}" autocomplete="off">
                                    </div>
                                    
                                    <div class="form-group col-md-4">
                                        <label for="name">Provinsi</label>
                                        <input type="text" class="form-control" id="name" aria-describedby="name" value="{{$dataUser->provinsi}}" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
 * 
 */
?>