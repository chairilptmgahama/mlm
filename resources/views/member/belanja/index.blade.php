@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="row">
                            @if($getData != null)
                            @foreach($getData as $row)
                                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 20px;text-align: center;">
                                    <div class="sc-product-item thumbnail card card-block">
                                        <div class="caption">
                                            <img data-name="product_image" src="{{$row->image}}" alt="..." style="width: 150px;">
                                            <h5 data-name="product_name">{{$row->name}} </h5>
                                            <h5><b>Rp. {{number_format($row->price, 0, ',', ',')}}</b></h5>
                                            <div>
                                                <div class="form-group2">
                                                    <div class="def-number-input number-input">
                                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus badge btn  btn-icon btn-outline-danger"><i class="feather icon-minus"></i></button>
                                                        <input class="sc-cart-item-qty cekInput{{$row->id}}" name="product_quantity" min="1" max="100" value="1" type="number">
                                                        <button onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus badge btn  btn-icon btn-outline-success"><i class="feather icon-plus"></i></button>
                                                    </div>
                                                </div>
                                                <input name="product_price" value="{{number_format($row->price, 0, ',', '')}}" type="hidden" />
                                                <input name="product_id" value="{{$row->id}}" type="hidden" />
                                                <input name="max_qty" value="100" type="hidden" />
                                                <button class="sc-add-to-cart btn btn-info btn-sm m-t-10"><i class="feather icon-shopping-cart"></i> &nbsp;&nbsp; Masuk</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        <h5>Keranjang Anda</h5>
                        @if($getData != null)
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-box tilebox-one">
                                        <div id="smartcart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="row">
            
        </div>-->
    </div>
</div>
@stop


@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/belanja.css') }}">
@stop
@section('javascript')
<script src="{{ asset('assets/js/jquery.cart.belanja.min.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#smartcart').smartCart();
        });
        
        @if($getData != null)
            @foreach($getData as $row1)
            $('.cekInput{{$row1->id}}').on('input', function () {
                var value = $(this).val();
                if ((value !== '') && (value.indexOf('.') === -1)) {
                    $(this).val(Math.max(Math.min(value, 100), 1));
                }
            });
            @endforeach
        @endif
</script>
<script>
    
    function inputSubmit(){
        $('#confirmSubmit').modal({'backdrop': 'static'});
        var cart_list = $("#cart_list").val();
         $.ajax({
             type: "GET",
             url: "{{ URL::to('/') }}/m/cek/belanja?cart_list="+cart_list,
             success: function(url){
                 $("#confirmDetail" ).empty();
                 $("#confirmDetail").html(url);
             }
         });
    }

     function confirmSubmit(){
         var dataInput = $("#form-add").serializeArray();
         $('#form-add').submit();
         $('#form-add').remove();
         $('#loading').show();
         $('#tutupModal').remove();
         $('#submit').remove();
     }
</script>
@stop