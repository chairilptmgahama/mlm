@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Transaksi</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tgl</th>
                                        <th>Total Harga</th>
<!--                                        <th>Daftar Belanja</th>-->
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                        <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            <?php
                                                $no++;
                                                $status = 'batal';
                                                $label = 'danger';
                                                if($row['status'] == 0){
                                                    $status = 'proses transfer';
                                                    $label = 'warning';
                                                }
                                                if($row['status'] == 1){
                                                    $status = 'proses admin';
                                                    $label = 'info';
                                                }
                                                if($row['status'] == 2){
                                                    $status = 'tuntas';
                                                    $label = 'success';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{date('d F Y', strtotime($row['sale_date']))}}</td>
                                                <td>{{number_format($row['total_price'], 0, ',', ',')}}</td>
<!--                                                <td>sss</td>-->
                                                <td><label class="badge badge-light-{{$label}}">{{$status}}</label></td>
                                                <td>
                                                    <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/detail/belanja/{{$row['id_transaction']}}">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop