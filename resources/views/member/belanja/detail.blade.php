@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5></h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="clearfix">
                            <div class="pull-left">
                                <h5>Invoice # <br>
                                    <small>{{$getData['invoice']}}</small>
                                </h5>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="pull-xs-left">
                                    <p><strong>Seller : </strong>Perusahaan</p>
                                    @if($getData['bank_name'] != null)
                                        <blockquote class='blockquote'>
                                            <i class="text-c-green d-block f-20">{{$getData['account_name']}}</i>
                                            <h4><span class="text-c-blgreenue">{{$getData['bank_name']}}</h4>
                                            <p class='f-18'>{{$getData['account_no']}}</p>
                                        </blockquote>
                                    @endif

                                    @if($getData['bank_name'] == null)
                                    <?php $no = 1; ?>
                                    <strong>Transfer Ke:</strong>
                                    @foreach($bankSeller as $rowBank)
                                        <?php $no++; ?>
                                        <blockquote class='blockquote'>
                                            <i class="d-block f-20">{{$rowBank->bank_name}}</i>
                                            <h4><span class="text-c-blgreenue">{{$rowBank->account_no}}</h4>
                                            <p class='f-18'>{{$rowBank->account_name}}</p>
                                        </blockquote>
                                    @endforeach
                                    @endif
                                        
                                </div>
                                <?php
                                    $status = 'Tuntas';
                                    $label = 'success';
                                    if($getData['status'] == 0){
                                        $status = 'Proses Transfer';
                                        $label = 'warning';
                                    }
                                    if($getData['status'] == 1){
                                        $status = 'Proses Admin';
                                        $label = 'info';
                                    }
                                    if($getData['status'] == 3){
                                        $status = 'Batal';
                                        $label = 'danger';
                                    }
                                ?>
                                <div class="pull-xs-right">
                                    <p><strong>Tanggal Order: </strong>{{date('d F Y', strtotime($getData['sale_date']))}}</p>
                                    <p class="m-t-10"><strong>Status: </strong> <span class="badge badge-{{$label}}">{{$status}}</span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="m-h-50"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead class="bg-faded">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Barang</th>
                                                <th>Jml Barang</th>
                                                <th>Harga (Rp.)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no1 = 0; ?>
                                            @foreach($getData['item_transaction'] as $rowItem)
                                                <?php 
                                                $no1++; 
                                                $harga = $rowItem['item_price'] * $rowItem['amount'] ;
                                                ?>
                                                <tr>
                                                    <td>{{$no1}}</td>
                                                    <td>{{$rowItem['name']}}</td>
                                                    <td>{{number_format($rowItem['amount'], 0, ',', '')}}</td>
                                                    <td>{{number_format($harga, 0, ',', ',')}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9 col-sm-6 col-xs-6">

                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                <p class="text-xs-right"><b>Sub-total:</b> Rp. {{number_format($getData['total_price'], 0, ',', ',')}}</p>
                                <hr>
                                <h3 class="text-xs-right">Rp. {{number_format($getData['total_price'], 0, ',', ',')}}</h3>
                            </div>
                        </div>
                        <hr>
                        @if($getData['status'] == 0)
                        <div class="hidden-print">
                            <div class="pull-xs-right">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/belanja">Kembali</a>
                                <button type="submit" class="btn btn-danger"  id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                                <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Confirm</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="confirmDetail">
                            </div>
                        </div>
                        <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="rejectDetail">
                            </div>
                        </div>
                        @endif
                        @if($getData['status'] == 1 || $getData['status'] == 2 || $getData['status'] == 3)
                        <div class="hidden-print">
                            <div class="pull-xs-right">
                                <a  class="btn btn-danger" href="{{ URL::to('/') }}/m/list/belanja">Kembali</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @if($getData['status'] == 0)
        <script>
               function inputSubmit(){
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/cek/confirm/belanja?id_trans={{$getData['id_transaction']}}&id_bank={{$bankSeller[0]->id}}",
                         success: function(url){
                             $("#confirmDetail" ).empty();
                             $("#confirmDetail").html(url);
                         }
                     });
               }

               function rejectSubmit(){
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/cek/reject/belanja?id_trans={{$getData['id_transaction']}}",
                         success: function(url){
                             $("#rejectDetail" ).empty();
                             $("#rejectDetail").html(url);
                         }
                     });
               }

                function confirmSubmit(){
                    var dataInput = $("#form-add").serializeArray();
                    $('#form-add').submit();
                    $('#form-add').remove();
                    $('#loading').show();
                    $('#tutupModal').remove();
                    $('#submit').remove();
                }
        </script>
    @endif
@stop