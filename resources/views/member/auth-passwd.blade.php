@extends('layout.member.auth')

@section('content')

<div class="vertical-align-wrap">
    <div class="vertical-align-middle auth-main">
        <div class="auth-box">
            <div class="card">

                <div class="header">
                    <img src="{{ URL('image/xone.jpg') }}" alt="" class="img-fluid mb-3" style="height:90px;">
                    <p class="lead">Recovery password anda</p>
                </div>
                <div class="body">
                    <form class="form-auth-small" method="post" action="/m/auth/passwd">
                        {{ csrf_field() }}
                        <input type="hidden" name="userID" value="{{$data->user_code}}">
                        <input type="hidden" name="authCode" value="{{$hiddenCode}}">
                        <input type="hidden" name="emailCheck" value="{{$data->email}}">
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="feather icon-lock"></i></span>
                            </div>
                            <input class="form-control" type="password" required="" placeholder="password" name="password" autocomplete="off">
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="feather icon-lock"></i></span>
                            </div>
                            <input class="form-control" type="password" required="" placeholder="tulis ulang password" name="repassword" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Ubah</button>
                        <div class="bottom">
                            <span class="helper-text m-b-10"><i class="fa fa-home"></i> <a href="/login">Kembali</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop