@extends('layout.member.auth')

@section('content')

<div class="vertical-align-wrap">
    <div class="vertical-align-middle auth-main">
        <div class="auth-box">
            <div class="card" style="background: linear-gradient(180deg, rgba(34,34,34,1) 66%, rgba(68,68,68,1) 100%);">
                <div class="header">
                    <img src="{{ URL('image/new_xone.png') }}" alt="" class="img-fluid mb-3">
                    <p class="lead text-white">Login akun anda</p>
                </div>
                <div class="body">
                    <form class="form-auth-small" method="post" action="/login_admin">
                        {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="feather icon-user"></i></span>
                            </div>
                            <input type="text" class="form-control" id="admin_email" placeholder="Username" name="admin_email">
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="feather icon-lock"></i></span>
                            </div>
                            <input type="password" class="form-control" placeholder="Password" name="admin_password">
                        </div>

                        <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                        <div class="bottom">
                            <span class="helper-text m-b-10"><i class="fa fa-lock text-white"></i> <a href="/m/forgot/passwd">Forgot password?</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
