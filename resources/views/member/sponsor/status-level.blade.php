@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Level</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 card-box table-responsive">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>UserID</th>
                                    <th>Tgl. Aktif</th>
                                    <th>Tgl. Placement</th>
                                    <th>Level</th>
                                    <th>Paket</th>
                                    <th>Pin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getData != null)
                                    <?php
                                        $no = 0;
                                        $myUplineDetail = explode(',', $dataUser->upline_detail);
                                        $myOnLevel = count($myUplineDetail);
                                    ?>
                                    @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $arrayUpline = explode(',', $row->upline_detail);
                                            $level = count($arrayUpline) - $myOnLevel;
                                            $placementdate = '--';
                                            if($row->placement_date != null){
                                                $placementdate = date('d M Y H:i', strtotime('+480 minutes', strtotime($row->placement_date)));
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->user_code}}</td>
                                                <td>{{date('d M Y H:i', strtotime($row->active_at))}}</td>
                                                <td>{{$placementdate}}</td>
                                                <td>{{$level}}</td>
                                                <td>{{$row->paket_name}}</td>
                                                <td>{{$row->pin}}</td>
                                            </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script src="/asset_member/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/asset_member/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
        });
    } );

</script>
@stop