@extends('layout.member.master')
@section('title', 'Sponsor')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        
        <div class="card">
            <div class="header">
                <h2>List</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>UserID</th>
                                <th>Email</th>
                                <th>No. HP</th>
                                <th>Status</th>
                                <th>Tgl. Join</th>
                                <th>Tgl Aktif</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($getData != null)
                                <?php $no = 0; ?>
                                @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $status = 'Blm Aktif';
                                        $color = 'danger';
                                        $tgl = '--';
                                        if($row->is_active == 1){
                                            $status = 'Aktif';
                                            $color = 'success';
                                            $tgl = date('d M Y H:i', strtotime($row->active_at));
                                        }
                                        
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->user_code}}</td>
                                        <td>{{$row->email}}</td>
                                        <td>{{$row->hp}}</td>
                                        <td><label class="badge badge-{{$color}}">{{$status}}</label></td>
                                        <td>{{date('d M Y H:i', strtotime($row->created_at))}}</td>
                                        <td>{{$tgl}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
