@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Claim Produk Aktifasi</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="clearfix">
                            <div class="pull-left">
                                <h5>Invoice #{{$getData->id}}{{date('dmY', strtotime($getData->created_at))}} <br>
                                </h5>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                    $status = 'Proses Admin';
                                    $label = 'info';
                                    if($getData->status == 1){
                                        $status = 'Proses Kirim';
                                        $label = 'success';
                                    }
                                    if($getData->status == 2){
                                        $status = 'Batal';
                                        $label = 'danger';
                                    }
                                ?>
                                <div class="pull-xs-right">
                                    <p><strong>Tanggal Claim: </strong>{{date('d F Y H:i', strtotime($getData->created_at))}}</p>
                                    @if($getData->status == 1)
                                        <p><strong>Tanggal Kirim: </strong>{{date('d F Y H:i', strtotime($getData->kirim_at))}}</p>
                                        <p><strong>No. Resi : </strong>{{$getData->no_resi}}</p>
                                    @endif
                                    <p class="m-t-10"><strong>Status: </strong> <span class="badge badge-{{$label}}">{{$status}}</span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="m-h-50"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead class="bg-faded">
                                            <tr>
                                                <th>Nama</th>
                                                <th>No. HP</th>
                                                <th>Alamat</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="table-success">
                                                <td>{{$getData->full_name}}</td>
                                                <td>{{$getData->hp}}</td>
                                                <td>{{$getData->alamat}} {{$getData->kelurahan}} {{$getData->kecamatan}} {{$getData->kota}} {{$getData->provinsi}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="hidden-print">
                            <div class="pull-xs-right">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/dashboard">Kembali</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop