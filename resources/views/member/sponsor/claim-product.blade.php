@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Claim Produk Aktifasi</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="full_name">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="full_name" name="full_name" autocomplete="off" required="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="no_hp">No. HP</label>
                                        <input type="text" class="form-control" id="no_hp" name="no_hp" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="alamat">Alamat</label>
                                        <textarea class="form-control" id="alamat" rows="2" name="alamat" autocomplete="off" required=""></textarea>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="kelurahan">Kelurahan</label>
                                        <input type="text" class="form-control" id="kelurahan" name="kelurahan" autocomplete="off" required="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="name">Kecamatan</label>
                                        <input type="kecamatan" class="form-control" id="kecamatan"  name="kecamatan" required="" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="kota">Kota/Kabupaten</label>
                                        <input type="text" class="form-control" id="kota" name="kota" autocomplete="off" required="">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="provinsi">Provinsi</label>
                                        <select class="form-control" name="provinsi" id="provinsi">
                                            <option value="Aceh">Aceh</option>
                                            <option value="Sumatera Utara">Sumatera Utara</option>
                                            <option value="Sumatera Barat">Sumatera Barat</option>
                                            <option value="Riau">Riau</option>
                                            <option value="Jambi">Jambi</option>
                                            <option value="Sumatera Selatan">Sumatera Selatan</option>
                                            <option value="Bengkulu">Bengkulu</option>
                                            <option value="Lampung">Lampung</option>
                                            <option value="Kep. Bangka Belitung">Kep. Bangka Belitung</option>
                                            <option value="Kepulauan Riau">Kepulauan Riau</option>
                                            <option value="DKI Jakarta">DKI Jakarta</option>
                                            <option value="Jawa Barat">Jawa Barat</option>
                                            <option value="Banten">Banten</option>
                                            <option value="Jawa Tengah">Jawa Tengah</option>
                                            <option value="Yogyakarta">Yogyakarta</option>
                                            <option value="Jawa Timur">Jawa Timur</option>
                                            <option value="Kalimantan Barat">Kalimantan Barat</option>
                                            <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                            <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                            <option value="Kalimantan Timur">Kalimantan Timur</option>
                                            <option value="Kalimantan Utara">Kalimantan Utara</option>
                                            <option value="Bali">Bali</option>
                                            <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                            <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                            <option value="Sulawesi Utara">Sulawesi Utara</option>
                                            <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                            <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                            <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                            <option value="Sulawesi Barat">Sulawesi Barat</option>
                                            <option value="Gorontalo">Gorontalo</option>
                                            <option value="Maluku">Maluku</option>
                                            <option value="Maluku Utara">Maluku Utara</option>
                                            <option value="Papua">Papua</option>
                                            <option value="Papua Barat">Papua Barat</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn  btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                                &nbsp;&nbsp;
                                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document" id="confirmDetail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
       function inputSubmit(){
           var full_name = $("#full_name").val();
           var no_hp = $("#no_hp").val();
           var kecamatan = $("#kecamatan").val();
           var kelurahan = $("#kelurahan").val();
           var alamat = $("#alamat").val();
           var kota = $("#kota").val();
           var provinsi = $("#provinsi").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/claim/activate/product?full_name="+full_name+"&no_hp="+no_hp+"&kecamatan="+kecamatan+"&kota="+kota+"&provinsi="+provinsi+"&kelurahan="+kelurahan+"&alamat="+alamat ,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#form-add').remove();
            $('#loading').show();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

</script>
@stop