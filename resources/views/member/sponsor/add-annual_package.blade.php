@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @if($tersedia > 0)
                                @foreach($getData as $row)
                                    @if($row->total_sisa > 0)
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px;text-align: center;">
                                            <div class="sc-product-item thumbnail card card-block">
                                                <div class="caption">
                                                    <img data-name="product_image" src="{{$row->image}}" alt="..." style="width: 150px;">
                                                    <h5 data-name="product_name">{{$row->name}} </h5>
                                                    <p>Tersedia: {{number_format($row->total_sisa, 0, ',', ',')}} </p>
                                                    <div>
                                                        <input name="product_id" value="{{$row->id}}" type="hidden" />
                                                        <input name="max_qty" value="{{number_format($row->qty, 0, ',', '')}}" type="hidden" />
                                                        <input name="nama_produk" value="{{$row->name}}" type="hidden" />
                                                        @if($row->qty > 0)
                                                        <a class="sc-add-to-cart btn btn-success btn-sm m-t-10" href="{{ URL::to('/') }}/m/add/annual-member?package={{$row->id}}">Pilih</a>
                                                        @endif
                                                        @if($row->qty <= 0)
                                                        <div class="btn btn-dark btn-sm m-t-10">Masuk ke keranjang</div>
                                                        @endif
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                            @if($tersedia <= 0)
                            <div class="col-md-12">
                                    <div class="alert alert-warning" role="alert">
                                            Anda tidak memiliki paket pin silakan order pin di <a href="/m/add/pin">stockist</a>
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/cart.css') }}">
@stop
@section('javascript')
<script src="{{ asset('assets/js/jquery.cart.min.js') }}"></script>
@stop