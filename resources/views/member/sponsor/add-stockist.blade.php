@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{$headerTitle}}</h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="input_email">Email</label>
                                        <input type="email" class="form-control" id="input_email" name="email" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-8">
                                        <label for="input_name">Nama</label>
                                        <input type="name" class="form-control" id="name" name="name" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="input_hp">No. HP</label>
                                        <input type="text" class="form-control allownumericwithoutdecimal" id="input_hp" name="hp" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="input_username">Username (Login User)</label>
                                        <input type="text" class="form-control" id="input_username" name="user_code" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="input_password">Password</label>
                                        <input type="password" class="form-control" id="input_password" name="password">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="input_repassword">Ketik Ulang Password</label>
                                        <input type="password" class="form-control" id="input_repassword" name="repassword">
                                    </div>
                                </div>
                                <button type="submit" class="btn  btn-primary" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                                &nbsp;&nbsp;
                                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document" id="confirmDetail">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
       function inputSubmit(){
           var email = $("#input_email").val();
           var name = $("#name").val();
           var password = $("#input_password").val();
           var repassword = $("#input_repassword").val();
           var user_code = $("#input_username").val();
           var hp = $("#input_hp").val();
           var package_id = $("#package_id").val();
           var name_package = $("#name_package").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/add-stockist?email="+email+"&password="+password+"&repassword="+repassword+"&user_code="+user_code+"&hp="+hp+"&package_id="+package_id+"&name_package="+name_package+"&name="+name ,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

</script>
@stop