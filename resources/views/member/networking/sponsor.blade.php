@extends('layout.member.master')
@section('title', 'Sponsor Diagram')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="region region-content">
                            <div id="block-system-main" class="block block-system clearfix">
                                <div class="binary-genealogy-tree binary_tree_extended">
                                    <div class="sponsor-tree-wrapper">
                                        <div class="eps-sponsor-tree eps-tree"> <?php // style="max-width: 270px;" ?>
                                            <ul>
                                                <li>
                                                    <div class="eps-nc" nid="12">
                                                        <div class="user-pic">
                                                            <div class="images_wrapper" style="font-size: 40px;margin: 10px 0;">
                                                                <a href="{{ URL::to('/') }}/m/my/sponsor-tree?get_id={{$dataUser->id}}" class="text-success">
                                                                    <i class="feather icon-user"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="user-name">
                                                           {{$dataUser->user_code}}
                                                        </div>
                                                        <div class="user-name" style="background: linear-gradient(45deg, #FFB64D, #ffcb80);">
                                                           Total Sp : {{$dataUser->total_sponsor}}
                                                        </div>
                                                    </div>
                                                    @if($getData != null)
                                                    <ul>
                                                        @foreach($getData as $row)
                                                            <?php
                                                                $icon = 'icon-user-unfollow';
                                                                $color = 'text-danger';
                                                                if($row->is_active == 1){
                                                                    $icon = 'icon-user-following';
                                                                    $color = 'text-success';
                                                                }
                                                            ?>
                                                            <li>
                                                                <div class="eps-nc" nid="13">
                                                                    <div class="user-pic">
                                                                        <div class="images_wrapper text-success" style="font-size: 40px;margin: 10px 0;">
                                                                            <a href="{{ URL::to('/') }}/m/my/sponsor-tree?get_id={{$row->id}}">
                                                                                <i class="feather {{$color}} {{$icon}}"></i>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="user-name" class="bg-c-blue">
                                                                        {{$row->user_code}}
                                                                    </div>
                                                                    <div class="user-name" style="background: linear-gradient(45deg, #FFB64D, #ffcb80);">
                                                                        Total Sp : {{$row->total_sponsor}}
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@stop


@section('styles')
<link href="{{ asset('asset_member/css/tree-style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/css/developer.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $("#get_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/usercode" + "?name=" + $(this).val() ,
                success: function(data){
                    $("#get_id-box").show();
                    $("#get_id-box").html(data);
                }
            });
        });
    });
    function selectUsername(val) {
        var valNew = val.split("____");
        $("#get_id").val(valNew[1]);
        $("#id_get_id").val(valNew[0]);
        $("#get_id-box").hide();
    }
</script>
@stop