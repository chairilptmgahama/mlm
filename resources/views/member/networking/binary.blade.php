@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Binary</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <form class="login100-form validate-form" method="get" action="/m/my/binary">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-12">
                                <fieldset class="form-group">
                                    <label>Cari Member (By Username)</label>
                                    <input type="text" class="form-control" id="get_id" autocomplete="off">
                                    <input type="hidden" name="get_id" id="id_get_id">
                                    <ul class="typeahead dropdown-menu form-control" style="max-height: 120px; overflow: auto;border: 1px solid #ddd;width: 98%;margin-left: 11px;" id="get_id-box"></ul>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <button type="submit" class="btn btn-primary" >Cari</button>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div class="card-box">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        @if($back == true)
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ URL::to('/') }}/m/my/binary" class="btn btn-dark btn-sm">
                                    My Structure
                                </a>
                                @if($dataUser->upline_id != $sessionUser->id)
                                <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$dataUser->upline_id}}" class="btn btn-purple btn-sm">
                                    Upline
                                </a>
                                @endif
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="region region-content">
                                    <div id="block-system-main" class="block block-system clearfix">
                                        <div class="binary-genealogy-tree binary_tree_extended">
                                            <div class="binary-genealogy-level-0 clearfix">
                                                <div class="no_padding parent-wrapper clearfix">
                                                    <div class="node-centere-item binary-level-width-100">
                                                        <div class="node-item-root">
                                                            <div class="binary-node-single-item user-block user-0">
                                                                <div class="images_wrapper" style="margin: 6px 0;">
                                                                    <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[0]['data']->id}}">
                                                                        <?php
                                                                            $img = 'r_b1.jpeg';
                                                                            if($getData[0]['data']->package_id == 1){
                                                                                $img = 'r_b1.jpeg';
                                                                            }
                                                                            if($getData[0]['data']->package_id == 2){
                                                                                $img = 'a_s1.jpg';
                                                                            }
                                                                            if($getData[0]['data']->package_id == 3){
                                                                                $img = 's_g1.jpg';
                                                                            }
                                                                            if($getData[0]['data']->package_id == 4){
                                                                                $img = 'ms_p1.jpg';
                                                                            }
                                                                        ?>
                                                                        <img src="/image/{{$img}}" alt="user" class="img-circle" style="width: 70px;">
                                                                    </a>
                                                                </div>
                                                                <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[0]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                    <p class="wrap_content ">
                                                                        {{$getData[0]['data']->user_code}}
                                                                        <br>
                                                                        {{$getData[0]['sisaKiri']}}   -   {{$getData[0]['sisaKanan']}}
                                                                    </p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="parent-wrapper clearfix">
                                                            <div class="node-left-item binary-level-width-50">
                                                                <?php
                                                                    $root = '';
                                                                    if($getData[1]['data'] != null){
                                                                        $root = 'node-item-root';
                                                                    }
                                                                ?>
                                                                <div class="{{$root}}">
                                                                    <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>
                                                                    <div class="node-item-1-child-left">
                                                                        @if($getData[1]['data'] != null)
                                                                        <div class="binary-node-single-item user-block user-9">
                                                                            <div class="images_wrapper" style="margin: 6px 0;">
                                                                                <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[1]['data']->id}}">
                                                                                    <?php
                                                                                        $img1 = 'r_b1.jpeg';
                                                                                        if($getData[1]['data']->package_id == 1){
                                                                                            $img1 = 'r_b1.jpeg';
                                                                                        }
                                                                                        if($getData[1]['data']->package_id == 2){
                                                                                            $img1 = 'a_s1.jpg';
                                                                                        }
                                                                                        if($getData[1]['data']->package_id == 3){
                                                                                            $img1 = 's_g1.jpg';
                                                                                        }
                                                                                        if($getData[1]['data']->package_id == 4){
                                                                                            $img1 = 'ms_p1.jpg';
                                                                                        }
                                                                                    ?>
                                                                                    <img src="/image/{{$img1}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                </a>
                                                                            </div>
                                                                            <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[1]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                <span class="wrap_content ">
                                                                                    {{$getData[1]['data']->user_code}}
                                                                                    <br>
                                                                                    {{$getData[1]['sisaKiri']}}   -   {{$getData[1]['sisaKanan']}}
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                        @else
                                                                            <div class="binary-node-single-item user-block user-13">
                                                                                <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                    <i class="icon-user text-success"></i>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                @if($getData[1]['data'] != null)
                                                                <div class="parent-wrapper clearfix">
                                                                    <div class="node-left-item binary-level-width-50">
                                                                            <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>
                                                                            <div class="node-item-1-child-left">
                                                                                @if($getData[3]['data'] != null)
                                                                                <div class="binary-node-single-item user-block user-9">
                                                                                    <div class="images_wrapper" style="margin: 6px 0;">
                                                                                        <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[3]['data']->id}}">
                                                                                            <?php
                                                                                                $img3 = 'r_b1.jpeg';
                                                                                                if($getData[3]['data']->package_id == 1){
                                                                                                    $img3 = 'r_b1.jpeg';
                                                                                                }
                                                                                                if($getData[3]['data']->package_id == 2){
                                                                                                    $img3 = 'a_s1.jpg';
                                                                                                }
                                                                                                if($getData[3]['data']->package_id == 3){
                                                                                                    $img3 = 's_g1.jpg';
                                                                                                }
                                                                                                if($getData[3]['data']->package_id == 4){
                                                                                                    $img3 = 'ms_p1.jpg';
                                                                                                }
                                                                                            ?>
                                                                                            <img src="/image/{{$img3}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[3]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                        <span class="wrap_content ">
                                                                                            {{$getData[3]['data']->user_code}}
                                                                                            <br>
                                                                                            {{$getData[3]['sisaKiri']}}   -   {{$getData[3]['sisaKanan']}}
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                                @else
                                                                                    <div class="binary-node-single-item user-block user-13">
                                                                                        <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                            <i class="icon-user text-success"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                                                @endif
                                                                            </div>
                                                                    </div>
                                                                    <div class="node-right-item binary-level-width-50">
                                                                            <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>
                                                                            <div class="node-item-1-child-right">
                                                                                @if($getData[4]['data'] != null)
                                                                                <div class="binary-node-single-item user-block user-10">
                                                                                    <div class="images_wrapper" style="margin: 6px 0;">
                                                                                        <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[4]['data']->id}}">
                                                                                            <?php
                                                                                                $img4 = 'r_b1.jpeg';
                                                                                                if($getData[4]['data']->package_id == 1){
                                                                                                    $img4 = 'r_b1.jpeg';
                                                                                                }
                                                                                                if($getData[4]['data']->package_id == 2){
                                                                                                    $img4 = 'a_s1.jpg';
                                                                                                }
                                                                                                if($getData[4]['data']->package_id == 3){
                                                                                                    $img4 = 's_g1.jpg';
                                                                                                }
                                                                                                if($getData[4]['data']->package_id == 4){
                                                                                                    $img4 = 'ms_p1.jpg';
                                                                                                }
                                                                                            ?>
                                                                                            <img src="/image/{{$img4}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[4]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                        <span class="wrap_content ">
                                                                                            {{$getData[4]['data']->user_code}}
                                                                                            <br>
                                                                                            {{$getData[4]['sisaKiri']}}   -   {{$getData[4]['sisaKanan']}}
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                                @else
                                                                                <div class="binary-node-single-item user-block user-13">
                                                                                    <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                        <i class="icon-user text-success"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                                                @endif
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="node-right-item binary-level-width-50">
                                                                <?php
                                                                    $root1 = '';
                                                                    if($getData[2]['data'] != null){
                                                                        $root1 = 'node-item-root';
                                                                    }
                                                                ?>
                                                                <div class="{{$root1}}">
                                                                    <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>
                                                                    <div class="node-item-1-child-right">
                                                                        @if($getData[2]['data'] != null)
                                                                        <div class="binary-node-single-item user-block user-10">
                                                                            <div class="images_wrapper" style="margin: 6px 0;">
                                                                                <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[2]['data']->id}}">
                                                                                    <?php
                                                                                        $img2 = 'r_b1.jpeg';
                                                                                        if($getData[2]['data']->package_id == 1){
                                                                                            $img2 = 'r_b1.jpeg';
                                                                                        }
                                                                                        if($getData[2]['data']->package_id == 2){
                                                                                            $img2 = 'a_s1.jpg';
                                                                                        }
                                                                                        if($getData[2]['data']->package_id == 3){
                                                                                            $img2 = 's_g1.jpg';
                                                                                        }
                                                                                        if($getData[2]['data']->package_id == 4){
                                                                                            $img2 = 'ms_p1.jpg';
                                                                                        }
                                                                                    ?>
                                                                                    <img src="/image/{{$img2}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                </a>
                                                                            </div>
                                                                            <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[2]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                <span class="wrap_content ">
                                                                                    {{$getData[2]['data']->user_code}}
                                                                                    <br>
                                                                                    {{$getData[2]['sisaKiri']}}   -   {{$getData[2]['sisaKanan']}}
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                        @else
                                                                        <div class="binary-node-single-item user-block user-13">
                                                                            <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                <i class="icon-user text-success"></i>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                @if($getData[2]['data'] != null)
                                                                <div class="parent-wrapper clearfix">
                                                                    <div class="node-left-item binary-level-width-50">
                                                                            <span class="binary-hr-line binar-hr-line-left binary-hr-line-width-25"></span>
                                                                            <div class="node-item-1-child-left">
                                                                                @if($getData[5]['data'] != null)
                                                                                <div class="binary-node-single-item user-block user-9">
                                                                                    <div class="images_wrapper" style="margin: 6px 0;">
                                                                                        <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[5]['data']->id}}">
                                                                                            <?php
                                                                                                $img5 = 'r_b1.jpeg';
                                                                                                if($getData[5]['data']->package_id == 1){
                                                                                                    $img5 = 'r_b1.jpeg';
                                                                                                }
                                                                                                if($getData[5]['data']->package_id == 2){
                                                                                                    $img5 = 'a_s1.jpg';
                                                                                                }
                                                                                                if($getData[5]['data']->package_id == 3){
                                                                                                    $img5 = 's_g1.jpg';
                                                                                                }
                                                                                                if($getData[5]['data']->package_id == 4){
                                                                                                    $img5 = 'ms_p1.jpg';
                                                                                                }
                                                                                            ?>
                                                                                            <img src="/image/{{$img5}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[5]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                        <span class="wrap_content ">
                                                                                            {{$getData[5]['data']->user_code}}
                                                                                            <br>
                                                                                            {{$getData[5]['sisaKiri']}}   -   {{$getData[5]['sisaKanan']}}
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                                @else
                                                                                    <div class="binary-node-single-item user-block user-13">
                                                                                        <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                            <i class="icon-user text-success"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                                                @endif
                                                                            </div>
                                                                    </div>
                                                                    <div class="node-right-item binary-level-width-50">
                                                                            <span class="binary-hr-line binar-hr-line-right binary-hr-line-width-25"></span>
                                                                            <div class="node-item-1-child-right">
                                                                                @if($getData[6]['data'] != null)
                                                                                <div class="binary-node-single-item user-block user-10">
                                                                                    <div class="images_wrapper" style="margin: 6px 0;">
                                                                                        <a href="{{ URL::to('/') }}/m/my/binary?get_id={{$getData[6]['data']->id}}">
                                                                                            <?php
                                                                                                $img6 = 'r_b1.jpeg';
                                                                                                if($getData[6]['data']->package_id == 1){
                                                                                                    $img6 = 'r_b1.jpeg';
                                                                                                }
                                                                                                if($getData[6]['data']->package_id == 2){
                                                                                                    $img6 = 'a_s1.jpg';
                                                                                                }
                                                                                                if($getData[6]['data']->package_id == 3){
                                                                                                    $img6 = 's_g1.jpg';
                                                                                                }
                                                                                                if($getData[6]['data']->package_id == 4){
                                                                                                    $img6 = 'ms_p1.jpg';
                                                                                                }
                                                                                            ?>
                                                                                            <img src="/image/{{$img6}}" alt="user" class="img-circle" style="width: 70px;">
                                                                                        </a>
                                                                                    </div>
                                                                                    <a rel="tooltip"  data-toggle="modal" data-target="#popUp" class="text-primary" href="{{ URL::to('/') }}/m/cek/detail/member-binary?get_id={{$getData[6]['data']->id}}" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">
                                                                                        <span class="wrap_content ">
                                                                                            {{$getData[6]['data']->user_code}}
                                                                                            <br>
                                                                                            {{$getData[6]['sisaKiri']}}   -   {{$getData[6]['sisaKanan']}}
                                                                                        </span>
                                                                                    </a>
                                                                                </div>
                                                                                @else
                                                                                <div class="binary-node-single-item user-block user-13">
                                                                                    <div class="images_wrapper" style="font-size: 40px;margin: 11px 0;">
                                                                                        <i class="icon-user text-success"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="last_level_user"><i class="fa fa-2x">&nbsp;</i></div>
                                                                                @endif
                                                                            </div>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="popUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/css/binary.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/css/developer.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('javascript')
<script type="text/javascript">
    $("#popUp").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).ready(function(){
        $("#get_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/usercode" + "?name=" + $(this).val() ,
                success: function(data){
                    $("#get_id-box").show();
                    $("#get_id-box").html(data);
                }
            });
        });
    });
    function selectUsername(val) {
        var valNew = val.split("____");
        $("#get_id").val(valNew[1]);
        $("#id_get_id").val(valNew[0]);
        $("#get_id-box").hide();
    }
</script>
@stop