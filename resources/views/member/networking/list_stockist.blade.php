@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Stockist</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>User ID</th>
                                        <th>Tot. Aktifasi (Bronze)</th>
                                        <th>Saldo Pin Bronze</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getStockist != null)
                                        <?php $no = 0; ?>
                                        @foreach($getStockist as $row)
                                            <?php
                                                $no++;
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->stockist_name}}</td>
                                                <td>{{$row->stockist_user_id}}</td>
                                                <td>{{$row->total_aktifasi}}</td>
                                                <td>{{$row->saldo_bronze}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop