@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Detail Member</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php
                $img = 'r_b1.jpeg';
                $title = 'Bronze';
                if($dataUser->package_id == 1){
                    $img = 'r_b1.jpeg';
                    $title = 'Bronze';
                }
                if($dataUser->package_id == 2){
                    $img = 'a_s1.jpg';
                    $title = 'Silver';
                }
                if($dataUser->package_id == 3){
                    $img = 's_g1.jpg';
                    $title = 'Gold';
                }
                if($dataUser->package_id == 4){
                    $img = 'ms_p1.jpg';
                    $title = 'Platinum';
                }
            ?>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-4">
                    <div class="card-box widget-user">
                        <div>
                             <img src="/image/{{$img}}" alt="user"class="img-responsive img-circle" style="width: 110px;height: 100px;">
                            <div class="wid-u-info">
                                <h5 class="m-t-20 m-b-5">{{$dataUser->user_code}}</h5>
                                <h5 class="inbox-item-author m-b-0 font-13">{{$dataUser->email}}</h5>
                                <h5 class="text-muted m-b-0 font-13">Joined {{date('d F Y', strtotime($dataUser->active_at))}}</h5>
                                <div class="user-position">
                                    <span class="text-warning font-weight-bold">{{$title}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-vector pull-xs-right text-muted text-purple"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Sisa Kiri</h6>
                        <h2 class="m-b-20">{{$detailUser->sisaKiri}}</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-vector pull-xs-right text-muted text-pink"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Sisa Kanan</h6>
                        <h2 class="m-b-20">{{$detailUser->sisaKanan}}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-vector pull-xs-right text-muted text-purple"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Kiri</h6>
                        <h2 class="m-b-20">{{$detailUser->kiri}}</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-vector pull-xs-right text-muted text-pink"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Kanan</h6>
                        <h2 class="m-b-20">{{$detailUser->kanan}}</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-basket-loaded pull-xs-right text-muted text-purple"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Omzet Kiri</h6>
                        <h2 class="m-b-20">{{number_format($detailUser->omzetKiri, 0, ',', '.')}}</h2>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-basket-loaded pull-xs-right text-muted text-pink"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Jml Omzet Kanan</h6>
                        <h2 class="m-b-20">{{number_format($detailUser->omzetKanan, 0, ',', '.')}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop