@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Upgrade Paket</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 pricing-table">
                                    <?php $no = 0; ?>
                                    @foreach($packageUpgrade as $row)
                                        <div class="pricing-item" style="padding: 10px 0px;">
                                            <div class="pricing-value">
                                                <img src="/image/{{$row->img_package}}" alt="user" class="img-circle" style="width: 180px;">
                                            </div>
                                            <div class="pricing-title">
                                                <h5><b>{{$row->name}}</b></h5>
                                            </div>
                                            <ul class="pricing-features">
                                                <li><span class="keywords">{{$row->short_desc}}</span> Paket Safra</li>
                                                <li>Bonus Sponsor Rp 75.000/PIN </li>
                                                <li>Bonus Binary Rp 15.000/Pasangan</li>
                                                <li>Bonus RO 10 Level</li>
                                                <li>Kuota Bonus /Bulan Max Rp. {{number_format($row->stock_wd, 0, ',', '.')}}</li>
                                                <li>Discount {{$row->safra_discount}}% Safra Poin</li>
                                            </ul>
                                            <a rel="tooltip" title="View" data-toggle="modal" data-target="#upgradePackage" class="btn  btn-primary" href="{{ URL::to('/') }}/m/cek/upgrade-package/{{$row->id}}">Upgrade</i></a>
                                        </div>
                                    @endforeach

                                    <div class="modal fade" id="upgradePackage" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script type="text/javascript">
    $("#upgradePackage").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }
</script>
@stop
