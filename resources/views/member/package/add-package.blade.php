@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Beli Paket</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        <div class="alert alert-info" role="alert" style="color:#222;">
                            Halo <b>{{$dataUser->name}}</b>, Selamat bergabung di keluarga SAFRA. Saat ini status keanggotaan anda belum aktif, Silakan memilih paket di bawah ini  dengan klik "Beli Paket"
                        </div>
                        <div class="row">
                            <div class="col-md-12 pricing-table">
                                    <?php $no = 0; ?>
                                    @foreach($allPackage as $row)
                                        <?php
                                        $no++;
                                        $price = $row->pin * $pinSetting->price;
                                        $feature = '';
                                        $label = '';
                                        if($no == 1){
                                            $img = 'r_b1.jpeg';
                                        }
                                        if($no == 2){
                                            $img = 'a_s1.jpg';
                                        }
                                        if($no == 3){
                                            $feature = 'pricing-featured';
                                            $label = '<div class="selected">Rekomendasi</div>';
                                            $img = 's_g1.jpg';
                                        }
                                        if($no == 4){
                                            $img = 'ms_p1.jpg';
                                        }
                                        ?>
                                        <div class="pricing-item {{$feature}}">
                                            <?php echo $label; ?>
                                            <div class="pricing-value">
                                                <img src="/image/{{$img}}" alt="user" class="img-circle" style="width: 180px;">
                                            </div>
                                            <div class="pricing-title">
                                                <h5><b>{{$row->name}}</b></h5>
                                                <h5><b>Rp. {{number_format($price, 0, ',', ',')}}</b></h5>
                                            </div>
                                            <ul class="pricing-features">
                                                <li><span class="keywords">{{$row->short_desc}}</span> Paket Safra</li>
                                                <li>Bonus Sponsor Rp 75.000/PIN </li>
                                                <li>Bonus Binary Rp 15.000/Pasangan</li>
                                                <li>Bonus RO 10 Level</li>
                                                <li>Discount {{$row->safra_discount}}% Safra Poin</li>
                                            </ul>
                                            <a rel="tooltip" title="View" data-toggle="modal" data-target="#orderPackage" class="btn  btn-primary" href="{{ URL::to('/') }}/m/cek/add-package/{{$row->id}}" style="margin-bottom: 15px;">Beli Paket</i></a>
                                        </div>
                                    @endforeach

                                    <div class="modal fade" id="orderPackage" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script type="text/javascript">
    $("#orderPackage").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }
</script>
@stop