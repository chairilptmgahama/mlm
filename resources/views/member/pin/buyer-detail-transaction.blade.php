@extends('layout.member.master')
@section('title', 'Order Coin')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <h5>Invoice Details : <strong class="text-primary">#{{$getData->transaction_code}}</strong></h5>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-6">
                                <?php
                                    $seller = 'Perusahaan';
                                    $hp_seller = '081233332222';
                                    if($dataUser->member_type != 1){
                                        $seller = $getData->user_code;
                                        $hp_seller = $getData->hp;
                                    }
                                ?>
                                <p><strong>Seller : </strong>{{$seller}}</p>
                                <p><strong>No. HP : </strong>{{$hp_seller}}</p>
                                
                                @if($getData->type == 1)
                                <address>
                                    <strong>Transfer Ke:</strong>
                                    @if($getData->bank_name != null)
                                        <br>
                                        Nama Rekening : <strong>{{$getData->account_name}}</strong>
                                        <br>
                                        Nama Bank: <strong>{{$getData->bank_name}}</strong>
                                        <br>
                                        No. Rekening: <strong>{{$getData->account_no}}</strong>
                                    @endif

                                    @if($getData->bank_name == null)
                                        <?php $no = 1; ?>
                                        @foreach($bankSeller as $rowBank)
                                            <?php $no++; ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="radio" id="radio{{$no}}" value="{{$rowBank->id}}">
                                                <label class="form-check-label" for="radio{{$no}}">
                                                    {{$rowBank->bank_name}} ({{$rowBank->account_name}} - {{$rowBank->account_no}})
                                                </label>
                                            </div>
                                        @endforeach
                                    @endif
                                </address>
                                @endif
                            </div>
                            <?php
                                $status = 'Tuntas';
                                $label = 'success';
                                if($getData->status == 0){
                                    $status = 'Proses Transfer';
                                    $label = 'danger';
                                }
                                if($getData->status == 1){
                                    $status = 'Menunggu Konfirmasi';
                                    $label = 'warning';
                                }
                                if($getData->status == 3){
                                    $status = 'Batal';
                                    $label = 'danger';
                                }
                                $type = 'Order Coin';
                                $jenis = 'Order';
                                if($getData->type == 2){
                                    $type = 'Transfer Coin';
                                    $jenis = 'Transfer';
                                }
                                if($getData->type == 3){
                                    $type = 'Jual Coin';
                                    $jenis = 'Jual';
                                }
                                if($getData->type == 4){
                                    $type = 'Posting Coin Start';
                                    $jenis = 'Posting Start';
                                }
                                if($getData->type == 5){
                                    $type = 'Posting Coin Finnish';
                                    $jenis = 'Posting Finnish';
                                }
                            ?>
                            <div class="col-md-6 col-sm-6 text-right">
                                <p class="m-b-0"><strong>Tanggal {{$jenis}}: </strong>{{date('d F Y H:i', strtotime($getData->created_at))}}</p>
                                <p class="m-b-0"><strong>Status: </strong> <span class="badge badge-{{$label}} m-b-0">{{$status}}</span></p>
                                <p><strong>Type: </strong> <span class="text-primary">#{{$type}}</span></p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead class="bg-faded">
                                            <tr>
                                                <th>#</th>
                                                <th>Jml Coin {{$jenis}}</th>
                                                @if($getData->type == 1)
                                                    <th>Harga (Rp.)</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0; ?>
                                            <?php 
                                            $no++; 
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{number_format($getData->total_coin, 4, ',', '.')}}</td>
                                                @if($getData->type == 1)
                                                    <td>{{number_format($getData->price, 0, ',', '.')}}</td>
                                                @endif
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row clearfix">
<!--                            <div class="col-md-6">
                                <h5>Note</h5>
                                <p>Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.</p>
                            </div>-->
                            @if($getData->type == 1)
                                <?php
                                   $total = $getData->price + $getData->unique_digit + $getData->admin_fee;
                                ?>
                                <div class="col-md-12 text-right">
                                    <p class="m-b-0"><b>Sub-total:</b> &nbsp; {{number_format($getData->price, 0, ',', '.')}}</p>
                                    <p class="m-b-0"><b>Fee:</b> &nbsp; {{number_format($getData->admin_fee, 0, ',', '.')}}</p>
                                    <p class="m-b-0"><b>Kode Unik:</b> &nbsp; {{number_format($getData->unique_digit, 0, ',', '.')}}</p>
                                    <hr>
                                    <h3 class="m-b-0 m-t-10">Rp. {{number_format($total, 0, ',', '.')}}</h3>
                                </div>
                            @endif
                            
                            @if($getData->type == 3)
                                <?php
                                    $total = $getData->price + $getData->unique_digit - $getData->admin_fee;
                                ?>
                                <div class="col-md-12 text-right">
                                    <p class="m-b-0"><b>Sub-total:</b> &nbsp; {{number_format($getData->price, 2, ',', '.')}}</p>
                                    <p class="m-b-0"><b>Fee:</b> &nbsp; {{number_format($getData->admin_fee, 2, ',', '.')}}</p>
                                    <hr>
                                    <h3 class="m-b-0 m-t-10">Rp. {{number_format($total, 2, ',', '.')}}</h3>
                                </div>
                            @endif
                            
                            @if($getData->status == 0)
                            <div class="hidden-print col-md-12 text-left">
                                <hr>
                                <input type="hidden" value="{{$getData->id}}" name="id_trans" id="id_trans">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/buyer/transactions">Kembali</a>
                                <button type="submit" class="btn btn-danger"  id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                                <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Confirm</button>
                            </div>
                            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="confirmDetail">
                                </div>
                            </div>
                            <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="rejectDetail">
                                </div>
                            </div>
                            @endif
                            @if($getData->status == 1 || $getData->status == 2 || $getData->status == 3)
                                <div class="hidden-print col-md-12 text-left">
                                    <hr>
                                    <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/buyer/transactions">Kembali</a>
                                </div>
                            @endif
                        </div>                                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@if($getData->status == 0)
@section('javascript')

    <script>
           function inputSubmit(){
                var id_trans = $("#id_trans").val();
//                var id_bank = $("#bank_name").val();
                var id_bank = $('input[name=radio]:checked').val(); 
                 $.ajax({
                     type: "GET",
                     url: "{{ URL::to('/') }}/m/add/buyer-transaction?id_trans="+id_trans+"&id_bank="+id_bank,
                     success: function(url){
                         $("#confirmDetail" ).empty();
                         $("#confirmDetail").html(url);
                     }
                 });
           }
           
           function rejectSubmit(){
                var id_trans = $("#id_trans").val();
                 $.ajax({
                     type: "GET",
                     url: "{{ URL::to('/') }}/m/reject/buyer-transaction?id_trans="+id_trans,
                     success: function(url){
                         $("#rejectDetail" ).empty();
                         $("#rejectDetail").html(url);
                     }
                 });
           }

            function confirmSubmit(){
                var dataInput = $("#form-add").serializeArray();
                $('#form-add').submit();
                $('#form-add').remove();
                $('#loading').show();
                $('#tutupModal').remove();
                $('#submit').remove();
            }
    </script>

@stop
@endif