@extends('layout.member.master')
@section('title', 'Detail Transaksi')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                <h5>Invoice Details : <strong class="text-primary">#{{$getData->transaction_code}}</strong></h5>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane in active" id="details" aria-expanded="true">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-6">
                                <p><strong>Seller : </strong>{{$getData->name}} ({{$getData->user_code}})</p>
                                <p><strong>No. HP : </strong>{{$getData->hp}}</p>
                                
                                @if($getData->type == 3)
                                <address>
                                    <strong>Transfer Ke:</strong>
                                    @if($getData->bank_name != null)
                                        <br>
                                        Nama Rekening : <strong>{{$getData->account_name}}</strong>
                                        <br>
                                        Nama Bank: <strong>{{$getData->bank_name}}</strong>
                                        <br>
                                        No. Rekening: <strong>{{$getData->account_no}}</strong>
                                    @endif
                                </address>
                                @endif
                                
                            </div>
                            <?php
                                $status = 'Tuntas';
                                $label = 'success';
                                if($getData->status == 0){
                                    $status = 'Proses Transfer';
                                    $label = 'danger';
                                }
                                if($getData->status == 1){
                                    $status = 'Menunggu Konfirmasi';
                                    $label = 'warning';
                                }
                                if($getData->status == 3){
                                    $status = 'Batal';
                                    $label = 'danger';
                                }
                                $type = 'Order Coin';
                                if($getData->type == 2){
                                    $type = 'Transfer Coin';
                                }
                                if($getData->type == 3){
                                    $type = 'Jual Coin';
                                }
                            ?>
                            <div class="col-md-6 col-sm-6 text-right">
                                <p class="m-b-0"><strong>Tanggal Order: </strong>{{date('d F Y', strtotime($getData->created_at))}}</p>
                                <p class="m-b-0"><strong>Order Status: </strong> <span class="badge badge-{{$label}} m-b-0">{{$status}}</span></p>
                                <p><strong>Type: </strong> <span class="text-primary">#{{$type}}</span></p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead class="bg-faded">
                                            <tr>
                                                <th>#</th>
                                                <th>Jml Order Coin</th>
                                                @if($getData->type == 1)
                                                    <th>Harga (Rp.)</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0; ?>
                                            <?php 
                                            $no++; 
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{number_format($getData->total_coin, 0, ',', '')}}</td>
                                                @if($getData->type == 1)
                                                    <td>{{number_format($getData->price, 0, ',', ',')}}</td>
                                                @endif
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row clearfix">
<!--                            <div class="col-md-6">
                                <h5>Note</h5>
                                <p>Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.</p>
                            </div>-->
                            @if($getData->type == 1)
                            <?php
                                $total = $getData->price + $getData->unique_digit + $getData->admin_fee;
                            ?>
                            <div class="col-md-12 text-right">
                                <p class="m-b-0"><b>Sub-total:</b> &nbsp; {{number_format($getData->price, 0, ',', ',')}}</p>
                                <p class="m-b-0"><b>Fee:</b> &nbsp; {{number_format($getData->admin_fee, 2, ',', '.')}}</p>
                                <p class="m-b-0"><b>Kode Unik:</b> &nbsp; {{number_format($getData->unique_digit, 0, ',', ',')}}</p>
                                <hr>
                                <h3 class="m-b-0 m-t-10">Rp. {{number_format($total, 0, ',', ',')}}</h3>
                            </div>
                            @endif
                            @if($getData->type == 3)
                            <?php
                                $total = $getData->price + $getData->unique_digit - $getData->admin_fee;
                            ?>
                            <div class="col-md-12 text-right">
                                <p class="m-b-0"><b>Sub-total:</b> &nbsp; {{number_format($getData->price, 2, ',', '.')}}</p>
                                <p class="m-b-0"><b>Fee:</b> &nbsp; {{number_format($getData->admin_fee, 2, ',', '.')}}</p>
                                <hr>
                                <h3 class="m-b-0 m-t-10">Rp. {{number_format($total, 2, ',', '.')}}</h3>
                            </div>
                            @endif
                            
                            @if($getData->status == 0)
                            <div class="hidden-print col-md-12 text-left">
                                <hr>
                                <input type="hidden" value="{{$getData->id}}" name="id_trans" id="id_trans">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/seller/transactions">Kembali</a>
                                <button type="submit" class="btn btn-danger"  id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                            </div>
                            <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="rejectDetail">
                                </div>
                            </div>
                            @endif
                            @if($getData->status == 1)
                            <div class="hidden-print col-md-12 text-left">
                                <hr>
                                <input type="hidden" value="{{$getData->id}}" name="id_trans" id="id_trans">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/seller/transactions">Kembali</a>
                                <button type="submit" class="btn btn-danger"  id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                                <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Confirm</button>
                            </div>
                            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="confirmDetail">
                                </div>
                            </div>
                            <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="rejectDetail">
                                </div>
                            </div>
                            @endif
                            @if($getData->status == 2 || $getData->status == 3)
                                <div class="hidden-print col-md-12 text-left">
                                    <hr>
                                    <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/seller/transactions">Kembali</a>
                                </div>
                            @endif
                        </div>                                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('javascript')
    @if($getData->status == 0)
        <script>
               function rejectSubmit(){
                    var id_trans = $("#id_trans").val();
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/reject/seller-transaction?id_trans="+id_trans,
                         success: function(url){
                             $("#rejectDetail" ).empty();
                             $("#rejectDetail").html(url);
                         }
                     });
               }

                function confirmSubmit(){
                    var dataInput = $("#form-add").serializeArray();
                    $('#form-add').submit();
                    $('#form-add').remove();
                    $('#loading').show();
                    $('#tutupModal').remove();
                    $('#submit').remove();
                }
        </script>
    @endif
    @if($getData->status == 1)
        <script>
               function inputSubmit(){
                    var id_trans = $("#id_trans").val();
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/add/seller-transaction?id_trans="+id_trans,
                         success: function(url){
                             $("#confirmDetail" ).empty();
                             $("#confirmDetail").html(url);
                         }
                     });
               }

               function rejectSubmit(){
                    var id_trans = $("#id_trans").val();
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/reject/seller-transaction?id_trans="+id_trans,
                         success: function(url){
                             $("#rejectDetail" ).empty();
                             $("#rejectDetail").html(url);
                         }
                     });
               }

                function confirmSubmit(){
                    var dataInput = $("#form-add").serializeArray();
                    $('#form-add').submit();
                    $('#form-add').remove();
                    $('#loading').show();
                    $('#tutupModal').remove();
                    $('#submit').remove();
                }
        </script>
    @endif
@stop