@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Pin Masuk</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Paket</th>
                                        <th>Jumlah</th>
                                        @if($dataUser->member_type == 2)
                                        <th>Type</th>
                                        @endif
                                        <th>Tgl</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 0; ?>
                                    @if($dataMasuk != null)
                                        @foreach($dataMasuk as $row)
                                        <?php 
                                        $no++; 
                                        $type = 'order';
                                        if($row->type == 2){
                                            $type = 'transfer';
                                        }
                                        ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->qty}}</td>
                                                @if($dataUser->member_type == 2)
                                                <td>{{$type}}</td>
                                                @endif
                                                <td>{{date('d F Y', strtotime($row->created_at))}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Pin Keluar</h5>
                        <span class="d-block m-t-5">Aktifasi</span>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Paket</th>
                                        <th>Username</th>
                                        <th>Tgl</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no1 = 0; ?>
                                    @if($dataKeluarAktifasi != null)
                                        @foreach($dataKeluarAktifasi as $row1)
                                        <?php 
                                        $no1++; 
                                        ?>
                                            <tr>
                                                <td>{{$no1}}</td>
                                                <td>{{$row1->name}}</td>
                                                <td>{{$row1->user_code}}</td>
                                                <td>{{date('d F Y', strtotime($row1->created_at))}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            @if($dataUser->member_type < 3)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Pin Keluar</h5>
                        <span class="d-block m-t-5">Penjualan</span>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>Nama</th>
                                        <th>Jumlah</th>
                                        @if($dataUser->member_type == 2)
                                            <th>Type</th>
                                        @endif
                                        <th>Tgl</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no2 = 0; ?>
                                    @if($dataKeluarPenjualan != null)
                                        @foreach($dataKeluarPenjualan as $row2)
                                        <?php 
                                        $no2++; 
                                         $status = 'proses pembeli';
                                         $color = 'info';
                                        if($row2->status == 1){
                                            $status = 'belum konfirmasi';
                                            $color = 'warning';
                                        }
                                        if($row2->status == 2){
                                            $status = 'tuntas';
                                            $color = 'success';
                                        }
                                        $type1 = 'order';
                                        if($row2->type == 2){
                                            $type1 = 'transfer';
                                        }
                                        ?>
                                            <tr>
                                                <td>{{$no2}}</td>
                                                <td>{{$row2->user_code}}</td>
                                                <td>{{$row2->name}}</td>
                                                <td>{{$row2->qty}}</td>
                                                @if($dataUser->member_type == 2)
                                                <td>{{$type1}}</td>
                                                @endif
                                                <td>{{date('d F Y', strtotime($row2->created_at))}}</td>
                                                <td>
                                                    <span class="badge badge-{{$color}}">{{$status}}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
            
        </div>
    </div>
</div>
@stop