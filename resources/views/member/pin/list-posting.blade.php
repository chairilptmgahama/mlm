@extends('layout.member.master')
@section('title', 'Mining')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        
        <div class="card">
            <div class="header">
                <h2>List Mining Xonecoin</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Total Xonecoin</th>
                                <th>Fase</th>
                                <th>Mining ke</th>
                                <th>Tgl Mulai</th>
                                <th>Status</th>
                                <th>###</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($getData != null)
                                <?php $no = 0; ?>
                                @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $status = 'selesai';
                                        $label = 'success';
                                        if($row->status == 0){
                                            $status = 'berjalan';
                                            $label = 'info';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{number_format($row->total_coin, 4, ',', '.')}}</td>
                                        <td>{{$row->phase}} bulan</td>
                                        <td>{{$row->total_post}}</td>
                                        <td>{{date('d F Y H:i', strtotime($row->created_at))}}</td>
                                        <td><label class="badge badge-{{$label}}">{{$status}}</label></td>
                                        <td>
                                            <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/my/posting/{{$row->id}}">detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop