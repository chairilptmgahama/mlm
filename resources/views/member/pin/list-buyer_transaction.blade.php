@extends('layout.member.master')
@section('title', 'Coin')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        
        <div class="card">
            <div class="header">
                <h2>List Transaksi</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Transaksi</th>
                                <th>Type</th>
                                <th>Tgl</th>
                                <th>Coin</th>
                                <th>Harga (Rp.)</th>
                                <th>Status</th>
                                <th>###</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($getData != null)
                                <?php $no = 0; ?>
                                @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $status = 'batal';
                                        $label = 'danger';
                                        if($row->status == 0){
                                            $status = 'proses transfer';
                                            $label = 'warning';
                                        }
                                        if($row->status == 1){
                                            $status = 'menunggu konfirmasi';
                                            $label = 'info';
                                        }
                                        if($row->status == 2){
                                            $status = 'tuntas';
                                            $label = 'success';
                                        }
                                        $price = number_format(($row->price + $row->admin_fee), 0, ',', '.');
                                        $type = 'Beli';
                                        if($row->type == 2){
                                            $price = '--';
                                            $type = 'Transfer';
                                        }
                                        if($row->type == 3){
                                            $price = number_format(($row->price + $row->admin_fee), 0, ',', '.');
                                            $type = 'Jual';
                                        }
                                        if($row->type == 4){
                                            $price = '--';
                                            $type = 'Posting Start';
                                        }
                                        if($row->type == 5){
                                            $price = '--';
                                            $type = 'Posting Finnish';
                                        }
                                        if($row->type == 6){
                                            $price = '--';
                                            $type = 'Convert Posting';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->transaction_code}}</td>
                                        <td>{{$type}}</td>
                                        <td>{{date('d F Y H:i', strtotime($row->created_at))}}</td>
                                        <td>{{number_format($row->total_coin, 4, ',', '.')}}</td>
                                        <td>{{$price}}</td>
                                        <td><label class="badge badge-{{$label}}">{{$status}}</label></td>
                                        <td>
                                            <a class="text-primary" href="{{ URL::to('/') }}/m/buyer/transaction/{{$row->id}}">detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
