@extends('layout.member.master')
@section('title', 'Mining')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                
                <div class="form-row">
                    
                    <div class="form-group col-md-12">
                        <h3 class="product-title m-b-0">Mining Xonecoin</h3>                                    
                        <hr>
                        <h6 class="vote"><strong>Wallet: </strong> <span class="text-primary"> &nbsp; {{number_format($saldo, 4, ',', '.')}} </span></h6>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Jml Xonecoin : &nbsp;</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nominal</span>
                            </div>
                            <input type="number" class="form-control cekInput" id="cekInput" placeholder="Nominal" aria-label="Nominal" aria-describedby="basic-addon1" min="{{$getData->min_posting}}" max="{{$saldo}}" value="{{$getData->min_posting}}" step="0.01"> <?php // {{$getData->min_posting}} ?>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Fase : &nbsp;</label>
                        <div class="input-group mb-3">
                            <select name="phase" id="single-selection" class="multiselect multiselect-custom">
                                <option value="3">3 Bulan</option>
                                <option value="6">6 Bulan</option>
                                <option value="9">9 Bulan</option>
                                <option value="12">12 Bulan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="action" style="margin-top: 15px;">
                    <button class="btn btn-primary" type="submit" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                </div>
                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('styles')
<link rel="stylesheet" href="/lucid/vendor/bootstrap-multiselect/bootstrap-multiselect.css"/>
@stop
@section('javascript')
<script src="/lucid/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script src="/lucid/vendor/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="/lucid/vendor/jquery.maskedinput/jquery.maskedinput.min.js"></script>
<script src="/lucid/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="/lucid/js/pages/forms/advanced-form-elements.js"></script>
<script>
    $('#cekInput').on('input', function () {
        var value = $(this).val();
        if ((value !== '') && (value.indexOf('.') === -1)) {
            $(this).val(Math.max(Math.min(value, {{$saldo}}), 1));
        }
    });
    
    
    function inputSubmit(){
        $('#confirmSubmit').modal({'backdrop': 'static'});
        var qty = $("#cekInput").val();
        var phase = $("#single-selection").val();
         $.ajax({
             type: "GET",
             url: "{{ URL::to('/') }}/m/cek/posting-pin?total_coin="+qty+"&phase="+phase,
             success: function(url){
                 $("#confirmDetail" ).empty();
                 $("#confirmDetail").html(url);
             }
         });
    }

     function confirmSubmit(){
         var dataInput = $("#form-add").serializeArray();
         $('#form-add').submit();
         $('#form-add').remove();
         $('#loading').show();
         $('#tutupModal').remove();
         $('#submit').remove();
     }
</script>
@stop
