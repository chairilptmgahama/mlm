@extends('layout.member.master')
@section('title', 'Jual')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">
                
                <div class="form-row">
                    
                    <div class="form-group col-md-12">
                        <h3 class="product-title m-b-0">Jual Xonecoin</h3>                                    
                        <hr>
                        <h5 class="price m-t-0">Harga: <span class="text-warning">&nbsp; Rp. {{number_format($getData->sell_price, 0, ',', '.')}}/Xonecoin</span></h5>
                        <h5 class="vote"><strong>Fee: </strong> <span class="text-warning"> &nbsp; {{$getData->sell_fee}}% </span></h5>
                        <h6 class="vote"><strong>Wallet: </strong> <span class="text-primary"> &nbsp; {{number_format($sisa, 4, ',', '.')}} </span></h6>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Jml Xonecoin : &nbsp;</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nominal</span>
                            </div>
                            <input type="number" class="form-control cekInput" id="cekInput" placeholder="Nominal" aria-label="Nominal" aria-describedby="basic-addon1" min="{{$getData->sell_min}}" max="{{$sisa}}" value="{{$getData->sell_min}}" step="0.01">
                        </div>
                    </div>
                    <div class="form-group col-md-3">
                        <label> &nbsp;</label>
                        <div class="action">
                            <button class="btn btn-primary" type="submit" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Jual</button>
                        </div>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="confirmDetail">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

@stop


@section('javascript')
<script>
    $('#cekInput').on('input', function () {
        var value = $(this).val();
        if ((value !== '') && (value.indexOf('.') === -1)) {
            $(this).val(Math.max(Math.min(value, {{$sisa}}), 1));
        }
    });
    
    
    function inputSubmit(){
        $('#confirmSubmit').modal({'backdrop': 'static'});
        var to_id = $("#id_ring_id").val();
        var qty = $("#cekInput").val();
         $.ajax({
             type: "GET",
             url: "{{ URL::to('/') }}/m/cek/jual-pin?total_coin="+qty+"&s={{$sellerID}}&p={{$getData->id}}",
             success: function(url){
                 $("#confirmDetail" ).empty();
                 $("#confirmDetail").html(url);
             }
         });
    }

     function confirmSubmit(){
         var dataInput = $("#form-add").serializeArray();
         $('#form-add').submit();
         $('#form-add').remove();
         $('#loading').show();
         $('#tutupModal').remove();
         $('#submit').remove();
     }
</script>
@stop
