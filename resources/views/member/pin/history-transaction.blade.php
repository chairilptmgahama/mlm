@extends('layout.member.master')
@section('title', 'Transaksi')

@section('content')

<div class="row clearfix">
    <div class="col-md-12">
        
        <div class="card">
            <div class="header">
                <h2>History</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Kode Transaksi</th>
                                <th>Label</th>
                                <th>Bonus</th>
                                <th>Tgl</th>
                                <th>Type</th>
                                <th>Jml Xone</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($getData != null)
                                <?php $no = 0; ?>
                                @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $coin_type = 'Xonecoin Keluar';
                                        $label_coin = 'danger';
                                        $jns_bonus = '--';
                                        if($row->coin_type == 1){
                                            $coin_type = 'Xonecoin Masuk';
                                            $label_coin = 'success';
                                            if($row->bonus_type == 1){
                                                $jns_bonus = '<label class="badge badge-muted">Sponsor '.$row->user_code.'</label>';
                                            }
                                            if($row->bonus_type == 2){
                                                $jns_bonus = '<label class="badge badge-muted">Profit '.$row->user_code.'</label>';
                                            }
                                        }
                                        $trans_type = 'Beli';
                                        if($row->trans_type == 2){
                                            $trans_type = 'Transfer';
                                        }
                                        if($row->trans_type == 3){
                                            $trans_type = 'Jual';
                                        }
                                        if($row->trans_type == 4){
                                            $trans_type = 'Mining';
                                        }
                                        if($row->trans_type == 5){
                                            $trans_type = 'Tuntas Mining';
                                        }
                                        if($row->trans_type == 6){
                                            $trans_type = 'Convert Mining';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->transaction_code}}</td>
                                        <td><label class="badge badge-info">{{$trans_type}}</label></td>
                                        <td><?php echo $jns_bonus; ?></td>
                                        <td>{{date('d F Y H:i', strtotime($row->created_at))}}</td>
                                        <td><label class="badge badge-{{$label_coin}}">{{$coin_type}}</label></td>
                                        <td>{{number_format($row->qty, 4, ',', '.')}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
