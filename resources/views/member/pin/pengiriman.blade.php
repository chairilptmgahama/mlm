@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Kirim Paket</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php
                $sum_pin_masuk = 0;
                $sum_pin_keluar = 0;
                if($dataPin->sum_pin_masuk != null){
                    $sum_pin_masuk = $dataPin->sum_pin_masuk;
                }
                if($dataPin->sum_pin_keluar != null){
                    $sum_pin_keluar = $dataPin->sum_pin_keluar;
                }
                $sum_pin_terpakai = 0;
                if($dataPin->sum_pin_terpakai != null){
                    $sum_pin_terpakai = $dataPin->sum_pin_terpakai;
                }
                $sum_pin_masuk_terima = 0;
                if($dataPin->sum_pin_masuk_terima != null){
                    $sum_pin_masuk_terima = $dataPin->sum_pin_masuk_terima;
                }
                $sum_pin_keluar_terima = 0;
                if($dataPin->sum_pin_keluar_terima != null){
                    $sum_pin_keluar_terima = $dataPin->sum_pin_keluar_terima;
                }
                $sum_pin_keluar_ditransfer = 0;
                if($dataPin->sum_pin_keluar_ditransfer != null){
                    $sum_pin_keluar_ditransfer = $dataPin->sum_pin_keluar_ditransfer;
                }

                $saldo_pin_aktifasi = $sum_pin_masuk - $sum_pin_keluar;
                $saldo_pin_transfer = $sum_pin_masuk - $sum_pin_keluar;
                $saldo_pin_kirim_paket = $sum_pin_masuk - $dataPengiriman->total_pin_proses_dan_tuntas - $sum_pin_keluar_ditransfer - $sum_pin_masuk_terima;
                $pin_paket_diterima = $dataPengiriman->total_pin_tuntas + $sum_pin_masuk_terima - $sum_pin_keluar_terima;
            ?>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="card-box tilebox-one">
                        <i class="icon-rocket pull-xs-right text-pink"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Saldo Pin Kirim Paket</h6>
                        <h2 class="m-b-20">{{$saldo_pin_kirim_paket}}</h2>
                        <p class="text-muted">Total Pin Terima : {{$sum_pin_masuk - $sum_pin_masuk_terima}}</p>
                        <p class="text-muted">Total Pin Terima/Proses Kirim : {{$dataPengiriman->total_pin_proses_dan_tuntas}}</p>
                        <p class="text-muted">Total Pin Ditransfer : {{$sum_pin_keluar_ditransfer}}</p>
                        <p class="text-muted">*Sisa Saldo  yang dapat dipakai untuk pengajuan Kirim Paket </p>
                        <p class="text-muted">*Pengajuan kirim paket termasuk paket yang masih proses pengiriman maupun diterima</p>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="card-box tilebox-one">
                        <i class="icon-basket-loaded pull-xs-right text-info"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Saldo Pin/Paket Diterima</h6>
                        <h2 class="m-b-20">{{$pin_paket_diterima}}</h2>
                        <p class="text-muted">Total Diterima : {{$dataPengiriman->total_pin_tuntas + $sum_pin_masuk_terima}}</p>
                        <p class="text-muted">Total Ditransfer : {{$sum_pin_keluar_terima}}</p>
                        <p class="text-muted">*Total Pin/Paket yang sudah diterima dengan status selesai</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xl-2 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="total_pin">Total Pin</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="total_pin" name="total_pin" autocomplete="off" required="">
                                </fieldset>
                            </div>
                            <div class="col-xl-10 col-xs-12">
                                    <fieldset class="form-group">
                                        <label for="alamat_kirim">Alamat Pengiriman</label>
                                        <textarea class="form-control" id="alamat_kirim" rows="2" name="alamat_kirim" autocomplete="off" required=""></textarea>
                                    </fieldset>
                            </div>
                        </div>
                        <b class="text-muted">Pilih Produk</b>
                        <div class="row">
                            <div class="col-xl-3 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="day_cream">Day Cream</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="day_cream" name="day_cream" autocomplete="off" required="" placeholder="Jml Day Cream">
                                </fieldset>
                            </div>
                            <div class="col-xl-3 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="night_cream">Night Cream</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="night_cream" name="night_cream" autocomplete="off" required="" placeholder="Jml Night Cream">
                                </fieldset>
                            </div>
                            <div class="col-xl-3 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="face_toner">Face Toner</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="face_toner" name="face_toner" autocomplete="off" required="" placeholder="Jml Face Toner">
                                </fieldset>
                            </div>
                            <div class="col-xl-3 col-xs-12">
                                <fieldset class="form-group">
                                    <label for="facial_wash">Facial Wash</label>
                                    <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="facial_wash" name="facial_wash" autocomplete="off" required="" placeholder="Jml Facial Wash">
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-xs-12">
                                <fieldset class="form-group">
                                    <div class="card">
                                    <div class="card-header">
                                        Informasi
                                    </div>
                                    <div class="card-block">
                                        <blockquote class="card-blockquote">
                                            <p style="margin-bottom: 2rem;">
                                                Tambahan Biaya Ongkos Kirim Untuk 1Kg nya berisi maksimal 2 Paket/PIN
                                            </p>
                                            <p style="margin-bottom: 2rem;">
                                                Silahkan Cek Ongkos Kirimnya di <a href="https://melisa.id/cek/ongkir/" target="_blank" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">LINK INI</a>
                                            </p>
                                            <p style="margin-bottom: 2rem;">
                                                Isi Kolom Kecamatan / Kabupaten / Kota Asal.
                                                <br>
                                                <b>Gunung Anyar - Kota Surabaya - Jawa Timur</b>
                                            </p>
                                            <p style="margin-bottom: 2rem;">
                                                Isi Kolom Kecamatan / Kabupaten / Kota tujuan.
                                            </p>
                                            <p style="margin-bottom: 2rem;">
                                                Jika sudah muncul biayanya , silahkan kalkulasi total untuk biaya pengirimannya.
                                            </p>
                                            <p style="margin-bottom: 2rem;">
                                                Biaya Ongkos Kirim Di transfer ke rekening :
                                                <br>
                                                    <b>BCA 2160.779.112</b>
                                                    <br>
                                                    <b>MANDIRI 142.001.771.8460</b>
                                                    <br>
                                                    an. <b>RUDY PRASETYONO</b>
                                            </p>
                                            <p>
                                                Setelah Transfer Biaya Ongkir , Konfirmasi ke
                                                <br>
                                                WhatsApp CS <b><a href="tel:0878 8878 7789" style="text-decoration:none" onmouseover="style='text-decoration:underline'" onmouseout="style='text-decoration:none'">0878 8878 7789</a></b>
                                            </p>
                                        </blockquote>
                                    </div>
                                </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                            </div>
                        </div>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="confirmDetail">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 card-box table-responsive">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tgl. Submit</th>
                                    <th>Tgl. Proses</th>
                                    <th>Total Pin</th>
                                    <th>Alamat Pengiriman</th>
                                    <th>Nama Kurir</th>
                                    <th>No. Resi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getData != null)
                                    <?php $no = 0; ?>
                                    @foreach($getData as $row)
                                        <?php
                                            $no++;
                                            $status = 'Selesai';
                                            $color = 'success';
                                            if($row->status == 0){
                                                $status = 'Proses Pengiriman';
                                                $color = 'info';
                                            }
                                            if($row->status == 2){
                                                $status = 'Batal';
                                                $color = 'danger';
                                            }
                                            $kirim_at = '--';
                                            if($row->kirim_at != null){
                                                $kirim_at = date('d M Y', strtotime($row->created_at));
                                            }
                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{date('d M Y', strtotime($row->created_at))}}</td>
                                            <td>{{$kirim_at}}</td>
                                            <td>{{$row->total_pin}}</td>
                                            <td>{{$row->alamat_kirim}}</td>
                                            <td>{{$row->kurir_name}}</td>
                                            <td>{{$row->no_resi}}</td>
                                            <td>
                                                <label class="label label-{{$color}}">{{$status}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="modal fade" id="activateBank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script>
       function inputSubmit(){
           var total_pin = $("#total_pin").val();
           var alamat_kirim = $("#alamat_kirim").val();
           var day_cream = $("#day_cream").val();
           var night_cream = $("#night_cream").val();
           var face_toner = $("#face_toner").val();
           var facial_wash = $("#facial_wash").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/kirim-paket?total_pin="+total_pin+"&alamat_kirim="+alamat_kirim+"&day_cream="+day_cream+"&night_cream="+night_cream+"&face_toner="+face_toner+"&facial_wash="+facial_wash ,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }

        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('.invalidpaste').on('paste', function (event) {
            if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                event.preventDefault();
            }
        });

</script>
<script type="text/javascript">
    $("#activateBank").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-dialog").load(link.attr("href"));
    });

    function activateSubmit(){
            var dataInput = $("#form-insert").serializeArray();
            $('#form-insert').submit();
        }
</script>
@stop