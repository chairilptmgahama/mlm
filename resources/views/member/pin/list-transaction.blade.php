@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Transaksi</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Kode Transaksi</th>
                                        <th>Tgl</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                        <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                            <?php
                                                $no++;
                                                $status = 'batal';
                                                $label = 'danger';
                                                if($row->status == 0){
                                                    $status = 'proses transfer';
                                                    $label = 'info';
                                                }
                                                if($row->status == 1){
                                                    $status = 'menunggu konfirmasi';
                                                    $label = 'info';
                                                }
                                                if($row->status == 2){
                                                    $status = 'tuntas';
                                                    $label = 'success';
                                                }
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->transaction_code}}</td>
                                                <td>{{date('d F Y', strtotime($row->created_at))}}</td>
                                                <td>{{number_format($row->price, 0, ',', ',')}}</td>
                                                <td><label class="label label-{{$label}}">{{$status}}</label></td>
                                                <td>
                                                    <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/pin/transaction/{{$row->id}}">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
       function inputSubmit(){
           var account_no = $("#account_no").val();
           var bank_name = $("#bank_name").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/add-bank?account_no="+account_no+"&bank_name="+bank_name ,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

</script>
<script type="text/javascript">
    $("#activateBank").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-dialog").load(link.attr("href"));
    });
    
    function activateSubmit(){
            var dataInput = $("#form-insert").serializeArray();
            $('#form-insert').submit();
        }
</script>
@stop