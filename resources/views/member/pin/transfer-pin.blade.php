@extends('layout.member.master')
@section('title', 'Coin')

@section('content')
<?php

$cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
?>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="body">

                <div class="form-row">

                    <div class="form-group col-md-12">
                        <h3 class="product-title m-b-0">Transfer Coin</h3>
                        <hr>
                        <h6 class="vote"><strong>Saldo Coin: </strong> <span class="text-primary"> &nbsp; {{number_format($cekSaldo, 4, ',', '.')}} </span> <small><sup>xone</sup></small></h6>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nama Tujuan</label>
                        <input type="text" class="form-control" id="ring_id" aria-describedby="ring_id" autocomplete="off" placeholder="Min 3 huruf">
                        <input type="hidden" id="id_ring_id">
                        <ul class="typeahead dropdown-menu"  id="ring_id-box" style="max-height: 120px; overflow: auto;border: 1px solid #ddd;width: 98%;margin-left: 5px;top: 77%;"></ul>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Jml Coin : &nbsp;</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Nominal</span>
                            </div>
                            <input type="number" class="form-control cekInput" id="cekInput" placeholder="Nominal" aria-label="Nominal" aria-describedby="basic-addon1" min="1" max="{{$cekSaldo}}" value="1" step="0.01">
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label> &nbsp;</label>
                        <div class="action">
                            <button class="btn btn-primary" type="submit" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Transfer</button>
                        </div>
                        <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" id="confirmDetail">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

@stop


@section('javascript')
<script>

    $('#cekInput').on('input', function () {
        var value = $(this).val();
        if ((value !== '') && (value.indexOf('.') === -1)) {
            $(this).val(Math.max(Math.min(value, {{$cekSaldo}}), 1));
        }
    });

    $(document).ready(function(){
        $("#ring_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/explore-member" + "?name=" + $(this).val() ,
                success: function(data){
                    if(data != null){
                        $("#ring_id-box").show();
                        $("#ring_id-box").html(data);
                    }
                }
            });
        });
    });
    function selectDataUser(val) {
        var valNew = val.split("____");
        $("#ring_id").val(valNew[1]);
        $("#id_ring_id").val(valNew[0]);
        $("#ring_id-box").hide();
    }

    function inputSubmit(){
        $('#confirmSubmit').modal({'backdrop': 'static'});
        var to_id = $("#id_ring_id").val();
        var qty = $("#cekInput").val();
         $.ajax({
             type: "GET",
             url: "{{ URL::to('/') }}/m/cek/transfer-coin?to_id="+to_id+"&qty="+qty,
             success: function(url){
                 $("#confirmDetail" ).empty();
                 $("#confirmDetail").html(url);
             }
         });
    }

     function confirmSubmit(){
         var dataInput = $("#form-add").serializeArray();
         $('#form-add').submit();
         $('#form-add').remove();
         $('#loading').show();
         $('#tutupModal').remove();
         $('#submit').remove();
     }
</script>
@stop



<?php
/*
@section('styles')
<link rel="stylesheet" href="{{ asset('assets/css/cart.css') }}">
@stop
@section('javascript')
<script src="{{ asset('assets/js/jquery.cart.stockist.min.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#smartcart').smartCart();
        });

        @foreach($getData as $row1)
        $('.cekInput{{$row1->id}}').on('input', function () {
            var value = $(this).val();
            if ((value !== '') && (value.indexOf('.') === -1)) {
                $(this).val(Math.max(Math.min(value, {{number_format($row1->total_sisa, 0, ',', '')}}), 1));
            }
        });
        @endforeach
</script>
<script>
    $(document).ready(function(){
        $("#ring_id").keyup(function(){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/explore-stockist" + "?name=" + $(this).val() ,
                success: function(data){
                    if(data != null){
                        $("#ring_id-box").show();
                        $("#ring_id-box").html(data);
                    }
                }
            });
        });
    });
    function selectDataUser(val) {
        var valNew = val.split("____");
        $("#ring_id").val(valNew[1]);
        $("#id_ring_id").val(valNew[0]);
        $("#ring_id-box").hide();
    }

    function inputSubmit(){
        $('#confirmSubmit').modal({'backdrop': 'static'});
        var to_id = $("#id_ring_id").val();
        var cart_list = $("#cart_list").val();
         $.ajax({
             type: "GET",
             url: "{{ URL::to('/') }}/m/cek/transfer-pin?to_id="+to_id+"&cart_list="+cart_list,
             success: function(url){
                 $("#confirmDetail" ).empty();
                 $("#confirmDetail").html(url);
             }
         });
    }

     function confirmSubmit(){
         var dataInput = $("#form-add").serializeArray();
         $('#form-add').submit();
         $('#form-add').remove();
         $('#loading').show();
         $('#tutupModal').remove();
         $('#submit').remove();
     }
</script>
@stop
 *
 */
?>