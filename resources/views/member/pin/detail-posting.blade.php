@extends('layout.member.master')
@section('title', 'Mining')

@section('content')

<div class="row clearfix">
    
    <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2>Detail Mining
                    @if($getData->status == 0)
                        @if($is_finnish == false)
                            @if($cekLastCoin24 == null)
                            <button type="submit" class="btn btn-primary float-right" id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">
                                Submit Mining
                            </button>
                            @endif
                            @if($cekLastCoin24 != null)
                            <button type="button" class="btn btn-primary float-right" disabled="disabled"><i class="fa fa-refresh fa-spin"></i> &nbsp; <span class="text-lowercase" id="countdown"></span></button>
                            @endif
                        @endif
                        @if($is_finnish == true)
                        <button type="submit" class="btn btn-primary float-right" id="submitBtn" data-toggle="modal" data-target="#confirmTuntas" onClick="inputTuntas()">
                            Submit Tuntas
                        </button>
                        @endif
                    @endif
                    @if($getData->status == 1)
                        <button type="button" class="btn btn-primary float-right" disabled="disabled"><i class="icon-power"></i> &nbsp; Tuntas</button>
                    @endif
                </h2>
<!--                <ul class="header-dropdown">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another Action</a></li>
                            <li><a href="javascript:void(0);">Something else</a></li>
                        </ul>
                    </li>
                </ul>-->
            </div>
            <?php
            $getPost = floor($getData->total_post / $phaseHari * 100);
            $dateStart = $getData->created_at;
            $dateNow = date('Y-m-d H:i:s');
            $diff = abs(strtotime($dateNow) - strtotime($dateStart));  
            $years = floor($diff / (365*60*60*24));  
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
            $days = floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24));
            $sisa = floor($days / $phaseHari * 100);
            $hariKe = ((int) $days) + 1;
            $saldo_coin_bonus = $totalGetCoin->coin_masuk - $totalGetCoin->coin_keluar;
            ?>
            <div class="body">
                <p>Lakukan Mining coin setiap hari untuk mendapatkan bonus berjalan.</p>
                <p>Tgl. mulai {{date('d F Y H:i', strtotime($getData->created_at))}}</p>
                <span class="text-primary">Progress Submit Posting</span>
                <div class="progress progress-xs progress-transparent custom-color-blue" style="height: 15px;">
                    <div class="progress-bar" data-transitiongoal="{{$getPost}}"></div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p>Mining Ke: {{$getData->total_post}}</p>
                        <p>Hari Ke: {{$hariKe}} dari {{$phaseHari * $getData->phase}} hari</p>
                        <h6>Mining: {{number_format($getData->total_coin, 4, ',', '.')}} <sup>xone</sup></h6>
                        <h6>
                            Bonus: {{number_format($saldo_coin_bonus, 4, ',', '.')}} <sup>xone</sup> 
                            &nbsp;&nbsp;
                            <button type="button" class="btn btn-sm btn-outline-success" style="padding: 0.1rem 0.4rem;" id="submitBtn" data-toggle="modal" data-target="#confirmClaim" onClick="inputClaim()">
                                <span class="text-lowercase">Convert to Wallet</span>
                            </button>
                        </h6>
                    </div>
                    
<!--                    <div class="col-5">
                        <div class="sparkline text-right m-t-10" data-type="bar" data-width="97%" data-height="26px" data-bar-Width="2" data-bar-Spacing="7" data-bar-Color="#7460ee">
                            0,3,1,4,56
                        </div>
                    </div>-->
                </div>
            </div>
            @if($cekLastCoin24 == null)
            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" id="confirmDetail">
                </div>
            </div>
            @endif
            <div class="modal fade" id="confirmClaim" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" id="claimDetail">
                </div>
            </div>
            @if($is_finnish == true)
            <div class="modal fade" id="confirmTuntas" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" id="tuntasDetail">
                </div>
            </div>
            @endif
        </div>
    </div>
    
    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12">
        
        <div class="card">
            <div class="header">
                <h2>List Mining</h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Tgl</th>
                                <th>Jml</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($allCoin != null)
                                <?php $no = 0; ?>
                                @foreach($allCoin as $row)
                                    <?php
                                        $no++;
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{date('d F Y H:i', strtotime($row->active_at))}}</td>
                                        <td>{{number_format($row->qty, 4, ',', '.')}}</td>
                                        <td>{{number_format($row->persentase, 2, ',', '.')}}%</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
</div>

@stop

@section('styles')
<link rel="stylesheet" href="/lucid/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"/>
@stop

@section('javascript')
<script>
    @if($getData->status == 0)
        @if($cekLastCoin24 == null)
           function inputSubmit(){
                $.ajax({
                    type: "GET",
                    url: "{{ URL::to('/') }}/m/cek/add-posting?id={{$getData->id}}",
                    success: function(url){
                        $("#confirmDetail" ).empty();
                        $("#confirmDetail").html(url);
                    }
                });
            }
        @endif
        
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#form-add').remove();
            $('#loading').show();
            $('#tutupModal').remove();
            $('#submit').remove();
        }
    @endif
        
    function inputClaim(){
        $.ajax({
            type: "GET",
            url: "{{ URL::to('/') }}/m/cek/claim-posting?id={{$getData->id}}",
            success: function(url){
                $("#claimDetail" ).empty();
                $("#claimDetail").html(url);
            }
        });
    }
        
    @if($getData->status == 0)
        @if($is_finnish == true)
            function inputTuntas(){
                $.ajax({
                    type: "GET",
                    url: "{{ URL::to('/') }}/m/cek/finnish-posting?id={{$getData->id}}",
                    success: function(url){
                        $("#claimDetail" ).empty();
                        $("#claimDetail").html(url);
                    }
                });
            }
        @endif
    @endif
</script>
<script type="text/javascript">
    $('.progress .progress-bar').progressbar({
            display_text: 'none'
    });
</script>

@if($getData->status == 0)
    @if($cekLastCoin24 != null)
    <?php 
        $next24Hour = date('M d, Y H:i:s', strtotime('+24 hours', strtotime($cekLastCoin24->active_at)));
    ?>
    <script>
        var countDownDate = new Date("{{$next24Hour}}").getTime();
        var x = setInterval(function() {
          var now = new Date().getTime();
          var distance = countDownDate - now;
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          document.getElementById("countdown").innerHTML = hours + " jam "  + minutes + " menit " + seconds + " detik ";
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "Selesai";
          }
        }, 1000);
    </script>
    @endif
@endif

@stop