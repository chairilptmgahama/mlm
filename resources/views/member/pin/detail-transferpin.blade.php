@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5></h5>
                    </div>
                    <div class="card-body">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="clearfix">
                            <div class="pull-left">
                                <h5>Invoice # <br>
                                    <small>{{$getData->transaction_code}}</small>
                                </h5>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="pull-xs-left">
                                    <?php
                                            $seller = $getData->user_code;
                                            $hp_seller = $getData->hp;
                                            $nama = 'Pengirim';
                                            if($type == 2){
                                                $nama = 'Penerima';
                                            }
                                    ?>
                                    <p><strong>{{$nama}} : </strong>{{$seller}}</p>
                                    <p><strong>No. HP : </strong>{{$hp_seller}}</p>
                                </div>
                                <?php
                                    $status = 'Tuntas';
                                    $label = 'success';
                                    if($getData->status == 0){
                                        $status = 'Konfirmasi';
                                        $label = 'info';
                                    }
                                    if($getData->status == 3){
                                        $status = 'Batal';
                                        $label = 'danger';
                                    }
                                ?>
                                <div class="pull-xs-right m-t-30">
                                    <p><strong>Tanggal Order: </strong>{{date('d F Y', strtotime($getData->created_at))}}</p>
                                    <p class="m-t-10"><strong>Order Status: </strong> <span class="badge badge-{{$label}}">{{$status}}</span></p>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->

                        <div class="m-h-50"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table m-t-30">
                                        <thead class="bg-faded">
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Paket</th>
                                                <th>Jml Transfer Pin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0; ?>
                                            @foreach($getPackagePin as $row)
                                            <?php 
                                            $no++; 
                                            ?>
                                            <tr>
                                                <td>{{$no}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->qty}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @if($getData->status == 0)
                        <div class="hidden-print">
                            <div class="pull-xs-right">
                                <input type="hidden" value="{{$getData->id}}" name="id_trans" id="id_trans">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/transfer/pin">Kembali</a>
                                <button type="submit" class="btn btn-danger"  id="submitBtn" data-toggle="modal" data-target="#rejectSubmit" onClick="rejectSubmit()">Batal</button>
                                @if($type == 1)
                                <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Confirm</button>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                            <div class="modal fade" id="rejectSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="rejectDetail">
                                </div>
                            </div>
                            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="confirmDetail">
                                </div>
                            </div>
                        </div>
                        @endif
                        
                        @if($getData->status == 2 || $getData->status == 3)
                        <div class="hidden-print">
                            <div class="pull-xs-right">
                                <a  class="btn btn-dark" href="{{ URL::to('/') }}/m/list/transfer/pin">Kembali</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    
    @if($getData->status == 0)
        <script>
                @if($type == 1)
               function inputSubmit(){
                    var id_trans = $("#id_trans").val();
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/add/transferpin?id_trans="+id_trans,
                         success: function(url){
                             $("#confirmDetail" ).empty();
                             $("#confirmDetail").html(url);
                         }
                     });
               }
               @endif
               function rejectSubmit(){
                    var id_trans = $("#id_trans").val();
                     $.ajax({
                         type: "GET",
                         url: "{{ URL::to('/') }}/m/reject/transferpin?id_trans="+id_trans,
                         success: function(url){
                             $("#rejectDetail" ).empty();
                             $("#rejectDetail").html(url);
                         }
                     });
               }

                function confirmSubmit(){
                    var dataInput = $("#form-add").serializeArray();
                    $('#form-add').submit();
                    $('#form-add').remove();
                    $('#loading').show();
                    $('#tutupModal').remove();
                    $('#submit').remove();
                }
        </script>
    @endif
@stop