@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            @if($dataUser->member_type == 2)
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Transfer Pin Masuk</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Paket</th>
                                        <th>Qty</th>
                                        <th>Tgl</th>
                                        <th>Status</th>
                                        <th>##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no1 = 0; ?>
                                    @if($getAllTransferMasuk != null)
                                        @foreach($getAllTransferMasuk as $row1)
                                        <?php 
                                            $no1++; 
                                            $status = 'konfirmasi';
                                            $label = 'info';
                                            if($row1->status == 2){
                                                $status = 'tuntas';
                                                $label = 'success';
                                            }
                                            if($row1->status == 3){
                                                $status = 'batal';
                                                $label = 'danger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no1}}</td>
                                                <td>{{$row1->name}} ({{$row1->user_code}})</td>
                                                <td>{{$row1->package_name}}</td>
                                                <td>{{$row1->qty}}</td>
                                                <td>{{date('d F Y', strtotime($row1->created_at))}}</td>
                                                <td><label class="badge badge-{{$label}}">{{$status}}</label></td>
                                                <td>
                                                    <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/detail/transfer/pin/{{$row1->id}}/1">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Transfer Pin Keluar</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Paket</th>
                                        <th>Qty</th>
                                        <th>Tgl</th>
                                        <th>Status</th>
                                        <th>##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no2 = 0; ?>
                                    @if($getAllTransferKeluar != null)
                                        @foreach($getAllTransferKeluar as $row2)
                                        <?php 
                                            $no2++; 
                                            $status = 'menunggu konfirmasi';
                                            $label = 'info';
                                            if($row2->status == 2){
                                                $status = 'tuntas';
                                                $label = 'success';
                                            }
                                            if($row2->status == 3){
                                                $status = 'batal';
                                                $label = 'danger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no2}}</td>
                                                <td>{{$row2->name}} ({{$row2->user_code}})</td>
                                                <td>{{$row2->package_name}}</td>
                                                <td>{{$row2->qty}}</td>
                                                <td>{{date('d F Y', strtotime($row2->created_at))}}</td>
                                                <td><label class="badge badge-{{$label}}">{{$status}}</label></td>
                                                <td>
                                                    <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/detail/transfer/pin/{{$row2->id}}/2">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
            @if($dataUser->member_type == 3)
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Transfer Pin Masuk</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Paket</th>
                                        <th>Qty</th>
                                        <th>Tgl</th>
                                        <th>Status</th>
                                        <th>##</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no1 = 0; ?>
                                    @if($getAllTransferMasuk != null)
                                        @foreach($getAllTransferMasuk as $row1)
                                        <?php 
                                            $no1++; 
                                            $status = 'konfirmasi';
                                            $label = 'info';
                                            if($row1->status == 2){
                                                $status = 'tuntas';
                                                $label = 'success';
                                            }
                                            if($row1->status == 3){
                                                $status = 'batal';
                                                $label = 'danger';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{$no1}}</td>
                                                <td>{{$row1->name}} ({{$row1->user_code}})</td>
                                                <td>{{$row1->package_name}}</td>
                                                <td>{{$row1->qty}}</td>
                                                <td>{{date('d F Y', strtotime($row1->created_at))}}</td>
                                                <td><label class="badge badge-{{$label}}">{{$status}}</label></td>
                                                <td>
                                                    <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/detail/transfer/pin/{{$row1->id}}/1">detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            
        </div>
    </div>
</div>
@stop