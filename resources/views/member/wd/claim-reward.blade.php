@extends('layout.member.main')

@section('content')
@include('layout.member.sidebar')

    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Claim Reward</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-share pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Jml Reward Kiri</h6>
                            <h3 class="m-b-20">{{$dataKaKi->kiri}}</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-share pull-xs-right text-muted text-success"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Jml Reward Kanan</h6>
                            <h3 class="m-b-20">{{$dataKaKi->kanan}}</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 card-box table-responsive">
                        <h5 class="card-text m-b-30">Ajukan Claim Reward anda disini</h5>
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                     <th>Total Kiri</th>
                                    <th>Total Kanan</th>
                                    <th>Reward Detail</th>
                                    <th>Nominal Reward</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getData != null)
                                    <?php $no = 0; ?>
                                    @foreach($getData as $row)
                                    <?php $no++; ?>
                                    @if($row->cek_claim == null)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->total_kiri}}</td>
                                        <td>{{$row->total_kanan}}</td>
                                        <td>{{$row->reward_detail}}</td>
                                        <td>{{number_format($row->reward_price, 0, ',', '.')}}</td>
                                        <td>
                                            <?php // @if($dataKaKi->kiri == $row->total_kiri && $dataKaKi->kanan == $row->total_kanan) ?>
                                            @if($dataKaKi->kiri >= $row->total_kiri && $dataKaKi->kanan >= $row->total_kanan)
                                                <a rel="tooltip"  data-toggle="modal" data-target="#confirmSubmit"  href="{{ URL::to('/') }}/m/cek/claim-reward/{{$row->id}}" class="btn btn-info waves-effect waves-light btn-sm">claim</a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('layout.member.footer')
@stop

@section('styles')
<link href="{{ asset('asset_member/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script src="/asset_member/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/asset_member/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script>
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

        $("#confirmSubmit").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-dialog").load(link.attr("href"));
        });

        $(document).ready(function() {
            $('#datatable').DataTable();
            var table = $('#datatable-buttons').DataTable({
                lengthChange: false,
            });
        } );
</script>
@stop