@extends('layout.member.main')

@section('content')
@include('layout.member.sidebar')

    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">WD Safra Poin</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <p class="card-text">Ajukan withdraw anda disini</p>
                                <div class="row">
                                    <div class="col-xl-8 col-xs-12">
                                        <fieldset class="form-group">
                                            <label for="input_jml">Jumlah (Rp.)</label>
                                            <input type="text" class="form-control allownumericwithoutdecimal invalidpaste" id="input_jml" name="jml_wd" autocomplete="off" placeholder="Minimum Withdraw Rp. 50.000">
                                        </fieldset>
                                    </div>
                                    <div class="col-xl-4 col-xs-12">
                                        <fieldset class="form-group">
                                            <label>Admin Fee</label>
                                            <input type="text" class="form-control" disabled="" value="Rp. {{number_format($dataAll->newFee, 0, ',', '.')}}">
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-6">
                                        <button type="submit" class="btn btn-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" onClick="inputSubmit()">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-trophy pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus</h6>
                            <h3 class="m-b-20">{{number_format($dataAll->total_bonus, 0, ',', '.')}}</h3>
                            <h6 class="text-primary">Sisa Saldo : <span class="text-muted">{{number_format($dataAll->saldo, 0, ',', '.')}}</span></h6>
                        </div>
                    </div>
                    <?php  $wd_tuntas = $dataAll->total_wd + $dataAll->fee_tuntas; ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-wallet pull-xs-right text-muted text-success"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Ditransfer</h6>
                            <h3 class="m-b-20">{{number_format($wd_tuntas, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                    <?php  $wd_proses = $dataAll->total_tunda + $dataAll->fee_tunda; ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-rocket pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Diproses</h6>
                            <h3 class="m-b-20">{{number_format($wd_proses, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-basket-loaded pull-xs-right text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Belanja Safra (Sp)</h6>
                            <h3 class="m-b-20">{{number_format($dataAll->belanjaSafra, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="confirmDetail">
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('layout.member.footer')
@stop

@section('javascript')
<script>
       function inputSubmit(){
           var input_jml_wd = $("#input_jml").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/m/cek/safra-poin/confirm-wd?input_jml_wd="+input_jml_wd,
                success: function(url){
                    $("#confirmDetail" ).empty();
                    $("#confirmDetail").html(url);
                }
            });
        }

        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#tutupModal').remove();
            $('#submit').remove();
        }

        $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('.invalidpaste').on('paste', function (event) {
            if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
                event.preventDefault();
            }
        });

</script>
@stop