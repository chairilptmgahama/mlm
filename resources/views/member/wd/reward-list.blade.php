@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Reward Info</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 card-box table-responsive">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tgl. Claim</th>
                                    <th>Kiri</th>
                                    <th>Kanan</th>
                                    <th>Reward</th>
                                    <th>Alasan </th>
                                    <th>Status </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getData != null)
                                <?php $no = 0; ?>
                                    @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $status = 'proses admin';
                                        $label = 'info';
                                        if($row->status == 1){
                                            $status = 'tuntas';
                                            $label = 'success';
                                        }
                                        if($row->status == 2){
                                            $status = 'reject';
                                            $label = 'danger';
                                        }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{date('d M Y', strtotime($row->claim_date))}}</td>
                                        <td>{{$row->total_kiri}}</td>
                                        <td>{{$row->total_kanan}}</td>
                                        <td>
                                            {{$row->reward_detail}}
                                            <br>
                                            ({{number_format($row->reward_price, 0, ',', '.')}})
                                        </td>
                                        <td>{{$row->reason}}</td>
                                        <td>
                                            <label class="label label-{{$label}}">{{$status}}</label>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop