@extends('layout.member.master')
@section('title', 'Dashboard')

<?php
$member_type = 'Member';
if($dataUser->member_type== 1){
    $member_type = 'Exchanger';
}
$cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
?>

@section('sidebar-user')
<hr>
<ul class="row list-unstyled">
    <li class="col-6">
        <small>Saldo Coin</small>
        <h6>{{ number_format($cekSaldo, 4, ',', '.') }} <small><sup>xone</sup></small></h6>
    </li>
    <li class="col-6">
        <small>Tot. Coin Keluar</small>
        <h6>{{ number_format($cekCoinTersedia->coin_keluar, 4, ',', '.') }} <small><sup>xone</sup></small></h6>
    </li>
</ul>
@stop

@section('content')
<div class="row clearfix">
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">

                <div class="icon text-warning"><i class="fa fa-user-circle"></i> </div>
                <div class="content">
                    <div class="text">Type Akun</div>
                    <h5 class="number">{{$member_type}}</h5>
                </div>
                <hr>
                <!--
                <div class="icon text-success"><i class="icon-user-following"></i> </div>
                <div class="content">
                    <div class="text">Tgl. Aktif</div>
                    <h6 class="number">{{date('d M Y H:i', strtotime($dataUser->created_at))}}</h6>
                </div>
                -->
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-warning"><i class="icon-wallet"></i> </div>
                <div class="content">
                    <div class="text">Saldo Coin</div>
                    <h5 class="number">{{number_format($cekSaldo, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
                <hr>
                <!--
                <div class="icon text-info"><i class="fa fa-exchange"></i> </div>
                <div class="content">
                    <div class="text">Total Coin Keluar</div>
                    <h5 class="number">{{number_format($cekCoinTersedia->coin_keluar, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
                -->
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-success"><i class="icon-user-following"></i> </div>
                <div class="content">
                    <div class="text">Tgl. Aktif</div>
                    <h6 class="number">{{date('d M Y H:i', strtotime($dataUser->active_at))}}</h6>
                </div>
                <hr>
                <!--
                <div class="icon text-success"><i class="icon-user-following"></i> </div>
                <div class="content">
                    <div class="text">Tgl. Aktif</div>
                    <h6 class="number">{{date('d M Y H:i', strtotime($dataUser->active_at))}}</h6>
                </div>
                -->
            </div>
        </div>
    </div>

</div>


</div>

@stop
