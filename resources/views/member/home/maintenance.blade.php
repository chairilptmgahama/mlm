@extends('layout.member.auth')
@section('title', 'Maintenance')

@section('content')

<div class="vertical-align-wrap">
    <div class="vertical-align-middle maintenance">

    <div class="text-center">
        <article>
            <h1>We&rsquo;ll be back soon!</h1>
            <div>
                <p>Masih dalam perbaikan/pengembangan</p>
                <p>&mdash; The Team</p>
            </div>
        </article>
        <div class="margin-top-30">
            <a href="javascript:history.go(-1)" class="btn btn-default"><i class="fa fa-arrow-left"></i> <span>Go Back</span></a>
            <a href="{{ URL::to('/') }}/m/dashboard" class="btn btn-info"><i class="fa fa-home"></i> <span>Home</span></a>
        </div>
    </div>
    </div>
</div>

@stop

