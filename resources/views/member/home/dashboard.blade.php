@extends('layout.member.master')
@section('title', 'Dashboard')

<?php
$member_type = 'Member';
if($dataUser->member_type== 1){
    $member_type = 'Exchanger';
}
$active_at = '--';
if($dataUser->active_at != null){
    $active_at = date('d M Y H:i', strtotime($dataUser->active_at));
}
$color_active = 'warning';
if($dataUser->is_active == 1){
    $color_active = 'success';
}
$cekSaldo = $cekCoinTersedia->coin_masuk - $cekCoinTersedia->coin_keluar - $cekCoinTersedia->coin_blm_tuntas;
$totalSpAktif = $dataUser->total_sponsor - $totalSponsotBlmAktif;
$saldoCoinPosting = $totalBonusPosting->coin_masuk - $totalBonusPosting->coin_keluar;
$coinPosting = $coinPosting->coin_posting + $saldoCoinPosting;

$saldoGlobalCoinPosting = $getCoinBonusMining->coin_masuk - $getCoinBonusMining->coin_keluar;
$getGlobalCoinMining = $getCoinMining->coin_posting + $saldoGlobalCoinPosting;

$cekSaldoGlobal = $getCoinGlobalBebas->coin_masuk - $getCoinGlobalBebas->coin_keluar - $getCoinGlobalBebas->coin_blm_tuntas;
?>

@section('sidebar-user')
<hr>
<ul class="row list-unstyled">
    <li class="col-6">
        <small>Saldo Coin</small>
        <h6>{{ number_format($cekSaldo, 4, ',', '.') }} <small><sup>xone</sup></small></h6>
    </li>
    <li class="col-6">
        <small>Saldo Posting</small>
        <h6>{{ number_format($coinPosting, 4, ',', '.') }} <small><sup>xone</sup></small></h6>
    </li>
</ul>
@stop

@section('content')
<div class="row clearfix">
    
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-success"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="width: 100%;"> </div>
                <div class="content">
                    <div class="text">Balance Coin Supply</div>
                    <h5 class="number">{{number_format($getBalanceCoin->sum_coin_balance, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-success"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="width: 100%;"> </div>
                <div class="content">
                    <div class="text">Circulating Supply</div>
                    <h5 class="number">{{number_format($cekSaldoGlobal, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-4 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-success"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="width: 100%;"> </div>
                <div class="content">
                    <div class="text">Mining Supply</div>
                    <h5 class="number">{{number_format($getGlobalCoinMining, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="col-lg-3 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <!--
                <div class="icon text-info"><i class="fa fa-user"></i> </div>
                <div class="content">
                    <div class="text">Total Supply</div>
                    <h5 class="number">530</h5>
                </div>
                <hr>
                -->
                <div class="icon text-{{$color_active}}"><i class=" icon-badge"></i> </div>
                <div class="content">
                    <div class="text">Type Akun</div>
                    <h5 class="number">{{$member_type}}</h5>
                </div>
                <hr>
                <div class="icon text-info"><i class="fa fa-code-fork"></i> </div>
                <div class="content">
                    <div class="text">Sponsor</div>
                    <h5 class="number">{{$sp_name}}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-success"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="width: 100%;"> </div>
                <div class="content">
                    <div class="text">Saldo Coin</div>
                    <h5 class="number">{{number_format($cekSaldo, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
                <hr>
                <div class="icon"><img src="{{url('/')}}/image/ico_logo_xone.png" alt="Xone Coin" style="width: 100%;filter: grayscale(1);"> </div>
                <div class="content">
                    <div class="text">Saldo Mining</div>
                    <h5 class="number">{{number_format($coinPosting, 4, ',', '.')}} <small><sup>xone</sup></small></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <div class="icon text-warning"><i class="icon-user-following"></i> </div>
                <div class="content">
                    <div class="text">Tgl. Join</div>
                    <h6 class="number">{{date('d M Y H:i', strtotime($dataUser->created_at))}}</h6>
                </div>
                <hr>
                <div class="icon text-success"><i class="icon-user-following"></i> </div>
                <div class="content">
                    <div class="text">Tgl. Aktif</div>
                    <h6 class="number">{{$active_at}}</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="card top_counter">
            <div class="body">
                <!--
                <div class="icon"><i class="fa fa-btc"></i> </div>
                <div class="content">
                    <div class="text">Deposit</div>
                    <h5 class="number">28</h5>
                </div>
                <hr>
                -->
                <div class="icon text-success"><i class="icon-users"></i> </div>
                <div class="content">
                    <div class="text">Sponsor Aktif</div>
                    <h5 class="number">{{$totalSpAktif}}</h5>
                </div>
                <hr>
                <div class="icon text-danger"><i class="icon-users"></i> </div>
                <div class="content">
                    <div class="text">Sponsor Blm Aktif</div>
                    <h5 class="number">{{$totalSponsotBlmAktif}}</h5>
                </div>
            </div>
        </div>
    </div>
</div>



@stop
