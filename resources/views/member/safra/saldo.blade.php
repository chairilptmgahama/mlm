@extends('layout.member.main')

@section('content')
@include('layout.member.sidebar')

    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">History Safra Poin</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-trophy pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus</h6>
                            <h3 class="m-b-20">{{number_format($dataAll->total_bonus, 0, ',', '.')}}</h3>
                            <h6 class="text-primary">Sisa Saldo : <span class="text-muted">{{number_format($dataAll->saldo, 0, ',', '.')}}</span></h6>
                        </div>
                    </div>
                    <?php  $wd_tuntas = $dataAll->total_wd + $dataAll->fee_tuntas; ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-wallet pull-xs-right text-muted text-success"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Ditransfer</h6>
                            <h3 class="m-b-20">{{number_format($wd_tuntas, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                    <?php  $wd_proses = $dataAll->total_tunda + $dataAll->fee_tunda; ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-rocket pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Diproses</h6>
                            <h3 class="m-b-20">{{number_format($wd_proses, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-basket-loaded pull-xs-right text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Belanja Safra (Sp)</h6>
                            <h3 class="m-b-20">{{number_format($dataAll->belanjaSafra, 0, ',', '.')}}</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 card-box table-responsive">
                            @if ( Session::has('message') )
                                <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    {{  Session::get('message')    }}
                                </div>
                            @endif
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jml Safra (Sp.)</th>
                                        <th>Tgl</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData2 != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData2 as $row)
                                        <?php
                                            $no++;
                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{number_format($row->bonus_price, 0, ',', ',')}}</td>
                                            <td>{{date('d F Y', strtotime($row->bonus_date))}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('layout.member.footer')
@stop

@section('styles')
<link href="{{ asset('asset_member/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script src="/asset_member/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/asset_member/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
        });
    } );

</script>
@stop