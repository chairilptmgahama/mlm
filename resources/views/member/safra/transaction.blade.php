@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Shoping Safra</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 card-box table-responsive">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                {{  Session::get('message')    }}
                            </div>
                        @endif
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kode</th>
                                    <th>Harga</th>
                                    <th>Quantity</th>
                                    <th>Pembayaran</th>
                                    <th>Potongan</th>
                                    <th>Tgl</th>
                                    <th>Status</th>
                                    <th>###</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($getData != null)
                                <?php $no = 0; ?>
                                    @foreach($getData as $row)
                                    <?php
                                        $no++;
                                        $total = $row->safra_total + $row->transfer_total;
                                        $status = 'batal';
                                    $label = 'danger';
                                    if($row->status == 0){
                                        $status = 'proses transfer';
                                        $label = 'info';
                                    }
                                    if($row->status == 1){
                                        $status = 'menunggu konfirmasi';
                                        $label = 'info';
                                    }
                                    if($row->status == 2){
                                        $status = 'tuntas';
                                        $label = 'success';
                                    }
                                    ?>
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$row->safra_code}}</td>
                                        <td>{{number_format($total, 0, ',', '.')}}</td>
                                        <td>{{$row->total_buy}}</td>
                                        <td>{{number_format($row->transfer_total, 0, ',', '.')}}</td>
                                        <td>{{number_format($row->safra_total, 0, ',', '.')}}</td>
                                        <td><?php  echo (date('d M Y', strtotime($row->created_at))) ?></td>
                                        <td><label class="label label-{{$label}}">{{$status}}</label></td>
                                        <td class="td-actions text-left" >
                                            <div class="table-icons">
                                                <a rel="tooltip" title="View" class="text-primary" href="{{ URL::to('/') }}/m/safra/transaction/{{$row->id}}">detail</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop

@section('styles')
<link href="{{ asset('asset_member/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('asset_member/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script src="/asset_member/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="/asset_member/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/asset_member/plugins/datatables/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
        });

        table.buttons().container()
                .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );

</script>
@stop
