@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Bonus PHU</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jml Bonus (Rp.)</th>
<!--                                        <th>UserID</th>-->
                                        <th>Tgl</th>
                                        <th>Jam</th>
                                        <!--<th>Level PHU</th>-->
                                        <th>Claim</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php  $no++; ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{number_format($row->bonus_price, 0, ',', '.')}}</td>
                                            <!--<td>{{$row->user_code}}</td>-->
                                            <td>{{date('d F Y', strtotime($row->created_at))}}</td>
                                            <td>{{date('H:i', strtotime($row->created_at))}}</td>
                                            <!--<td>{{$row->fly_id}}</td>-->
                                            <td>
                                                @if($row->is_phu_claim == 0)
                                                    <a type="submit" class="badge badge-primary"  id="submitBtn" data-toggle="modal" data-target="#confirmSubmit" href="{{ URL::to('/') }}/m/cek/claim/bonus-phu?b_id={{$row->id}}">Claim</a>
                                                @endif
                                                @if($row->is_phu_claim == 1)
                                                    <a type="submit" class="badge badge-success"  id="submitBtn" href="#">tuntas</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="confirmDetail">
                                    <div class="modal-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script type="text/javascript">
    $("#confirmSubmit").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });

    function confirmSubmit(){
        var dataInput = $("#form-add").serializeArray();
        $('#form-add').submit();
        $('#form-add').remove();
        $('#loading').show();
        $('#tutupModal').remove();
        $('#submit').remove();
    }
</script>
@stop