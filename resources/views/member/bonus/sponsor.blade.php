@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Bonus Sponsor</h5>
                    </div>
                    <div class="card-body table-border-style">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Jml Bonus (Rp.)</th>
                                        <th>UserID</th>
                                        <th>Tgl</th>
                                        <th>Jam</th>
                                        <th>Paket</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($getData != null)
                                    <?php $no = 0; ?>
                                        @foreach($getData as $row)
                                        <?php  $no++; ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{number_format($row->bonus_price, 0, ',', '.')}}</td>
                                            <td>{{$row->user_code}}</td>
                                            <td>{{date('d F Y', strtotime($row->created_at))}}</td>
                                            <td>{{date('H:i', strtotime($row->created_at))}}</td>
                                            <td>{{$row->package_name}}</td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop