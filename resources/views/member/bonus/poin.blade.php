@extends('layout.member.main')
@section('content')
<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>
@include('layout.member.sidebar')
@include('layout.member.header')

<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="row">
            <?php
            $saldoPoin = $totBonusPoin - $totWDPoin;
            ?>
            <div class="col-md-6 col-xl-4">
                <div class="card bg-c-yellow order-card">
                    <div class="card-body bg-patern">
                        <h6 class="text-white">Saldo Poin</h6>
                        <h2 class="text-right text-white"><i class="feather float-left"></i><span>{{number_format($saldoPoin, 0, ',', '.')}}</span></h2>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card bg-c-green order-card">
                    <div class="card-body bg-patern">
                        <h6 class="text-white">Poin Withdraw</h6>
                        <h2 class="text-right text-white"><i class="feather float-left"></i><span>{{number_format($totWDPoin, 0, ',', '.')}}</span></h2>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Ajukan Claim Reward anda disini</h5>
                    </div>
                    <div class="card-body table-border-style">
                        @if ( Session::has('message') )
                            <div class="alert alert-{{ Session::get('messageclass') }} alert-dismissible fade show" role="alert">
                                {{  Session::get('message')    }} 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Total Poin</th>
                                        <th>Reward Detail</th>
                                        <th>###</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($dataPoin != null)
                                    <?php $no = 0; ?>
                                        @foreach($dataPoin as $row)
                                        <?php  
                                        $no++; 
                                         $button = '<span class="badge badge-dark">claim</span>';
                                        if($row->can_claim == true){
                                            $button = '<a rel="tooltip"  data-toggle="modal" data-target="#confirmSubmit" class="badge badge-success" href="/m/cek/claim/poin/'.$row->id.'">claim</a>';
                                            if($getAllWDPoin != null){
                                                if (in_array($row->id, $getAllWDPoin)){
                                                    $button = '<span class="badge badge-danger">done</span>';
                                                }
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$row->jml_poin}}</td>
                                            <td>{{$row->name}}</td>
                                            <td><?php echo $button; ?></td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="modal fade" id="confirmSubmit" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document" id="confirmDetail">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
        function confirmSubmit(){
            var dataInput = $("#form-add").serializeArray();
            $('#form-add').submit();
            $('#form-add').remove();
            $('#loading').show();
            $('#tutupModal').remove();
            $('#submit').remove();
        }
        
        $("#confirmSubmit").on("show.bs.modal", function(e) {
            var link = $(e.relatedTarget);
            $(this).find(".modal-dialog").load(link.attr("href"));
        });

</script>
@stop