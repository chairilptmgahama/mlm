@extends('layout.member.main')

@section('content')
@include('layout.member.sidebar')

    <div class="content-page">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Ringkasan Bonus</h4>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-4">
                        <div class="card-box tilebox-one">
                            <i class="icon-trophy pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Total Bonus (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($dataAll->total_bonus, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                    <?php
                    $wd_ditransfer = $dataAll->total_wd + $dataAll->fee_tuntas;
                    $wd_tunda = $dataAll->total_tunda + $dataAll->fee_tunda;
                ?>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-4">
                        <div class="card-box tilebox-one">
                            <i class="icon-wallet pull-xs-right text-muted text-success"></i>
                            <h6 class="text-muted text-uppercase m-b-20">WD Ditransfer (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($wd_ditransfer, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-4">
                        <div class="card-box tilebox-one">
                            <i class="icon-rocket pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">WD Diproses (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($wd_tunda, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-vector pull-xs-right text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus Sponsor (Rp.)</h6>
                            <h3 class="m-b-20">{{number_format($bonusType->total_bonus_start, 0, ',', '.')}}</h3>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-people pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus Pasangan (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($bonusType->total_bonus_binary, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-basket-loaded pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus Repeat Order (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($bonusType->total_bonus_ro, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="card-box tilebox-one">
                            <i class="icon-options-vertical pull-xs-right text-muted text-warning"></i>
                            <h6 class="text-muted text-uppercase m-b-20">Bonus Level (Rp.)</h6>
                            <h2 class="m-b-20">{{number_format($bonusType->total_bonus_level, 0, ',', '.')}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('layout.member.footer')
@stop