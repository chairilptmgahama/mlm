@if($getData != null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Berita</h5>
    </div>
    <div class="modal-body">
        <p><img src="{{$getData->image}}" style=" width: 100%;"></p>
        <hr>
        <h3>{{$getData->title}}</h3>
        <br>
        <?php echo $getData->full_desc; ?>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
    </div>
</div>
@endif

@if($getData == null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Berita</h5>
    </div>
    <div class="modal-body">
        <h4 class="text-danger" style="text-align: center;"> Data tidak ditemukan </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
</div>
@endif