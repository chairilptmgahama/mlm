@if($check->can == true)
    
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Pulsa {{$data->jaringan}}</h5>
            </div>
            <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
                <form id="form-add" method="POST" action="/m/req/pulsa">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <fieldset class="form-group">
                                <label for="jaringan">Nominal Pulsa</label>
                                <select class="form-control" name="pulsa" id="jaringan">
                                    @if($data->data_jaringan != null)
                                        @foreach($data->data_jaringan as $row)
                                            <option value="{{$row['pulsa_code']}};{{$row['pulsa_nominal']}};{{$row['pulsa_op']}};{{$row['pulsa_price']}}">{{$row['pulsa_nominal']}} : Harga Rp. {{$row['pulsa_price']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>No. HP</label>
                                <input type="text" class="form-control" name="no_hp">
                            </div>
                        </div>
                    </div>
                </form>    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
            </div>
        </div>

@endif

@if($check->can == false)
    
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
        </div>
        <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
            <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        </div>
    </div>
@endif