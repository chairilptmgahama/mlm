@if($check->can == true)

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <form id="form-add" method="POST" action="/m/add/kirim-paket">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Total Pin</label>
                        <input type="text" class="form-control" readonly="" name="total_pin" value="{{$dataRequest->total_pin}}">
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="form-group">
                        <label>Alamat Kirim</label>
                        <textarea class="form-control" id="alamat_kirim" rows="2" readonly="" name="alamat_kirim" autocomplete="off">{{$dataRequest->alamat_kirim}}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Day Cream</label>
                        <input type="text" class="form-control" readonly="" name="day_cream" value="{{$dataRequest->day_cream}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Night Cream</label>
                        <input type="text" class="form-control" readonly="" name="night_cream" value="{{$dataRequest->night_cream}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Face Toner</label>
                        <input type="text" class="form-control" readonly="" name="face_toner" value="{{$dataRequest->face_toner}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Facial Wash</label>
                        <input type="text" class="form-control" readonly="" name="facial_wash" value="{{$dataRequest->facial_wash}}">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" id="tutupModal"  data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
    </div>
</div>



@endif

@if($check->can == false)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
</div>
@endif