<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Detail Pembelian</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <form id="form-add" method="post" action="/m/shop">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" name="id_barang" value="{{$data->dataBarang->id}}">
                        <input type="hidden" name="potongan" value="{{$data->potongan}}">
                        <input type="hidden" name="bayar" value="{{$data->dibayar}}">
                        <input type="hidden" name="total_buy" value="{{$data->total_buy}}">
                        <h4 class="lead text-dark" style="display: block;text-align: center;">Apakah anda ingin barang ini?</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="text" class="form-control" readonly="" value="{{$data->total_buy}}">
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" class="form-control" readonly="" value="Rp. {{number_format($data->total_buy * $data->dataBarang->main_price, 0, ',', '.')}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Potongan Safra</label>
                        <input type="text" class="form-control" readonly="" value="Rp. {{number_format($data->potongan, 0, ',', '.')}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Dibayar</label>
                        <input type="text" class="form-control" readonly="" value="Rp. {{number_format($data->dibayar, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
    </div>
</div>