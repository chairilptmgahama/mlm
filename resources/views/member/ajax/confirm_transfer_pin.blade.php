@if($check->can == true)

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Transfer Coin</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-primary" style="display: block;text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-fw"></i> <span>Proses loading ......</span>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/m/add/transfer-coin">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                        <label>Nominal Coin</label>
                        <input type="text" class="form-control" readonly="" value="{{number_format($dataRequest, 4, ',', '.')}}">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="form-group">
                        <label>Nama Penerima</label>
                        <input type="text" class="form-control" readonly="" value="{{$to_member->name}} ({{$to_member->user_code}})">
                    </div>
                </div>
                <input type="hidden" name="to_id" value="{{$to_member->id}}">
                <input type="hidden" name="qty" value="{{$dataRequest}}">
                <div class="col-md-4 col-xs-12">
                    <div class="form-group">
                        <label>No. Handphone</label>
                        <input type="text" class="form-control" readonly="" value="{{$to_member->hp}}">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
    </div>
</div>



@endif

@if($check->can == false)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
</div>
@endif