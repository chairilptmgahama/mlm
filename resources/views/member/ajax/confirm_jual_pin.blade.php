@if($data != null)

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Jual</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-primary" style="display: block;text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-fw"></i> <span>Proses loading ......</span>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/m/sell/coin">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="form-group">
                        <label>Harga</label>
                        <input type="text" class="form-control" disabled="" value="Rp {{number_format($data->price, 0, ',', ',')}}">
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <div class="form-group">
                        <label>Fee</label>
                        <input type="text" class="form-control" disabled="" value="Rp. {{number_format($data->admin_fee, 0, ',', ',')}}">
                    </div>
                </div>
                <input type="hidden" name="exchanger_id" value="{{$data->exchanger_id}}">
                <input type="hidden" name="setting_id" value="{{$data->setting_id}}">
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Total Coin</label>
                        <input type="text" class="form-control" readonly="" name="total_coin" value="{{$data->total_coin}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Kode Pin</label>
                        <input type="text" class="form-control" name="pin_code">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
    </div>
</div>

@endif 

@if($data == null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
</div>
@endif
