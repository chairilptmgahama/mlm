@if($check->can == true)
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
            </div>
            <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
                <form id="form-add" method="POST" action="/m/claim/reward">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Reward</label>
                                <input type="text" class="form-control" readonly="" value="{{$data->getData->reward_detail}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Nominal Reward</label>
                                <input type="text" class="form-control" readonly="" value="{{number_format($data->getData->reward_price, 0, ',', '.')}}">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="cekId" value="{{$data->getData->id}}">
                    <input type="hidden" name="kiri" value="{{$data->kiri}}">
                    <input type="hidden" name="kanan" value="{{$data->kanan}}">
                    <input type="hidden" name="bank_id" value="{{$data->bank->id}}">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Order</button>
            </div>
        </div>
@endif


@if($check->can == false)
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
        </div>
        <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
            <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        </div>
    </div>
@endif