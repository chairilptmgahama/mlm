<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        @if($check->can == true)
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-danger" style="display: block;text-align: center;">
                        <div class="spinner-border" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/m/add/belanja">
            {{ csrf_field() }}
            <?php $sum = 0; ?>
            @foreach($dataRequest as $row)
                <?php
                    $sum += $row['product_price'] * $row['product_quantity'];
                ?>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control" readonly="" value="{{$row['product_name']}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="form-group">
                            <label>Jml</label>
                            <input type="text" class="form-control" readonly="" value="{{$row['product_quantity']}}">
                        </div>
                    </div>
                    <input type="hidden" name="data[]" value="{{$row['product_id']}}__{{$row['product_quantity']}}__{{$row['product_price']}}">
                    
                    <div class="col-md-8 col-xs-12">
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" class="form-control" readonly="" value="Rp. {{number_format($row['product_price'], 0, ',', '.')}}">
                        </div>
                    </div>
                </div>
            @endforeach
            <hr>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                        <label>Total harga</label>
                        <input type="hidden" name="total_harga" value="{{$sum}}">
                        <input type="text" class="form-control" readonly="" value="Rp. {{number_format($sum, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
        </form>  
        @endif
        
        @if($check->can == false)
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="form-group">
                    <h4 class="text-danger" style="text-align: center;">{{$check->pesan}}</h4>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
        @if($check->can == true)
            <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
        @endif
    </div>
</div>
