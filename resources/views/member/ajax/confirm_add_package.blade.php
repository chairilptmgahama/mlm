<form class="login100-form validate-form" id="form-add"  method="post" action="/m/add/package">
    {{ csrf_field() }}
    <div class="modal-header justify-content-center">
        <h4 class="title title-up">Konfirmasi Order Paket</h4>
    </div>
    <div class="modal-body">
        <?php 
            $price = $getData->pin * $pinSetting->price;
        ?>
        <div class="pricing-table">
                                    <?php 
                                        $feature = '';
                                        $label = '';
                                        if($getData->id == 1){
                                            $img = 'r_b1.jpeg';
                                        }
                                        if($getData->id == 2){
                                            $img = 'a_s1.jpg';
                                        }
                                        if($getData->id == 3){
                                            $feature = 'pricing-featured';
                                            $label = '<div class="selected">Rekomendasi</div>';
                                            $img = 's_g1.jpg';
                                        }
                                        if($getData->id == 4){
                                            $img = 'ms_p1.jpg';
                                        }
                                        ?>
                                        <div class="pricing-item {{$feature}}" style="width: 300px;">
                                            <?php echo $label; ?>
                                            <div class="pricing-value">
                                                <img src="/image/{{$img}}" alt="user" class="img-circle" style="width: 180px;">
                                            </div>
                                            <div class="pricing-title">
                                                <h5><b>{{$getData->name}}</b></h5>
                                                <h5><b>Rp. {{number_format($price, 0, ',', ',')}}</b></h5>
                                            </div>
                                            <ul class="pricing-features">
                                                <li><span class="keywords">{{$getData->short_desc}}</span> Paket Safra</li>
                                                <li>Bonus Sponsor Rp 75.000/PIN </li>
                                                <li>Bonus Binary Rp 15.000/Pasangan</li>
                                                <li>Bonus RO 10 Level</li>
                                                <li>Discount {{$getData->safra_discount}}% Safra Poin</li>
                                            </ul>
                                        </div>
                            </div>
    </div>
    <input type="hidden" name="id_paket" value="{{$getData->id}}">
</form>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" id="tutupModal" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Order</button>
    </div>
