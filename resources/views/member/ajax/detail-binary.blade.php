<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Detail Member</h5>
    </div>
    <div class="modal-body" >
        <?php
            $img = 'r_b1.jpeg';
            $title = 'Bronze';
            if($dataUser->package_id == 1){
                $img = 'r_b1.jpeg';
                $title = 'Bronze';
            }
            if($dataUser->package_id == 2){
                $img = 'a_s1.jpg';
                $title = 'Silver';
            }
            if($dataUser->package_id == 3){
                $img = 's_g1.jpg';
                $title = 'Gold';
            }
            if($dataUser->package_id == 4){
                $img = 'ms_p1.jpg';
                $title = 'Platinum';
            }
        ?>
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="form-group" style="text-align: center;">
                    <img src="/image/{{$img}}" alt="user"class="img-responsive img-circle" style="width: 130px;height: 120px;">
                    <br><br>
                    Joined : {{date('d F Y', strtotime($dataUser->active_at))}}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label>User ID</label>
                    <input type="text" class="form-control" readonly="" value="{{$dataUser->user_code}}">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" readonly="" value="{{$dataUser->email}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="form-group">
                    <label>Jml Sisa Kiri</label>
                    <input type="text" class="form-control" readonly="" value="{{$detailUser->sisaKiri}}">
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="form-group">
                    <label>Jml Sisa Kanan</label>
                    <input type="text" class="form-control" readonly="" value="{{$detailUser->sisaKanan}}">
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="form-group">
                    <label>Jml Kiri</label>
                    <input type="text" class="form-control" readonly="" value="{{$detailUser->kiri}}">
                </div>
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="form-group">
                    <label>Jml Kanan</label>
                    <input type="text" class="form-control" readonly="" value="{{$detailUser->kanan}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label>Omzet Kiri</label>
                    <input type="text" class="form-control" readonly="" value="Rp. {{number_format($detailUser->omzetKiri, 0, ',', '.')}}">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <label>Omzet kanan</label>
                    <input type="text" class="form-control" readonly="" value="Rp. {{number_format($detailUser->omzetKanan, 0, ',', '.')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
    </div>
</div>

