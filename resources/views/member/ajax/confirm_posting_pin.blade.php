@if($data != null)

<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Mining</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-primary" style="display: block;text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-fw"></i> <span>Proses loading ......</span>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/m/posting/coin">
            {{ csrf_field() }}
            <div class="row">
                <input type="hidden" name="phase" value="{{$data->phase}}">
                <input type="hidden" name="setting_id" value="{{$setting->id}}">
                <input type="hidden" name="persentase" value="{{$setting->persentase}}">
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Jml Coin</label>
                        <input type="text" class="form-control" readonly="" name="total_coin" value="{{$data->total_coin}}">
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-group">
                        <label>Fase</label>
                        <input type="text" class="form-control" readonly="" value="{{$data->phase}} Bulan">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                        <?php
                            $awal = '';
                            if($data->phase == 3){
                                $awal = '30 %';
                            }
                            if($data->phase == 6){
                                $awal = '60 %';
                            }
                            if($data->phase == 9){
                                $awal = '90 %';
                            }
                            if($data->phase == 12){
                                $awal = '120 %';
                            }
                        ?>
                        <label>Persentase Berjalan</label>
                        <input type="text" class="form-control" readonly="" value="Maksimal Potensi {{$awal}} sd {{$setting->persentase * $data->phase}}%">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
    </div>
</div>

@endif 

@if($data == null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Data</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> {{$check->pesan}} </h4>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
</div>
@endif
