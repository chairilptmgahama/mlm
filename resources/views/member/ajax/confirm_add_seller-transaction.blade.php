@if($getData != null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi</h5>
    </div>
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <div class="row" id="loading" style="display:none;">
            <div class="col-md-12">
                <div class="form-group">
                    <h5 class="text-primary" style="display: block;text-align: center;">
                        <i class="fa fa-spinner fa-pulse fa-fw"></i> <span>Proses loading ......</span>
                    </h5>
                </div>
            </div>
        </div>
        <form id="form-add" method="POST" action="/m/add/seller-transaction">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" name="id_trans" value="{{$data->id_trans}}">
                    </div>
                </div>
            </div>
            <?php
                $seller = $getData->user_code;
                $hp_seller = $getData->hp;
                
                $total = $getData->price + $getData->unique_digit + $getData->admin_fee;
                if($getData->type == 3){
                    $total = $getData->price - $getData->unique_digit - $getData->admin_fee;
                }
            ?>
            <div class="row">
                <div class="col-md-7 col-xs-12">
                    <div class="form-group">
                        <label>Nama Seller</label>
                        <input type="text" class="form-control" disabled="" value="{{$seller}}">
                    </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <div class="form-group">
                        <label>No. HP</label>
                        <input type="text" class="form-control" disabled="" value="{{$hp_seller}}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="form-group">
                        <label>Total Harga</label>
                        <input type="text" class="form-control" disabled="" value="Rp. {{number_format($total, 0, ',', '.')}}">
                    </div>
                </div>
            </div>
        </form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary waves-effect waves-light" id="submit" onclick="confirmSubmit()">Submit</button>
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
    </div>
</div>
@endif

@if($getData == null)
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">Konfirmasi Transfer</h5>
    </div>
    
    <div class="modal-body"  style="overflow-y: auto;max-height: 330px;">
        <h4 class="text-danger" style="text-align: center;"> anda tidak memiliki saldo, harap membeli saldo kepada perusahaan </h4>
    </div> 
    
    <div class="modal-footer">
        <button type="button" class="btn btn-danger waves-effect" id="tutupModal" data-dismiss="modal">Tutup</button>
    </div>
</div>
@endif
