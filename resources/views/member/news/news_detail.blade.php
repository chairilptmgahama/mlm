@extends('layout.member.main')
@section('content')
@include('layout.member.sidebar')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Detail Berita</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-lg-8 col-xs-12 col-md-offset-2">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title"><?php echo $getData->title; ?></h4>
                        </div>
                        <div class="card-block" style="text-align: center;">
                            <img class="img-fluid"  src="{{$getData->image}}" style="max-width: 600px;display: inline;">
                        </div>
                        <div class="card-block">
                            <p class="card-text">
                                <?php echo $getData->full_desc; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layout.member.footer')
@stop
@section('styles')
<link href="{{ asset('asset_member/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@stop
@section('javascript')
<script type="text/javascript">
    $("#orderPackage").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>
@stop