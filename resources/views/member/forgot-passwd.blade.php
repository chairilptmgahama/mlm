@extends('layout.member.auth')

@section('content')

<div class="vertical-align-wrap">
    <div class="vertical-align-middle auth-main">
        <div class="auth-box">
            <div class="card">

                <div class="header">
                    <img src="{{ URL('image/xone.jpg') }}" alt="" class="img-fluid mb-3" style="height:90px;">
                    <p class="lead">Masukan userID anda, dan kami akan mengirimkan instruksi melalui email</p>
                </div>
                <div class="body">
                    <form class="form-auth-small" method="post" action="/m/forgot/passwd">
                        {{ csrf_field() }}

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="feather icon-user"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="UserID" name="user_id" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Kirim Email</button>
                        <div class="bottom">
                            <span class="helper-text m-b-10"><i class="fa fa-home"></i> <a href="/login">Kembali</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
